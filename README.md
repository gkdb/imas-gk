# IMAS Gyrokinetics

This projects contains the documentation of the IMAS 'gyrokinetics' Data Dictionnary, that standardizes inputs and outputs of tokamak plasmas gyrokinetic simulations, in the folder `./doc/data_dictionary/`.

Documentation about the back and forth conversion between widely used local delta-f gyrokinetic codes and instances of the IMAS 'gyrokinetics' data dictionary (the so called IDSs) is also included in the `./doc/` folder. 

TO UPDATE - The library in `./IDSpy/` can be used to automatically generate static Python classes from the description of an IMAS data dictionary (an `.xsd` file).  
The class corresponding to the 'gyrokinetics' data dictionary is stored in `./idspy_toolkit/ids_gyrokinetics/`. It can be used to create 'gyrokinetics' IDSs in Python.  
Basic functionalities (read/write in hdf5, fill defaults) to deal with IDSs generated from `IDSpy` classes are provided in the `./idspy_toolkit/` folder.  
The purpose of `IDSpy` is to provide a lightweight (no IMAS installation required) and flexible (the static Python classes can be direclty edited/modified) way to create and manipulate IMAS IDSs in Python. The Python datastructure is fully consistent with the IMAS data model but the generated hdf5 files are not following IMAS specifications. 
As such, `IDSpy` should be seen as a complement to [IMASpy](https://git.iter.org/projects/IMAS/repos/imaspy/browse) (which provides interface with the iMAS backend) for users wanting to develop Python tools dealing with IDSs before IMASpy is released and made open source.

Specific tools to manipulate 'gyrokinetics' IDSs are available in the `gkids_toolkit` folder.

### More information on IMAS: 
IMAS is the [Integrated Modelling and Analysis Suite](https://hal-cea.archives-ouvertes.fr/cea-01576460/document) that is developed and will be used for [ITER](https://www.iter.org/) exploitation.  
The latest version of the IMAS Data Model is available on the [ITER Integrated Modelling Home page](https://confluence.iter.org/display/IMP/Integrated+Modelling+Home+Page) (account required).

