import numpy as np
from scipy.special import ive

def convert_moments(ids,sw='gy2part'):
  """ Computes the moments of the particle distribution function from the moments of the gyrocenter distribution function (or vice versa)
  
  Inputs:
    ids           dict with the content of a gyrokinetic IDS
    sw            switch to specify the direction of the conversion, either 'gy2part' or 'part2gy'
              
  Outputs:    
    ids           updated dict with the new moments

  See section 7.2 of the GKDB manual at https://gitlab.com/gkdb/gkdb/raw/master/doc/general/IOGKDB.pdf
  """
  print("Warning: this function needs to be updated, do not use as is")
  return
  # check input validity
  assert (sw in ('gy2part','part2gy')), "Accepted values for sw are 'gy2part' or 'part2gy' ('"+sw+"' is not allowed)"

  # define combinations of bessel functions used for the moments conversion
  def c0(b):
    return 0.5*ive(0,b)
  def c2(b):
    return 0.5*((1-b)*ive(0,b)+b*ive(1,b))
  def c4(b):
    return 0.5*(2*(1-b)**2*ive(0,b)+(3-2*b)*b*ive(1,b))
  def d0(b):
    return 0.25*(ive(0,b)-ive(1,b))
  def d2(b):
    return 0.25*(2*(1-b)*ive(0,b)+(2*b-1)*ive(1,b))
  def d4(b):
    return 0.25*(b-2)*((4*b-3)*ive(0,b)+(1-4*b)*ive(1,b))

  # define the integrals involved in the moments conversion
  moments=['density','j_density','pressure_parallel','pressure_perpendicular','heat_flux_parallel','v_parallel_energy_perpendicular','v_perpendicular_square_energy']
  aa = {
    'density' : np.sqrt(np.pi),
    'j_density' : 0,
    'pressure_parallel' : np.sqrt(np.pi)/2,
    'pressure_perpendicular' : np.sqrt(np.pi),
    'heat_flux_parallel' : [0 , 0],
    'v_parallel_energy_perpendicular' : 0,
    'v_perpendicular_square_energy' : [np.sqrt(np.pi)/2 , np.sqrt(np.pi)]
  }
  bb = {
    'density' : 0.5,
    'j_density' : 0.5,
    'pressure_parallel' : 0.5,
    'pressure_perpendicular' : 0.5,
    'heat_flux_parallel' : [0.5 , 0.5],
    'v_parallel_energy_perpendicular' : 0.5,
    'v_perpendicular_square_energy' : [0.5 , 1]
  }
  cc = {
    'density' : c0,
    'j_density' : c0,
    'pressure_parallel' : c0,
    'pressure_perpendicular' : c2,
    'heat_flux_parallel' : [c2 , c0],
    'v_parallel_energy_perpendicular' : c2,
    'v_perpendicular_square_energy' : [c2 , c4]
  }
  dd = {
    'density' : d0,
    'j_density' : d0,
    'pressure_parallel' : d0,
    'pressure_perpendicular' : d2,
    'heat_flux_parallel' : [d2 , d0],
    'v_parallel_energy_perpendicular' : d2,
    'v_perpendicular_square_energy' : [d2 , d4]
  }

  try:
    # loop over wavevectors
    for ii in range(len(ids['wavevector'])):
      krstar=ids['wavevector'][ii]['radial_component_norm']
      kthstar=ids['wavevector'][ii]['binormal_component_norm']
      # loop over eigenmodes
      for jj in range(len(ids['wavevector'][ii]['eigenmode'])):        
        phi=np.squeeze(np.asarray(ids['wavevector'][ii]['eigenmode'][jj]['phi_potential_perturbed_norm_real'])) \
           +1j*np.squeeze(np.asarray(ids['wavevector'][ii]['eigenmode'][jj]['phi_potential_perturbed_norm_imaginary']))
        if ids['model']['include_b_field_parallel']:
          bpar=np.squeeze(np.asarray(ids['wavevector'][ii]['eigenmode'][jj]['b_field_parallel_perturbed_norm_real'])) \
              +1j*np.squeeze(np.asarray(ids['wavevector'][ii]['eigenmode'][jj]['b_field_parallel_perturbed_norm_imaginary']))
        else:
          bpar=phi*0
        th_in=np.asarray(ids['wavevector'][ii]['eigenmode'][jj]['poloidal_angle'])
        (th_out,amin,R,Z,J_r,dVdr,grad_r,dpsidr,bt,bp,grr,grth,gthth)=get_metric(ids,th_in)
        kperp_rhoref = krstar**2*grr + 2*krstar*kthstar*grth + kthstar**2*gthth
        B=np.sqrt(bp**2+bt**2)
        # loop over species
        for kk in range(len(ids['species'])):
          charge=ids['species'][kk]['charge_norm']
          m=ids['species'][kk]['mass_norm']
          T=ids['species'][kk]['temperature_norm']
          prefactor = {
            'density' : 1,
            'j_density' : Z,
            'pressure_parallel' : m/3,
            'pressure_perpendicular' : m/3, 
            'heat_flux_parallel' : m/2,
            'v_parallel_energy_perpendicular' : m/2,
            'v_perpendicular_square_energy' : m/2
          }
          b=0.5*(kperp_rhoref*np.sqrt(m*T)/charge/B)**2
          for moment in moments:
            if type(aa[moment])!=list:
              dum_pol=aa[moment]*(cc[moment](b)-bb[moment])
              dum_mag=aa[moment]*dd[moment](b)
            else:
              dum_pol=0*b
              dum_mag=0*b
              for ll in range(aa[moment].size):
                dum_pol = dum_pol + aa[moment](ll)*(cc[moment](ll)(b)-bb[moment](ll))
                dum_mag = dum_mag + aa[moment](ll)*dd[moment](ll)(b)
            M_pol = charge*phi/T*prefactor[moment]*2/np.sqrt(np.pi)*dum_pol
            M_mag = 2*bpar/B*prefactor[moment]*2/np.sqrt(np.pi)*dum_mag
            if sw=='gy2part':
              ids['wavevector'][ii]['eigenmode'][jj]['fluxes_moments'][kk]['moments_norm_particles']={}
              ids['wavevector'][ii]['eigenmode'][jj]['fluxes_moments'][kk]['moments_norm_particles'][moment+'_real']=ids['wavevector'][ii]['eigenmode'][jj]['fluxes_moments'][kk]['moments_norm_gyrocenters'][moment+'_real'] + M_pol.real + M_mag.real
              ids['wavevector'][ii]['eigenmode'][jj]['fluxes_moments'][kk]['moments_norm_particles'][moment+'_imaginary']=ids['wavevector'][ii]['eigenmode'][jj]['fluxes_moments'][kk]['moments_norm_gyrocenters'][moment+'_imaginary'] + M_pol.imag + M_mag.imag
            if sw=='part2gy':
              ids['wavevector'][ii]['eigenmode'][jj]['fluxes_moments'][kk]['moments_norm_gyrocenters']={}
              ids['wavevector'][ii]['eigenmode'][jj]['fluxes_moments'][kk]['moments_norm_gyrocenters'][moment+'_real']=ids['wavevector'][ii]['eigenmode'][jj]['fluxes_moments'][kk]['moments_norm_particles'][moment+'_real'] - M_pol.real - M_mag.real
              ids['wavevector'][ii]['eigenmode'][jj]['fluxes_moments'][kk]['moments_norm_gyrocenters'][moment+'_imaginary']=ids['wavevector'][ii]['eigenmode'][jj]['fluxes_moments'][kk]['moments_norm_particles'][moment+'_imaginary'] - M_pol.imag - M_mag.imag

  except KeyError as key_error:
    print('Key '+key_error+' not found in the GK IDS')
