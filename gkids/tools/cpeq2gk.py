# required modules
import imaspy
import numpy as np
from scipy.interpolate import splrep
from scipy.interpolate import splev
import scipy.constants as codata
import matplotlib.pyplot as plt
import FS_param as FS


def sp_overview(cp):
  """ Overview of species included in a core_profile IDS
  Inputs
    cp              core_profile IDS from IMASPy
  Outputs
    sp_th_list      list of thermal species content (one per time slice)
    sp_fast_list    list of suprathermal species content (one per time slice)
  """
  # thermal species
  sp_th_list=[]
  sp_fast_list=[]
  for ii_t in range(len(cp.profiles_1d)):
    sp_thermal="Thermal: "
    sp_fast="Fast: "
    if np.any(cp.profiles_1d[ii_t].electrons.density_thermal>0):
      sp_thermal=sp_thermal+"e-"+", "
    nions=len(cp.profiles_1d[ii_t].ion)
    for ii_ion in range(nions):
      if np.any(cp.profiles_1d[ii_t].ion[ii_ion].density_thermal>0):
        sp_thermal=sp_thermal+cp.profiles_1d[ii_t].ion[ii_ion].label+", "
    sp_thermal=sp_thermal[:-2]

    if np.any(cp.profiles_1d[ii_t].electrons.density_fast>0):
      sp_fast=sp_fast+"e-"+", "
    nions=len(cp.profiles_1d[ii_t].ion)
    for ii_ion in range(nions):
      if np.any(cp.profiles_1d[ii_t].ion[ii_ion].density_fast>0):
        sp_fast=sp_fast+cp.profiles_1d[ii_t].ion[ii_ion].label+", "
    if len(sp_fast)>6:
      sp_fast=sp_fast[:-2]

    sp_th_list.append(sp_thermal)
    sp_fast_list.append(sp_fast)
  return sp_th_list, sp_fast_list

def cpeq2gk(cp,eq,rhotor_target,opt,t_target=None,smooth=1e-5,verbose=True,doplots=True):
  """ Generates one or several gyrokinetics_local IDS from a core_profile and an equilibrium IDS
  Inputs
    cp            core_profile IDS from IMASPy
    eq            equilibrium IDS from IMASPy
    rhotor_target    Radial positions for which the GK IDS are generated 
                  specified with the normalised toroidal flux (IMAS definition)
    opt           choice of options to generate the GK IDS (see detailed description below)
    t_target      time [s] for which the GK IDS are generated (default: None)
                    None: input core_profile and equilibrium contain only 1 time slice
                    [t_start,t_end]: find all time points within the interval and average
    smooth        If non-zero, applies smoothing when computing the radial derivatives (default: 1e-5)
                  The higher the value, the stronger the smoothing
                  (check carefully the outputs if no smoothing is applied)
    verbose       if True displays error messages (default: True)
    doplots       if True perform plots to check all is going well (default: True)                  

  Outputs
    gkall         list of gyrokinetics_local IDS (one per radial position)
    log           list of error messages and warnings

  Available options are:
  opt={'species':              'main only'       2 species: electrons and main ion (defined as highest density ion)
                                                 electron density taken to be thermal+fast
                                                 ion density modified to enforce quasi-neutrality (based on electron density)
                                                 e-i collisions scaled up by factor Zeff, from cp.profiles_1d[ii_t].zeff 
                               'main+imp'        main species and impurity species
                                                 all species density taken to be thermal+fast    
                                                 ion density modified to enforce quasi-neutrality (based on electron and impurity densities)
                               'main+fast'       not implemented yet
                               'all'             not implemented yet
       'toroidal flow':        'none'            velocity_tor_norm set to zero
                               'from omphi'      velocity_tor_norm filled using the ExB flow from cp.profiles_1d[ii_t].rotation_frequency_tor_sonic
       'ExB shear':            'none'            shearing_rate_norm set to zero
                               'from omphi'      shearing_rate_norm filled using the ExB flow from cp.profiles_1d[ii_t].rotation_frequency_tor_sonic
       'parallel flow shear':  'none'            velocity_tor_gradient_norm set to zero
                               'from omphi'      velocity_tor_gradient_norm filled using the ExB flow from cp.profiles_1d[ii_t].rotation_frequency_tor_sonic
                                                 same value for all species
        }

  """

  def msg_log(msg,log,verbose,critical=False): 
    """ Generic routine to store and optionally display warning/error messages 
    Inputs    
      msg         message to be stored/displayed
      log         list to store the messages
      verbose     if True, displays the message on screen
      critical    if True, exits the code
    """
    if critical:
      log.append("Error: "+msg)
      raise Exception(msg)
    else:
      log.append("Warning: "+msg)
      if verbose is True:
        print(msg)
    
  def coulomb_logarithm(sp_type,n_norm,T_norm,zz,A,nref_19,Tref_keV):
    """ Compute the Coulomb logarithm matrix to be used to compute species collisionality
        Formulae valid for thermal species only
    Inputs
      sp_type    list of particles type, contains 'e' (electron) or 'i' (ion)
                 can contain several particles of the same type
      n_norm     corresponding normalised density
      T_norm     corresponding normalised temperature
      zz         corresponding species charge
      A          corresponding species atomic masses
      nref_19    reference density in 10^19 m-3
      Tref_kev   reference temperature in keV
    Output
      lnlambda   nsp*nsp matrix with the ln Lambda^i/j values
                 i lines, j columns and nsp=len(sp_type)
    """
    nsp=len(sp_type)
    lnlambda=np.full((nsp,nsp),0.0)
    for ii,sp1 in enumerate(sp_type):
      for jj,sp2 in enumerate(sp_type):
        if sp1=='e' and sp2=='e':
          lnlambda[ii,jj]=14.9-0.5*np.log(0.1*n_norm[ii]*nref_19)+np.log(T_norm[ii]*Tref_keV)
        if sp1=='e' and sp2=='i':
          if T_norm[ii]*Tref_keV<0.01*zz[jj]**2:
            lnlambda[ii,jj]=17.2-0.5*np.log(0.1*zz[jj]**2*n_norm[ii]*nref_19)+1.5*np.log(T_norm[ii]*Tref_keV)
          else:
            lnlambda[ii,jj]=14.8-0.5*np.log(0.1*n_norm[ii]*nref_19)+np.log(T_norm[ii]*Tref_keV)
        if sp1=='i' and sp2=='e':
          if T_norm[jj]*Tref_keV<0.01*zz[ii]**2:
            lnlambda[ii,jj]=17.2-0.5*np.log(0.1*zz[ii]**2*n_norm[jj]*nref_19)+1.5*np.log(T_norm[jj]*Tref_keV)
          else:
            lnlambda[ii,jj]=14.8-0.5*np.log(0.1*n_norm[jj]*nref_19)+np.log(T_norm[jj]*Tref_keV)
        if sp1=='i' and sp2=='i':
          lnlambda[ii,jj]=(17.3-np.log((zz[ii]*zz[jj]*(A[ii]+A[jj]))/(A[ii]*T_norm[jj]*Tref_keV+A[jj]*T_norm[ii]*Tref_keV))
                        -0.5*np.log(0.1*nref_19/Tref_keV)-0.5*np.log(n_norm[ii]*zz[ii]**2/T_norm[ii]+n_norm[jj]*zz[jj]**2/T_norm[jj]))
    return lnlambda

  # Should check version here to know if DD is before 4.0 or not (change of COCOS...)
  # Since 4.0 is not yet released, sign_cocos=-1:
  sign_cocos=-1 

  gkall=[]
  log=[]
  rhotor_target=np.array(rhotor_target)
  Nrho=rhotor_target.size
  smooth=np.abs(smooth)
  opt_ref={'species': ('main only','main+imp'),
           'toroidal flow': ('none','from omphi'),
           'parallel flow shear': ('none','from omphi'),
           'ExB shear': ('none','from omphi'),
           }

  # checks
  if t_target is None:
    if cp.time.size>1:
      msg="More than 1 time slice in input core_profile"
      msg_log(msg,log,verbose,critical=True)
    if eq.time.size>1:
      msg="More than 1 time slice in input equilibrium"
      msg_log(msg,log,verbose,critical=True)
    if cp.time[0]==eq.time[0]:
      ii_t_eq=0
      ii_t_cp=0
    else:
      msg="core_profile and equilibrium time slices do not match"
      msg_log(msg,log,verbose,critical=True)
  else:
    msg="Time averages not implemented yet"
    msg_log(msg,log,verbose,critical=True)
  for kk in opt.keys():
    if kk in opt_ref.keys():
      if not opt[kk] in opt_ref[kk]:
        msg="Invalid option in opt['"+kk+"']: "+"'"+opt[kk]+"'"
        msg_log(msg,log,verbose,critical=True)
    else:
      msg="Invalid key in opt: '"+kk+"'"
      msg_log(msg,log,verbose,critical=True)


  # constants
  me=codata.physical_constants['electron mass'][0] # electron mass [kg] 
  mD=codata.physical_constants['deuteron mass'][0] # deuterium mass [kg] 
  mH=codata.physical_constants['proton mass'][0]
  mT=codata.physical_constants['triton mass'][0]
  amu=codata.physical_constants['unified atomic mass unit'][0]
  eV=codata.physical_constants['elementary charge'][0] # elementary charge [C] / electron volt [J]
  eps0=codata.physical_constants['vacuum electric permittivity'][0]
  mu0=codata.physical_constants['vacuum mag. permeability'][0]

  # global equilibrium quantities (sign of plasma current and toroidal magnetic field)
  if eq.time_slice[ii_t_eq].global_quantities.has_value:
    if eq.time_slice[ii_t_eq].global_quantities.ip>0:
      ip_sign = 1
    else:
      ip_sign = -1
  else:
    msg="Missing value in eq.time_slice["+str(ii_t_eq)+"].global_quantities.ip"
    msg_log(msg,log,verbose,critical=True)

  if eq.vacuum_toroidal_field.b0.has_value:
    if eq.vacuum_toroidal_field.b0[ii_t_eq]>0:
      b_field_tor_sign = 1
    else:
      b_field_tor_sign = -1
  else:
    msg="Missing value in eq.vacuum_toroidal_field.b0"
    msg_log(msg,log,verbose,critical=True)

  # load psi(R,Z)
  if len(eq.time_slice[ii_t_eq].profiles_2d)>0: # attempts to retrieve psi2D from profiles_2d
    # check if rectangular grid is available for poloidal flux description
    is_rectangular=[ii_2d for ii_2d in eq.time_slice[ii_t_eq].profiles_2d.coordinates[0] if eq.time_slice[ii_t_eq].profiles_2d[ii_2d].grid_type.name=='rectangular']
    if len(is_rectangular)==1:
      ii_grid=is_rectangular[0]
    else:
      msg="None or multiple rectangular grids available for psi 2D description. Check equilibrium IDS manually."
      msg_log(msg,log,verbose,critical=True)
    Rg=eq.time_slice[ii_t_eq].profiles_2d[ii_grid].grid.dim1
    Zg=eq.time_slice[ii_t_eq].profiles_2d[ii_grid].grid.dim2
    psi2D=eq.time_slice[ii_t_eq].profiles_2d[ii_grid].psi
  else: # attempts to retrieve psi2D from ggd - preliminary, would deserve better understanding of the indexes
    Rg=np.array([oo.geometry[0] for oo in eq.grids_ggd[0].grid[0].space[0].objects_per_dimension[0].object])
    Zg=np.array([oo.geometry[1] for oo in eq.grids_ggd[0].grid[0].space[0].objects_per_dimension[0].object])
    psi2D=eq.time_slice[ii_t_eq].ggd[0].psi[0].values
    msg="Loading equilibrium from ggd still in development"
    msg_log(msg,log,verbose,critical=True)


  # compute contours and MXH parametrisation on psiN_mxh grid
  psi_axis=eq.time_slice[ii_t_eq].global_quantities.psi_axis
  psi_bnd=eq.time_slice[ii_t_eq].global_quantities.psi_boundary
  psiN2D=(psi2D-psi_axis)/(psi_bnd-psi_axis)
  if eq.time_slice[ii_t_eq].boundary_separatrix.outline.r.has_value:
    R_lcfs=eq.time_slice[ii_t_eq].boundary_separatrix.outline.r
  else:
    R_lcfs=None
  if eq.time_slice[ii_t_eq].boundary_separatrix.outline.z.has_value:
    Z_lcfs=eq.time_slice[ii_t_eq].boundary_separatrix.outline.z
  else:
    Z_lcfs=None

  psiN_mxh=np.linspace(0.01,0.9,80) # normalised poloidal flux, 0 on axis and 1 at the LCFS
  R,Z = FS.psi2rz(Rg,Zg,psiN2D,psiN_out=psiN_mxh,R_lcfs=R_lcfs,Z_lcfs=Z_lcfs,doplots=doplots) 
  r_mxh, R0, dR0dr, Z0, dZ0dr, k, dkdr, c, dcdr, s,dsdr, R_out, Z_out, err_out = FS.rz2mxh(R,Z,doplots=doplots)
  r_spl=splrep(psiN_mxh,r_mxh)
  R0_spl=splrep(psiN_mxh,R0)
  dR0dr_spl=splrep(psiN_mxh,dR0dr)
  dZ0dr_spl=splrep(psiN_mxh,dZ0dr)
  k_spl=splrep(psiN_mxh,k)
  dkdr_spl=splrep(psiN_mxh,dkdr)
  c_spl=[]
  dcdr_spl=[]
  s_spl=[]
  dsdr_spl=[]
  Nsh=c.shape[1]
  for jj in range(Nsh):
    c_spl.append(splrep(psiN_mxh,c[:,jj]))
    s_spl.append(splrep(psiN_mxh,s[:,jj]))
    dcdr_spl.append(splrep(psiN_mxh,dcdr[:,jj]))
    dsdr_spl.append(splrep(psiN_mxh,dsdr[:,jj]))

  # 1D equilibrium quantities (on r_eq, psiN_eq)
  psi_eq=eq.time_slice[ii_t_eq].profiles_1d.psi
  psiN_eq=(psi_eq-psi_axis)/(psi_bnd-psi_axis)
  Npsi=psi_eq.size
  tens=(Npsi-np.sqrt(2*Npsi))*smooth
  r_eq=splev(psiN_eq,r_spl)
  psi_spl_r=splrep(r_eq,psi_eq,s=tens)
  dpsidr_eq=splev(r_eq,psi_spl_r,der=1)

  Rref_eq=splev(psiN_eq,R0_spl)
  Rref_spl_r=splrep(r_eq,Rref_eq)
  F_eq=eq.time_slice[ii_t_eq].profiles_1d.f
  Bref_eq=np.abs(F_eq)/Rref_eq
  Bref_spl_r=splrep(r_eq,Bref_eq)

  q_eq=eq.time_slice[ii_t_eq].profiles_1d.q
  q_spl_r=splrep(r_eq,q_eq,s=tens)
  dqdr_eq=splev(r_eq,q_spl_r,der=1)
  shear_eq=r_eq/q_eq*dqdr_eq

  p_eq=eq.time_slice[ii_t_eq].profiles_1d.pressure
  p_spl_r=splrep(r_eq,p_eq,s=tens*np.max(p_eq)/100)
  dpdr_eq=splev(r_eq,p_spl_r,der=1)

  dR0dr_eq=splev(psiN_eq,dR0dr_spl)
  dZ0dr_eq=splev(psiN_eq,dZ0dr_spl)
  k_eq=splev(psiN_eq,k_spl)
  dkdr_eq=splev(psiN_eq,dkdr_spl)
  c_eq,s_eq,dcdr_eq,dsdr_eq=np.full((4,Npsi,Nsh),np.nan)
  for jj in range(Nsh):
    c_eq[:,jj]=splev(psiN_eq,c_spl[jj])
    s_eq[:,jj]=splev(psiN_eq,s_spl[jj])
    dcdr_eq[:,jj]=splev(psiN_eq,dcdr_spl[jj])
    dsdr_eq[:,jj]=splev(psiN_eq,dsdr_spl[jj])

  # interpolation at output radial positions (r_out, psiN_out)
  rhotor_eq=eq.time_slice[ii_t_eq].profiles_1d.rho_tor_norm
  psiN_spl_out=splrep(rhotor_eq,psiN_eq,k=3)
  psiN_out=splev(rhotor_target,psiN_spl_out)
  r_out=splev(psiN_out,r_spl)

  dpsidr_out=splev(r_out,psi_spl_r)

  Rref_out=splev(r_out,Rref_spl_r)
  Bref_out=splev(r_out,Bref_spl_r)
 
  q_out=splev(r_out,q_spl_r)
  dqdr_out=splev(r_out,q_spl_r,der=1)
  shear_out=r_out/q_out*dqdr_out
  p_out=splev(r_out,p_spl_r)
  dpdr_out=splev(r_out,p_spl_r,der=1)

  dR0dr_out=splev(psiN_out,dR0dr_spl)
  dZ0dr_out=splev(psiN_out,dZ0dr_spl)
  k_out=splev(psiN_out,k_spl)
  dkdr_out=splev(psiN_out,dkdr_spl)
  c_out,s_out,dcdr_out,dsdr_out=np.full((4,Nrho,Nsh),np.nan)
  for jj in range(c.shape[1]):
    c_out[:,jj]=splev(psiN_out,c_spl[jj])
    s_out[:,jj]=splev(psiN_out,s_spl[jj])
    dcdr_out[:,jj]=splev(psiN_out,dcdr_spl[jj])
    dsdr_out[:,jj]=splev(psiN_out,dsdr_spl[jj])

  # figures
  if doplots:
    fig,ax=plt.subplots()
    ax.plot(psiN_mxh,r_mxh)
    ax.plot(psiN_eq,r_eq,'+')
    ax.plot(psiN_out,r_out,'or')
    plt.xlabel('psiN')
    plt.ylabel('r')
    plt.legend(['from contours detection','interp on psiN grid of profiles_1d','output radial positions'])    

    fig,ax=plt.subplots()
    ax.plot(r_eq,Rref_eq)
    ax.plot(r_out,Rref_out,'or')
    ax.plot(r_eq,Bref_eq)
    ax.plot(r_out,Bref_out,'or')
    plt.xlabel('r')
    plt.ylabel('Rref, Bref')
   
    fig,ax=plt.subplots()
    ax.plot(r_eq,q_eq)
    ax.plot(r_out,q_out,'or')
    ax.plot(r_eq,shear_eq)
    ax.plot(r_out,shear_out,'or')
    plt.xlabel('r')
    plt.ylabel('q, shear')
    
    fig,ax=plt.subplots()
    ax.plot(r_eq,p_eq)
    ax.plot(r_out,p_out,'or')
    ax.plot(r_eq,dpdr_eq)
    ax.plot(r_out,dpdr_out,'or')
    plt.xlabel('r')
    plt.ylabel('p, dpdr')

    fig,ax=plt.subplots()
    ax.plot(r_eq,dR0dr_eq)
    ax.plot(r_out,dR0dr_out,'or')
    ax.plot(r_eq,dZ0dr_eq)
    ax.plot(r_out,dZ0dr_out,'or')
    plt.xlabel('r')
    plt.ylabel('dR0dr, dZ0dr')
 
    fig,ax=plt.subplots()
    ax.plot(r_eq,k_eq)
    ax.plot(r_out,k_out,'or')
    ax.plot(r_eq,dkdr_eq)
    ax.plot(r_out,dkdr_out,'or')
    plt.xlabel('r')
    plt.ylabel('k, dkdr')

    fig,ax=plt.subplots()
    ax.plot(r_eq,c_eq)
    ax.plot(r_out,c_out,'or')
    ax.plot(r_eq,s_eq)
    ax.plot(r_out,s_out,'or')
    plt.xlabel('r')
    plt.ylabel('c, s')

    fig,ax=plt.subplots()
    ax.plot(r_eq,dcdr_eq)
    ax.plot(r_out,dcdr_out,'or')
    ax.plot(r_eq,dsdr_eq)
    ax.plot(r_out,dsdr_out,'or')
    plt.xlabel('r')
    plt.ylabel('dcdr, dsdr')


  # 1D core profile quantities (r_cp, psiN_cp)
  rhotor_cp=cp.profiles_1d[ii_t_cp].grid.rho_tor_norm 
  r_spl_rhotor=splrep(rhotor_eq,r_eq)
  #psi_cp=cp.profiles_1d[ii_t_cp].grid.psi
  #psiN_cp=(psi_cp-psi_axis)/(psi_bnd-psi_axis)
  #r_cp=splev(psiN_cp,r_spl)
  r_cp=splev(rhotor_cp,r_spl_rhotor)
  tens=(rhotor_cp.size-np.sqrt(2*rhotor_cp.size))*smooth/100

  # electrons
  nelec=1
  ne_cp=cp.profiles_1d[ii_t_cp].electrons.density_thermal     # m-3 
  ne_spl_r=splrep(r_cp,ne_cp,s=tens*np.max(ne_cp)**2)
  dnedr_cp=splev(r_cp,ne_spl_r,der=1)
  te_cp=cp.profiles_1d[ii_t_cp].electrons.temperature # eV
  te_spl_r=splrep(r_cp,te_cp,s=tens*np.max(te_cp)**2)
  dtedr_cp=splev(r_cp,te_spl_r,der=1)
  #if np.any(cp.profiles_1d[ii_t_cp].electrons.pressure!=cp.profiles_1d[ii_t_cp].electrons.pressure_thermal):
  if np.any(cp.profiles_1d[ii_t_cp].electrons.density_fast>0):
    nelec_fast=1
    ne_fast_cp=cp.profiles_1d[ii_t_cp].electrons.density_fast     # m-3 
    ne_fast_spl_r=splrep(r_cp,ne_fast_cp,s=tens*np.max(ne_fast_cp)**2)
    dnefastdr_cp=splev(r_cp,ne_fast_spl_r,der=1)
    te_fast_cp=(cp.profiles_1d[ii_t_cp].electrons.pressure-cp.profiles_1d[ii_t_cp].electrons.pressure_thermal)/ne_fast_cp
    te_fast_spl_r=splrep(r_cp,te_fast_cp,s=tens*np.max(te_fast_cp)**2)
    dtefastdr_cp=splev(r_cp,te_fast_spl_r,der=1)
  else:
    nelec_fast=0

  # ions
  nions=len(cp.profiles_1d[ii_t_cp].ion)
  if nions==0:
    msg="No ion species in cp.profiles_1d["+str(ii_t_cp)+"].ion"
    msg_log(msg,log,verbose,critical=True)
  nions_fast=0
  I_ion_fast=[]
  ii_main_ion=0 # index of the ion with the highest density
  ni_highest=cp.profiles_1d[ii_t_cp].ion[0].density[0]
  for ii_ion in range(nions):
    #if np.any(cp.profiles_1d[ii_t_cp].ion[ii_ion].pressure!=cp.profiles_1d[ii_t_cp].ion[ii_ion].pressure_thermal):
    if np.any(cp.profiles_1d[ii_t_cp].ion[ii_ion].density_fast>0):
      nions_fast=nions_fast+1
      I_ion_fast.append(ii_ion)
    if cp.profiles_1d[ii_t_cp].ion[ii_ion].density[0]>ni_highest:
      ii_main_ion=ii_ion
      ni_highest=cp.profiles_1d[ii_t_cp].ion[ii_ion].density[0]
  z,mass=np.full((2,nions),np.nan)
  ti_cp,ni_cp,omi_tor_cp=np.full((3,rhotor_cp.size,nions),0.)
  dtidr_cp,dnidr_cp,domidr_cp=np.full((3,rhotor_cp.size,nions),0.)
  ti_spl_r=[]
  ni_spl_r=[]
  omi_tor_spl_r=[]
  for ii_ion in range(nions):
    if cp.profiles_1d[ii_t_cp].ion[ii_ion].state.has_value:
      msg="Multiple states for ion species not handled yet, will use the average values"
      msg_log(msg,log,verbose,critical=False)

    # type of ions
    if len(cp.profiles_1d[ii_t_cp].ion[ii_ion].element)==1:
      z[ii_ion]=cp.profiles_1d[ii_t_cp].ion[ii_ion].element[0].z_n
      mass[ii_ion]=cp.profiles_1d[ii_t_cp].ion[ii_ion].element[0].a*amu
    else:
      msg="Molecules not handled, check content of cp.profiles_1d["+str(ii_t_cp)+"].ion["+str(ii_ion)+"].element"
      msg_log(msg,log,verbose,critical=True)
    # uses codata values if H, D or T
    if cp.profiles_1d[ii_t_cp].ion[ii_ion].label[0]=='H':
      mass[ii_ion]=mH
    if cp.profiles_1d[ii_t_cp].ion[ii_ion].label[0]=='D':
      mass[ii_ion]=mD
    if cp.profiles_1d[ii_t_cp].ion[ii_ion].label[0]=='T':
      mass[ii_ion]=mT

    # density and temperature profiles
    ni_cp[:,ii_ion]=cp.profiles_1d[ii_t_cp].ion[ii_ion].density_thermal
    ni_spl_r.append(splrep(r_cp,ni_cp[:,ii_ion],s=tens*np.max(ni_cp[:,ii_ion])**2))
    dnidr_cp[:,ii_ion]=splev(r_cp,ni_spl_r[ii_ion],der=1)
    ti_cp[:,ii_ion]=cp.profiles_1d[ii_t_cp].ion[ii_ion].temperature
    ti_spl_r.append(splrep(r_cp,ti_cp[:,ii_ion],s=tens*np.max(ti_cp[:,ii_ion])**2))
    dtidr_cp[:,ii_ion]=splev(r_cp,ti_spl_r[ii_ion],der=1)

    # toroidal rotation (if available)
    if cp.profiles_1d[ii_t_cp].ion[ii_ion].rotation_frequency_tor.has_value:
      omi_tor_cp[:,ii_ion]=cp.profiles_1d[ii_t_cp].ion[ii_ion].rotation_frequency_tor
    omi_tor_spl_r.append(splrep(r_cp,omi_tor_cp[:,ii_ion],s=tens*np.max(np.abs(omi_tor_cp))**2))
    domidr_cp[:,ii_ion]=splev(r_cp,omi_tor_spl_r[ii_ion],der=1)

  # fast ions
  if nions_fast>0:
    ti_fast_cp,ni_fast_cp=np.full((2,rhotor_cp.size,nions),0.)
    dtifastdr_cp,dnifastdr_cp=np.full((2,rhotor_cp.size,nions),0.)
    ti_fast_spl_r=[]
    ni_fast_spl_r=[]
    for ii_ion in I_ion_fast:
      # density and temperature profiles
      ni_fast_cp[:,ii_ion]=cp.profiles_1d[ii_t_cp].ion[ii_ion].density_fast
      ni_fast_spl_r.append(splrep(r_cp,ni_fast_cp[:,ii_ion],s=10*tens*np.max(ni_fast_cp[:,ii_ion])**2))
      dnifastdr_cp[:,ii_ion]=splev(r_cp,ni_fast_spl_r[ii_ion],der=1)
      ti_fast_cp[:,ii_ion]=(cp.profiles_1d[ii_t_cp].ion[ii_ion].pressure
                           -cp.profiles_1d[ii_t_cp].ion[ii_ion].pressure_thermal)/(ni_fast_cp[:,ii_ion])/eV
      ti_fast_cp[np.isinf(ti_fast_cp[:,ii_ion]),ii_ion]=np.nan
      Iok=(np.isnan(ti_fast_cp[:,ii_ion])!=True)
      ti_fast_spl_r.append(splrep(r_cp[Iok],ti_fast_cp[Iok,ii_ion],s=10*tens*np.max(ti_fast_cp[Iok,ii_ion])**2))
      dtifastdr_cp[:,ii_ion]=splev(r_cp,ti_fast_spl_r[ii_ion],der=1)

    
  # ExB toroidal rotation frequency (if available)
  omphi_cp,domphidr_cp=np.full((2,rhotor_cp.size),0.)
  if cp.profiles_1d[ii_t_cp].rotation_frequency_tor_sonic.has_value:
    omphi_cp=cp.profiles_1d[ii_t_cp].rotation_frequency_tor_sonic
  omphi_spl_r= splrep(r_cp,omphi_cp,s=tens*np.max(np.abs(omphi_cp))**2)
  domphidr_cp=splev(r_cp,omphi_spl_r,der=1)

  zeff_cp=cp.profiles_1d[ii_t_cp].zeff
  zeff_spl_r=splrep(r_cp,zeff_cp) 

  # interpolation at output radial positions (r_out, psiN_out)
  ne_out=splev(r_out,ne_spl_r)
  dnedr_out=splev(r_out,ne_spl_r,der=1)
  te_out=splev(r_out,te_spl_r)
  dtedr_out=splev(r_out,te_spl_r,der=1)
  if nelec_fast>0:
    ne_fast_out=splev(r_out,ne_fast_spl_r)
    dnefastdr_out=splev(r_out,ne_fast_spl_r,der=1)
    te_fast_out=splev(r_out,te_fast_spl_r)
    dtefastdr_out=splev(r_out,te_fast_spl_r,der=1)
  ti_out,ni_out,omi_tor_out,omphi_out=np.full((4,Nrho,nions),0.)
  dtidr_out,dnidr_out,domidr_out,domphidr_out=np.full((4,Nrho,nions),0.)
  if nions_fast>0:
    ti_fast_out,ni_fast_out=np.full((2,Nrho,nions),0.)
    dtifastdr_out,dnifastdr_out=np.full((2,Nrho,nions),0.)

  for ii_ion in range(nions):
    ni_out[:,ii_ion]=splev(r_out,ni_spl_r[ii_ion])
    dnidr_out[:,ii_ion]=splev(r_out,ni_spl_r[ii_ion],der=1)
    ti_out[:,ii_ion]=splev(r_out,ti_spl_r[ii_ion])
    dtidr_out[:,ii_ion]=splev(r_out,ti_spl_r[ii_ion],der=1)
    omi_tor_out[:,ii_ion]=splev(r_out,omi_tor_spl_r[ii_ion])
    domidr_out[:,ii_ion]=splev(r_out,omi_tor_spl_r[ii_ion],der=1)
  for ii_ion in I_ion_fast:
    ni_fast_out[:,ii_ion]=splev(r_out,ni_fast_spl_r[ii_ion])
    dnifastdr_out[:,ii_ion]=splev(r_out,ni_fast_spl_r[ii_ion],der=1)
    ti_fast_out[:,ii_ion]=splev(r_out,ti_fast_spl_r[ii_ion])
    dtifastdr_out[:,ii_ion]=splev(r_out,ti_fast_spl_r[ii_ion],der=1)
    
  omphi_out=splev(r_out,omphi_spl_r)
  domphidr_out=splev(r_out,omphi_spl_r,der=1)
  zeff_out=splev(r_out,zeff_spl_r)

  if doplots:
    fig,ax=plt.subplots()
    ax.plot(r_cp,ne_cp)
    ax.plot(r_out,ne_out,'or')
    ax.plot(r_cp,dnedr_cp)
    ax.plot(r_out,dnedr_out,'or')
    plt.xlabel('r')
    plt.ylabel('n_e, dnedr')
 
    fig,ax=plt.subplots()
    ax.plot(r_cp,te_cp)
    ax.plot(r_out,te_out,'or')
    ax.plot(r_cp,dtedr_cp)
    ax.plot(r_out,dtedr_out,'or')
    plt.xlabel('r')
    plt.ylabel('t_e, dtedr')
 
    fig,ax=plt.subplots()
    ax.plot(r_cp,ni_cp)
    ax.plot(r_out,ni_out,'or')
    ax.plot(r_cp,dnidr_cp)
    ax.plot(r_out,dnidr_out,'or')
    plt.xlabel('r')
    plt.ylabel('n_i, dnidr')
 
    fig,ax=plt.subplots()
    ax.plot(r_cp,ti_cp)
    ax.plot(r_out,ti_out,'or')
    ax.plot(r_cp,dtidr_cp)
    ax.plot(r_out,dtidr_out,'or')
    plt.xlabel('r')
    plt.ylabel('t_i, dtidr')
    
    if nions_fast>0:
      fig,ax=plt.subplots()
      ax.plot(r_cp,ni_fast_cp)
      ax.plot(r_out,ni_fast_out,'or')
      ax.plot(r_cp,dnifastdr_cp)
      ax.plot(r_out,dnifastdr_out,'or')
      plt.xlabel('r')
      plt.ylabel('n_i_fast, dnifastdr')

      fig,ax=plt.subplots()
      ax.plot(r_cp,ti_fast_cp)
      ax.plot(r_out,ti_fast_out,'or')
      ax.plot(r_cp,dtifastdr_cp)
      ax.plot(r_out,dtifastdr_out,'or')
      plt.xlabel('r')
      plt.ylabel('t_i_fast, dtifastdr')
      
    #fig,ax=plt.subplots()
    #ax.plot(r_cp,omi_tor_cp)
    #ax.plot(r_out,omi_tor_out,'or')
    #ax.plot(r_cp,domidr_cp)
    #ax.plot(r_out,domidr_out,'or')
    #plt.xlabel('r')
    #plt.ylabel('omi_tor, domitordr')

    fig,ax=plt.subplots()
    ax.plot(r_cp,omphi_cp)
    ax.plot(r_out,omphi_out,'or')
    ax.plot(r_cp,domphidr_cp)
    ax.plot(r_out,domphidr_out,'or')
    plt.xlabel('r')
    plt.ylabel('omphi, domphidr')

    fig,ax=plt.subplots()
    ax.plot(r_cp,zeff_cp)
    ax.plot(r_out,zeff_out,'or')
    plt.xlabel('r')
    plt.ylabel('Zeff')


  # generate the gyrokinetics_local IDSs
  qref=eV
  mref=mD    

  ids_factory=imaspy.IDSFactory("3.41.0")
  for ii_rho in range(Nrho):
    gkids=ids_factory.gyrokinetics_local()

    Rref=Rref_out[ii_rho]
    Bref=Bref_out[ii_rho]
    Tref=te_out[ii_rho]
    if nelec_fast==0:
      nref=ne_out[ii_rho] 
    else:
      nref=ne_out[ii_rho]+ne_fast_out[ii_rho]
    vthref=np.sqrt(2*Tref*eV/mref)
    rhoref=mref*vthref/(qref*Bref)

    # normalizing_quantities
    gkids.normalizing_quantities.r=Rref
    gkids.normalizing_quantities.b_field_tor=Bref
    gkids.normalizing_quantities.n_e=nref
    gkids.normalizing_quantities.t_e=Tref
    
    # flux_surface
    gkids.flux_surface.ip_sign=ip_sign
    gkids.flux_surface.b_field_tor_sign=b_field_tor_sign
    gkids.flux_surface.r_minor_norm=r_out[ii_rho]/Rref
    gkids.flux_surface.q=q_out[ii_rho]
    gkids.flux_surface.magnetic_shear_r_minor=r_out[ii_rho]/q_out[ii_rho]*dqdr_out[ii_rho]
    gkids.flux_surface.pressure_gradient_norm=-(2*mu0*Rref/Bref**2)*dpdr_out[ii_rho]
    gkids.flux_surface.dgeometric_axis_r_dr_minor=dR0dr_out[ii_rho]
    gkids.flux_surface.dgeometric_axis_z_dr_minor=dZ0dr_out[ii_rho]
    gkids.flux_surface.elongation=k_out[ii_rho]
    gkids.flux_surface.delongation_dr_minor_norm=dkdr_out[ii_rho]*Rref
    gkids.flux_surface.shape_coefficients_c=c_out[ii_rho,:]
    gkids.flux_surface.shape_coefficients_s=s_out[ii_rho,:]
    gkids.flux_surface.dc_dr_minor_norm=dcdr_out[ii_rho,:]*Rref
    gkids.flux_surface.ds_dr_minor_norm=dsdr_out[ii_rho,:]*Rref

    # species_all
    if opt['toroidal flow']=='none':
      gkids.species_all.velocity_tor_norm=0.0
    if opt['toroidal flow']=='from omphi':
      gkids.species_all.velocity_tor_norm=omphi_out[ii_rho]*Rref/vthref

    if opt['ExB shear']=='none':
      gkids.species_all.shearing_rate_norm=0.0
    if opt['ExB shear']=='from omphi':
      gkids.species_all.shearing_rate_norm=-sign_cocos/(2*np.pi)*dpsidr_out[ii_rho]*domphidr_out[ii_rho]*Rref/(vthref*Bref)
    
    gkids.species_all.beta_reference=2*mu0*nref*Tref*eV/Bref**2
    gkids.species_all.debye_length_norm=np.sqrt(eps0*Tref*eV/(nref*qref**2))/rhoref

    # species
    if opt['species']=='main only': # 2 species: electrons and main ion (defined as highest density ion), 
      nsp=2
      gkids.species.resize(nsp)
      # electron species
      gkids.species[0].charge_norm=-1
      gkids.species[0].mass_norm=me/mref
      if nelec_fast==0:
        gkids.species[0].density_norm=ne_out[ii_rho]/nref
        gkids.species[0].density_log_gradient_norm=-Rref/ne_out[ii_rho]*dnedr_out[ii_rho]
      else:
        gkids.species[0].density_norm=(ne_out[ii_rho]+ne_fast_out[ii_rho])/nref
        gkids.species[0].density_log_gradient_norm=-Rref/(ne_out[ii_rho]+ne_fast_out[ii_rho])*(dnedr_out[ii_rho]+dnefastdr_out[ii_rho])
      gkids.species[0].temperature_norm=te_out[ii_rho]/Tref
      gkids.species[0].temperature_log_gradient_norm=-Rref/te_out[ii_rho]*dtedr_out[ii_rho]

      # ion species
      gkids.species[1].charge_norm=z[ii_main_ion]
      gkids.species[1].mass_norm=mass[ii_main_ion]/mref
      gkids.species[1].density_norm=ne_out[ii_rho]/nref # uses electron density to enforce quasi-neutrality
      gkids.species[1].density_log_gradient_norm=-Rref/ne_out[ii_rho]*dnedr_out[ii_rho]
      gkids.species[1].temperature_norm=ti_out[ii_rho,ii_main_ion]/Tref
      gkids.species[1].temperature_log_gradient_norm=-Rref/ti_out[ii_rho,ii_main_ion]*dtidr_out[ii_rho,ii_main_ion]

    if opt['species']=='main+imp':
      nsp=nions+1
      gkids.species.resize(nsp)
      # electron species
      gkids.species[0].charge_norm=-1
      gkids.species[0].mass_norm=me/mref
      if nelec_fast==0:
        gkids.species[0].density_norm=ne_out[ii_rho]/nref
        gkids.species[0].density_log_gradient_norm=-Rref/ne_out[ii_rho]*dnedr_out[ii_rho]
      else:
        gkids.species[0].density_norm=(ne_out[ii_rho]+ne_fast_out[ii_rho])/nref
        gkids.species[0].density_log_gradient_norm=-Rref/(ne_out[ii_rho]+ne_fast_out[ii_rho])*(dnedr_out[ii_rho]+dnefastdr_out[ii_rho])
      gkids.species[0].temperature_norm=te_out[ii_rho]/Tref
      gkids.species[0].temperature_log_gradient_norm=-Rref/te_out[ii_rho]*dtedr_out[ii_rho]

      # ions species
      for ii_ion in range(nions):
        gkids.species[ii_ion+1].charge_norm=z[ii_ion]
        gkids.species[ii_ion+1].mass_norm=mass[ii_ion]/mref
        if nions_fast==0:
          gkids.species[ii_ion+1].density_norm=ni_out[ii_rho,ii_ion]/nref
          gkids.species[ii_ion+1].density_log_gradient_norm=-Rref/ni_out[ii_rho,ii_ion]*dnidr_out[ii_rho,ii_ion]
        else:
          gkids.species[ii_ion+1].density_norm=(ni_out[ii_rho,ii_ion]+ni_fast_out[ii_rho,ii_ion])/nref
          gkids.species[ii_ion+1].density_log_gradient_norm=-Rref/(ni_out[ii_rho,ii_ion]+ni_fast_out[ii_rho,ii_ion])*(dnidr_out[ii_rho,ii_ion]+dnifastdr_out[ii_rho,ii_ion])
        gkids.species[ii_ion+1].temperature_norm=ti_out[ii_rho,ii_ion]/Tref
        gkids.species[ii_ion+1].temperature_log_gradient_norm=-Rref/ti_out[ii_rho,ii_ion]*dtidr_out[ii_rho,ii_ion]

      # adjust main ion density
      ni_dum=ni_out[ii_rho,:]
      dnidr_dum=dnidr_out[ii_rho,:]
      ni_dum[ii_main_ion]=0
      dnidr_dum[ii_main_ion]=0
      gkids.species[ii_main_ion+1].density_norm=(ne_out[ii_rho]-np.sum(z*ni_dum))/nref
      gkids.species[ii_main_ion+1].density_log_gradient_norm=-Rref/(ne_out[ii_rho]-np.sum(z*ni_dum))*(dnedr_out[ii_rho]-np.sum(z*dnidr_dum))

    # parallel flow shear
    for ii_sp in range(nsp):
      if opt['parallel flow shear']=='none':
        gkids.species[ii_sp].velocity_tor_gradient_norm=0.0
      if opt['parallel flow shear']=='from omphi':
        gkids.species[ii_sp].velocity_tor_gradient_norm=-Rref**2/vthref*domphidr_out[ii_rho]

    # collisions
    n_norm=np.array([sp.density_norm for sp in gkids.species])
    T_norm=np.array([sp.temperature_norm for sp in gkids.species])
    zz=np.array([sp.charge_norm for sp in gkids.species])
    sp_type=['e' if zzz==-1 else 'i' for zzz in zz]
    mm=np.array([sp.mass_norm for sp in gkids.species])
    A=mm*mref/amu
    vth=np.sqrt(2*T_norm*eV*Tref/(mm*mref))
    Lambda=coulomb_logarithm(sp_type,n_norm,T_norm,zz,A,nref/1e19,Tref/1e3)
    nuab=np.full((nsp,nsp),0.0)
    for ii in range(nsp):
      for jj in range(nsp):
        nuab[ii,jj]=Rref/vthref*n_norm[jj]*nref*zz[ii]**2*zz[jj]**2*eV**4/(4*np.pi*eps0**2
                            *(mm[ii]*mref)**2*vth[ii]**3)
    nuab=nuab*Lambda
    nuab[0,1]=nuab[0,1]*zeff_out[ii_rho]
    gkids.collisions.collisionality_norm=nuab


    gkall.append(gkids)
  return gkall,log
