# This is an example script to generate a GK IDS and a GKW input file from Aaron JET database

import imaspy
import numpy as np
import os
os.chdir('/home/yann/projects/imasgk_git/gkids/tools')
import cpeq2gk
os.chdir('/home/yann/codes/gkw/python/yc')
import gkwrun
import gkwimas
import matplotlib.pyplot as plt
import matplotlib

hdf5_backend=imaspy.ids_defs.HDF5_BACKEND
dbname='JET_DB_Aaron'
dbpath='/home/yann/runs3/'

shot=68751 # no fast ions 
run=2
shot=91603 # with fast ions
run=1

# load required IDSs
with imaspy.DBEntry(hdf5_backend,dbname,shot,run,dbpath) as dbentry:
  cp=dbentry.get("core_profiles")
  eq=dbentry.get("equilibrium")
  ss=dbentry.get("summary")

rhotor_target=[0.02, 0.2 , 0.3 , 0.6 , 0.8 , 0.9]
opt={'species': 'main only',
     'toroidal flow': 'from omphi',
     'parallel flow shear': 'from omphi',
     'ExB shear': 'from omphi',
      }

gkids_all,log=cpeq2gk.cpeq2gk(cp,eq,rhotor_target,opt,t_target=None,smooth=1e-5,verbose=True,doplots=True)


# load GKW reference input file
gkw_home='/home/yann/runs3/gkw/'
proj_ref='TCV_RAMPUP_2'
flnm_ref='64965r07t1_kth_11'     # linear run template
ref=gkwrun.GKWrun()
ref.set_storage(gkw_home+proj_ref,'multiple_folders',flnm_ref)
ref.get_inputs()

for ii,gk in enumerate(gkids_all):
  # fill in model 
  gk.model.adiabatic_electrons=0
  gk.model.include_a_field_parallel=0
  gk.model.include_b_field_parallel=0
  gk.model.include_full_curvature_drift=1
  gk.model.include_coriolis_drift=1
  gk.model.include_centrifugal_effects=1
  gk.model.collisions_pitch_only=0
  gk.model.collisions_momentum_conservation=1
  gk.model.collisions_energy_conservation=1
  gk.model.collisions_finite_larmor_radius=0
  
  # fill in wavevectors
  gk.linear.wavevector.resize(1) #only works with IMASPy
  gk.linear.wavevector[0].binormal_wavevector_norm=0.3
  gk.linear.wavevector[0].radial_wavevector_norm=0.0

  # generate input file from IDS
  dum=gkwimas.ids2gkw(gk,'linear',params_source='gkwref',gkwref=ref)
  
  # write GKW input file to disk
  dum.write_input('/home/yann/tmp/gkwinput_'+str(ii)+'.dat')


# give overview of species  shot, run, thermal, fast
ov=[]
for shot in os.scandir(dbpath+dbname+'/3/'):
  if shot.is_dir():
    ov_line=[int(shot.name)]
    for run in os.scandir(dbpath+dbname+'/3/'+shot.name):
      if run.is_dir():
        ov_line.append(int(run.name))
        with imaspy.DBEntry(hdf5_backend,dbname,int(shot.name),int(run.name),dbpath) as dbentry:
          cp=dbentry.get("core_profiles")
        sp_th,sp_fast=cpeq2gk.sp_overview(cp)
        ov_line.append(sp_th[0])
        ov_line.append(sp_fast[0])
        ov.append(ov_line)

