# Set of functions to manipulate gyrokinetics IDSs (created with idspy)
# YC - 23.08.2023
from idspy_dictionaries import ids_gyrokinetics_local as gkids 
import idspy_toolkit as idspy
import numpy as np

def ids_diff(ids1,ids2,rtol=1e-5,atol=1e-8):
  """ Check the difference between two gyrokinetics IDSs (created with idspy) for built-in lists of attributes
  Inputs
    ids1             First gyrokinetics IDS
    ids2             Second gyrokinetics IDS
    rtol             relative tolerance used in numpy.allclose
    atol             absolute tolerance used in numpy.allclose

  Outputs
    check            True is the two IDS subsets are identical
    log              Dict listing the different or missing attributes in the two IDSs    
  """
  def is_attr_same(sub_ids1,sub_ids2,attr,rtol,atol):
    "Check existence and difference between ids1.node.attr and ids2.node.attr"
    nonlocal log,check
    attr1=None
    attr2=None
    try:
      attr1=getattr(sub_ids1,attr)
    except AttributeError:  
      log['missing attributes in ids1'].append(attr)
      check=False
    try:
      attr2=getattr(sub_ids2,attr)
    except AttributeError:  
      log['missing attributes in ids2'].append(attr)
      check=False

    # check attributes difference
    if not attr1 is None and not attr2 is None:
      if isinstance(attr1,(str,bool,int)):
        same=(attr1==attr2)
      else: 
        same=np.allclose(attr1,attr2,rtol,atol)
    return same

  check=True
  log={}
  log['missing attributes in ids1']=[]
  log['missing attributes in ids2']=[]
  ## list of attributes to check
  nodes={'ids_properties': ('provider',),
         'code': gkids.Code().__slots__,
         'flux_surface': gkids.FluxSurface().__slots__,
         'model': gkids.Model().__slots__,
         'species_all': gkids.InputSpeciesGlobal().__slots__,
         'collisions': gkids.Collisions().__slots__,
         'species': gkids.Species().__slots__}
  no_check=('max_repr_length','version','library')  

  for node in nodes.keys():
    if node=='species':  # should do something similar for code/library which is also a list
      nelem=len(getattr(ids1,node))
      if nelem==len(getattr(ids1,node)):
        log[node]=[]
        for ii in range(nelem):
          log[node].append([])
          for attr in nodes[node]:
            if not attr in no_check:
              if not is_attr_same(getattr(ids1,node)[ii],getattr(ids2,node)[ii],attr,rtol,atol):
                log[node][ii].append("Difference in "+attr)
                check=False
      else:
        log['species']="Different number of species"        
    else:
      log[node]=[]
      for attr in nodes[node]:
        if not attr in no_check:
          if not is_attr_same(getattr(ids1,node),getattr(ids2,node),attr,rtol,atol):
            log[node].append("Difference in "+attr)
            check=False

  return check,log

 

def add_eigenmode(ids1,ids2):
  """ Add linear eigenmodes from an existing gyrokinetics IDS to another gyrokinetics IDS. 
      Will check first that the input parts of the two IDSs are identical.

  Inputs
    ids1             Reference gyrokinetics IDS where the eigenmodes will be added
    ids2             Gyrokinetics IDS with the extra eigenmode

  Outputs
    ids1             Reference 'gyrokinetics' IDS with the new eigenmode added to it
  """
  # check that input parts are identical
  check,log=ids_diff(ids1,ids2)
  i_count=0
  if not check:
    print("Differences found in the input parts of ids1 and ids2")
    for key, value in log.items():
      print(key,': ', value)
  else:
    for new_wav in ids2.linear.wavevector:
      is_new=True
      for ii_wav,wav in enumerate(ids1.linear.wavevector):
        if np.allclose([new_wav.radial_wavevector_norm,new_wav.binormal_wavevector_norm],[wav.radial_wavevector_norm,wav.binormal_wavevector_norm]):
          is_new=False
          ref_ii_wav=ii_wav
      if is_new: #insert it
        ids1.linear.wavevector.append(new_wav)
        i_count=i_count+1
      else: #check eigenmodes
        any_ini_run=np.any([eiv.initial_value_run==True for eiv in ids1.linear.wavevector[ref_ii_wav].eigenmode])
        any_eiv_run=np.any([eiv.initial_value_run==False for eiv in ids1.linear.wavevector[ref_ii_wav].eigenmode])
        for eiv in new_wav.eigenmode:
          if eiv.initial_value_run==True:
            if not any_ini_run:  # then insert new initial value run (only one)
              ids1.linear.wavevector[ref_ii_wav].eigenmode.append(eiv)
              i_count=i_count+1
              any_ini_run=True
          else: 
            if not any_eiv_run:  # then insert new eigenvalue runs
              ids1.linear.wavevector[ref_ii_wav].eigenmode.append(eiv)
              i_count=i_count+1
  print("Inserted "+str(i_count)+" new eigenmodes in the reference IDS")
  return ids1
