# Set of routines to deal with flux surfaces parametrisation
# Contains:
#   psi2rz        (R,Z) flux surface description from poloidal flux 2D map
#   fourier2rz    (R,Z) flux surface description from Fourier parametrisation
#   mxh2rz        (R,Z) flux surface description from Miller eXtended Harmonic
#   miller2rz     (R,Z) flux surface description from Miller parametrisation
#   rz2fourier    Fourier parametrisation from (R,Z) flux surface description
#   rz2mxh        Miller eXtended Harmonic from (R,Z) flux surface description
#   rz2miller     Miller parametrisation from (R,Z) flux surface description

import numpy as np
from scipy.interpolate import CubicSpline
from scipy.interpolate import splrep
from scipy.interpolate import splev
from scipy.interpolate import interpn
from scipy.optimize import fmin
import matplotlib.pyplot as plt

def psi2rz(Rg,Zg,psiN,psiN_out=np.linspace(0.01,0.9,num=35),Nth=101,R_lcfs=None,Z_lcfs=None,doplots=True,interp=True):
  """ return R,Z
      Compute the contours of the poloidal flux given in inputs on the (Rg,Zg) grid
      Dense (Rg,Zg) grid needed to find the contours close to the magnetic axis

      Inputs
        Rg,Zg     the R,Z grid on which the psi values is given
                    Rg and Zg are 1D arrays of size Nr and Nz, respectively
        psiN      values of the normalised poloidal flux on the grid above
                    psiN=(psi-psi_axis)/(psi_edge-psi_axis)
                    2D array of shape (Nr,Nz)
        psiN_out  values of psiN for which the contours are computed
        Nth       number of poloidal points used to discretize the flux surface contours
        R_lcfs,   Optional - (R,Z) description of the last closed flux surface
          Z_lcfs    If given the contours of psiN are computed only for 
                      min(R_lcfs)<R<max(R_lcfs) and min(Z_lcfs)<R<max(Z_lcfs)
                    Needed when X-points are present
        doplots   Perform plots to check the contour detection if True (default: True)
        interp    If true, interpolate psiN on a finer grid when Rg.size<120 or Zg.size<240 (default: True)
                  Improves considerably the results for low resolution inputs (e.g. JET EQDSK)

      Outputs
        R,Z       discretised flux surface contours for psiN_out values
                    2D arrays of shape (Npsi,Nth), no double points    
  """
  Rg=np.asarray(Rg)
  Zg=np.asarray(Zg)
  psiN=np.asarray(psiN)
  if psiN.shape!=(Rg.size,Zg.size):
    print("Shape of psi2D does not match that of Rg,Zg")
    return
  if np.max(psiN_out)>1 or np.min(psiN_out)<0:
    print("psiN_out values must be in the interval [0,1]")
    return
  if not (R_lcfs is None or Z_lcfs is None):
    Rg[Rg>np.max(R_lcfs)]=np.nan
    Rg[Rg<np.min(R_lcfs)]=np.nan
    Zg[Zg>np.max(Z_lcfs)]=np.nan
    Zg[Zg<np.min(Z_lcfs)]=np.nan 

  # remove NaNs
  IRg_ok = np.isnan(Rg)==False
  IZg_ok = np.isnan(Zg)==False
  Rg=Rg[IRg_ok]
  Zg=Zg[IZg_ok]
  psiN=psiN[IRg_ok,:]
  psiN=psiN[:,IZg_ok]

  # interpolate psiN on a finer grid if resolution too low
  Nrg=120
  Nzg=240
  if interp and (Rg.size<Nrg or Zg.size<Nzg):
    Rg_new=np.linspace(np.min(Rg),np.max(Rg),Nrg)
    Zg_new=np.linspace(np.min(Zg),np.max(Zg),Nzg)
    RZ_dum=np.array(np.meshgrid(Rg_new,Zg_new))
    RZ_dum=np.moveaxis(RZ_dum,0,2).reshape(Nrg*Nzg,2)
    psiN=np.transpose(interpn((Rg,Zg),psiN,RZ_dum,method='cubic').reshape(Nzg,Nrg))
    Rg=Rg_new
    Zg=Zg_new

  if doplots:
    fig,ax=plt.subplots()
    cs=ax.contour(Rg,Zg,np.transpose(psiN),psiN_out)
    if (R_lcfs is not None) and (Z_lcfs is not None):
      ax.plot(R_lcfs,Z_lcfs,'r')
    plt.axis('equal')
    plt.xlabel('R')
    plt.ylabel('Z')
  else: 
    fig,ax=plt.subplots()
    cs=ax.contour(Rg,Zg,np.transpose(psiN),psiN_out)
    plt.close(fig)


  Npsi=len(psiN_out)
  R=np.full((Npsi,Nth),np.nan)
  Z=np.full((Npsi,Nth),np.nan)
  th_out=np.linspace(-np.pi,np.pi,num=Nth,endpoint=True)
  ll=cs.allsegs
  for ii_psi in range(Npsi):
    # check that there is only one element without NaN per level
    ii_ll_ok=[]
    for ii_ll in range(len(ll[ii_psi])):
      if not np.isnan(ll[ii_psi][ii_ll]).any():
        ii_ll_ok.append(ii_ll)
    if len(ii_ll_ok)==1:
      rr,zz=ll[ii_psi][ii_ll_ok[0]].T
    else:
      if len(ii_ll_ok)<1:
        print("No contour found for level "+str(ii_psi))  
      if len(ii_ll_ok)>1:
        print("More than one contour found for level "+str(ii_psi)+", something went wrong")
        #return  
    # compute poloidal angle and interpolate contours on finer grid
    Rc=np.mean(rr[0:-1])
    Zc=np.mean(zz[0:-1])
    th=np.arctan2((zz[0:-1]-Zc),rr[0:-1]-Rc)
    th_sorted,Ith=np.unique(th,return_index=True)
    thth=np.concatenate((th_sorted,[th_sorted[0]+2*np.pi]))
    RR=np.concatenate((rr[Ith],[rr[Ith[0]]]))
    ZZ=np.concatenate((zz[Ith],[zz[Ith[0]]]))
    Rspl=splrep(thth,RR,per=True)
    Zspl=splrep(thth,ZZ,per=True)
    th_dum=th_out
    for ii_dum,th in enumerate(th_dum): # this is needed to avoid extrapolation
      if th<thth[0]:
        th_dum[ii_dum]=th+2*np.pi
      if th>thth[-1]:
        th_dum[ii_dum]=th-2*np.pi
    R[ii_psi,:]=splev(th_dum,Rspl,ext=0)
    Z[ii_psi,:]=splev(th_dum,Zspl,ext=0)

  if doplots:
    ax.plot(np.transpose(R),np.transpose(Z),'b--')
    plt.ion()
    plt.show()

  return R[:,0:-1],Z[:,0:-1]


def fourier2rz(c,s,dcdr,dsdr,code='gkw',dr_frac=0.01,R0=1,Z0=0,Nth=500):
  """ return r,R,Z
      Compute the (R,Z) description of a flux surface and two adjacent neighbours from its Fourier parametrisation.
      The flux surfaces are computed as:
          R(r,theta) = R0 + a(r,theta)*cos(theta)
          Z(r,theta) = Z0 + sign_theta*a(r,theta)*sin(theta)
      with
          a(r,theta) = sum_{n=0 to N} [ c_n(r)*cos(n*theta) + s_n*sin(n*theta) ]

      The definition of theta and r are:
          tan(theta) = sign_theta * (Z-Z0)/(R-R0)
             with sign_theta code dependent
          r=(max(R)-min(R))/2

      This parametrisation is close to that introduced in Candy PPCF 51, 105009 (2009)

      Inputs:
        c,s            Fourier parametrisation of the flux surface 
        dcdr, dsdr     Radial derivative of the coefficients above 
        code           Code convention: 'gkw' or 'imas'
                          gkw:  theta=0 at Z=Z0 on the LFS and increasing counter-clockwise, sign_theta=+1
                          imas: theta=0 at Z=Z0 on the LFS and increasing clockwise (GK IDS convention), sign_theta=-1
        R0, Z0         reference point, optional (default: R0=1, Z0=0)
        dr_frac        neighbouring flux surfaces will be computed at r0*(1-dr_frac) and r0*(1+dr_frec)
                       optional (default dr_frac=0.01)
        Nth            number of points to discretize the flux surfaces
                       optional (default: Nth=500)
      Outputs:
        r              Minor radius of the three FS, given at r0*[1-dr_frac, 1, 1 + dr_frac]
        R,Z            Flux surface description (3,Nth) in cylindrical coordinates (no double points)
                       
      Note that c, s, R0, Z0 are assumed to all be given with the same normalisation/units, which 
      can be arbitrary. 
      The (R,Z) and r values will be given with the same normalisation/units as used in input.
  """  
  c=np.asarray(c)
  s=np.asarray(s)
  dcdr=np.asarray(dcdr)
  dsdr=np.asarray(dsdr)
  Nsh = c.size
  if not np.all(np.array([s.size,dcdr.size,dsdr.size])==Nsh):
   print("Fourier coefficient arrays should all have the same size")
   return

  if code=='gkw':
   sign_theta=1
  elif code=='imas':
   sign_theta=-1
  else:
   print("Unknown code convention. Available: 'gkw' or 'imas'")
   return

  Nr=3    # total number of FS: 1 + 2 neighbours  
  th_grid=np.linspace(0,2*np.pi,num=Nth,endpoint=False)
  n=np.arange(Nsh)
  
  THETA=np.tile(th_grid,(Nsh,1))
  N=np.transpose(np.tile(n,(Nth,1)))
  C=np.transpose(np.tile(c,(Nth,1)))
  S=np.transpose(np.tile(s,(Nth,1)))
  DCDR=np.transpose(np.tile(dcdr,(Nth,1)))
  DSDR=np.transpose(np.tile(dsdr,(Nth,1)))

  # distance to the reference point, as a function of theta
  aN = np.sum(C*np.cos(N*THETA)+S*np.sin(N*THETA),axis=0)
  daNdr = np.sum(DCDR*np.cos(N*THETA)+DSDR*np.sin(N*THETA),axis=0)
  
  # minor radius r0
  Rdum=aN*np.cos(th_grid)
  r0 = (np.max(Rdum)-np.min(Rdum))/2
  
  # neighbouring flux surfaces
  dr=r0*dr_frac
  aNp = aN + daNdr*dr
  aNm = aN - daNdr*dr

  R = R0 + np.stack((aNm,aN,aNp))*np.cos(np.tile(th_grid,(3,1)))
  Z = Z0 + sign_theta*np.stack((aNm,aN,aNp))*np.sin(np.tile(th_grid,(3,1)))
  r = r0*np.array([1-dr_frac,1,1+dr_frac])

  return r,R,Z

def mxh2rz(r0,R0,Z0,k,c,s,dR0dr,dZ0dr,dkdr,dcdr,dsdr,code='imas',dr_frac=0.01,Nth=500):
  """ return r,R,Z
      Compute the (R,Z) description of a flux surface and two adjacent neighbours from 
      its MXH parametrisation, see Arbon PPCF 63, 012001 (2021).
      The flux surfaces are computed as:
          R(r,theta) = R0 + r*cos(theta_R)
          Z(r,theta) = Z0 + sign_theta*r*k*sin(theta)
      with
          theta_R(r,theta) = theta + sum_{n=0 to Nsh-1} [ c_n(r)*cos(n*theta) + s_n*sin(n*theta) ]

      The definition of theta and r are:
          tan(theta) = sign_theta * (Z-Z0)/(R-R0)
             with sign_theta code dependent (but assuming theta=0 at the LFS)
          r=(max(R)-min(R))/2

      Inputs:
        r0             minor radius
        R0, Z0         flux surface center
        k              elongation
        c,s            Miller harmonic coefficients
        dR0dr          radial derivative of R0
        dZ0dr          radial derivative of Z0
        dkdr           radial derivative of k (exact definition code dependent)
        dcdr,dsdr      radial derivative of c and s
        code           Code convention (for the sign of theta): 'gkw' or 'imas'
                          gkw:  theta=0 at Z=Z0 on the LFS and increasing counter-clockwise, sign_theta=+1
                          imas: theta=0 at Z=Z0 on the LFS and increasing clockwise (GK IDS convention), sign_theta=-1
        dr_frac        neighbouring flux surfaces will be computed at r0*(1-dr_frac) and r0*(1+dr_frec)
                       optional (default dr_frac=0.01)
        Nth            number of points to discretize the flux surfaces
                       optional (default: Nth=500)
      Outputs:
        r              Minor radius of the three FS, given at r0*[1-dr_frac, 1, 1 + dr_frac]
        R,Z            Flux surface description (3,Nth) in cylindrical coordinates (no double points)
                       
      Note that r0,R0,Z0,k,c,s,dR0dr,dZ0dr,dkdr,dcdr,dsdr  are assumed to all be given with the same normalisation/units, 
      which can be arbitrary. 
      The (R,Z) and r values will be given with the same normalisation/units as used in input.
  """  
  c=np.asarray(c)
  s=np.asarray(s)
  dcdr=np.asarray(dcdr)
  dsdr=np.asarray(dsdr)
  Nsh = c.size
  if not np.all(np.array([s.size,dcdr.size,dsdr.size])==Nsh):
   print("MXH coefficient arrays should all have the same size")
   return

  if code=='gkw':
   sign_theta=1
  elif code=='imas':
   sign_theta=-1
  else:
   print("Unknown code convention. Available: 'gkw' or 'imas'")
   return

  Nr=3    # total number of FS: 1 + 2 neighbours  
  th_grid=np.linspace(0,2*np.pi,num=Nth,endpoint=False)
  dr=r0*np.array([-dr_frac, 0, dr_frac])
  r=r0+dr
  n=np.arange(Nsh)
  THETA=np.tile(th_grid,(Nsh,1))
  N=np.transpose(np.tile(n,(Nth,1)))
  C=np.transpose(np.tile(c,(Nth,1)))
  S=np.transpose(np.tile(s,(Nth,1)))
  DCDR=np.transpose(np.tile(dcdr,(Nth,1)))
  DSDR=np.transpose(np.tile(dsdr,(Nth,1)))

  # angle theta_R, as a function of theta
  thR = th_grid + np.sum(C*np.cos(N*THETA)+S*np.sin(N*THETA),axis=0)
  dthRdr = np.sum(DCDR*np.cos(N*THETA)+DSDR*np.sin(N*THETA),axis=0)

  # compute shape description at all radii
  r_all=np.transpose(np.tile(r,(Nth,1)))
  th_all=np.tile(th_grid,(Nr,1))
  thR_all=np.tile(thR,(Nr,1))+np.tile(dthRdr,(Nr,1))*np.transpose(np.tile(dr,(Nth,1)))
  R0_all=np.transpose(np.tile(R0+dR0dr*dr,(Nth,1)))
  Z0_all=np.transpose(np.tile(Z0+dZ0dr*dr,(Nth,1)))
  k_all=np.transpose(np.tile(k+dkdr*dr,(Nth,1)))

  R = R0_all + r_all*np.cos(thR_all)
  Z = Z0_all + sign_theta*r_all*k_all*np.sin(th_all)

  return r,R,Z


def miller2rz(r0,Rmil,Zmil,k,d,z,dRmildr,dZmildr,sk,sd,sz,code='gkw',dr_frac=0.01,Nth=500):
  """ return r,R,Z
      Compute the (R,Z) description of a flux surface and two adjacent neighbours from its Miller parametrisation.
      The flux surfaces are computed as:
          R(r,theta) = Rmil + r*cos(theta + arcsin(d)*sin(theta)))
          Z(r,theta) = Zmil + r*k*sin(theta+z*sin(2*theta))

      The definition of theta and r are:
          tan(theta) = sign_theta * (Z-Z0)/(R-R0)
             with sign_theta code dependent
          r=(max(R)-min(R))/2

      Inputs:
        r0             minor radius
        Rmil, Zmil     flux surface center
        k,d,z          elongation, triangularity, squareness
        dRmildr        radial derivative of Rmil
        dZmildr        radial derivative of Zmil
        sk,sd,sz       radial derivatives of k, d, z (exact definition code dependent)
        code           Code convention: 'gkw' (default) or 'tglf'
                          gkw:  see GKW manual
        dr_frac        neighbouring flux surfaces will be computed at r0*(1-dr_frac) and r0*(1+dr_frec)
                       optional (default dr_frac=0.01)
        Nth            number of points to discretize the flux surfaces
                       optional (default: Nth=500)
      Outputs:
        r              Minor radius of the three FS, given at r0*[1-dr_frac, 1, 1 + dr_frac]
        R,Z            Flux surface description (3,Nth) in cylindrical coordinates (no double points)
                       
      Note that r0,Rmil,Zmil,k,d,z,dRmildr,dZmildr,sk,sd,sz are assumed to all be given with the same normalisation/units, 
      which can be arbitrary. 
      The (R,Z) and r values will be given with the same normalisation/units as used in input.
  """  
  if code=='gkw':
    sign_theta=1
  elif code=='tglf':
    sign_theta=-1
    sd = sd/np.sqrt(1.0-d**2)
  else:
    print("Unknown code convention. Available: 'gkw' or 'tglf'")
    return

  Nr=3    # total number of FS: 1 + 2 neighbours  
  th_grid=np.linspace(0,2*np.pi,num=Nth,endpoint=False)
  dr=r0*np.array([-dr_frac, 0, dr_frac])
  r=r0+dr

  th_all=np.tile(th_grid,(Nr,1))
  r_all=np.transpose(np.tile(r,(Nth,1)))
  Rmil_all=np.transpose(np.tile(Rmil+dRmildr*dr,(Nth,1)))
  Zmil_all=np.transpose(np.tile(Zmil+dZmildr*dr,(Nth,1)))
  k_all=np.transpose(np.tile(k+sk*k/r*dr,(Nth,1)))
  d_all=np.transpose(np.tile(d+sd*np.sqrt(1-d**2)/r*dr,(Nth,1)))
  z_all=np.transpose(np.tile(z+sz/r*dr,(Nth,1)))

  R = Rmil_all + r_all*np.cos(th_all + np.arcsin(d_all)*np.sin(th_all))
  Z = Zmil_all + r_all*k_all*np.sin(th_all+z_all*np.sin(2*th_all))

  return r,R,Z


def rz2fourier(R,Z,r0,code='gkw',Nsh=10,Nth_fine=500,doplots=True):
  """ return c,s,dcdr,dsdr,R0,Z0,R_out,Z_out,err_out
      Compute the Fourier parametrisation of flux surfaces at a given radial location.
      The radial coordinate is defined as r=(max(R)-min(R))/2
      The reference point R0,Z0 is computed as:
          R0 = (max(R(r0)) + min(R(r0)))/2
          Z0 = (max(Z(r0)) + min(Z(r0)))/2
      with interpolation since the R,Z description of the flux surface can be coarse.
      The distance to the reference point is then:
          a = sqrt((R-R0)**2+(Z-Z0)**2)
      From which the Fourier coefficients are obtained
          a(r,theta) = sum_{n=0 to Nsh} [ c_n(r)*cos(n*theta) + s_n*sin(n*theta) ]
      with the definition of theta generally code dependent:
          tan(theta) = sign_theta * (Z-Z0)/(R-R0)

      This parametrisation is close to that introduced in Candy PPCF 51, 105009 (2009)

      Inputs:
        R,Z            Flux surface description (nrho,ntheta) without double points
        r0             Minor radius at which the parametrisation is interpolated                         
        code           Code convention: 'gkw' or 'imas'
                          gkw:  theta=0 at Z=Z0 on the LFS and increasing counter-clockwise, sign_theta=+1
                          imas: theta=0 at Z=Z0 on the LFS and increasing clockwise (GK IDS convention), sign_theta=-1
        Nsh            Number of terms kept in the Fourier expansion
        Nth_fine       Number of points of the internal theta grid used for the parametrisation
        doplots        Perform plots to check the parametrisation if True (default: True)

      Outputs:
        c,s            Fourier parametrisation of the flux surface 
        dcdr, dsdr     Radial derivative of the coefficients above 
        R0, Z0         Reference point used for the parametrisation
        R_out,Z_out    Flux surface description of the parametrised flux surface
        err_out        Average relative error on the parametrisation of a(r0,theta)

      Note that (R,Z) and r0  are assumed to all be given with the same normalisation/units, which 
      can be arbitrary. 
      The c, s, R0, Z0 values will be given with the same normalisation/units as used in input.
  """  
  if code=='gkw':
   sign_theta=1
  elif code=='imas':
   sign_theta=-1
  else:
   print("Unknown code convention. Available: 'gkw' or 'imas'")
   return

  (Nr,Nth)=R.shape
  err_thr = 0.003;  # threshold to warn for poor parametrisation accuracy

  # approximate minor radius and (R0,Z0) of all FS (no interpolation)
  r_coarse = (np.max(R,1)-np.min(R,1))/2
  R0_coarse = np.reshape((np.max(R,1)+np.min(R,1))/2,(Nr,1))
  Z0_coarse = np.reshape((np.max(Z,1)+np.min(Z,1))/2,(Nr,1))

  # check requested FS does not requires extrapolation
  if np.min(r0)<np.min(r_coarse) or np.max(r0)>np.max(r_coarse):
    print("No extrapolation allowed, check value(s) of r0")
    return

  # interpolate for a more accurate calculation of r and the local value of (R0, Z0)
  th_coarse = np.arctan2(sign_theta*(Z-np.tile(Z0_coarse,(1,Nth))),R-np.tile(R0_coarse,(1,Nth))) 
  th_fine = np.linspace(-np.pi,np.pi,num=Nth_fine,endpoint=False)
  r,R0_fine,Z0_fine=np.full((3,Nr),np.nan)
  for ii in range(Nr):
    th_sorted,Ith=np.unique(th_coarse[ii,:],return_index=True)
    thth=np.concatenate(([th_sorted[-1]-2*np.pi],th_sorted))
    RR=np.concatenate(([R[ii,Ith[-1]]],R[ii,Ith]))
    ZZ=np.concatenate(([Z[ii,Ith[-1]]],Z[ii,Ith]))
    Rspl=splrep(thth,RR,per=True)
    Zspl=splrep(thth,ZZ,per=True)
    R_fine=splev(th_fine,Rspl,ext=0)
    Z_fine=splev(th_fine,Zspl,ext=0)
    Rmax=np.max(R_fine)
    Rmin=np.min(R_fine)
    Zmax=np.max(Z_fine)
    Zmin=np.min(Z_fine)
    r[ii]=(Rmax-Rmin)/2
    R0_fine[ii]=(Rmax+Rmin)/2
    Z0_fine[ii]=(Zmax+Zmin)/2
  
  f = CubicSpline(r,R0_fine)
  R0=f(r0)
  f = CubicSpline(r,Z0_fine)
  Z0=f(r0)

  # distance of FS to the local reference point 
  a = np.sqrt((R-R0)**2+(Z-Z0)**2)

  # corresponding poloidal angle (from 0 to 2*pi)
  th = np.arctan2(sign_theta*(Z-Z0),R-R0) 
  th = np.mod(th+2*np.pi,2*np.pi)

  # interpolate on a regular theta grid to perform the Fourier transform
  th_grid = np.linspace(0,2*np.pi,num=Nth,endpoint=False)
  ath = np.full((Nr,Nth),np.nan)
  coeffs = np.full((Nr,Nth),np.nan*1j)
  for ii in range(Nr):
    th_sorted,Ith=np.unique(th[ii,:],return_index=True)
    thth=np.concatenate(([th_sorted[-1]-2*np.pi],th_sorted))
    aa=np.concatenate(([a[ii,Ith[-1]]],a[ii,Ith]))
    aspl=splrep(thth,aa,per=True)
    ath[ii,:]=splev(th_grid,aspl,ext=0)
    coeffs[ii,:]=np.fft.fft(ath[ii,:])
  c_all = coeffs[:,0:Nsh].real/Nth
  c_all[:,1:Nsh] = 2*c_all[:,1:Nsh]
  s_all = -coeffs[:,0:Nsh].imag/Nth
  s_all[:,1:Nsh] = 2*s_all[:,1:Nsh]

  # compute the parametrisation quality  
  THETA=np.tile(th_grid,(Nr,Nsh,1))
  N=np.moveaxis(np.tile(np.arange(Nsh),(Nr,Nth,1)),1,2)
  C=np.moveaxis(np.tile(c_all,(Nth,1,1)),0,2)
  S=np.moveaxis(np.tile(s_all,(Nth,1,1)),0,2)

  a_out = np.sum(C*np.cos(N*THETA)+S*np.sin(N*THETA),1)
  R_out = R0 + a_out*np.cos(THETA[:,0,:]) 
  Z_out = Z0 + sign_theta*a_out*np.sin(THETA[:,0,:]) 

  err=np.sum(abs((ath-a_out)/ath),1)/Nth
  f = CubicSpline(r,err)
  err_out=f(r0)
  if err_out>err_thr:
    print("Warning, parametrisation quality is poor at r=r0, consider increasing Nsh")

  # interpolate the shaping coefficients at the desired location r0
  c,s,dcdr,dsdr=np.full((4,Nsh),np.nan)
  k=np.min([3, Nr-1]) # to deal with cases with 3 radial grid points
  for ii in range(Nsh):
    c_spl=splrep(r,c_all[:,ii],k=k)
    s_spl=splrep(r,s_all[:,ii],k=k)
    c[ii]=splev(r0,c_spl)
    dcdr[ii]=splev(r0,c_spl,der=1)
    s[ii]=splev(r0,s_spl)
    dsdr[ii]=splev(r0,s_spl,der=1)

  # perform a few plots
  if doplots:
    fig,ax=plt.subplots()
    ax.plot([0, 1],[err_thr, err_thr],'r')
    ax.plot(r/np.max(r),err)
    plt.xlabel('$r/r_{max}$')
    plt.ylabel('relative error on the parametrisation')

    plt.figure()
    plt.plot(np.transpose(R),np.transpose(Z),'b')
    plt.plot(np.transpose(R_out[3:,:]),np.transpose(Z_out[3:,:]),'r--')

  return c,s,dcdr,dsdr,R0,Z0,R_out,Z_out,err_out


def rz2mxh(R,Z,code='imas',r0=None,Nsh=10,smooth=1e-8,Nth_fine=500,doplots=True):
  """ return r0, R0, dR0dr, Z0, dZ0dr, k, dkdr, c, dcdr, s,dsdr, R_out, Z_out, err_out
      Compute the Miller extended harmonic parametrisation of flux surfaces for the input flux surfaces
      The radial coordinate is defined as r=(max(R)-min(R))/2
      The reference point R0,Z0 is computed for each flux surface as:
          R0 = (max(R(r)) + min(R(r)))/2
          Z0 = (max(Z(r)) + min(Z(r)))/2
      with interpolation since the R,Z description of the flux surface can be coarse.
      The parametrisation of the flux surfaces is defined as:
         R(r,theta)=R0(r) + r*cos(theta_R(r,theta))
         Z(r,theta)=Z0(r) + sign_theta*r*k*sin(theta)
      with
         theta_R(r,theta) = theta + sum_{n=0 to Nsh-1} [ c_n(r)*cos(n*theta) + s_n*sin(n*theta) ]
      The direction of increasing theta is generally code dependent, but we always assume theta=0 at the LFS:
          tan(theta) = sign_theta * (Z-Z0)/(R-R0)

      This parametrisation is described in Arbon PPCF 63, 012001 (2021).

      Inputs:
        R,Z            Flux surface description (nrho,ntheta) without double points
 
        code           Code convention (for sign_theta): 'gkw' or 'imas'
                          gkw:  theta=0 at Z=Z0 on the LFS and increasing counter-clockwise, sign_theta=+1
                          imas: theta=0 at Z=Z0 on the LFS and increasing clockwise (GK IDS convention), sign_theta=-1
        r0             Minor radius at which the parametrisation coefficients in output are given 
                       If r0=None (default), give the coefficients for all flux surfaces in input
        Nsh            Number of terms kept in the Fourier expansion 
        smooth         If non-zero, applies smoothing when computing the radial derivatives of the shaping coefficients (default: 1e-8)
                       The higher the value, the stronger the smoothing
                       (check carefully the outputs if no smoothing is applied)
        Nth_fine       Number of points of the internal theta grid used for the parametrisation
        doplots        Perform plots to check the parametrisation if True (default: True)

      Outputs:
        R0, Z0         Reference point used for the parametrisation
        dR0dr, dZ0dr   Radial derivative of the coefficients above
        k              Elongation
        dkdr           Radial derivative of the elongation
        c,s            extended Miller harmonics (from n=0)
        dcdr, dsdr     Radial derivative of the coefficients above 
        R_out,Z_out    Flux surface description of the parametrised flux surface
        err_out        Average relative error on the parametrisation of a(r0,theta)

      Note that (R,Z) and r0  are assumed to all be given with the same normalisation/units, which 
      can be arbitrary. 
      The c, s, k, R0, Z0 values will be given with the same normalisation/units as used in input.
  """  
  if code=='gkw':
   sign_theta=1
  elif code=='imas':
   sign_theta=-1
  else:
   print("Unknown code convention. Available: 'gkw' or 'imas'")
   return

  (Nr,Nth)=R.shape
  err_thr = 0.003;  # threshold to warn for poor parametrisation accuracy
  smooth=np.abs(smooth)

  # regular theta grid used for Fourier transform and to check parametrisation quality
  th_grid = np.linspace(0,2*np.pi,num=Nth,endpoint=False)

  # approximate minor radius and (R0,Z0) of all FS (no interpolation)
  r_coarse = (np.max(R,1)-np.min(R,1))/2
  R0_coarse = np.reshape((np.max(R,1)+np.min(R,1))/2,(Nr,1))
  Z0_coarse = np.reshape((np.max(Z,1)+np.min(Z,1))/2,(Nr,1))

  # check requested FS does not requires extrapolation
  if not r0 is None: 
    if np.min(r0)<np.min(r_coarse) or np.max(r0)>np.max(r_coarse):
      print("No extrapolation allowed, check value(s) of r0")
      return

  # interpolate for a more accurate calculation of r and the local value of (R0, Z0)
  th_coarse = np.arctan2((Z-np.tile(Z0_coarse,(1,Nth))),R-np.tile(R0_coarse,(1,Nth))) 
  th_fine = np.linspace(-np.pi,np.pi,num=Nth_fine,endpoint=False)
  r,R0,Z0,k=np.full((4,Nr),np.nan)
  coeffs=np.full((Nr,Nth),np.nan*1j)
  a_check=np.full((Nr,Nth),np.nan)
  for ii in range(Nr):
    th_sorted,Ith=np.unique(th_coarse[ii,:],return_index=True)
    thth=np.concatenate(([th_sorted[-1]-2*np.pi],th_sorted))
    RR=np.concatenate(([R[ii,Ith[-1]]],R[ii,Ith]))
    ZZ=np.concatenate(([Z[ii,Ith[-1]]],Z[ii,Ith]))
    Rspl=splrep(thth,RR,per=True)
    Zspl=splrep(thth,ZZ,per=True)
    R_fine=splev(th_fine,Rspl,ext=0)
    Z_fine=splev(th_fine,Zspl,ext=0)
    Rmax=np.max(R_fine)
    th_Rmax=th_fine[Rmax==R_fine]
    Rmin=np.min(R_fine)
    th_Rmin=th_fine[Rmin==R_fine]
    Zmax=np.max(Z_fine)
    th_Zmax=th_fine[Zmax==Z_fine]
    Zmin=np.min(Z_fine)
    th_Zmin=th_fine[Zmin==Z_fine]
    r[ii]=(Rmax-Rmin)/2
    R0[ii]=(Rmax+Rmin)/2
    Z0[ii]=(Zmax+Zmin)/2
    k[ii]=(Zmax-Zmin)/2/r[ii]
    # distance of FS to the local reference point (used to check parametrisation quality)
    a_fine=np.sqrt((R_fine-R0[ii])**2+(Z_fine-Z0[ii])**2) 
    # compute the angles theta_R with cos(theta_R) = (R-R0)/r
    th_R=np.arccos(np.round((2*R_fine-Rmax-Rmin)/(Rmax-Rmin),14))  
    if th_Rmax>th_Rmin:
      II = (th_fine>=th_Rmin) & (th_fine<th_Rmax)
    else:
      II = (th_fine>=th_Rmin) | (th_fine<th_Rmax)
    th_R[II]=2*np.pi-th_R[II]
    # compute the angle theta with sin(theta) = (Z-Z0)/(k*r)
    th=np.arcsin(np.round((2*Z_fine-Zmax-Zmin)/(Zmax-Zmin),14))
    II=(th_fine>th_Zmax) | (th_fine<th_Zmin)
    th[II]=np.pi-th[II]
    th=np.mod(th,2*np.pi)
    # angle difference on which to perform the fourier transform
    delta_theta=-np.mod(th-th_R+np.pi,2*np.pi)+np.pi
    # interpolate on a regular theta grid to perform the Fourier transform
    th_sorted,Ith=np.unique(th,return_index=True)
    thth=np.concatenate(([th_sorted[-1]-2*np.pi],th_sorted))
    dthdth=np.concatenate(([delta_theta[Ith[-1]]],delta_theta[Ith]))
    dthspl=splrep(thth,dthdth,per=True)
    delta_theta_fourier=splev(th_grid,dthspl,ext=0)
    aa=np.concatenate(([a_fine[Ith[-1]]],a_fine[Ith]))
    aspl=splrep(thth,aa,per=True)
    a_check[ii,:]=splev(th_grid,aspl,ext=0)
    # perform the Fourier transform of theta_R-theta
    coeffs[ii,:]=np.fft.fft(delta_theta_fourier)/Nth

  # fill in the Fourier coefficients
  c_all = sign_theta*coeffs[:,0:Nsh].real
  c_all[:,1:Nsh] = 2*c_all[:,1:Nsh]
  s_all = -coeffs[:,0:Nsh].imag
  s_all[:,1:Nsh] = 2*s_all[:,1:Nsh]

  # check the parametrisation quality
  THETA=np.tile(th_grid,(Nr,Nsh,1))
  N=np.moveaxis(np.tile(np.arange(Nsh),(Nr,Nth,1)),1,2)
  C=np.moveaxis(np.tile(c_all,(Nth,1,1)),0,2)
  S=np.moveaxis(np.tile(s_all,(Nth,1,1)),0,2)

  theta_R_out = np.tile(th_grid,(Nr,1)) + np.sum(sign_theta*C*np.cos(N*THETA)+S*np.sin(N*THETA),1)
  RR_out =  np.moveaxis(np.tile(R0,(Nth,1)),0,1) +  np.moveaxis(np.tile(r,(Nth,1)),0,1)*np.cos(theta_R_out)
  ZZ_out =  np.moveaxis(np.tile(Z0,(Nth,1)),0,1) +  np.moveaxis(np.tile(k*r,(Nth,1)),0,1)*np.sin(np.tile(th_grid,(Nr,1)))
  a_out = np.sqrt((np.moveaxis(np.tile(r,(Nth,1)),0,1)*np.cos(theta_R_out))**2 + 
                  (np.moveaxis(np.tile(k*r,(Nth,1)),0,1)*np.sin(np.tile(th_grid,(Nr,1))))**2)

  err=np.sum(abs((a_check-a_out)/a_check),1)/Nth

  # interpolate the shaping coefficients and compute the radial derivatives
  if r0 is not None:
    r_out = r0
    Nr_out = np.array(r0).size
  else:
    r_out = r
    Nr_out = Nr
  kk=np.min([3, Nr-1]) # to deal with cases with 3 radial grid points
  if smooth and kk==3:
    ss=(Nr-np.sqrt(2*Nr))*smooth
  else:
    ss=None
  # first, without smoothing for reference
  R0_spl_ns=splrep(r,R0,k=kk)
  Z0_spl_ns=splrep(r,Z0,k=kk)
  k_spl_ns=splrep(r,k,k=kk)
  R0_ns=splev(r_out,R0_spl_ns)
  dR0dr_ns=splev(r_out,R0_spl_ns,der=1)
  Z0_ns=splev(r_out,Z0_spl_ns)
  dZ0dr_ns=splev(r_out,Z0_spl_ns,der=1)
  k_ns=splev(r_out,k_spl_ns)
  dkdr_ns=splev(r_out,k_spl_ns,der=1)
  c_ns,s_ns,dcdr_ns,dsdr_ns=np.full((4,Nr_out,Nsh),np.nan)
  for ii in range(0,Nsh):
    c_spl_ns=splrep(r,c_all[:,ii],k=kk)
    s_spl_ns=splrep(r,s_all[:,ii],k=kk)
    c_ns[:,ii]=splev(r_out,c_spl_ns)
    dcdr_ns[:,ii]=splev(r_out,c_spl_ns,der=1)
    s_ns[:,ii]=splev(r_out,s_spl_ns)
    dsdr_ns[:,ii]=splev(r_out,s_spl_ns,der=1)
  # then, with smoothing
  R0_spl=splrep(r,R0,k=kk,s=ss)
  Z0_spl=splrep(r,Z0,k=kk,s=ss)
  err_spl=splrep(r,err,k=kk)
  k_spl=splrep(r,k,k=kk,s=ss)
  R0=splev(r_out,R0_spl)
  dR0dr=splev(r_out,R0_spl,der=1)
  Z0=splev(r_out,Z0_spl)
  dZ0dr=splev(r_out,Z0_spl,der=1)
  err_out=splev(r_out,err_spl)
  k=splev(r_out,k_spl)
  dkdr=splev(r_out,k_spl,der=1)
  c,s,dcdr,dsdr=np.full((4,Nr_out,Nsh),np.nan)
  if ss:
    ss_fine = ss*10
  else:
    ss_fine = None
  for ii in range(0,Nsh):
    c_spl=splrep(r,c_all[:,ii],k=kk,s=ss_fine)
    s_spl=splrep(r,s_all[:,ii],k=kk,s=ss_fine)
    c[:,ii]=splev(r_out,c_spl)
    dcdr[:,ii]=splev(r_out,c_spl,der=1)
    s[:,ii]=splev(r_out,s_spl)
    dsdr[:,ii]=splev(r_out,s_spl,der=1)


  # check the final parametrisation quality (after interpolation)
  THETA=np.tile(th_grid,(Nr_out,Nsh,1))
  N=np.moveaxis(np.tile(np.arange(Nsh),(Nr_out,Nth,1)),1,2)
  C=np.moveaxis(np.tile(c,(Nth,1,1)),0,2)
  S=np.moveaxis(np.tile(s,(Nth,1,1)),0,2)

  theta_R_out = np.tile(th_grid,(Nr_out,1)) + np.sum(sign_theta*C*np.cos(N*THETA)+S*np.sin(N*THETA),1)
  R_out =  np.moveaxis(np.tile(R0,(Nth,1)),0,1) +  np.moveaxis(np.tile(r_out,(Nth,1)),0,1)*np.cos(theta_R_out)
  Z_out =  np.moveaxis(np.tile(Z0,(Nth,1)),0,1) +  np.moveaxis(np.tile(k*r_out,(Nth,1)),0,1)*np.sin(np.tile(th_grid,(Nr_out,1)))


  # perform a few plots
  if doplots:
    fig,ax=plt.subplots()
    ax.plot([0, 1],[err_thr, err_thr],'r')
    ax.plot(r_out,err_out)
    plt.xlabel('$r$')
    plt.ylabel('relative error on the parametrisation')

    fig,ax=plt.subplots()
    ax.plot(r_out,k_ns,'b--')
    ax.plot(r_out,dkdr_ns,'b--')
    ax.plot(r_out,k,'r-')
    ax.plot(r_out,dkdr,'r-')
    plt.legend(['k','dkdr','k smooth','dkdr smooth'])


    fig,ax=plt.subplots()
    ax.plot(r_out,dR0dr_ns,'b--')
    ax.plot(r_out,dZ0dr_ns,'b--')
    ax.plot(r_out,dR0dr,'r-')
    ax.plot(r_out,dZ0dr,'r-')
    plt.xlabel('$r$')
    plt.legend(['dR0dr','dZ0dr','dR0dr smooth','dZ0dr smooth'])

    fig,ax=plt.subplots()
    ax.plot(r_out,c_ns,'--')
    ax.plot(r_out,s_ns,'--')
    ax.plot(r_out,c,'-')
    ax.plot(r_out,s,'-')
    plt.xlabel('$r$')
    plt.ylabel('c and s')    

    fig,ax=plt.subplots()
    ax.plot(r_out,dcdr_ns,'--')
    ax.plot(r_out,dsdr_ns,'--')
    ax.plot(r_out,dcdr,'-')
    ax.plot(r_out,dsdr,'-')
    plt.xlabel('$r$')
    plt.ylabel('dcdr and dsdr')    

    fig,ax=plt.subplots()
    h1=ax.plot(np.transpose(R),np.transpose(Z),'b')
    h2=ax.plot(np.transpose(RR_out),np.transpose(ZZ_out),'r--')
    h3=ax.plot(np.transpose(R_out),np.transpose(Z_out),'g--')
    plt.legend([h1[0],h2[0],h3[0]],['input','fit','fit + radial interp'])
    plt.axis('equal')
    plt.xlabel('R')
    plt.ylabel('Z')
    plt.ion()
    plt.show()

  return r_out, R0, dR0dr, Z0, dZ0dr, k, dkdr, np.squeeze(c), np.squeeze(dcdr), np.squeeze(s),np.squeeze(dsdr), R_out, Z_out, err_out


def rz2miller(R,Z,code='gkw',r0=None,Nth_fine=500,doplots=True):
  """ return k,d,z,sk,sd,sz,Rmil,Zmil,r,dRmildr,dZmildr
      Computes the Miller parameters from  R,Z flux surfaces description.
      The radial coordinate is defined as r=(max(R)-min(R))/2
      The reference point R0,Z0 is computed as:
          R0 = (max(R(r0)) + min(R(r0)))/2
          Z0 = (max(Z(r0)) + min(Z(r0)))/2
      with interpolation since the R,Z description of the flux surface can be coarse.
      The flux surfaces are then defined as:
          R(r,theta) = R0(r) + r cos(theta + arcsin(d) sin(theta))
          Z(r,theta) = Z0(r) + k r sin(theta + z sin(2*theta))          
        
      with the definition of theta generally code dependent: 
          tan(theta) = sign_theta * (Z-Z0)/(R-R0)

      This parametrisation is similar to that introduced in R.L. Miller, Phys. Plasmas, 5, 973 (1998)

      Inputs:
        R,Z            Flux surface description (nrho,ntheta) without double points
        code           Code convention (for sign_theta): 'gkw' or 'tglf'
                          gkw:  theta=0 at Z=Z0 on the LFS and increasing counter-clockwise, sign_theta=+1
                          tglf: theta=0 at Z=Z0 on the LFS and increasing clockwise, sign_theta=-1
        r0             Minor radius at which the parametrisation coefficients in output are given 
                       If r0=None (default), give the coefficients for all flux surfaces in input
        Nth_fine       Number of points of the internal theta grid used for the parametrisation
        doplots        Perform plots to check the parametrisation if True (default: True)

      Outputs:
        r              minor radius
        Rmil, Zmil     flux surface center
        k,d,z          elongation, triangularity, squareness
        dRmildr        radial derivative of Rmil
        dZmildr        radial derivative of Zmil
        sk,sd,sz       radial derivatives of k, d, z (exact definition code dependent)
        Rout, Zout:  parametrised flux surfaces in [m]
        chi2: parametrisation quality (chi2>0.05 starts to be quite bad, switch to full equilibrium advised!)
         
      The k, d, z, Rmil, Zmil values will be given with the same normalisation/units as used in input.
      
      Triangularity and squareness obtained by minimising the distance between the input flux surfaces and the
      parametrised solutions.
      
      Matlab version: YC 22.11.2012 
      Translation to Python 3.9: AN 28.09.2022
  """      
  if code=='gkw':
   sign_theta=1
  elif code=='imas':
   sign_theta=-1
  elif code=='tglf':
   sign_theta=-1
  else:
   print("Unknown code convention. Available: 'gkw', 'imas' or 'tglf'")
   return


  (Nr,Nth)=R.shape
  chi2_thr = 0.05;  # threshold to warn for poor parametrisation accuracy
  Nt = max(Nth*5,2000)
  # approximate minor radius and (R0,Z0) of all FS (no interpolation)
  r_coarse = (np.max(R,1)-np.min(R,1))/2
  R0_coarse = np.reshape((np.max(R,1)+np.min(R,1))/2,(Nr,1))
  Z0_coarse = np.reshape((np.max(Z,1)+np.min(Z,1))/2,(Nr,1))

  # interpolate for a more accurate calculation of r and the local value of Rmil and Zmil
  th_coarse = np.arctan2(sign_theta*(Z-np.tile(Z0_coarse,(1,Nth))),R-np.tile(R0_coarse,(1,Nth))) 
  th_fine = np.linspace(-np.pi,np.pi,num=Nth_fine,endpoint=False)
  r,R0_fine,Z0_fine=np.full((3,Nr),np.nan)
  for ii in range(Nr):
    th_sorted,Ith=np.unique(th_coarse[ii,:],return_index=True)
    thth=np.concatenate(([th_sorted[-1]-2*np.pi],th_sorted))
    RR=np.concatenate(([R[ii,Ith[-1]]],R[ii,Ith]))
    ZZ=np.concatenate(([Z[ii,Ith[-1]]],Z[ii,Ith]))
    Rspl=splrep(thth,RR,per=True)
    Zspl=splrep(thth,ZZ,per=True)
    R_fine=splev(th_fine,Rspl,ext=0)
    Z_fine=splev(th_fine,Zspl,ext=0)
    Rmax=np.max(R_fine)
    Rmin=np.min(R_fine)
    Zmax=np.max(Z_fine)
    Zmin=np.min(Z_fine)
    r[ii]=(Rmax-Rmin)/2
    R0_fine[ii]=(Rmax+Rmin)/2
    Z0_fine[ii]=(Zmax+Zmin)/2
  
  f = CubicSpline(r,R0_fine)
  Rmil=f(r)
  f = CubicSpline(r,Z0_fine)
  Zmil=f(r)
  
  ## Computes a poloidal angle (th) zero at LFS midplane, positive going upward Defined as:
  #   R-Rmil = a cos(th)
  #   Z-Zmil = a sin(th)
  Rr = R - np.transpose(np.tile(Rmil,(Nth,1)))
  Zz = Z - np.transpose(np.tile(Zmil,(Nth,1)))
  
  th = np.full(Rr.shape,np.nan)

  th[Rr!=0]=np.arctan(Zz[Rr!=0]/Rr[Rr!=0])
  th[np.logical_and(Rr==0,Zz>0)]=np.pi/2
  th[np.logical_and(Rr==0,Zz<0)]=-np.pi/2
  
  # pi shifts to have th in [0, 2*pi]
  th[Rr<0]=th[Rr<0]+np.pi
  th[np.logical_and(Rr>=0,Zz<0)]=th[np.logical_and(Rr>=0,Zz<0)] + 2*np.pi

  # Sort from 0 to 2*pi
  Isort = np.argsort(th,axis=1)
  for ii in range(Nr):
    th[ii,:]=th[ii,Isort[ii,:]]  
    R[ii,:]=R[ii,Isort[ii,:]]  
    Z[ii,:]=Z[ii,Isort[ii,:]]  
    Rr[ii,:]=Rr[ii,Isort[ii,:]]  
    Zz[ii,:]=Zz[ii,Isort[ii,:]]  

  # detect phase jumps 
  if np.any(np.abs(np.diff(th, axis=1)) > 3 ) :
      print('Large jumps in th, something went wrong')
      print('Check your R,Z inputs, program stopped')
      return 
  
  # local minor radius
  a = np.sqrt(Rr**2 + Zz**2)
  
  ## Now get the shape parameters    
  # elongation 
  k = np.zeros(r.shape)
  for ii in range(Nr):   
   k[ii] = (np.max(Z[ii]) - np.min(Z[ii])) / (2 * r[ii])
  # get delta and zeta from a minimisation
  ths = np.linspace(0,2*np.pi,Nt,endpoint=False)
  d,z,chi2 = np.zeros(k.shape), np.zeros(k.shape), np.zeros(k.shape)
  Rout,Zout = np.zeros((Nr,Nt)), np.zeros((Nr,Nt))
  x_init=[0.,0.]

  for ii in range(Nr):
    a_spl = splrep(th[ii],a[ii])
    a_fine = splev(ths,a_spl)
    
    xout = fmin(func=chi_rz2miller, x0 = x_init,args=(r[ii],k[ii],ths,a_fine),xtol=1e-5, ftol=1e-5,disp=False)
    chi2[ii] = chi_rz2miller(xout,r[ii],k[ii],ths,a_fine)
    d[ii] = np.sin(xout[0])
    z[ii] = xout[1]
    Rout[ii] = r[ii]*np.cos(ths + xout[0]*np.sin(ths))
    Zout[ii] = r[ii]*k[ii]*np.sin(ths + xout[1]*np.sin(2*ths))
    x_init = xout
    
  chi2 = np.sqrt(chi2)*100.
  if np.any(chi2 > chi2_thr):
      print('Warning, for chi2>'+str(chi2_thr)+' parametrisation quality is poor (see plots), better to use the full geometry')
      print('chi2 = ',chi2)
      doplots = True

  #Shift datas so the centre of flux surface are Rmil and Zmil
  Rout = Rout + np.transpose(np.tile(Rmil,(Nt,1)))
  Zout = Zout + np.transpose(np.tile(Zmil,(Nt,1)))
      
  # interpolate the shaping coefficients and compute the radial derivatives
  if r0 is not None:
    r_out = r0
    Nr_out = np.array(r0).size
  else:
    r_out = r
    Nr_out = Nr

  Rmil_spl = splrep(r,Rmil, k=2)
  Zmil_spl = splrep(r,Zmil, k=2)
  Rmil_out = splev(r_out,Rmil_spl)
  dRmildr_out = splev(r_out,Rmil_spl,der=1)
  Zmil_out = splev(r_out,Zmil_spl)
  dZmildr_out = splev(r_out,Zmil_spl,der=1)

  k_spl = splrep(r,k, k=2)
  k_out=splev(r_out,k_spl)
  sk_out = (r_out/k_out)*splev(r_out,k_spl,der=1)
  sk=r/k*splev(r,k_spl,der=1)
  
  z_spl = splrep(r,z, k=2)
  z_out=splev(r_out,z_spl)
  sz_out = r_out*splev(r_out,z_spl,der=1)
  sz=r*splev(r,z_spl,der=1)
  
  d_spl = splrep(r,d, k=2)
  d_out=splev(r_out,d_spl)
  if code in ['gkw','imas']:
      sd_out = (r_out/np.sqrt(1-d_out**2))*splev(r_out,d_spl,der=1)
      sd=(r/np.sqrt(1-d**2))*splev(r,d_spl,der=1)
  elif code in ['tglf']:
      sd_out = r_out*splev(r_out,d_spl,der=1)
      sd = r*splev(r,d_spl,der=1)

  ## plots
  if doplots == True:
    fig,ax=plt.subplots()
    ax.plot([0, 1],[chi2_thr, chi2_thr],'r')
    ax.plot(r/np.amax(r),chi2)
    plt.xlabel('$r/r_{max}$')
    plt.ylabel('chi2')
    fig,ax=plt.subplots()
    ax.plot(r/np.amax(r),d,label='delta')
    ax.plot(r/np.amax(r),z,label='zeta')
    plt.xlabel(r'$r/r_{max}$')
    plt.ylabel(r'$\delta$  and  $\zeta$')
    plt.legend()
    fig,ax=plt.subplots()
    ax.plot(r/np.amax(r),sk,label=r'$s_{\kappa}$')
    ax.plot(r/np.amax(r),sd,label=r'$s_{\delta}$')
    ax.plot(r/np.amax(r),sz,label=r'$s_{\zeta}$')
    plt.xlabel(r'$r/r_{max}$')
    plt.ylabel(r'$s_\kappa$ , $s_\delta$ and $s_{\zeta}$')
    plt.legend()
    fig,ax=plt.subplots()
    ax.plot(np.transpose(R),np.transpose(Z),'b-')
    ax.plot(np.transpose(Rout),np.transpose(Zout),'r--')
    plt.xlabel('R')
    plt.ylabel('Z')
    plt.axis('equal')
    plt.show()
      
  return k_out,d_out,z_out,sk_out,sd_out,sz_out,Rmil_out,Zmil_out,r_out,dRmildr_out,dZmildr_out
   
def chi_rz2miller(x0,r0,k0,ths,a_data): 
    if (np.abs(np.sin(x0[0])) > 1 or  np.abs(x0[1]) > 0.7):
        chi2 = np.nan
    else:
        Nt0 = len(ths)
        t0 = np.linspace(0,2*np.pi,Nt0,endpoint=False)
        Rr0 = r0*np.cos(t0 + x0[0]*np.sin(t0))
        Zz0 = r0*k0*np.sin(t0 + x0[1]*np.sin(2*t0))
        
        th0 = np.full(Rr0.shape,np.nan)
        th0[Rr0!=0]=np.arctan(Zz0[Rr0!=0]/Rr0[Rr0!=0])
        th0[np.logical_and(Rr0==0,Zz0>0)]=np.pi/2
        th0[np.logical_and(Rr0==0,Zz0<0)]=-np.pi/2
        # pi shifts to have th in [0, 2*pi]
        th0[Rr0<0]=th0[Rr0<0]+np.pi
        th0[np.logical_and(Rr0>=0,Zz0<0)]=th0[np.logical_and(Rr0>=0,Zz0<0)] + 2*np.pi
        # Sort from 0 to 2*pi
        Isort = np.argsort(th0)
        Rr0=Rr0[Isort]
        Zz0=Zz0[Isort]
        
        a0 = np.sqrt(Rr0**2 + Zz0**2)
        a0_spl = splrep(th0,a0,per=True)
        a0_fine = splev(ths,a0_spl)
        

        chi2 = np.sum(( (a0_fine - a_data)/a_data )**2) / Nt0**2

    return chi2
