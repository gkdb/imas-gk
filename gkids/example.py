# how to manage a GK IDS with the idspy_toolkit

from idspy_dictionaries import ids_gyrokinetics as gkids # can also import other IDS classes (ids_core_profiles, ids_equilibrium,...)
import idspy_toolkit as idspy

# generate an gyrokinetics IDS
# this initialises the root structure, subbranches are left empty
new_ids=gkids.Gyrokinetics()

# to generate and fill the whole structure with IMAS default values
idspy.fill_default_values_ids(new_ids)

# to update values
new_ids.model.include_centrifugal_effects=True
new_ids.flux_surface.elongation=1.6

# Note that wavevector, eigenmode, species are empty lists
# To fill them do:

# first species
dum=gkids.Species()
dum.charge_norm=1
new_ids.species.append(dum)

# second species
dum=gkids.Species()
dum.charge_norm=-1
new_ids.species.append(dum)

# Same spirit for wavevector and eigenmode using gkids.Wavevector() and gkids.eigenmode()
dum=gkids.Wavevector()
new_ids.wavevector.append(dum)

# Ah yes, in eigenmode, you will need to use gkids.CodePartialConstant() to fill the code branch
ii_wav=0
dum=gkids.Eigenmode()
dum.poloidal_turns=4
dum.code=gkids.CodePartialConstant()
dum.initial_value_run=True
new_ids.wavevector[ii_wav].eigenmode.append(dum)


# then you can save the IDS on disk in hdf5. Note that this file can not be read with the IMAS access layer.
g,k=idspy.ids_to_hdf5(new_ids,'/home/yann/tmp/test.hdf5') # Error if file already exists (I would like to change this, not convenient)

# and then to read the IDS file
ids2=gkids.Gyrokinetics()
idspy.fill_default_values_ids(ids2)
idspy.hdf5_to_ids('/home/yann/tmp/test.hdf5',ids2)
