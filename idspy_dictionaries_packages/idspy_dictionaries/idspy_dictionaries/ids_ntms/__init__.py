# __version__ = "040001.4.1"
# __version_imas__ = "4.0.0-65-g2c31c72"

from idspy_dictionaries.ids_ntms.ntms import (
BTorVacuum1,
IdsProvenanceNodeReference,
IdsProvenanceNode,
Library,
IdsProvenance,
IdsProperties,
Code,
NtmTimeSliceModeDetailedEvolutionDeltaw,
NtmTimeSliceModeDetailedEvolutionTorque,
NtmTimeSliceModeDetailedEvolution,
NtmTimeSliceModeEvolutionDeltaw,
NtmTimeSliceModeEvolutionTorque,
NtmTimeSliceModeOnset,
NtmTimeSliceMode,
NtmTimeSlice,
Ntms,

)

__all__ = [
"BTorVacuum1",
"IdsProvenanceNodeReference",
"IdsProvenanceNode",
"Library",
"IdsProvenance",
"IdsProperties",
"Code",
"NtmTimeSliceModeDetailedEvolutionDeltaw",
"NtmTimeSliceModeDetailedEvolutionTorque",
"NtmTimeSliceModeDetailedEvolution",
"NtmTimeSliceModeEvolutionDeltaw",
"NtmTimeSliceModeEvolutionTorque",
"NtmTimeSliceModeOnset",
"NtmTimeSliceMode",
"NtmTimeSlice",
"Ntms",
]