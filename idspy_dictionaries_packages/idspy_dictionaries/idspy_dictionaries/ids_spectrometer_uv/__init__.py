# __version__ = "040001.4.1"
# __version_imas__ = "4.0.0-65-g2c31c72"

from idspy_dictionaries.ids_spectrometer_uv.spectrometer_uv import (
Rphiz1DDynamicAos1CommonTime,
LineOfSight2PointsDynamicAos1,
Xyz0DStatic,
IdentifierStatic,
X1X21DStatic,
SignalFlt1D,
DetectorAperture,
Rphiz0DStatic,
SignalFlt2D,
SignalInt2D,
IdsProvenanceNodeReference,
IdsProvenanceNode,
Library,
IdsProvenance,
IdsProperties,
Code,
SpectroUvSupply,
SpectroUvDetector,
SpectroUvChannelWavelengthCalibration,
SpectroUvChannelProcessedLine,
SpectroUvChannelGratingImage,
SpectroUvChannelGrating,
SpectroUvChannel,
SpectrometerUv,

)

__all__ = [
"Rphiz1DDynamicAos1CommonTime",
"LineOfSight2PointsDynamicAos1",
"Xyz0DStatic",
"IdentifierStatic",
"X1X21DStatic",
"SignalFlt1D",
"DetectorAperture",
"Rphiz0DStatic",
"SignalFlt2D",
"SignalInt2D",
"IdsProvenanceNodeReference",
"IdsProvenanceNode",
"Library",
"IdsProvenance",
"IdsProperties",
"Code",
"SpectroUvSupply",
"SpectroUvDetector",
"SpectroUvChannelWavelengthCalibration",
"SpectroUvChannelProcessedLine",
"SpectroUvChannelGratingImage",
"SpectroUvChannelGrating",
"SpectroUvChannel",
"SpectrometerUv",
]