# __version__ = "040001.4.1"
# __version_imas__ = "4.0.0-65-g2c31c72"

from idspy_dictionaries.ids_camera_ir.camera_ir import (
Xyz0DStatic,
Rphiz0DStatic,
DetectorAperture,
X1X21DStatic,
CurvedSurface,
OpticalElementMaterial,
OpticalElement,
FibreBundle,
IdentifierStatic,
IdsProvenanceNodeReference,
IdsProvenanceNode,
Library,
IdsProvenance,
IdsProperties,
Code,
CameraIrCalibration,
CameraIrFrameAnalysis,
CameraIrFrame,
CameraIr,

)

__all__ = [
"Xyz0DStatic",
"Rphiz0DStatic",
"DetectorAperture",
"X1X21DStatic",
"CurvedSurface",
"OpticalElementMaterial",
"OpticalElement",
"FibreBundle",
"IdentifierStatic",
"IdsProvenanceNodeReference",
"IdsProvenanceNode",
"Library",
"IdsProvenance",
"IdsProperties",
"Code",
"CameraIrCalibration",
"CameraIrFrameAnalysis",
"CameraIrFrame",
"CameraIr",
]