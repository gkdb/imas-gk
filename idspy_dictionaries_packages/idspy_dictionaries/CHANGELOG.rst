IDSPy_Dictionaries Changelog
============================

* v 040001.4.1
   - Update IMAS Data Dictionary v4.00 develop commit 2c31c72 due to an issue in the main branch of the gyrokinetic DD 

* v 040000.4.0
   - Update IMAS Data Dictionary v4.00 main commit 6ed980cb3e
   - Expected to be final version of the Data Dictionaries
   - version <40000.4.0 will be considered as deprecated in next python package release

* v 34200.4.0
   - Update IMAS Data Dictionary v3.42 main/3 commit a3964a20fde
   - Add the possibility to compare version class member in between ids and with a string
   - IMAS Data dictionary expected final version will be 4.0 (can still change with version 3.X)
   - Type checking when using apend on structarray
   - Duplicate and deprecated files removed

* v 34100.3.0
   - Update IMAS Data Dictionary v3.41 develop/3 commit 0c200ea992f
   - Modified Base Dataclass and add function to get all dataclass member
   - Added Metadata to Dataclasses to have IMAS corresponding details : data type, coordinates, dims
   - Formatted Changelog
   - Previous Data Dictionnaries are not supported anymore and will lead to exception raised while using the toolkit
   - IMAS Data dictionary will be frozen soon

* v 340.2.1
   - Update IMAS Data Dictionary v3.40 commit 504cc4575f6
   - changed way IDS printed as strings

* v 340.2.0
   - IMAS Data Dictionary v 3.40
   - ids_gyrokinetics removed and replaced by ids_gyrokinetics_local
   - added package and data dictionary versions in ids attributes
   - same package for python 3.9 and 3.10+


* v 339.1.3
   - IMAS Data Dictionary v 3.39
   - corrected a bug in the expected type linked to numpy arrays
   - updated package version

 * v 3381.1.2
    - missing provenance data member
    - datatype for the time data member

 * v 33801.1.1
    - typo in changelog
    - correction of readme and minimum python requirement.

 * v 3_38_1.0.1
    - new versioning system implemented. Based on v 0.4.0
    - IMAS Version 3.38.1

 * v 0.4.0
    - IMAS Data Dictionary v 3.38.1
    - Add IDS_Provenance member for IDS_Properties
    - add global variables __version__ and __version_data_dictionary__
    - default values for numpy.arrays is now empty list []
