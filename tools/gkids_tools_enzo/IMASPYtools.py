"""
06/11/2024
Author: Enzo Vergnaud
enzo.vergnaud@cea.fr
Very depending on your Access Layer and IMAS version, be careful.
For the raw save/opening with ids_save_hdf5 and ids_open_hdf5, the code is not functionnal on WEST...
"""
import numpy as np
import imaspy
import copy
import os
from imas import imasdef

interp_list=[imasdef.LINEAR_INTERP, imasdef.CLOSEST_INTERP, imasdef.PREVIOUS_INTERP]

def msg_log(msg,log,verbose,critical=False): 
    """ Generic routine to store and optionally display warning/error messages 
    Inputs    
        msg         message to be stored/displayed
        log         list to store the messages
        verbose     if True, displays the message on screen
        critical    if True, exits the code
        
    """
    if critical:
        log.append("Error: "+msg)
        raise Exception(msg)
    else:
        log.append("Warning: "+msg)
        if verbose is True:
            print(msg)

def check_path_exist(path):
    """ return a bool if the path exists
    INPUTS:
        -   path            str of a path
    OUTPUTS:
        -   bool
    """
    return os.path.exists(path)

def check_file_exist(file_path):
    """ return a bool if the file exists
    INPUTS:
        -   path            str of a path/file
    OUTPUTS:
        -   bool
    """
    return os.path.isfile(file_path)

# IMASPY IN ACESS LAYER 5
# HDF5 (MDS+ doesn't work currently)
def ids_save_hdf5(ids, occurrence=0, save_path = "./"):
    """THIS FUNCTION WORKS ONLY WITH AN ACCESS LAYER VERSION > 5.0 AND IMASPY OBJECT
    INPUTS:
        -   ids                 an ids
        -   occurrence          the occurrence of the save
    OUTPUTS:
        -   no outputs, only master.h5 + the ids.h5 in the repertory specified (save_path)

    """
    log = []

    if not check_path_exist(save_path):
        msg="The save path in input doesn't exist"
        msg_log(msg, log, verbose=True, critical=True)
    
    if check_file_exist(os.path.join(save_path, "master.h5")):
        hdf5_dbentry = imaspy.DBEntry("imas:hdf5?path="+save_path, "a") 
    else:
        hdf5_dbentry = imaspy.DBEntry("imas:hdf5?path="+save_path, "x") 
    
    if ids.ids_properties.homogeneous_time.has_value==False:
        ids.ids_properties.homogeneous_time=0 
    
    if len(ids.time)==1:
        hdf5_dbentry.put_slice(ids, occurrence=occurrence)
    else:
        hdf5_dbentry.put(ids, occurrence=occurrence)

def ids_save_shot_hdf5(ids,shot, machine, run, user, occurrence=0):
    """THIS FUNCTION WORKS ONLY WITH AN ACCESS LAYER VERSION > 5.0
    INPUTS:
        -   ids                 an ids
        -   occurrence          the occurrence of the save
    OUTPUTS:
        -   no outputs, only master.h5 + the ids.h5 in the repertory specified (save_path)

    """
    os.environ["IMAS_AL_DISABLE_VALIDATE"] = "1" # to avoid this ERROR :   IDS core_profiles is not valid. You can disable automatic IDS validation by setting the environment variable IMAS_AL_DISABLE_VALIDATE=1
    log = []
    hdf5_dbentry=imaspy.DBEntry(imasdef.HDF5_BACKEND, machine, shot, run, user)

    try:
        hdf5_dbentry.open()
    except:
        print(f"Creation of IDS shot:{shot} |machine:{machine}|run:{run}|occurrence:{occurrence}|user:{user}")
        hdf5_dbentry.create()
    
    if len(ids.time)==1:
        hdf5_dbentry.put_slice(ids, occurrence=occurrence)
    else:
        hdf5_dbentry.put(ids, occurrence=occurrence)
     
def ids_open_hdf5(ids_name, occurrence=0, time=None, save_path = "./", interp=imasdef.CLOSEST_INTERP):
    """THIS FUNCTION WORKS ONLY WITH AN ACCESS LAYER VERSION > 5.0
    INPUTS:
        -   type_ids            str containing the ids type the user wants
        -   occurrence          the occurrence of the save
    OUTPUTS:
        -   ids
    """
    log=[]
    if not check_path_exist(save_path):
        msg="The save path in input doesn't exist"
        msg_log(msg, log, verbose=True, critical=True)

    if not interp in interp_list:
        msg="Type of interpolation doesn't exist"
        msg_log(msg, log, verbose=True, critical=True)

    if check_file_exist(os.path.join(save_path, "master.h5")):
        hdf5_dbentry = imaspy.DBEntry("imas:hdf5?path="+save_path, "r") 
    else:
        msg="There is no IDS in the path put in inputs"
        msg_log(msg, log, verbose=True, critical=True)
    
    if isinstance(time, int):
        return hdf5_dbentry.get_slice(ids_name, time, interp, occurrence=occurrence)
    else:
        return hdf5_dbentry.get(ids_name, occurrence=occurrence)

def ids_open_shot_hdf5(shot, ids_name, run=0, occurrence=0, machine="west", user=None, imas_version=None, backend=imasdef.HDF5_BACKEND):
    """
    INPUTS:
        -   type_ids            str containing the ids type the user wants
        -   occurrence          the occurrence of the save
    OUTPUTS:
        -   ids

    """
    log=[]
    backend_list=[imasdef.MDSPLUS_BACKEND, imasdef.HDF5_BACKEND]
    if not backend in backend_list:
        msg=f"This backend{backend} doesn't exist in imasdef"
        msg_log(msg, log, verbose=True, critical=True)

    imas_entry=imaspy.DBEntry(backend, machine, shot, run, user_name=user, data_version=imas_version)
    try:
        imas_entry.open()
        
        return imas_entry.get(ids_name, occurrence=occurrence)
    except:
        msg = f"The ids {ids_name} of the shot {shot} run {run} machine {machine} user {user} in HDF5 backend you want to open doesn't exist"
        msg_log(msg, log, verbose=True, critical=False)
        
def ids_open_shot_slice_hdf5(shot, time,  ids_name, run=0, occurrence=0, machine="west", user=None, imas_version=None, backend=imasdef.HDF5_BACKEND, interp=imasdef.CLOSEST_INTERP):
    """
    INPUTS:
        -   type_ids            str containing the ids type the user wants
        -   occurrence          the occurrence of the save
    OUTPUTS:
        -   ids

    """
    log=[]
    backend_list=[imasdef.MDSPLUS_BACKEND, imasdef.HDF5_BACKEND]
    if not backend in backend_list:
        msg=f"This backend{backend} doesn't exist in imasdef"
        msg_log(msg, log, verbose=True, critical=True)

    imas_entry=imaspy.DBEntry(backend, machine, shot, run, user_name=user, data_version=imas_version)
    imas_entry.open()
    if not interp in interp_list:
        msg="Type of interpolation doesn't exist"
        msg_log(msg, log, verbose=True, critical=True)
    else:
        return imas_entry.get_slice(ids_name, time, interp, occurrence=occurrence)

def list_path_ids_nonempty(ids, list_path):
    """
    Gives all the filled path in an imaspy ids
    INPUTS:
        ids             an imaspy ids
        list_path       the list_path in which will be stored all the path under the imaspy.ids_primitive corresponding type (take the ._path of theses leaf to have the str)
    OUTPUTS:
        list_path       the filled list_path"""
    if isinstance(ids, imaspy.ids_primitive.IDSPrimitive):
        return list_path.append(ids)
    else:
        if isinstance(ids, imaspy.ids_structure.IDSStructArray):
            for children in ids[0].iter_nonempty_():
                list_path_ids_nonempty(children, list_path)
        else:
            for children in ids.iter_nonempty_():
                list_path_ids_nonempty(children, list_path)
    return list_path

# DEPRECATED But works
def ids_mean(ids_list):
    """ return the mean IDS of a IDS list (only float, int and numerical array leafs of the IDS are averaged, for the other leaf it is the value of the first IDS of the ids_list)
    INPUTS:
        -   ids_list
    OUPUTS:
        -   ids_mean
    """
    # Be careful: works also for core_profiles BUT only if there is less than 9 ions
    nids = len(ids_list)
    # ids_copy = copy.deepcopy(check_resize_ids(ids_list))
    ids_copy = copy.deepcopy(ids_list[0])
    pathlist = []
    list_path_ids_nonempty(ids_copy, pathlist)
    path_list_filtered = []
    
    for leaf_path in pathlist:
        if isinstance(leaf_path,imaspy.ids_primitive.IDSNumericArray) or isinstance(leaf_path,imaspy.ids_primitive.IDSInt0D) or isinstance(leaf_path,imaspy.ids_primitive.IDSFloat0D):
            path_list_filtered.append(leaf_path)

    leaf_path_list = []
    for leaf_path in path_list_filtered:
        # leaf_path_list.append(correction_leaf_path(leaf_path.metadata.__dict__['path_string']))
        leaf_path_list.append(leaf_path._path)
    
    for ids in ids_list[1:]:
        for leaf_path in leaf_path_list:
            ids_copy.__setitem__(leaf_path, ids_copy.__getitem__(leaf_path) + ids.__getitem__(leaf_path))
            # if '/ion[' in leaf_path:
            #     nions = len(ids_copy.profiles_1d[0].ion)
            #     avant_ion, apres_ion = leaf_path.split("/ion[")
            #     ionlabel_path = avant_ion + '/ion['+apres_ion[0:2] + ".label"
            #     ionlabel = ids_copy.__getitem__(ionlabel_path)
            #     for ii in range(len(ids.profiles_1d[0].ion)):
            #         if ids.profiles_1d[0].ion[ii].label == ionlabel:
            #             ids_copy.__setitem__(leaf_path, ids_copy.__getitem__(leaf_path) + ids.__getitem__(avant_ion+'/ion['+str(ii)+apres_ion[1:]))

            # else:
            #     ids_copy.__setitem__(leaf_path, ids_copy.__getitem__(leaf_path) + ids.__getitem__(leaf_path))
    
    for leaf_path in leaf_path_list:
        if isinstance(ids_copy.__getitem__(leaf_path), imaspy.ids_primitive.IDSNumericArray):
            treshold = 1e-12
            data_array = ids_copy.__getitem__(leaf_path)
            data_array[np.abs(data_array) < treshold] = 0.0
            ids_copy.__setitem__(leaf_path, data_array/nids)
        else:
            ids_copy.__setitem__(leaf_path, ids_copy.__getitem__(leaf_path)/nids)
    
    return ids_copy

def check_homogeneite_table(table):
    """ check if a 2D arrays is homogenoeous (ie if it is a correct matrix (n,m) and not an array of random array with different lengths)
    INPUTS:
        - table     a 2D array
    OUTPUTS:
        - bool
    """
    try:
        np.nanmean(table)
        return True
    except ValueError as e:
        return False

def ids_sum(ids_list):
    """ Do the sum of the numerical values at all the leafs of the IDS of all the IDS in the ids_list
    INPUTS:
        ids_list                        list of imaspy.IDSStructure.ids_structure of same nature

    OUTPUTS:
        ids_sum               imaspy.IDSStructure.ids_structure as the list type with sum values for
                                        numerical values (means on imaspy.ids_primitive.IDSNumericArray
                                                                    imaspy.ids_primitive.IDSFloat0D
                                                                    imaspy.ids_primitive.IDSInt0D)
                                        others values (imaspy.ids_primive.IDSString) are the one of the first ids of the input list

    """
    ids_copy = copy.deepcopy(ids_list[0])
    pathlist = []
    list_path_ids_nonempty(ids_copy, pathlist)

    path_list_filtered = []
    for leaf_path in pathlist:
        if isinstance(leaf_path,imaspy.ids_primitive.IDSNumericArray) or isinstance(leaf_path,imaspy.ids_primitive.IDSInt0D) or isinstance(leaf_path,imaspy.ids_primitive.IDSFloat0D):
            path_list_filtered.append(leaf_path)
    del pathlist
    leaf_path_list = []
    for leaf_path in path_list_filtered:
        leaf_path_list.append(leaf_path._path)

    leaf_dico = {leaf_path: [] for leaf_path in leaf_path_list}

    for ids in ids_list:
        for leaf_path in leaf_path_list:
            leaf_dico[leaf_path].append(ids.__getitem__(leaf_path))

    for leaf, value in leaf_dico.items():
        if check_homogeneite_table(value)==False:
            dum=[]
            nids=len(ids_list)
            ind_middle_ids_list=int(nids/2)
            dum.append(ids_list[ind_middle_ids_list].__getitem__(leaf))
            leaf_dico[leaf]=dum


    ids_sum = copy.deepcopy(ids_list[0])
    for leaf_path in leaf_path_list:
        if isinstance(ids_sum.__getitem__(leaf_path), imaspy.ids_primitive.IDSNumericArray):
            ids_sum.__setitem__(leaf_path, np.nansum(leaf_dico[leaf_path], axis=0))
        else:
            ids_sum.__setitem__(leaf_path, np.nansum(leaf_dico[leaf_path]))
    
    del ids_copy, leaf_dico, ids_list, leaf_path_list

    return ids_sum

def ids_multiplication(ids, m):
    """ Do the multiplication of the numerical values at all the leafs by m of the IDS 
    INPUTS:
        ids_list                        list of imaspy.IDSStructure.ids_structure of same nature

    OUTPUTS:
        idsmult               imaspy.IDSStructure.ids_structure as the list type with multiplication values by m for
                                        numerical values (means on imaspy.ids_primitive.IDSNumericArray
                                                                    imaspy.ids_primitive.IDSFloat0D
                                                                    imaspy.ids_primitive.IDSInt0D)
                                        others values (imaspy.ids_primive.IDSString) are the one of the IDS

    """
    ids_copy = copy.deepcopy(ids)
    pathlist = []
    list_path_ids_nonempty(ids_copy, pathlist)
    err=''
    path_list_filtered = []
    for leaf_path in pathlist:
        if isinstance(leaf_path,imaspy.ids_primitive.IDSNumericArray) or isinstance(leaf_path,imaspy.ids_primitive.IDSInt0D) or isinstance(leaf_path,imaspy.ids_primitive.IDSFloat0D):
            path_list_filtered.append(leaf_path)
    del pathlist
    leaf_path_list = []
    for leaf_path in path_list_filtered:
        leaf_path_list.append(leaf_path._path)

    leaf_dico = {leaf_path: [] for leaf_path in leaf_path_list}

    for leaf_path in leaf_path_list:
        leaf_dico[leaf_path].append(ids.__getitem__(leaf_path))

    del ids_copy

    ids_mult = copy.deepcopy(ids)
    del ids
    for leaf_path in leaf_path_list:
        if isinstance(ids_mult.__getitem__(leaf_path), imaspy.ids_primitive.IDSNumericArray):
            try:
                ids_mult.__setitem__(leaf_path, m*leaf_dico[leaf_path][0])
            except:
                err=f"pb of hyper low or high values in {leaf_dico}"
        else:
            ids_mult.__setitem__(leaf_path, m*leaf_dico[leaf_path][0])

    return ids_mult, err

def ids_mean_std(ids_list):
    """
    INPUTS:
        ids_list                        list of imaspy.IDSStructure.ids_structure of same nature

    OUTPUTS:
        ids_mean, ids_std               imaspy.IDSStructure.ids_structure as the list type with mean values for
                                        numerical values (means on imaspy.ids_primitive.IDSNumericArray
                                                                    imaspy.ids_primitive.IDSFloat0D
                                                                    imaspy.ids_primitive.IDSInt0D)
                                        others values (imaspy.ids_primive.IDSString) are the one of the first ids of the input list

    """
    ids_copy = copy.deepcopy(ids_list[0])
    pathlist = []
    list_path_ids_nonempty(ids_copy, pathlist)

    path_list_filtered = []
    for leaf_path in pathlist:
        if isinstance(leaf_path,imaspy.ids_primitive.IDSNumericArray) or isinstance(leaf_path,imaspy.ids_primitive.IDSInt0D) or isinstance(leaf_path,imaspy.ids_primitive.IDSFloat0D):
            path_list_filtered.append(leaf_path)

    leaf_path_list = []
    for leaf_path in path_list_filtered:
        leaf_path_list.append(leaf_path._path)

    leaf_dico = {leaf_path: [] for leaf_path in leaf_path_list}

    for ids in ids_list:
        for leaf_path in leaf_path_list:
            
            leaf_dico[leaf_path].append(ids.__getitem__(leaf_path))

    for leaf, value in leaf_dico.items():
        if check_homogeneite_table(value)==False:
            dum=[]
            nids=len(ids_list)
            ind_middle_ids_list=int(nids/2)
            dum.append(ids_list[ind_middle_ids_list].__getitem__(leaf))
            leaf_dico[leaf]=dum

    ids_mean = copy.deepcopy(ids_list[0])
    ids_std  = copy.deepcopy(ids_list[0])
    for leaf_path in leaf_path_list:
        if isinstance(ids_mean.__getitem__(leaf_path), imaspy.ids_primitive.IDSNumericArray):
            ids_mean.__setitem__(leaf_path, np.nanmean(leaf_dico[leaf_path], axis=0))
            ids_std.__setitem__(leaf_path, np.nanstd(leaf_dico[leaf_path], axis=0))
        else:
            ids_mean.__setitem__(leaf_path, np.nanmean(leaf_dico[leaf_path]))
            ids_std.__setitem__(leaf_path, np.nanstd(leaf_dico[leaf_path]))
        
    del ids_copy, leaf_dico, ids_list, leaf_path_list # it is for memory management but noramally no useful bc python is a garbage collector language
    return ids_mean, ids_std

def ids_diff(ids1, ids2, verbose=False):
    """
    INPUTS:
        ids1, ids2                     2 ids of same nature (need to put a check of the type of the ids at the beginning of the function)

    OUTPUTS:None (only print difference between the 2 ids)

    """
    pathlist = []
    list_path_ids_nonempty(ids1, pathlist)

    leaf_path_list = []
    for leaf_path in pathlist:
        leaf_path_list.append(leaf_path._path)
    # print(leaf_path_list)

    log = []
    for leaf_path in leaf_path_list:
        try:
            if isinstance(ids1.__getitem__(leaf_path), imaspy.ids_primitive.IDSNumericArray):
                if len(ids1.__getitem__(leaf_path))!=len(ids2.__getitem__(leaf_path)):
                    print("\n")
                    print("***", leaf_path, "***")
                    print("IDS1 len() : ", len(ids1.__getitem__(leaf_path)))
                    print("IDS2 len() : ", len(ids2.__getitem__(leaf_path)))
                elif np.any(ids1.__getitem__(leaf_path) != ids2.__getitem__(leaf_path))  & ~(np.isnan(ids1.__getitem__(leaf_path)) & np.isnan(ids2.__getitem__(leaf_path))):
                    print("\n")
                    print("***", leaf_path, "***")
                    print("IDS1 : ", ids1.__getitem__(leaf_path))
                    print("IDS2 : ", ids2.__getitem__(leaf_path))
            else:
                if ids1.__getitem__(leaf_path)!=ids2.__getitem__(leaf_path):
                    print("\n")
                    print("***", leaf_path, "***")
                    print("IDS1 : ", ids1.__getitem__(leaf_path))
                    print("IDS2 : ", ids2.__getitem__(leaf_path))
            
        except ValueError:
            msg = f"Pb with node {leaf_path}"
            msg_log(msg, log, verbose=verbose, critical=False)
    if verbose:
        print(log)