'''
06/11/2024
Author: Enzo Vergnaud
enzo.vergnaud@cea.fr
Creation of a gytokinetic IDS with the GA standard case input parameters. 
Useful for benchmark (reference case for GK codes)
'''
import numpy as np
from scipy.constants import codata
import os 

try:
    import imaspy
    imaspy_loaded=True
except:
    print("***IMASPY NOT USED FOR THE GA STD CASE GK IDS CONSTRUTION: idspy_dictionaries and idspy_toolkit loaded ")
    from idspy_dictionaries import ids_gyrokinetics_local
    import idspy_toolkit
    imaspy_loaded=False

def gastd_gkids_init(ky=None, imaspy_loaded=imaspy_loaded): # show to check that x = r/a = 0.5 ???
    """ Creation of the ga standard case gyrokinetic_local IDS
    INPUTS:
        -   ky                  optional :float of a value for ky, if not, it is ky=0.4 that is set by default
    OUTPUTS:
        -   ga_ids              gyrokinetic_local ids of the GA std case : imaspy structure or ids_gyrokinetic_local structure depending on the user choice
    """
    if imaspy_loaded:
        ids_factory=imaspy.IDSFactory("3.41.0")
        IDS_GASTD=ids_factory.gyrokinetics_local()
    else:
        from idspy_dictionaries import ids_gyrokinetics_local
        import idspy_toolkit
        IDS_GASTD  = ids_gyrokinetics_local.GyrokineticsLocal()
        idspy_toolkit.fill_default_values_ids(IDS_GASTD)

    '''******************************************************'''
    '''*********************---INPUTS---*********************'''
    '''******************************************************'''


    ### --------------------------------
    ### -------IDS.ids_properties-------
    ### --------------------------------
    IDS_GASTD.ids_properties.provider = 'Name : '
    IDS_GASTD.ids_properties.comment  = 'Machine : WEST ; Shot : '

    ### --------------------------------
    ### -----------IDS.code-------------
    ### --------------------------------
    IDS_GASTD.code.name = 'GA STANDARD CASE IDS inputs for GK simulations benchmark'
    IDS_GASTD.code.repository = 'no url'
    IDS_GASTD.code.commit = 'no commit'
    IDS_GASTD.code.version = 'no version'
    # IDS_GASTD.code.library = np.array(['no library'])
    # IDS_GASTD.code.output_flag = 0


    ### --------------------------------
    ### ---IDS.normalizing_quantities---
    ### --------------------------------
    IDS_GASTD.normalizing_quantities.t_e = 10000 #eV
    IDS_GASTD.normalizing_quantities.n_e = 1e19 #m^-3
    IDS_GASTD.normalizing_quantities.b_field_tor = 3.0 #T
    IDS_GASTD.normalizing_quantities.r = 3.0 

    ### --------------------------------
    ### -----------IDS.model------------
    ### --------------------------------
    IDS_GASTD.model.adiabatic_electrons = 0 # under which experimental considerations could elecrons be considered as adiabatic?
    IDS_GASTD.model.include_a_field_parallel = 1 # same commentary
    IDS_GASTD.model.include_b_field_parallel = 1 # same commentary
    #IDS_GASTD.model.use_mhd_rule = 0 # same commentary NOT UPDATED 
    IDS_GASTD.model.include_coriolis_drift = 1 # same commentary
    IDS_GASTD.model.include_centrifugal_effects = 1 # same commentary
    IDS_GASTD.model.collisions_pitch_only = 1 # same commentary
    IDS_GASTD.model.collisions_momentum_conservation = 1 # same commentary
    IDS_GASTD.model.collisions_energy_conservation = 1 # same commentary
    IDS_GASTD.model.collisions_momentum_conservation = 1 # same commentary
    IDS_GASTD.model.collisions_finite_larmor_radius = 0
    
    ### --------------------------------
    ### --------IDS.flux_surface--------
    ### --------------------------------
    IDS_GASTD.flux_surface.ip_sign = 1
    IDS_GASTD.flux_surface.b_field_tor_sign = 1
    IDS_GASTD.flux_surface.r_minor_norm = 0.1666667 # ????
    IDS_GASTD.flux_surface.q = 2.0
    IDS_GASTD.flux_surface.magnetic_shear_r_minor = 1.0
    IDS_GASTD.flux_surface.pressure_gradient_norm = 0 # ????
    IDS_GASTD.flux_surface.dgeometric_axis_r_dr_minor = 0 # ????
    IDS_GASTD.flux_surface.dgeometric_axis_z_dr_minor = 0 # ????
    IDS_GASTD.flux_surface.elongation = 1.0 # ????# the one of the flux surface under consideration
    IDS_GASTD.flux_surface.delongation_dr_minor_norm = 0# ???? # the one of the flux surface under consideration
    IDS_GASTD.flux_surface.shape_coefficients_c = np.array([0]) # ????
    IDS_GASTD.flux_surface.shape_coefficients_s = np.array([0]) # ????
    IDS_GASTD.flux_surface.dc_dr_minor_norm = np.array([0]) # ????
    IDS_GASTD.flux_surface.ds_dr_minor_norm = np.array([0]) # ????

    ### --------------------------------
    ### --------IDS.species_all---------
    ### --------------------------------
    # Tref = 10e3 # eV ????
    # nref = 1e19 # ????
    # qref = codata.physical_constants['elementary charge'][0] #elementary charge (C) TO DO WITH CODATA
    mref = codata.physical_constants['deuteron mass'][0]#Deuterium mass (kg) TO DO WITH CODATA
    # Bref = 3.0 # ????
    # vthref = np.sqrt(2*Tref*qref/mref)
    # rhoref = mref*vthref/(qref*Bref)

    IDS_GASTD.species_all.beta_reference = 0.0
    IDS_GASTD.species_all.velocity_tor_norm = 0 # ????
    IDS_GASTD.species_all.shearing_rate_norm = 0 # ????
    IDS_GASTD.species_all.debye_length_norm = 0 # 1/rhoref * np.sqrt(epsilon_0*Tref/(nref*qref**2)) ???
    IDS_GASTD.species_all.angle_pol = np.array([0]) # ????

    ### --------------------------------
    ### ----------IDS.species-----------
    ### --------------------------------
    if imaspy_loaded:
        IDS_GASTD.species.resize(2)
        IDS_GASTD.species[0].charge_norm = -1 # ???
        IDS_GASTD.species[0].mass_norm = codata.physical_constants['electron mass'][0]/mref
        IDS_GASTD.species[0].density_norm = 1 # ???
        IDS_GASTD.species[0].density_log_gradient_norm = 3.0
        IDS_GASTD.species[0].temperature_norm = 1 # ???# ???
        IDS_GASTD.species[0].temperature_log_gradient_norm = 9.0
        IDS_GASTD.species[0].velocity_tor_gradient_norm = 0 # ???

        IDS_GASTD.species[1].charge_norm = 1.0 # ???
        IDS_GASTD.species[1].mass_norm = 1.0 # ???
        IDS_GASTD.species[1].density_norm = 1.0 # ???
        IDS_GASTD.species[1].density_log_gradient_norm = 3.0
        IDS_GASTD.species[1].temperature_norm = 1.0
        IDS_GASTD.species[1].temperature_log_gradient_norm = 9.0 # ???
        IDS_GASTD.species[1].velocity_tor_gradient_norm = 0 # ???
    else:
        dum_e = ids_gyrokinetics_local.Species()
        dum_e.charge_norm = -1 # ???
        dum_e.mass_norm = codata.physical_constants['electron mass'][0]/mref
        dum_e.density_norm = 1 # ???
        dum_e.density_log_gradient_norm = 3.0
        dum_e.temperature_norm = 1 # ???# ???
        dum_e.temperature_log_gradient_norm = 9.0
        dum_e.velocity_tor_gradient_norm = 0 # ???

        IDS_GASTD.species.append(dum_e)

        dum_i = ids_gyrokinetics_local.Species()
        dum_i.charge_norm = 1.0 # ???
        dum_i.mass_norm = 1.0 # ???
        dum_i.density_norm = 1.0 # ???
        dum_i.density_log_gradient_norm = 3.0
        dum_i.temperature_norm = 1.0
        dum_i.temperature_log_gradient_norm = 9.0 # ???
        dum_i.velocity_tor_gradient_norm = 0 # ???

        IDS_GASTD.species.append(dum_i)

    

    
    ### --------------------------------
    ### --------IDS.collisions----------
    ### --------------------------------
    IDS_GASTD.collisions.collisionality_norm = np.array([[0,0],[0,0]]) #matrix number_species x number_species (only electron and one ion)

    ### ----------------------------
    ### --------IDS.linear----------
    ### ----------------------------
    if imaspy_loaded:
        IDS_GASTD.linear.wavevector.resize(1)
        if ky==None:
            print("NO KY IN INPUTS: DEFAULT VALUE SET AT KY=0.4")
            IDS_GASTD.linear.wavevector[0].binormal_wavevector_norm = 0.4
        else:
            IDS_GASTD.linear.wavevector[0].binormal_wavevector_norm = ky
        IDS_GASTD.linear.wavevector[0].radial_wavevector_norm = 0
    else:
        wvvector = ids_gyrokinetics_local.Wavevector()
        if ky==None:
            print("NO KY IN INPUTS: DEFAULT VALUE SET AT KY=0.4")
            wvvector.binormal_wavevector_norm = 0.4
        else:
            wvvector.binormal_wavevector_norm = ky
        wvvector.radial_wavevector_norm = 0
        IDS_GASTD.linear.wavevector.append(wvvector)


    print("***********************************************************************")
    print("PLEASE DON'T FORGET TO ADAPT THE MODEL FIELDS FOR YOUR SPECIFIC RUNS!!!")
    print("***********************************************************************")

    return IDS_GASTD

            
