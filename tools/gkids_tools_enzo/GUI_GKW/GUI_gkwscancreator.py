import sys 
import os
sys.path.append("/Home/EV273809/Documents/These/Python/mes_codes/enzo_utilitaries/")

import GUItools as guitools

from SWITCHIMASalversiontools import *

print("==================================")
if int(os.getenv("UAL_VERSION")[0])>=5:
    import WESTtools as westtools 
    print("WESTTools loaded.")
    import GKIDStools as gkidstools
    from GKIDStools import dico_param
    print("GKIDStools loaded (gkwimas usable)")
else:
    print("WESTTools and GKIDSTOOLS are not loaded.")
    unusable_ENZOtools_functions=['scan_gkcodes']
    print(f"Functions not usable in {sys.argv[0]}:")
    for fct in unusable_ENZOtools_functions:
        print(fct)
print("==================================")


def gui_scan_gkw():
    log=[]
    files=guitools.GUI_files_selector()
    if len(files)!=1:
        msg="Please select only one gkw_runpath_file"
        gkidstools.msg_log(msg,log,verbose=True,critical=False)
    gkw_path=files[0]
    print(gkw_path)
    gkwobj_ref=gkidstools.read_gkw_run_to_gkwrun_obj(gkw_path)
    dico_dico_param=gkidstools.gkwrun_input_to_dicodico(gkwobj_ref) 
    param, subparam=guitools.GUI_dicodico_param_list(dico_dico_param)
    scan_list=guitools.GUI_scan_list_creator()
    if param=="gridsize":
        scan_list= [int(scanval) for scanval in scan_list]
    gkidstools.gkw_scan_creation(gkwobj_ref, param, subparam, scan_list, gkw_path)


if __name__ == "__main__":
    # Exemple d'utilisation de GUI_files_selector
    selected_files = gui_scan_gkw()
