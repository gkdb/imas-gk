import matplotlib.pyplot as plt 
import matplotlib.gridspec as gridspec # for disposition
import sys 
import os
sys.path.append("/Home/EV273809/Documents/These/Python/mes_codes/enzo_utilitaries/")
import PLOTtools as plottools
import GUItools as guitools

from SWITCHIMASalversiontools import *

print("==================================")
if int(os.getenv("UAL_VERSION")[0])>=5:
    import WESTtools as westtools 
    print("WESTTools loaded.")
    import GKIDStools as gkidstools
    from GKIDStools import dico_param
    print("GKIDStools loaded (gkwimas usable)")
else:
    print("WESTTools and GKIDSTOOLS are not loaded.")
    unusable_ENZOtools_functions=['scan_gkcodes']
    print(f"Functions not usable in {sys.argv[0]}:")
    for fct in unusable_ENZOtools_functions:
        print(fct)
print("==================================")


def gui_scan_gr_from_gkwrun():
    log=[]
    files=guitools.GUI_files_selector()
    if len(files)!=1:
        msg="Please select only one gkw_runpath_file"
        gkidstools.msg_log(msg,log,verbose=True,critical=False)
    gk_ids_list=[]
    scan_list_hyp=[]
    for gkw_path in files:
        gk_ids_list.append(gkidstools.read_gkw_run(gkw_fullrunpath=gkw_path))
        dico_param_gkw_path=gkidstools.guess_param_gkwpath(os.path.basename(gkw_path))
        param_hyp = list(dico_param_gkw_path.keys())[-1]
        value_hyp = dico_param_gkw_path[param_hyp]
        scan_list_hyp.append(value_hyp)
    gr, freq =gkidstools.eigenvalues_gkids_list(gk_ids_list)
    scan_list = scan_list_hyp
    param=param_hyp
    try:
        param_final=dico_param[param]
    except:
        param_final=param
        
    gr_graph= plottools.GraphDico(scan_list, gr, x_label=param_final, y_label=r"$\gamma$")
    freq_graph= plottools.GraphDico(scan_list, freq, x_label=param_final, y_label=r"$\omega_r$")
    print(scan_list)
    print(gr.tolist())
    print(freq.tolist())
    plottools.plot_settings()
    # Crée une figure
    fig = plt.figure(figsize=(10, 6))  # Taille de la figure
    # Utilise GridSpec pour organiser les sous-figures
    gs = gridspec.GridSpec(1, 2)  # Crée une grille de 2 lignes et 2 colonnes
    ax1 = fig.add_subplot(gs[0, 0]) 
    ax2 = fig.add_subplot(gs[0, 1])  
    plottools.plot_stack_graph(ax1, [gr_graph])
    plottools.plot_stack_graph(ax2, [freq_graph])

    # total_title         = title + '\n' + plottools.title_gkids(gk_ids,param)
    # fig.suptitle(total_title)
    plottools.position_figure(fig,x=0,y=0,width=1900,height=1000)
    # plottools.fig_title_adjustement(fig, total_title) # Need to be finished

    # Ajustement des espaces entre les sous-figures
    plt.subplots_adjust(left=0.1, right=0.9, wspace=0.3, hspace=0.3)  # Ajuste les marges

    plt.show()


if __name__ == "__main__":
    # Exemple d'utilisation de GUI_files_selector
    selected_files = gui_scan_gr_from_gkwrun()