'''
06/11/2024
Author: Enzo Vergnaud
enzo.vergnaud@cea.fr
Tools useful for scan and other treatments with gyrokinetics_local IDS
CAREFUL : every runpath (folder where the runs is stored) end with '/' (example: runpath='/home/user/gkw/runs/')
Discomposition of the python file:
-   0: functions useful in other ids functions below
-   1: read/print gk_ids 
-   2: functions for easy scans of with TGLF, QLK and GKW
-   3: scan tools for TGLF, QLK and GKW scans
-   4: post scan treatments
-   5: latex table construction from gyrokinetic_local IDS
'''
import numpy as np 
import matplotlib.pyplot as plt 
import scipy.constants as codata
import copy


import os
import sys
try:
    sys.path.append('/Home/EV273809/Documents/These/GKW/gkw/python/yc')
    import gkwimas
    import gkwrun
except:
    print("No GKW repository found...")

sys.path.append(os.path.relpath("imas-gk/tools/tglf"))
import TGLFimaspy_implementation as tglfimas
import TGLFrun as tglfrun

sys.path.append(os.path.relpath("imas-gk/tools/qlk/qlk_enzo_alpha"))
from QLKclass import QLK_class 
import qlk2ids

imaspy_loaded=False
# for ids save in part 2 of this script file:
try:
    import IMASPYtools as imaspytools
    imaspy_loaded=True
except:
    print("No imaspy detected, idspy_toolkit and idspy_dicionaries used by default... (install it with pip install if you don't have it)")
sys.path.append(os.path.relpath("imas-gk/idspy_toolkit/idspy_toolkit"))
import idspy_toolkit 
from idspy_dictionaries import ids_gyrokinetics_local


# for latex table
import periodictable

# constants
me=codata.physical_constants['electron mass'][0] # electron mass [kg] 
mD=codata.physical_constants['deuteron mass'][0] # deuterium mass [kg] 
mH=codata.physical_constants['proton mass'][0]
mT=codata.physical_constants['triton mass'][0]
amu=codata.physical_constants['unified atomic mass unit'][0]
eV=codata.physical_constants['elementary charge'][0] # elementary charge [C] / electron volt [J]
eps0=codata.physical_constants['vacuum electric permittivity'][0]
mu0=codata.physical_constants['vacuum mag. permeability'][0]
kB=codata.physical_constants['Boltzmann constant'][0]

dico_param = {'ktheta' : r'$k_\theta \rho_i$', 
              'ky' : r'$k_\theta \rho_i$',
                  'rlte': r'$R/L_{Te}$', 
                  'rlne':r'$R/L_{ne}$',
                  'rlt': r'$R/L_{T}$', 
                  'rln':r'$R/L_{n}$',
                  'rlti': r'$R/L_{Ti}$', 
                  'rlni':r'$R/L_{ni}$',
                  'q' : r'q ',
                  'beta': r'$\beta$ %',
                  'magn_shear': r'$\^s$ ',
                  'ti_te': r'$T_i/T_e$ ',
                  'beta': r'$\beta_{IMAS}$',
                #   'alpha': r'$\alpha_{MHD}$ ',
                  'nu_star': r'$\nu*$',
                  'elongation' : r'$\kappa$',
                  'elong' : r'$\kappa$'}

###################################
###################################
#######       PART 0        #######
#USEFUL FUNCTIONS  FOR OTHER PARTS#
###################################
###################################

def msg_log(msg,log,verbose,critical=False): 
    """ Generic routine to store and optionally display warning/error messages 
    Inputs    
        msg         message to be stored/displayed
        log         list to store the messages
        verbose     if True, displays the message on screen
        critical    if True, exits the code
        
    """
    if critical:
        log.append("Error: "+msg)
        raise Exception(msg)
    else:
        log.append("Warning: "+msg)
        if verbose is True:
            print(msg)

def float_str(x):
    """Convert a float in a str under the form "(integer part)P(decimal part)"
    INPUTS:
        -   x           float
    OUTPUTS:
        -   chaine      str
    """
    chaine = str(x).split('.')
    return chaine[0] + 'P' + chaine[1]

def str_float(s):
    """Convert a str  under the form "(integer part)P(decimal part)" into a float
    INPUTS:
        -   s           str
    OUTPUTS:
        -   x           float or int (int if the decimal part = 0 ie no P in s)
    """
    if 'P' in s:
        return float(s.replace('P', '.'))
    else:
        return int(s)

def get_closest_index(value, input_list):
    """ Return the index of the closest value put in inputs
    INPUTS:
        -   value           float
        -   input_list      list
    OUTPUTS:
        -   index           the corresponding first index in the input_list to the value
    """
    lst = np.abs(np.array(input_list)-value)
    min = np.min(lst)
    
    return np.where(lst == min)[0][0]

def two_folders_before(filepath):
    """For a given path, will back up 2 folders before the end of the path (useful for gkwrun)
    INPUTS:
        -   filepath        a path under the string format
    OUTPUTS:
        -   path            the same path truncated of the two last nodes
    """
    log=[]
    normalized_filepath=os.path.normpath(filepath)
    filepath_components=normalized_filepath.split(os.sep)
    if len(filepath_components)<3:
        msg="The path selected is too small to back up two folders "
        msg_log(msg,log, verbose=True, critical=False)
    
    return os.sep.join(filepath_components[:-2])

def check_int(s):
    """Check if there is a int in a str
    INPUTS:
        -   s               a string
    OUTPUTS:
        -   bool            a bool
    """
    for char in s:
        if char.isdigit():  
            return True
    return False 

def vthref_rhoref(gkids):
    """Compurte the thermal velocity and reference larmor radius of a corresponding gyrokinetic_local IDS
    INPUTS:
        -   gkids           gyrokinetic_local IDS
    OUTPUTS:
        -   vth, rho        a couple of float corresponding respectively to the thermal velocity and the reference larmor radius
    """
    vth = np.sqrt(2*gkids.normalizing_quantities.t_e*eV/ mD)
    rho = mD*vth/(eV*gkids.normalizing_quantities.b_field_tor)
    return vth, rho

def min_wavevector_for_approx_local(gkids):
    """ give the ky limit of the local approximation assumption
    INPUTS:
        -   gkids           a gyrokinetic_local IDS
    OUTPUTS:
        -   ky_min          the minimum ky*rho to have the local approximation validated
    """
    R = gkids.normalizing_quantities.r 
    ns = len(gkids.species)
    LTs = []
    Lns = []
    for ss in range(ns):
        RLT = gkids.species[ss].temperature_log_gradient_norm 
        RLn = gkids.species[ss].density_log_gradient_norm  
        LTs.append(R/RLT)
        Lns.append(R/RLn)
    gradient_lenghts = LTs + Lns
    lambda_min = np.min(gradient_lenghts)
    ky_min = 2*np.pi/lambda_min
    vthref, rhoref = vthref_rhoref(gkids)
    return ky_min*rhoref

def format_number(number, significant_figures=2):
    """ Return a float number with the significant figures desired
    INPUTS:
        -   number                  a float or str/int that can be converted in float
        -   significant_figures     int
    OUTPUTS:
        -   formatted_number        str with the corresponding significant figures desired
    """
    number = float(number)
    format_string = f"{{:.{significant_figures}g}}"
    formatted_number = format_string.format(number)
    
    if "." in formatted_number:
        formatted_number = f"{float(formatted_number):.{significant_figures}g}"
        

    return formatted_number

def element_name_from_mass(approx_mass):
    """ give the estimated element from the periodic table with the mass
    INPUTS:
        -   approx_mass             float
    OUPUTS:
        -   closest_element         elements corresponding to the mass
    """
    closest_element = None
    tolerance = approx_mass*0.025
    min_diff = float('inf')
    
    for element in periodictable.elements:
        if element.mass is not None:
            diff = abs(element.mass - approx_mass)
            if diff < min_diff and diff <= tolerance:
                min_diff = diff
                closest_element = element
        # Vérifier les isotopes de chaque élément
        for isotope in element:
            if isotope is not None and isotope.mass is not None:
                diff = abs(isotope.mass - approx_mass)
                if diff < min_diff and diff <= tolerance:
                    min_diff = diff
                    closest_element = isotope
    
    if closest_element:
        return f"{closest_element.name}"
    else:
        return "ERROR NO ELEMENT FOUND"

###################################
###################################
#######       PART 1        #######
#######  READ/PRINT GK IDS  #######
###################################

def read_gk_ids(gk_ids):
    """Print all the gyrokinetic_local ids
    INPUTS:
        -   gk_ids       gyrokinetic_local ids
    OUTPUTS: None
    """
    print("*** ids_properties ***")
    print("ids_properties.provider: ", gk_ids.ids_properties.provider)
    print("ids_properties.creation_date: ", gk_ids.ids_properties.creation_date)
    print("ids_properties.comment: ", gk_ids.ids_properties.comment)
    print("ids_properties.homogeneous_time: ", gk_ids.ids_properties.homogeneous_time)

    print("*** code ***")
    print("code.name: ", gk_ids.code.name)
    print("code.repository: ", gk_ids.code.repository)
    print("code.commit: ", gk_ids.code.commit)
    print("code.version: ", gk_ids.code.version)
    print("code.library: ", gk_ids.code.library)

    print("*** normalizing_quantities ***")
    print("normalizing_quantities.r: ", gk_ids.normalizing_quantities.r)
    print("normalizing_quantities.b_field_tor: ", gk_ids.normalizing_quantities.b_field_tor)
    print("normalizing_quantities.n_e: ", gk_ids.normalizing_quantities.n_e)
    print("normalizing_quantities.t_e: ", gk_ids.normalizing_quantities.t_e)

    print("*** model ***")
    print("model.adiabatic_electrons: ", gk_ids.model.adiabatic_electrons)
    print("model.include_a_field_parallel: ", gk_ids.model.include_a_field_parallel)
    print("model.include_b_field_parallel: ", gk_ids.model.include_b_field_parallel)
    # print("model.use_mhd_rule: ", gk_ids.model.use_mhd_rule)
    print("model.include_coriolis_drift: ", gk_ids.model.include_coriolis_drift)
    print("model.include_centrifugal_effects: ", gk_ids.model.include_centrifugal_effects)
    print("model.collisions_pitch_only: ", gk_ids.model.collisions_pitch_only)
    print("model.collisions_momentum_conservation: ", gk_ids.model.collisions_momentum_conservation)
    print("model.collisions_energy_conservation: ", gk_ids.model.collisions_energy_conservation)
    print("model.collisions_finite_larmor_radius: ", gk_ids.model.collisions_finite_larmor_radius)

    print("*** flux_surface ***")
    print("flux_surface.ip_sign: ", gk_ids.flux_surface.ip_sign)
    print("flux_surface.b_field_tor_sign: ", gk_ids.flux_surface.b_field_tor_sign)
    print("flux_surface.r_minor_norm: ", gk_ids.flux_surface.r_minor_norm)
    print("flux_surface.q: ", gk_ids.flux_surface.q)
    print("flux_surface.magnetic_shear_r_minor: ", gk_ids.flux_surface.magnetic_shear_r_minor)
    print("flux_surface.pressure_gradient_norm: ", gk_ids.flux_surface.pressure_gradient_norm)
    print("flux_surface.dgeometric_axis_r_dr_minor: ", gk_ids.flux_surface.dgeometric_axis_r_dr_minor)
    print("flux_surface.dgeometric_axis_z_dr_minor: ", gk_ids.flux_surface.dgeometric_axis_z_dr_minor)
    print("flux_surface.elongation: ", gk_ids.flux_surface.elongation)
    print("flux_surface.delongation_dr_minor_norm: ", gk_ids.flux_surface.delongation_dr_minor_norm)
    print("flux_surface.shape_coefficients_c: ", gk_ids.flux_surface.shape_coefficients_c)
    print("flux_surface.dc_dr_minor_norm: ", gk_ids.flux_surface.dc_dr_minor_norm)
    print("flux_surface.shape_coefficients_s: ", gk_ids.flux_surface.shape_coefficients_s)
    print("flux_surface.ds_dr_minor_norm: ", gk_ids.flux_surface.ds_dr_minor_norm)

    print("*** species_all ***")
    print("species_all.velocity_tor_norm: ", gk_ids.species_all.velocity_tor_norm)
    print("species_all.shearing_rate_norm: ", gk_ids.species_all.shearing_rate_norm)
    print("species_all.beta_reference: ", gk_ids.species_all.beta_reference)
    print("species_all.debye_length_norm: ", gk_ids.species_all.debye_length_norm)
    print("species_all.angle_pol: ", gk_ids.species_all.angle_pol)

    print("*** species ***")
    print('Nombre d\'espèces: ', len(gk_ids.species))
    for ee in range(len(gk_ids.species)):
        # print("species[{}].label : ".format(ee), gk_ids.species[ee].label)
        print("species[{}].charge_norm: ".format(ee), gk_ids.species[ee].charge_norm)
        print("species[{}].mass_norm: ".format(ee), gk_ids.species[ee].mass_norm)
        print("species[{}].density_norm: ".format(ee), gk_ids.species[ee].density_norm)
        print("species[{}].density_log_gradient_norm: ".format(ee), gk_ids.species[ee].density_log_gradient_norm)
        print("species[{}].temperature_norm: ".format(ee), gk_ids.species[ee].temperature_norm)
        print("species[{}].temperature_log_gradient_norm: ".format(ee), gk_ids.species[ee].temperature_log_gradient_norm)
        print("species[{}].velocity_tor_gradient_norm: ".format(ee), gk_ids.species[ee].velocity_tor_gradient_norm)
        # print("species[{}].potential_energy_norm: ".format(ee), gk_ids.species[ee].potential_energy_norm)
        # print("species[{}].potential_energy_gradient_norm: ".format(ee), gk_ids.species[ee].potential_energy_gradient_norm)

    print("*** collisions ***")
    print("collisions.collisionality_norm: ", gk_ids.collisions.collisionality_norm)

    print("*** linear ***")
    
    nbr_wavevctor = len(gk_ids.linear.wavevector)
    for ii in range(nbr_wavevctor):
        print("linear.wavevector[{}].binormal_wavevector_norm: ".format(ee), gk_ids.linear.wavevector[ii].binormal_wavevector_norm)
        for ee in range(len(gk_ids.linear.wavevector[ii].eigenmode)):
            print(f"linear.wavevector[{ii}].eigenmode[{ee}].growth_rate_norm: ", gk_ids.linear.wavevector[ii].eigenmode[ee].growth_rate_norm)

def comparison_gk_ids(gk_ids1, gk_ids2):
    """Compare two gyrokinetic_local gk_ids
    INPUTS:
        -   gk_ids1       gyrokinetic_local gk_ids
        -   gk_ids2       gyrokinetic_local gk_ids
    OUTPUTS: None
    """
    # print("*** ids_properties ***")
    # if gk_ids1.ids_properties.provider != gk_ids2.gk_ids_properties.provider:
    #     print('ids_properties.provider: 1:', str(gk_ids1.gk_ids_properties.provider), '; 2:', str(gk_ids2.gk_ids_properties.provider))
    # if gk_ids1.ids_properties.creation_date != gk_ids2.gk_ids_properties.creation_date:
    #     print('ids_properties.creation_date: 1:', str(gk_ids1.gk_ids_properties.creation_date), '; 2:', str(gk_ids2.gk_ids_properties.creation_date))
    # if gk_ids1.ids_properties.creation_date != gk_ids2.gk_ids_properties.creation_date:
    #     print('ids_properties.creation_date: 1:', str(gk_ids1.gk_ids_properties.creation_date), '; 2:', str(gk_ids2.gk_ids_properties.creation_date))
    # if gk_ids1.ids_properties.homogeneous_time != gk_ids2.gk_ids_properties.homogeneous_time:
    #     print('ids_properties.homogeneous_time: 1:', str(gk_ids1.gk_ids_properties.homogeneous_time), '; 2:', str(gk_ids2.gk_ids_properties.homogeneous_time))



    # print("*** code ***")
    # if gk_ids1.code.name != gk_ids2.code.name:
    #     print('code.name: 1:', str(gk_ids1.code.name), '; 2:', str(gk_ids2.code.name))
    # if gk_ids1.code.repository != gk_ids2.code.repository:
    #     print('code.repository: 1:', str(gk_ids1.code.repository), '; 2:', str(gk_ids2.code.repository))
    # if gk_ids1.code.commit != gk_ids2.code.commit:
    #     print('code.commit: 1:', str(gk_ids1.code.commit), '; 2:', str(gk_ids2.code.commit))
    # if gk_ids1.code.version != gk_ids2.code.version:
    #     print('code.version: 1:', str(gk_ids1.code.version), '; 2:', str(gk_ids2.code.version))
    # if gk_ids1.code.library != gk_ids2.code.library:
    #     print('code.library: 1:', str(gk_ids1.code.library), '; 2:', str(gk_ids2.code.library))


    print("*** normalizing_quantities ***")
    if gk_ids1.normalizing_quantities.r != gk_ids2.normalizing_quantities.r:
        print('normalizing_quantities.r: 1:', str(gk_ids1.normalizing_quantities.r), '; 2:', str(gk_ids2.normalizing_quantities.r))

    if gk_ids1.normalizing_quantities.b_field_tor != gk_ids2.normalizing_quantities.b_field_tor:
        print('normalizing_quantities.b_field_tor: 1:', str(gk_ids1.normalizing_quantities.b_field_tor), '; 2:', str(gk_ids2.normalizing_quantities.b_field_tor))

    if gk_ids1.normalizing_quantities.n_e != gk_ids2.normalizing_quantities.n_e:
        print('normalizing_quantities.n_e: 1:', str(gk_ids1.normalizing_quantities.n_e), '; 2:', str(gk_ids2.normalizing_quantities.n_e))

    if gk_ids1.normalizing_quantities.t_e != gk_ids2.normalizing_quantities.t_e:
        print('normalizing_quantities.t_e: 1:', str(gk_ids1.normalizing_quantities.t_e), '; 2:', str(gk_ids2.normalizing_quantities.t_e))


    print("*** model ***")
    if gk_ids1.model.adiabatic_electrons != gk_ids2.model.adiabatic_electrons:
        print('model.adiabatic_electrons: 1:', str(gk_ids1.model.adiabatic_electrons), '; 2:', str(gk_ids2.model.adiabatic_electrons))

    if gk_ids1.model.include_a_field_parallel != gk_ids2.model.include_a_field_parallel:
        print('model.include_a_field_parallel: 1:', str(gk_ids1.model.include_a_field_parallel), '; 2:', str(gk_ids2.model.include_a_field_parallel))

    if gk_ids1.model.include_b_field_parallel != gk_ids2.model.include_b_field_parallel:
        print('model.include_b_field_parallel: 1:', str(gk_ids1.model.include_b_field_parallel), '; 2:', str(gk_ids2.model.include_b_field_parallel))

    if gk_ids1.model.include_coriolis_drift != gk_ids2.model.include_coriolis_drift:
        print('model.include_coriolis_drift: 1:', str(gk_ids1.model.include_coriolis_drift), '; 2:', str(gk_ids2.model.include_coriolis_drift))

    if gk_ids1.model.include_centrifugal_effects != gk_ids2.model.include_centrifugal_effects:
        print('model.include_centrifugal_effects: 1:', str(gk_ids1.model.include_centrifugal_effects), '; 2:', str(gk_ids2.model.include_centrifugal_effects))

    if gk_ids1.model.collisions_pitch_only != gk_ids2.model.collisions_pitch_only:
        print('model.collisions_pitch_only: 1:', str(gk_ids1.model.collisions_pitch_only), '; 2:', str(gk_ids2.model.collisions_pitch_only))

    if gk_ids1.model.collisions_momentum_conservation != gk_ids2.model.collisions_momentum_conservation:
        print('model.collisions_momentum_conservation: 1:', str(gk_ids1.model.collisions_momentum_conservation), '; 2:', str(gk_ids2.model.collisions_momentum_conservation))

    if gk_ids1.model.collisions_energy_conservation != gk_ids2.model.collisions_energy_conservation:
        print('model.collisions_energy_conservation: 1:', str(gk_ids1.model.collisions_energy_conservation), '; 2:', str(gk_ids2.model.collisions_energy_conservation))

    if gk_ids1.model.collisions_finite_larmor_radius != gk_ids2.model.collisions_finite_larmor_radius:
        print('model.collisions_finite_larmor_radius: 1:', str(gk_ids1.model.collisions_finite_larmor_radius), '; 2:', str(gk_ids2.model.collisions_finite_larmor_radius))


    print("*** flux_surface ***")
    if gk_ids1.flux_surface.ip_sign != gk_ids2.flux_surface.ip_sign:
        print('flux_surface.ip_sign: 1:', str(gk_ids1.flux_surface.ip_sign), '; 2:', str(gk_ids2.flux_surface.ip_sign))
    if gk_ids1.flux_surface.b_field_tor_sign != gk_ids2.flux_surface.b_field_tor_sign:
        print('flux_surface.b_field_tor_sign: 1:', str(gk_ids1.flux_surface.b_field_tor_sign), '; 2:', str(gk_ids2.flux_surface.b_field_tor_sign))
    if gk_ids1.flux_surface.r_minor_norm != gk_ids2.flux_surface.r_minor_norm:
        print('flux_surface.r_minor_norm: 1:', str(gk_ids1.flux_surface.r_minor_norm), '; 2:', str(gk_ids2.flux_surface.r_minor_norm))
    if gk_ids1.flux_surface.q != gk_ids2.flux_surface.q:
        print('flux_surface.q: 1:', str(gk_ids1.flux_surface.q), '; 2:', str(gk_ids2.flux_surface.q))

    if gk_ids1.flux_surface.magnetic_shear_r_minor != gk_ids2.flux_surface.magnetic_shear_r_minor:
        print('flux_surface.magnetic_shear_r_minor: 1:', str(gk_ids1.flux_surface.magnetic_shear_r_minor), '; 2:', str(gk_ids2.flux_surface.magnetic_shear_r_minor))
    if gk_ids1.flux_surface.pressure_gradient_norm != gk_ids2.flux_surface.pressure_gradient_norm:
        print('flux_surface.pressure_gradient_norm: 1:', str(gk_ids1.flux_surface.pressure_gradient_norm), '; 2:', str(gk_ids2.flux_surface.pressure_gradient_norm))
    if gk_ids1.flux_surface.dgeometric_axis_r_dr_minor != gk_ids2.flux_surface.dgeometric_axis_r_dr_minor:
        print('flux_surface.dgeometric_axis_r_dr_minor: 1:', str(gk_ids1.flux_surface.dgeometric_axis_r_dr_minor), '; 2:', str(gk_ids2.flux_surface.dgeometric_axis_r_dr_minor))
    if gk_ids1.flux_surface.dgeometric_axis_z_dr_minor != gk_ids2.flux_surface.dgeometric_axis_z_dr_minor:
        print('flux_surface.dgeometric_axis_z_dr_minor: 1:', str(gk_ids1.flux_surface.dgeometric_axis_z_dr_minor), '; 2:', str(gk_ids2.flux_surface.dgeometric_axis_z_dr_minor))
    if gk_ids1.flux_surface.elongation != gk_ids2.flux_surface.elongation:
        print('flux_surface.elongation: 1:', str(gk_ids1.flux_surface.elongation), '; 2:', str(gk_ids2.flux_surface.elongation))
    if gk_ids1.flux_surface.delongation_dr_minor_norm != gk_ids2.flux_surface.delongation_dr_minor_norm:
        print('flux_surface.delongation_dr_minor_norm: 1:', str(gk_ids1.flux_surface.delongation_dr_minor_norm), '; 2:', str(gk_ids2.flux_surface.delongation_dr_minor_norm))
    if len(gk_ids1.flux_surface.shape_coefficients_c)==len(gk_ids2.flux_surface.shape_coefficients_c):
        if any(e1 != e2 for e1,e2 in zip((gk_ids1.flux_surface.shape_coefficients_c,gk_ids2.flux_surface.shape_coefficients_c))):
            print('flux_surface.shape_coefficients_c: 1:', str(gk_ids1.flux_surface.shape_coefficients_c), '; 2:', str(gk_ids2.flux_surface.shape_coefficients_c))
    else: 
        print('flux_surface.shape_coefficients_c: 1:', str(gk_ids1.flux_surface.shape_coefficients_c), '; 2:', str(gk_ids2.flux_surface.shape_coefficients_c))
    if len(gk_ids1.flux_surface.ds_dr_minor_norm)==len(gk_ids2.flux_surface.ds_dr_minor_norm):
        if any(e1 != e2 for e1,e2 in zip((gk_ids1.flux_surface.ds_dr_minor_norm,gk_ids2.flux_surface.ds_dr_minor_norm))):
            print('flux_surface.ds_dr_minor_norm: 1:', str(gk_ids1.flux_surface.ds_dr_minor_norm), '; 2:', str(gk_ids2.flux_surface.ds_dr_minor_norm))
    else: 
        print('flux_surface.ds_dr_minor_norm: 1:', str(gk_ids1.flux_surface.ds_dr_minor_norm), '; 2:', str(gk_ids2.flux_surface.ds_dr_minor_norm))
    if len(gk_ids1.flux_surface.shape_coefficients_s)==len(gk_ids2.flux_surface.shape_coefficients_s):
        if any(e1 != e2 for e1,e2 in zip((gk_ids1.flux_surface.shape_coefficients_s,gk_ids2.flux_surface.shape_coefficients_s))):
            print('flux_surface.shape_coefficients_s: 1:', str(gk_ids1.flux_surface.shape_coefficients_s), '; 2:', str(gk_ids2.flux_surface.shape_coefficients_s))
    else: 
        print('flux_surface.shape_coefficients_s: 1:', str(gk_ids1.flux_surface.shape_coefficients_s), '; 2:', str(gk_ids2.flux_surface.shape_coefficients_s))
    if len(gk_ids1.flux_surface.dc_dr_minor_norm)==len(gk_ids2.flux_surface.dc_dr_minor_norm):
        if any(e1 != e2 for e1,e2 in zip((gk_ids1.flux_surface.dc_dr_minor_norm,gk_ids2.flux_surface.dc_dr_minor_norm))):
            print('flux_surface.dc_dr_minor_norm: 1:', str(gk_ids1.flux_surface.dc_dr_minor_norm), '; 2:', str(gk_ids2.flux_surface.dc_dr_minor_norm))
    else: 
        print('flux_surface.dc_dr_minor_norm: 1:', str(gk_ids1.flux_surface.dc_dr_minor_norm), '; 2:', str(gk_ids2.flux_surface.dc_dr_minor_norm))
    


    print("*** species_all ***")
    if gk_ids1.species_all.velocity_tor_norm != gk_ids2.species_all.velocity_tor_norm:
        print('species_all.velocity_tor_norm: 1:', str(gk_ids1.species_all.velocity_tor_norm), '; 2:', str(gk_ids2.species_all.velocity_tor_norm))
    if gk_ids1.species_all.shearing_rate_norm != gk_ids2.species_all.shearing_rate_norm:
        print('species_all.shearing_rate_norm: 1:', str(gk_ids1.species_all.shearing_rate_norm), '; 2:', str(gk_ids2.species_all.shearing_rate_norm))
    if gk_ids1.species_all.beta_reference != gk_ids2.species_all.beta_reference:
        print('species_all.beta_reference: 1:', str(gk_ids1.species_all.beta_reference), '; 2:', str(gk_ids2.species_all.beta_reference))
    if gk_ids1.species_all.debye_length_norm != gk_ids2.species_all.debye_length_norm:
        print('species_all.debye_length_norm: 1:', str(gk_ids1.species_all.debye_length_norm), '; 2:', str(gk_ids2.species_all.debye_length_norm))
    if len(gk_ids1.species_all.angle_pol)==len(gk_ids2.species_all.angle_pol):
        if any(e1 != e2 for e1,e2 in zip((gk_ids1.species_all.angle_pol,gk_ids2.species_all.angle_pol))):
            print('species_all.angle_pol: 1:', str(gk_ids1.species_all.angle_pol), '; 2:', str(gk_ids2.species_all.angle_pol))
    else: 
        print('species_all.angle_pol: 1:', str(gk_ids1.species_all.angle_pol), '; 2:', str(gk_ids2.species_all.angle_pol))
    

    print("*** species ***")
    print('Nombre d\'espèces: ', len(gk_ids1.species))
    for ee in range(len(gk_ids1.species)):
        if gk_ids1.species[ee].charge_norm != gk_ids2.species[ee].charge_norm:
            print('species[{}].charge_norm: 1:'.format(ee), str(gk_ids1.species[ee].charge_norm), '; 2:', str(gk_ids2.species[ee].charge_norm))
        if gk_ids1.species[ee].mass_norm != gk_ids2.species[ee].mass_norm:
            print('species[{}].mass_norm: 1:'.format(ee), str(gk_ids1.species[ee].mass_norm), '; 2:', str(gk_ids2.species[ee].mass_norm))
        if gk_ids1.species[ee].density_norm != gk_ids2.species[ee].density_norm:
            print('species[{}].density_norm: 1:'.format(ee), str(gk_ids1.species[ee].density_norm), '; 2:', str(gk_ids2.species[ee].density_norm))
        if gk_ids1.species[ee].density_log_gradient_norm != gk_ids2.species[ee].density_log_gradient_norm:
            print('species[{}].density_log_gradient_norm: 1:'.format(ee), str(gk_ids1.species[ee].density_log_gradient_norm), '; 2:', str(gk_ids2.species[ee].density_log_gradient_norm))
        if gk_ids1.species[ee].temperature_norm != gk_ids2.species[ee].temperature_norm:
            print('species[{}].temperature_norm: 1:'.format(ee), str(gk_ids1.species[ee].temperature_norm), '; 2:', str(gk_ids2.species[ee].temperature_norm))
        if gk_ids1.species[ee].temperature_log_gradient_norm != gk_ids2.species[ee].temperature_log_gradient_norm:
            print('species[{}].temperature_log_gradient_norm: 1:'.format(ee), str(gk_ids1.species[ee].temperature_log_gradient_norm), '; 2:', str(gk_ids2.species[ee].temperature_log_gradient_norm))
        if gk_ids1.species[ee].velocity_tor_gradient_norm != gk_ids2.species[ee].velocity_tor_gradient_norm:
            print('species[{}].velocity_tor_gradient_norm: 1:'.format(ee), str(gk_ids1.species[ee].velocity_tor_gradient_norm), '; 2:', str(gk_ids2.species[ee].velocity_tor_gradient_norm))

        # print(gk_ids.species[ee].potential_energy_norm)
        # print(gk_ids.species[ee].potential_energy_gradient_norm)

    print("*** collisions ***")
    if len(gk_ids1.collisions.collisionality_norm)==len(gk_ids2.collisions.collisionality_norm):
        if np.any((np.asarray(gk_ids1.collisions.collisionality_norm) != np.asarray(gk_ids2.collisions.collisionality_norm))):
            print('collisions.collisionality_norm: 1:', str(gk_ids1.collisions.collisionality_norm), '; 2:', str(gk_ids2.collisions.collisionality_norm))
                
    else: 
        print('collisions.collisionality_norm: 1:', str(gk_ids1.collisions.collisionality_norm), '; 2:', str(gk_ids2.collisions.collisionality_norm))

def title_gkids(gkids, param=None):
    """ Give all the setup of the gk_ids for title
    INPUTS:
        -   gkids               a gyrokinetic_local ids
        -   param               param analysed for the scan
    OUTPUTS:
        -   title_param         a string containing the title
    """
    ktheta = gkids.linear.wavevector[0].binormal_wavevector_norm 
    ind_main_ion = 0
    val_main_ion = 0
    for sp in range(len(gkids.species)):
        if gkids.species[sp].charge_norm < 0: # electron
            rlte = gkids.species[sp].temperature_log_gradient_norm
            rlne = gkids.species[sp].density_log_gradient_norm
        else: #ion
            if gkids.species[sp].density_norm>val_main_ion:
                val_main_ion = gkids.species[sp].density_norm 
                ind_main_ion = sp
    
    rlti = gkids.species[ind_main_ion].temperature_log_gradient_norm
    rlni = gkids.species[ind_main_ion].density_log_gradient_norm
    ti_te = gkids.species[ind_main_ion].temperature_norm
    q = gkids.flux_surface.q
    magn_shear = gkids.flux_surface.magnetic_shear_r_minor
    beta = gkids.species_all.beta_reference
    collisionality = gkids.collisions.collisionality_norm[0][1]
    elongation = gkids.flux_surface.elongation
    #ti_te = ids.
    dico_param_value ={ 'ktheta' : ktheta,
                        'rlne': rlne,
                        'rlte': rlte,
                        'rlni': rlni,
                        'rlti': rlti,
                        'q' : q,
                        'magn_shear': magn_shear,
                        'ti_te': ti_te,
                        'beta': beta,
                        #'alpha': r'$\alpha_{MHD}$ ',
                        'nu_star': collisionality,
                        'elongation':elongation,
                        'elong': elongation}
    
    if param!=None:
        if param=='rln':
            param='rlne'
        if param=='rlt':
            param='rlti'
        if param=='ky':
            param='ktheta'
        del dico_param_value[param]
        
    title_param = ''
    for key, value in dico_param_value.items():
        if key == 'beta':
            title_param += dico_param[key]  + r': ' + f'{round(value*100,2)}' + '%'+ r'| '
        elif key == 'nu_star':
            title_param += dico_param[key]+r': ' + f'{round((value),2)}' + r'| '
        elif key == 'elong':
            title_param = title_param
        else:
            title_param += dico_param[key] + r': ' + f'{round(value,2)}' + r'| '

    return title_param[:-2]

###################################
###################################
#######       PART 2        #######
#######  GK CODES PYTHON    #######
###################################
###################################

def save_gk_ids(gk_ids, name, save_path, overwrite=False):
    full_save_path = save_path+"/"+name 
    imaspy_loaded=False # remove this line when imaspy save will be operational
    if imaspy_loaded:
        imaspytools.ids_save_hdf5(gk_ids, occurrence=0, save_path=full_save_path)
    else:
        print("******WARNING*******")
        print("Be careful, the linear_weights_rotating_frame.momentum_tor_parallel_a_field_parallel is set to a float = 0 because the open function from idspy_tookit doesn't work if it is not well set up...")
        print("********************")
        idspy_toolkit.ids_to_hdf5(gk_ids, full_save_path, overwrite=overwrite)
    print(f"Save of {name} gk IDS in folder {save_path}")

def open_gk_ids(name, save_path):
    full_save_path = save_path +"/"+name
    imaspy_loaded=False # remove this line when imaspy save will be operational
    if imaspy_loaded:
        return imaspytools.ids_open_hdf5(full_save_path)
    else:
        gk_ids = ids_gyrokinetics_local.GyrokineticsLocal()
        idspy_toolkit.fill_default_values_ids(gk_ids)
        return idspy_toolkit.hdf5_to_ids(full_save_path, gk_ids)

# TO IMPROVE
def param_modification(gkids, param, value):
    gkids_modified = copy.deepcopy(gkids)

    for ii in range(len(gkids.species)):
        if gkids.species[ii].charge_norm == -1: #electron:
            Kn = gkids.species[ii].density_log_gradient_norm

    for ii in range(len(gkids.species)):
        if gkids.species[ii].mass_norm== 1: #deuterium:
            KT = gkids.species[ii].temperature_log_gradient_norm

    if param == 'ktheta' or param== 'ky':
        gkids_modified.linear.wavevector[0].binormal_wavevector_norm = value
    if param == 'rlte':
        for sp in range(len(gkids.species)):
            if gkids.species[sp].charge_norm < 0: # electron
                gkids_modified.species[sp].temperature_log_gradient_norm = value 
                break
    if param == 'rlt':
        for sp in range(len(gkids.species)):
            gkids_modified.species[sp].temperature_log_gradient_norm = value *gkids.species[sp].temperature_log_gradient_norm/KT
    if param == 'rlne':
        for sp in range(len(gkids.species)):
            if gkids.species[sp].charge_norm < 0: # electron
                gkids_modified.species[sp].density_log_gradient_norm = value 
                break
    if param == 'rln':
        for sp in range(len(gkids.species)):
            gkids_modified.species[sp].density_log_gradient_norm = value *gkids.species[sp].density_log_gradient_norm/Kn
    if param =='q':
        gkids_modified.flux_surface.q = value
    if param == 'magn_shear':
        gkids_modified.flux_surface.magnetic_shear_r_minor = value 
    if param == 'elongation':
        gkids_modified.flux_surface.elongation = value 
    if param == 'elong':
        gkids_modified.flux_surface.elongation = value 
    if param == 'beta':
        gkids_modified.species_all.beta_reference = value /100 # %
    if param =='ti_te':
        ind_main_ion = 0
        val_main_ion = 0
        for sp in range(len(gkids.species)):
            if gkids.species[sp].charge_norm < 0:
                if gkids.species[sp].density_norm > val_main_ion:
                    val_main_ion = gkids.species[sp].density_norm 
                    ind_main_ion = sp
        gkids_modified.species[ind_main_ion].temperature_norm = value
    if param =='rlni':
        ind_main_ion = 0
        val_main_ion = 0
        for sp in range(len(gkids.species)):
            if gkids.species[sp].charge_norm < 0:
                if gkids.species[sp].density_norm > val_main_ion:
                    val_main_ion = gkids.species[sp].density_norm 
                    ind_main_ion = sp
        gkids_modified.species[ind_main_ion].density_log_gradient_norm = value
    if param =='rlti':
        ind_main_ion = 0
        val_main_ion = 0
        for sp in range(len(gkids.species)):
            if gkids.species[sp].charge_norm < 0:
                if gkids.species[sp].density_norm > val_main_ion:
                    val_main_ion = gkids.species[sp].density_norm 
                    ind_main_ion = sp
        gkids_modified.species[ind_main_ion].temperature_log_gradient_norm = value
    return gkids_modified

#===== GKW =====

def guess_param_gkwpath(gkw_runnm):
    """ Give the dico of param/value of param of a gkwrun according to my specific way of denomination of gkwrun 
    INPUTS:
        -   gkw_runnm       a gkw_runnm specific to Enzo denomination : example : MACHINEshot_specification_number_time_number_radius_number_param1_number_param2_number_..._paramN_number
    OUTPUTS:
        -   dictionnary     dico containing every param and number of the gkw_runnm
    """
    parts = gkw_runnm.split('_')
    params = {}
    param_tmp = ''
    for part in parts:
        if check_int(part):
            params[param_tmp[1:]]=str_float(part)
            param_tmp=''
        else:
            param_tmp+='_'+part
    return params

def gkwrun_input_to_dico(gkwobj, param, sp=None):
    """ From a gkwrun object and a param of its inputs, give the all subparameters in a dictionnary with their respectives values
    INPUTS:
        -   gkwobj          a gkwrun
        -   param           the parameter desired (example: gridsize)
        -   sp              specified the species if its one of the species that is desired
    OUPUTS:
        -   param_dico      a dico of the subparameters of the gkw input parameter desired
    """
    param_dico={}
    if isinstance(sp,int):
        for subparam in gkwobj.input[param][sp]:
            param_dico[subparam]=gkwobj.input[param][sp][subparam]
    else:
        for subparam in gkwobj.input[param]:
            param_dico[subparam]=gkwobj.input[param][subparam]
    
    return param_dico

def gkwrun_input_to_dicodico(gkwobj):
    """ From a gkwrun object, give a dico of parameter dico from the fonction gkwrun_input_to_dico()
    INPUTS:
        -   gkwobj          a gkwrun
    OUPUTS:
        -   param_dico      a dico with gkw input parameter as keys and as value dico of their respectives subparameters that are obtained with the function gkwrun_input_to_dico() 
    """
    dicodico_param={}
    sp=0
    for param in gkwobj.input:
        if str(param) =='species':
            dicodico_param['species_'+str(sp)]=gkwrun_input_to_dico(gkwobj, str(param), sp=sp)
            sp+=1
        else:
            dicodico_param[str(param)]=gkwrun_input_to_dico(gkwobj, str(param))
    return dicodico_param

# STILL IN CONSTRUCTION: NEED TO DEALS WITH THE CASE OF SPECIES (I don't need it so I skip it for the moment...)
def gkw_scan_creation(gkwobj_ref, param, subparam, scan_list, gkw_fullrunpath_origin,  gkw_projpath=None, gkw_runnm_root=None, verbose=False):
    """
    INPUTS:
        -   gkwobj_ref                  a gkwrun object of reference for the scan
        -   param                       the input parameter in which the subparameter that will be scanned is located
        -   subparam                    the subparameter on which the scan will be done
        -   scan_list                   can be either a float or int list, if int list the value in the gkw input will be int
        -   gkw_fullrunpath_origin      the complete path of the run on which every other param will be used : NOT USEFUL, EVERY DATA IS CONTAINED IN THE gkwobj_ref NORMALLY
        -   gkw_projpath                if the user doesn't want to change where the gkw inputs file is located, it is useless to specify it. Otherwise, the user can explecitely put the path of the gkw project where he wants to store the scan inputs
        -   gkw_runnm_root              the root of the scan inputs, if it is not specified, it is the one of the runnm of reference in the gkw_fullrunpath_origin that is taken
    OUTPUTS: None (inputs are created in the gkw project inputs specified in this function)
    """
    
    # ref=gkwrun.GKWrun() 
    # ref.set_storage(gkw_projpath_origin,'multiple_folders',gkw_runnm_origin) 
    # ref.get_inputs()
    gkw_projpath_origin=two_folders_before(gkw_fullrunpath_origin)
    gkw_runnm_origin=os.path.basename(gkw_fullrunpath_origin)
    if gkw_projpath==None:
        gkw_projpath=gkw_projpath_origin
    if gkw_runnm_root==None:
        gkw_runnm=gkw_runnm_origin
    for ii in scan_list:
        gkw_runnm=gkw_runnm_origin+'_'+subparam+'_'+float_str(float(ii))
        gkwobj_scan=copy.deepcopy(gkwobj_ref)
        gkwobj_scan.settings['verbose']=verbose
        gkwobj_scan.set_storage(gkw_projpath_origin,'multiple_folders',gkw_runnm)
        gkwobj_scan.get_inputs()
        gkwobj_scan.input[param][subparam]=ii
        gkwobj_scan.write_input(gkw_projpath+'/input/'+gkw_runnm)
    print(f"===GKW INPUTS FOR SCAN READY IN FILE {gkw_projpath}===")

def gkw_scan_creation_from_a_gkids(gk_ids, param, scan_list, gkw_fullrunpath_origin,  gkw_runnm_root=None, gkw_projpath=None):
    '''Take a gk_ids and the file reference to create a gkw inputs file for a scan on the param defined in the function
    INPUT:
        -   gk_ids                      gyrokinetic_local ids of reference for the scan
        -   param                       str available in the param_dico keys            
        -   scan_list                   list of values for the param scanned
        -   gkw_fullrunpath_origin      the complete path of the run on which every other param will be used : NOT USEFUL, EVERY DATA IS CONTAINED IN THE gkwobj_ref NORMALLY
        -   gkw_projpath                if the user doesn't want to change where the gkw inputs file is located, it is useless to specify it. Otherwise, the user can explecitely put the path of the gkw project where he wants to store the scan inputs
        -   gkw_runnm_root              the root of the scan inputs, if it is not specified, it is the one of the runnm of reference in the gkw_fullrunpath_origin that is taken
     OUTPUTS: None (creation of gkw inputs)
    '''
    log=[]
    if not(param in dico_param.keys()):
        msg=f"The parameters {param} is not a parameter currently available for scan. PLease chekc the param_dico function and modify if it is necessary."
        msg_log(msg, log, verbose=True, critical=True)
    gkw_projpath_origin=two_folders_before(gkw_fullrunpath_origin)+'/'
    if gkw_projpath==None:
        gkw_projpath=gkw_projpath_origin
    gk_ids_list=scan_gk_ids(gk_ids, param, scan_list)
    for ii,scanval in enumerate(scan_list):
        gkw_runnm=gkw_runnm_root+'_'+param+'_'+float_str(scanval)
        gkw_fullrunpath=gkw_projpath+'input/'+gkw_runnm
        gkids_to_inputs_gkw(gk_ids_list[ii], gkw_fullrunpath = gkw_fullrunpath, gkw_fullrunpath_origin = gkw_fullrunpath_origin)
    print(f"===GKW INPUTS FOR SCAN READY IN FILE {gkw_projpath}===")

def gkw_scan2D_creation_from_a_gkids(gk_ids, param1, param2, scan_list1, scan_list2, gkw_fullrunpath_origin, gkw_runnm_root=None, gkw_projpath=None ):
    '''Take a gk_ids and the file reference to create a gkw inputs file for a 2D scan on the 2 params defined in the function
    INPUT:
        -   gk_ids                      gyrokinetic_local ids of reference for the scan
        -   param1                       str available in the param_dico keys            
        -   scan_list1                   list of values for the param scanned
        -   param2                       str available in the param_dico keys            
        -   scan_list2                   list of values for the param scanned
        -   gkw_fullrunpath_origin      the complete path of the run on which every other param will be used : NOT USEFUL, EVERY DATA IS CONTAINED IN THE gkwobj_ref NORMALLY
        -   gkw_projpath                if the user doesn't want to change where the gkw inputs file is located, it is useless to specify it. Otherwise, the user can explecitely put the path of the gkw project where he wants to store the scan inputs
        -   gkw_runnm_root              the root of the scan inputs, if it is not specified, it is the one of the runnm of reference in the gkw_fullrunpath_origin that is taken
     OUTPUTS: None (creation of gkw inputs)
    '''
    gk_ids_list1 = scan_gk_ids(gk_ids, param1, scan_list1)
    for ii,gk_idss in enumerate(gk_ids_list1):
        gkw_runnm_root_scan=gkw_runnm_root+'_'+param1+'_'+float_str(scan_list1[ii])
        gkw_scan_creation_from_a_gkids(gk_idss, param2, scan_list2, gkw_fullrunpath_origin,  gkw_runnm_root=gkw_runnm_root_scan, gkw_projpath=gkw_projpath)

def read_gkw_run_to_gkwrun_obj(gkw_fullrunpath, verbose=False):
    """TO IMPLEMENT INTO read_gkw_run function below
    WORKS ONLY IN mutiple_folders method
    determine the gkwprojpath in function of the run path
    INPUTS:
        -   gkw_fullrunpath             the full path of a gkw run
    OUTPUTS:
        -   gk_obj                      the gkw run object corresponding to the gkw run
    """
    gkwobj=gkwrun.GKWrun() 
    gkwobj.settings['verbose']=verbose
    gkw_projpath=two_folders_before(gkw_fullrunpath)
    gkw_runname=os.path.basename(gkw_fullrunpath) 
    print(gkw_projpath)
    print(gkw_runname)
    gkwobj.set_storage(gkw_projpath,'multiple_folders',gkw_runname) 
    gkwobj.read()
    return gkwobj

def read_gkw_run_to_gkids(gkw_fullrunpath = '', verbose=False, provider='', idssave_path=None, overwrite=False): # NEED VERIFICATION
    """WORKS ONLY IN mutiple_folders method
    INPUTS:
        -   gkw_fullrunpath             the full path of a gkw run
    OUTPUTS: 
        -   gk_ids                      the gyrokinetic_local ids corresponding to the gkw run
    """
    gkw_runnm=os.path.basename(gkw_fullrunpath)
    gkw_runname="gkids_gkw_"+gkw_runnm
    if isinstance(idssave_path, str) and os.path.isfile(idssave_path+'/'+gkw_runname) and overwrite==False:
        print(f"The h5 IDS corresponding to the {idssave_path} path exists, by default we take this file. If you want to overwrite it, please specify it in the intputs of run_tglf().")
        IDS_gkw = open_gk_ids(gkw_runname, idssave_path)
    else:
        gkw_projpath=two_folders_before(gkw_fullrunpath)
        tt=gkwrun.GKWrun() 
        tt.settings['verbose']=verbose
        print("gkwprojpath: ", gkw_projpath)
        print("gkw_runnm: ", gkw_runnm)
        tt.set_storage(gkw_projpath,'multiple_folders',gkw_runnm) 
        tt.read()
        tt.get_kykxs(prec="dp")
        tt.get_kykxs_spectra()
        tt.get_transport_fluxes()
        IDS_gkw =gkwimas.gkw2ids(tt,Nsh=21,provider=provider) 
        IDS_gkw.ids_properties.name = gkw_runnm
        if idssave_path!=None:
            save_gk_ids(IDS_gkw, gkw_runname, idssave_path, overwrite=overwrite)

    return IDS_gkw

def gkids_to_inputs_gkw(gk_ids, gkw_fullrunpath = '', gkw_fullrunpath_origin = ''):#WaltzL_ref file modified (number of CPU allocated in GRIDSIZE different)
    """WORKS ONLY IN mutiple_folders method
    INPUTS:
        -   gk_ids                      the gyrokinetic_local ids corresponding to the gkw run
        -   gkw_fullrunpath             the full path of a gkw run
        -   gkw_fullrunpath_origin      the origin full gkw run path for take the same settings (numerical settings)
    OUTPUTS: None (gkw inputs files in the gkw_fullrunpath)
    """
    gkw_projpath=two_folders_before(gkw_fullrunpath)+'/'
    gkw_runnm=os.path.basename(gkw_fullrunpath)
    gkw_projpath_origin=two_folders_before(gkw_fullrunpath_origin)
    gkw_runnm_origin=os.path.basename(gkw_fullrunpath_origin)
    ref=gkwrun.GKWrun() 
    ref.set_storage(gkw_projpath_origin,'multiple_folders',gkw_runnm_origin) 
    ref.get_inputs()
    dum=gkwimas.ids2gkw(gk_ids,'linear',params_source='gkwref',gkwref=ref) 
    dum.write_input(gkw_projpath+'input/'+gkw_runnm)

# IN CONSTRUCTION: 
def scan_gkids_to_gkw_inputs_from_gkw_runs(gk_ids, param='', param_root='', scan_list=[], gkw_runnm_root='', gkw_runnm_origin_root = '', gkw_projpath='', gkw_projpath_origin = ''): # NEED MSG_LOG CONDITIONS 
    '''Take a list of gkw run already ran and one ids to create the equivalent list of gkw run input with the gk ids in enter
    TO IMPROVE!!! AND INCREASE THE MODULARITY BY USING MORE SUBFUNCTIONS
    '''
    if param_root=='':
        param_root=param
    gk_ids_list = scan_gk_ids(gk_ids, param, scan_list)
    for ii, scanval in enumerate(scan_list):
        gkw_runnm=gkw_runnm_root+f"_{param}_{float_str(scanval)}"
        gkw_runnm_origin=gkw_runnm_origin_root+f"_{param_root}_{float_str(scanval)}"
        gkids_to_inputs_gkw(gk_ids_list[ii], gkw_fullrunpath=gkw_projpath+'input/'+gkw_runnm, gkw_fullrunpath_origin=gkw_projpath_origin+'input/'+gkw_runnm_origin)

#===== TGLF =====

def run_tglf(gk_ids, runname, runpath='/Home/EV273809/Documents/These/TGLF/runs/', param=None, verbose=False, provider='', idssave_path=None, overwrite=False):
    '''
    INPUTS:
        -   gk_ids          gk ids
        -   runname         name of the run that the user will create
        -   runpath         folder where the run will be stored
        -   param           specific if this function is used in a scan : for some parameters scanned
        (ktheta)  some tglf numerical inputs are set
        -   provider            name of the person that generates the gk ids
    OUTPUTS:
        -   TGLF_gk_ids     the ouptput gk ids
    '''
    if not runname[0:11]=="gkids_tglf_":
        tglf_runname="gkids_tglf_"+runname
    else:
        tglf_runname=runname
    if isinstance(idssave_path, str) and os.path.isfile(idssave_path+'/'+tglf_runname) and overwrite==False:
        print(f"The h5 IDS corresponding to the {idssave_path} path exists, by default we take this file. If you want to overwrite it, please specify it in the intputs of run_tglf().")
        IDS_tglf = open_gk_ids(tglf_runname, idssave_path)
    else:
        # On TGLF with Anass script
        tglf_object = tglfrun.TGLFrun()
        tglf_object.settings['verbose']=verbose
        tglf_object = tglfimas.ids2tglf(gk_ids) #, geometry_flag = geometry_flag_tglf) # FOR S ALPHA : TO REDO IF I NEED IT IN THE ANASS SCRIPT
        # Set the storage path for the modified TGLFrun object
        tglf_object.set_storage(runpath+tglf_runname)
        # tglf_object.input['USE_MHD_RULE'] = True
        if param=='ktheta' or param=='ky':
            tglf_object.input['FILTER'] = 0
            # Write the modified input parameters to the input file
            tglf_object.input['KYGRID_MODEL'] = 0 #seulement qd on fait le scan en ky
            tglf_object.input['NKY'] = 120 #seulement qd on fait le scan en ky
        tglf_object.write_input()
        # Run TGLF using the modified input file
        tglf_object.run()
        tglf_object.read()
        # Convert TGLFrun object to an IDS object
        IDS_tglf = tglfimas.tglf2ids(tglf_object, provider=provider)
        IDS_tglf.ids_properties.name = tglf_runname

        if idssave_path!=None:
            save_gk_ids(IDS_tglf, tglf_runname, idssave_path, overwrite=overwrite)

    return IDS_tglf

def run_tglf_gkids_list(gk_ids_list, runpath='/Home/EV273809/Documents/These/TGLF/runs/', param=None, verbose=False, provider='', idssave_path=None, overwrite=False):
    '''
    INPUTS:
        -   gk_ids_list         gk ids provided by a gk code with the run name
                                stored in the gk_ids.ids_properties.name
        -   runpath             folder where the run will be stored
        -   param               specific if this function is used in a scan : 
                                for some parameters scanned (ktheta) some 
                                tglf numerical inputs are set
    OUTPUTS:
        -   TGLF_gk_ids         the ouptput gk ids
    '''
    gk_ids_tglf_list = []
    ngkidslist=len(gk_ids_list)
    for ii,gk_ids in enumerate(gk_ids_list):
        print(f"===RUN TGLF {ii+1}/{ngkidslist}===")
        gk_ids_tglf_list.append(run_tglf(gk_ids, gk_ids.ids_properties.name, runpath=runpath,param=param, verbose=verbose, provider=provider, idssave_path=idssave_path, overwrite=overwrite))

    return gk_ids_tglf_list

#===== QLK =====

def run_qlk(gk_ids, runname, runpath='/Home/EV273809/Documents/These/QuaLiKiz/runs/', qlkpath='/Home/EV273809/Documents/These/QuaLiKiz', provider='', CPU=1, verbose=False, idssave_path=None, overwrite=False):
    '''
    INPUTS:
        -   gk_ids              gk ids
        -   runname             name of the run that the user will create
        -   runpath             folder where the run will be stored
        -   qlkpath             folder where the QuaLiKiz executable is stored
        -   provider            name of the person that generates the gk ids
        -   CPU                 number of CPU used for the simulation
    OUTPUTS:
        -   QLK_gk_ids          the ouptput gk ids
    '''
    if not runname[0:10]=="gkids_qlk_":
        qlk_runname = "gkids_qlk_"+runname
    else:
        qlk_runname = runname
    if isinstance(idssave_path, str) and os.path.isfile(idssave_path+'/'+qlk_runname) and overwrite==False:
        print(f"The h5 IDS corresponding to the {idssave_path} path exists, by default we take this file. If you want to overwrite it, please specify it in the intputs of run_tglf().")
        IDS_qlk = open_gk_ids(qlk_runname, idssave_path)
    else:        
        qlk_object = QLK_class()
        qlk_object.load_IMAS_ids(ids=gk_ids)
        qlk_object.run_QLK(qlk_runname, runpath, qlkpath, CPU=CPU, verbose=verbose)
        qlk_object.load_runfolder(runpath+qlk_runname)
        IDS_qlk = qlk_object.to_IMAS_ids(provider)
        if idssave_path!=None:
            save_gk_ids(IDS_qlk, qlk_runname, idssave_path, overwrite=overwrite)
    
    return IDS_qlk

def run_qlk_gkids_list(gk_ids_list, runpath='/Home/EV273809/Documents/These/QuaLiKiz/runs/', qlkpath='/Home/EV273809/Documents/These/QuaLiKiz', provider='', idssave_path=None, overwrite=False, CPU=1,verbose=False):
    '''
    INPUTS:
        -   gk_ids_list         gk ids provided by a gk code with the run name
                                stored in the gk_ids.ids_properties.name
        -   runpath             folder where the run will be stored
        -   qlkpath             folder where the QuaLiKiz executable is stored
        -   provider            name of the person that generates the gk ids
        -   CPU                 number of CPU used for the simulation

    OUTPUTS:
        -   QLK_gk_ids         the ouptput gk ids
    '''
    gk_ids_qlk_list = []
    ngkidslist=len(gk_ids_list)
    for ii,gk_ids in enumerate(gk_ids_list):
        print(f"===RUN QuaLiKiz {ii+1}/{ngkidslist}===")
        gk_ids_qlk_list.append(run_qlk(gk_ids, gk_ids.ids_properties.name, runpath=runpath, qlkpath=qlkpath, provider=provider, idssave_path=idssave_path, overwrite=overwrite, CPU=CPU, verbose=verbose))

    return gk_ids_qlk_list


###################################
###################################
#######       PART 3        #######
####### GK CODES BAsIC SCANS ######
###################################
###################################

def scan_gk_ids(gk_ids, param, scan_list):
    """
    INPUTS:
        -   gk_ids          gyrokinetic_local ids on which the scan will be done
        -   param           parameter (string) on which the scan will be done
        -   scan_list       list of float giving the numerical value of the scan
    OUTPUTS:
        -   gk_ids_list     list of the gk ids from the scan
    """
    if param not in list(dico_param):
        print("Please choose a scan parameter between those: ", list(dico_param))
        return 
    
    gk_ids_list=[]

    for scan_value in scan_list:
        gk_ids_list.append(param_modification(gk_ids, param, scan_value))

    return gk_ids_list

def scan_TGLF(gk_ids, param = 'ktheta', scan_list = np.linspace(0.1,2.0,4), runname_root='', runpath = '/Home/EV273809/Documents/These/TGLF/runs/', idssave_path = '', overwrite=False, verbose=False, provider=''):# NEED VERIFICATION
    """
    INPUTS:
        -   gk_ids          gyrokinetic_local ids on which the scan will be done
        -   param           parameter (string) on which the scan will be done
        -   scan_list       list of float giving the numerical value of the scan
        -   runname_root    
        -   runpath
        -   idssave_path    IN CONSTRUCTION : string for the path of the gk ids of the runs save
    OUTPUTS:
        -   gk_ids_list     list of the gk ids from the scan
    """
    gk_ids_list=scan_gk_ids(gk_ids, param, scan_list)
    for ii,gk_ids in enumerate(gk_ids_list):
        if runname_root=='':
            gk_ids.ids_properties.name=f"gkids_tglf_{param}_{float_str(scan_list[ii])}"
        else:
            gk_ids.ids_properties.name=f"gkids_tglf_{runname_root}_{param}_{float_str(scan_list[ii])}"

    return run_tglf_gkids_list(gk_ids_list,runpath=runpath,param=param, verbose=verbose, provider=provider, idssave_path = idssave_path, overwrite=overwrite)

def scan_QLK(gk_ids, param = 'ktheta', scan_list = np.linspace(0.1,2.0,4), runname_root='', runpath = '/Home/EV273809/Documents/These/QuaLiKiz/runs/', qlkpath='/Home/EV273809/Documents/These/QuaLiKiz/', provider='', CPU=1, idssave_path = '', overwrite=False, verbose=False):# NEED VERIFICATION
    """
    INPUTS:
        -   gk_ids          gyrokinetic_local ids on which the scan will be done
        -   param           parameter (string) on which the scan will be done
        -   scan_list       list of float giving the numerical value of the scan
        -   runname_root
        -   runpath
        -   qlkpath
        -   provider
        -   CPU
        -   idssave_path    IN CONSTRUCTION : string for the path of the gk ids of the runs save
    OUTPUTS:
        -   gk_ids_list     list of the gk ids from the scan
    """
    gk_ids_list=scan_gk_ids(gk_ids, param, scan_list)
    for ii,gk_ids in enumerate(gk_ids_list):
        if runname_root!='':
            gk_ids.ids_properties.name=f"gkids_qlk_{param}_{float_str(scan_list[ii])}"
        else:
            gk_ids.ids_properties.name=f"gkids_qlk_{runname_root}_{param}_{float_str(scan_list[ii])}"
    
    return run_qlk_gkids_list(gk_ids_list,runpath=runpath,qlkpath=qlkpath,provider=provider,idssave_path =idssave_path, overwrite=overwrite, CPU=CPU, verbose=verbose)

def scan_GKW(param = 'ktheta', scan_list = np.linspace(0.1,2.0,4), gkw_flpath = '', gkw_flnm = '',  gkw_flroot = '', idssave_path = '', overwrite=False, verbose=False, provider=''):# NEED VERIFICATION
    """
    INPUTS:
        -   gk_ids          gyrokinetic_local ids on which the scan will be done
        -   param           parameter (string) on which the scan will be done
        -   list_scan       list of float giving the numerical value of the scan
        -   runsave_path    string for the path of the creation of the runs
        -   idssave_path    IN CONSTRUCTION : string for the path of the gk ids of the runs save
    OUTPUTS:
        -   gk_ids_list     list of the gk ids from the scan
    """
    if param not in list(dico_param):
        print("Please choose a scan parameter between those: ", list(dico_param))
        return 
    
    gk_ids_list=[]
    for rr in range(len(scan_list)):
        print('\n********************************************')
        print(f'***      SCAN STATUS: VALUE {rr+1}/{len(scan_list)}      ***')
        print('********************************************\n')
        
        gkw_runnm = gkw_flroot +f'_{param}_'+float_str(scan_list[rr]) 
        IDS_gkw = read_gkw_run_to_gkids(gkw_fullrunpath = gkw_flpath+gkw_flnm+'/input/'+gkw_runnm,  verbose=verbose, provider=provider, idssave_path=idssave_path)

        gk_ids_list.append(IDS_gkw)

        print('\n*********************************************')
        print('****   CONGRATS!!! GKW GK IDS ready!!!   ****')
        print('*********************************************\n')

    return gk_ids_list

###################################
###################################
#######       PART 4        #######
#######  POST SCAN TREAMENT #######
###################################
###################################


import re

def get_nested_attr(obj, attr_path, **kwargs):
    """
    Dynamically access a nested attribute, supporting placeholders in curly braces
    (e.g., {index}, {option}) and mixed attribute paths.
    INPUTS:
        -obj            The root object to access attributes from.
        -attr_path      The path to the attribute as a string, e.g.,
                      "branch1[0].branch2[{index1}].branch_3_{option}[{index2}].values".
        -kwargs         Dynamic arguments to replace placeholders (e.g., {index1}, {option}, {index2}).
    OUTPUTS:
        -return         The value of the specified nested attribute.
    """
    # Replace placeholders (e.g., {index1}) with actual values from kwargs
    def replace_placeholders(match):
        key = match.group(1)  # Extract the placeholder key inside the braces
        if key in kwargs:
            return str(kwargs[key])
        else:
            raise ValueError(f"Missing value for placeholder: {key}")
        
    # Use regex to replace all placeholders in the path
    formatted_path = re.sub(r"\{(\w+)\}", replace_placeholders, attr_path)
 
    # Split the path into parts based on '.' to navigate through nested attributes
    parts = formatted_path.split('.')
    current_obj = obj

    for part in parts:
        if "[" in part and "]" in part:  # Handle list indexing, e.g., "branch1[0]"
            # Extract the attribute name and index
            attr_name, index = part[:-1].split("[")
            index = int(index)  # Convert the index to an integer
            current_obj = getattr(current_obj, attr_name)[index]
        else:  # Handle direct attribute access
            current_obj = getattr(current_obj, part)
            
    return current_obj

def eigenvalues_gkids_list(gk_ids_list):# NEED VERIFICATION
    """
    BE CAREFUL: give only the growth rate of the last wavevector
    INPUTS:
        -   gk_ids_list     list of the gk ids
    OUPTUS:
        -   gr_array         np.array of the corresponding growth rate of the last ky in the same order of the gk ids in the list
        -   freq_array       np.array of the corresponding real frequencies of the last ky in the same order of the gk ids in the list
    """
    gr_list = []
    freq_list = []
    for gg in gk_ids_list:
        gr_list.append(gg.linear.wavevector[-1].eigenmode[0].growth_rate_norm)
        freq_list.append(gg.linear.wavevector[-1].eigenmode[0].frequency_norm)
    return np.asarray(gr_list), np.asarray(freq_list)

def contribution_linear_field_gkids_list(gk_ids_list, eigenmode=0): # NEED VERIFICATION AND COMPREHENSION OF THE DATA
    """
    INPUTS:
        -   gk_ids_list     list of the gk ids with linear field filld
    OUPTUS:
        -   phi_list        list of the electrostatic potential contribution of the flux weights
        -   apar_list       list of the A_parallel field contribution of the flux weights
        -   bpar_list       list of the B_parallel field potential contribution of the flux weights
    """
    phi_list, apar_list, bpar_list = [], [], []
    for gg in gk_ids_list:
        phi_list.append([get_nested_attr(gg,'linear.wavevector[-1].eigenmode[{eigenmode}].fields.phi_potential_perturbated_norm', eigenmode=eigenmode), # complex
                         get_nested_attr(gg,'linear.wavevector[-1].eigenmode[{eigenmode}].fields.phi_potential_perturbated_weight', eigenmode=eigenmode)])
        apar_list.append([get_nested_attr(gg,'linear.wavevector[-1].eigenmode[{eigenmode}].fields.a_field_parallel_perturbated_norm', eigenmode=eigenmode), # complex
                         get_nested_attr(gg,'linear.wavevector[-1].eigenmode[{eigenmode}].fields.a_field_parallel_perturbated_weight', eigenmode=eigenmode)])
        bpar_list.append([get_nested_attr(gg,'linear.wavevector[-1].eigenmode[{eigenmode}].fields.b_field_parallel_perturbated_norm', eigenmode=eigenmode), # complex
                         get_nested_attr(gg,'linear.wavevector[-1].eigenmode[{eigenmode}].fields.b_field_parallel_perturbated_weight', eigenmode=eigenmode)])
                        
    return np.array(phi_list), np.array(apar_list), np.array(bpar_list)

def contribution_weights_gkids_list(gk_ids_list, eigenmode=0, option='rotating_frame'): # NEED VERIFICATION
    """
    INPUTS:
        -   gk_ids_list     list of the gk ids with linear field filld
    OUPTUS:
        -   phi_list        list of the electrostatic potential contribution of the flux weights
        -   apar_list       list of the A_parallel field contribution of the flux weights
        -   bpar_list       list of the B_parallel field potential contribution of the flux weights
    """
    phi_list, apar_list, bpar_list = [], [], []
    for gg in gk_ids_list:
        phi_list.append([get_nested_attr(gg,'linear.wavevector[-1].eigenmode[{eigenmode}].linear_weights_{option}.particles_phi_potential', eigenmode=eigenmode, option=option),
                         get_nested_attr(gg,'linear.wavevector[-1].eigenmode[{eigenmode}].linear_weights_{option}.energy_phi_potential', eigenmode=eigenmode, option=option)])
        apar_list.append([get_nested_attr(gg,'linear.wavevector[-1].eigenmode[{eigenmode}].linear_weights_{option}.particles_a_field_parallel', eigenmode=eigenmode, option=option),
                         get_nested_attr(gg,'linear.wavevector[-1].eigenmode[{eigenmode}].linear_weights_{option}.energy_a_field_parallel', eigenmode=eigenmode, option=option)])
        bpar_list.append([get_nested_attr(gg,'linear.wavevector[-1].eigenmode[{eigenmode}].linear_weights_{option}.particles_b_field_parallel', eigenmode=eigenmode, option=option),
                         get_nested_attr(gg,'linear.wavevector[-1].eigenmode[{eigenmode}].linear_weights_{option}.energy_b_field_parallel', eigenmode=eigenmode, option=option)])

    return np.array(phi_list), np.array(apar_list), np.array(bpar_list)

def contribution_fluxes_gkids_list(gk_ids_list, option='rotating_frame'):# NEED VERIFICATION
    """
    Need several ky in each gk_ids of the gk_ids_list noramlly if the non_linear field is filled
    INPUTS:
        -   gk_ids_list     list of the gk ids with non_linear field filled
    OUPTUS:
        -   phi_list        list of the electrostatic potential contribution of the fluxes
        -   apar_list       list of the A_parallel field contribution of the fluxes
        -   bpar_list       list of the B_parallel field potential contribution of the fluxes
    """
    phi_list, apar_list, bpar_list = [], [], []
    for gg in gk_ids_list:
        phi_list.append([getattr(gg,f'non_linear.fluxes_1d_{option}.particles_phi_potential'),
                         getattr(gg,f'non_linear.fluxes_1d_{option}.energy_phi_potential')])
        apar_list.append([getattr(gg,f'non_linear.fluxes_1d_{option}.particles_a_field_parallel'),
                         getattr(gg,f'non_linear.fluxes_1d_{option}.energy_a_field_parallel')])
        bpar_list.append([getattr(gg,f'non_linear.fluxes_1d_{option}.particles_b_field_parallel'),
                         getattr(gg,f'non_linear.fluxes_1d_{option}.energy_b_field_parallel')])

    return np.array(phi_list), np.array(apar_list), np.array(bpar_list)

def linear_fluxes_gkids_list(gk_ids_list):# NEED VERIFICATION
    """
    Need one ky in each gk_ids of the gk_ids_list noramlly if the non_linear field is filled
    INPUTS:
        -   gk_ids_list     list of the gk ids
    OUPTUS:
        -   fluxes_list     list of the fluxes
    """
    phi_list, apar_list, bpar_list = contribution_weights_gkids_list(gk_ids_list)
    nscan=len(gk_ids_list)
    particles_flux = 0
    ion_energy_flux = 0
    electron_energy_flux = 0
    if np.shape(phi_list)==(nscan,2,2):
        particles_flux+=phi_list[:,0,0]
        ion_energy_flux+=phi_list[:,1,1]
        electron_energy_flux+=phi_list[:,1,0]
    if np.shape(apar_list)==(nscan,2,2):
        particles_flux+=apar_list[:,0,0]
        ion_energy_flux+=apar_list[:,1,1]
        electron_energy_flux+=apar_list[:,1,0]
    if np.shape(bpar_list)==(nscan,2,2):
        particles_flux+=bpar_list[:,0,0]
        ion_energy_flux+=bpar_list[:,1,1]
        electron_energy_flux+=bpar_list[:,1,0]
        
    return particles_flux, ion_energy_flux, electron_energy_flux

def linear_diffusivities_gkids_spectrum_list(gk_ids_list):
    part_flux, ion_flux, elec_flux = linear_fluxes_gkids_list(gk_ids_list)
    nspectrum = len(gk_ids_list)
    Te_norm, Ti_norm, ne_norm, ni_norm, dTe_norm, dTi_norm = [], [], [], [], [], []
    for ky in range(nspectrum):
        Te_norm.append(gk_ids_list[ky].species[0].temperature_norm)
        ne_norm.append(gk_ids_list[ky].species[0].density_norm)
        dTe_norm.append(gk_ids_list[ky].species[0].temperature_log_gradient_norm)
        Ti_norm.append(gk_ids_list[ky].species[1].temperature_norm)
        ni_norm.append(gk_ids_list[ky].species[1].density_norm)
        dTi_norm.append(gk_ids_list[ky].species[1].temperature_log_gradient_norm)
    
    Te_norm=np.asarray(Te_norm)
    ne_norm=np.asarray(ne_norm)
    dTe_norm=np.asarray(dTe_norm)
    Ti_norm=np.asarray(Ti_norm)
    ni_norm=np.asarray(ni_norm)
    dTi_norm=np.asarray(dTi_norm)
    
    qe = elec_flux-3/2*Te_norm*part_flux
    qi = ion_flux-3/2*Ti_norm*part_flux
    
    elec_diff = qe/(ne_norm*Te_norm*dTe_norm)
    ion_diff = qi/(ni_norm*Ti_norm*dTi_norm)
    
    return elec_diff, ion_diff

def nonlinear_fluxes_gkids_list(gk_ids_list):# NEED VERIFICATION
    """
    Need several ky in each gk_ids of the gk_ids_list noramlly if the non_linear field is filled
    INPUTS:
        -   gk_ids_list     list of the gk ids
    OUPTUS:
        -   fluxes_list     list of the fluxes
    """
    phi_list, apar_list, bpar_list = contribution_fluxes_gkids(gk_ids_list)
    fluxes_list=[]
    nscan=len(phi_list)
    for ii in range(nscan):
        fluxes_list.append(phi_list[ii]+apar_list[ii]+bpar_list[ii])
 
    return fluxes_list

def thermal_diffusivities_gkids_list(gk_ids_list):
    thermali_diffusivites_list = []
    freq_list = []
    gr_list = []
    for gg in gk_ids_list:
        temp_diff = 0
        gr_list.append(gg.linear.wavevector[-1].eigenmode[0].growth_rate_norm)
        freq_list.append(gg.linear.wavevector[-1].eigenmode[0].frequency_norm)
    return np.asarray(gr_list), np.asarray(freq_list)

###################################
###################################
#######       PART 5        #######
#######    LATEX TABLES     #######
###################################
###################################

def generate_latex_table(gk_ids_list, label_for_each_gk_ids_dico={}, label_for_all_gk_ids_dico={}, significant_figures=2, title=""):
    """Need periodictable library!!!
    INPUTS:
        -   gk_ids_list                     list of the gk ids
        -   label_for_each_gk_ids_dico      dico containing label for each gk ids meaning that it is a dico with the param as key and a list with the same length as the gk_ids_list one. These lists can contain float, int or str
        -   label_for_all_gk_ids_dico       dico containing label specific to the all gyrokinetic_local IDS in the gk_ids_list. Tbh, it is not very useful, you can manually put these labels in the title
        -   significant_figures             significant figures displayed in the table
        -   title                           I think it is a title
    OUPTUS:None (the latex table is printed in your terminal)
    """
    log = []
    ngkids = len(gk_ids_list)
    
    sp_list = []
    for gk_ids in gk_ids_list:
        for sp in gk_ids.species:
            if sp.charge_norm <0:
                if not 'Electron' in sp_list:
                    sp_list.append('Electron')
            else:
                if not element_name_from_mass(sp.mass_norm*2) in sp_list:
                    sp_list.append(element_name_from_mass(sp.mass_norm*2))
    
    # Commence la chaîne LaTeX
    latex_code = r"""\begin{table}[H]
    \begin{center}
    \caption{"""+str(title)+r"""}
    \label{tab:table_transposed1}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{|c|c|""" + "c|" * len(gk_ids_list) + r"""}
    \hline
    """

    for key, value in label_for_all_gk_ids_dico.items():
        latex_code += r"\multicolumn{2}{|c|}{\textbf{"+key+r"}}"
        latex_code += " & " +  r"\multicolumn{"+str(ngkids)+r"}{c|}{"+str(value)+"}"
        latex_code += r" \\ \hline" + "\n"
    
    for key, value_list in label_for_each_gk_ids_dico.items():
        if len(value_list)!=ngkids:
            msg=f"the {key} in the keys of the label_dico has not a correct value list length corresponding to the number of gyrokinetic_local ids in the gk_ids_list"
            msg_log(msg, log, verbose=True, critical=True)
        latex_code += r"\multicolumn{2}{|c|}{\textbf{"+key+r"}}"
        for value in value_list:
            latex_code += " & " + format_number(value, significant_figures=significant_figures)
        latex_code += r" \\ \hline" + "\n"


    latex_code += r"\multicolumn{2}{|c|}{\textbf{$\beta$ (\%)}}"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.species_all.beta_reference*100, significant_figures=significant_figures)
    latex_code += r" \\ \hline" + "\n"
    # Partie Géométrie
    latex_code += r"\multirow{4}{*}{\textbf{Geometry}} & $q$"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.flux_surface.q, significant_figures=significant_figures)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\hat{s}$"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.flux_surface.magnetic_shear_r_minor, significant_figures=significant_figures)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\kappa$"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.flux_surface.elongation, significant_figures=significant_figures)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $r/R$"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.flux_surface.r_minor_norm, significant_figures=significant_figures)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
        
    latex_code += r" \\ \hline" + "\n"

    for sp_name in sp_list:
        if sp_name=='Electron':
             # Partie Electron
            latex_code += r"\multirow{3}{*}{\textbf{Electron}} & R/L$_T$"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(sp.temperature_log_gradient_norm, significant_figures=significant_figures)
                    
            # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
            latex_code += r" \\ " + "\n"

            latex_code += r"& R/L$_n$"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(sp.density_log_gradient_norm, significant_figures=significant_figures)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $T_e$ (keV)"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.normalizing_quantities.t_e*1e-3, significant_figures=significant_figures)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $n_e$ ($10^{-19}m^{-3}$)"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.normalizing_quantities.n_e*1e-19, significant_figures=significant_figures)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $\nu_{e/e}$"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,0], significant_figures=significant_figures)
                    
            latex_code += r" \\ \hline" + "\n"
        
        else:
            # Partie Ions

                latex_code += r"\multirow{7}{*}{\textbf{"+sp_name+r"}} & R/L$_T$"

                for gk_ids in gk_ids_list:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.temperature_log_gradient_norm, significant_figures=significant_figures)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& R/L$_n$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_log_gradient_norm, significant_figures=significant_figures)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $T_i/T_e$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.temperature_norm, significant_figures=significant_figures)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $n_i/n_e$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_norm, significant_figures=significant_figures)
                    if not sp_find:
                        latex_code += " & {} "
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $\nu_{e/i}$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,ii], significant_figures=significant_figures)

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/e}$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,0], significant_figures=significant_figures)

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/i}$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,ii], significant_figures=significant_figures)

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "


                latex_code += r" \\ \hline" + "\n"
    
    
    
    # Fin du tableau
    latex_code += r"""\end{tabular}}
    \end{center}
    \end{table}
    """
    print(latex_code)
    return latex_code

### Following fonctions : DEPRECATED
def generate_latex_tablev0(shot_list, time_list,  rho_tor_norm,  gk_ids_list, cp_list=[], eq_list=[], significant_figures=2):
    """
    Génère un tableau LaTeX en prenant en entrée plusieurs colonnes de valeurs.
    
    Chaque colonne correspond à une liste de valeurs, et le nombre de colonnes peut varier.
    """
    ngkids = len(gk_ids_list)
    
    sp_list = []
    for gk_ids in gk_ids_list:
        for sp in gk_ids.species:
            if sp.charge_norm <0:
                if not 'Electron' in sp_list:
                    sp_list.append('Electron')
            else:
                if not element_name_from_mass(sp.mass_norm*2) in sp_list:
                    sp_list.append(element_name_from_mass(sp.mass_norm*2))
    
    # Commence la chaîne LaTeX
    latex_code = r"""\begin{table}[H]
    \begin{center}
    \caption{$\rho_{tor,N}="""+str(rho_tor_norm)+r"""$}
    \label{tab:table_transposed1}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{|c|c|""" + "c|" * len(gk_ids_list) + r"""}
    \hline
    """
    
    # Première ligne (titres de colonnes) : Shot
    latex_code += r"\multicolumn{2}{|c|}{\textbf{Shot}}"
    for shot in shot_list:
        latex_code += " & " + str(shot)
    latex_code += r" \\ \hline" + "\n"
    
    # Deuxième ligne : Time (s)
    latex_code += r"\multicolumn{2}{|c|}{\textbf{Time (s)}}"
    for time in time_list:
        latex_code += " & " + str(time)
    latex_code += r" \\ \hline" + "\n"

    campaign_list = corresponding_campaign(shot_list)
    # Troisième ligne : Ip (MA)
    latex_code += r"\multicolumn{2}{|c|}{\textbf{WEST Campaign}}"
    for cc in campaign_list:
        latex_code += " & " + cc
    latex_code += r" \\ \hline" + "\n"
    
    if len(eq_list)!=0:
        # Troisième ligne : Ip (MA)
        latex_code += r"\multicolumn{2}{|c|}{\textbf{$I_P$ (MA)}}"
        for eq in eq_list:
            latex_code += " & " + format_number(np.abs(eq.time_slice[0].global_quantities.ip)*1e-6)
        latex_code += r" \\ \hline" + "\n"

    if len(cp_list)!=0:
        # Quatrième ligne : Zeff
        latex_code += r"\multicolumn{2}{|c|}{\textbf{$Z_{eff,resistive}$}}"
        for cp in cp_list:
            latex_code += " & " + format_number(cp.profiles_1d[0].zeff[0])
        latex_code += r" \\ \hline" + "\n"
    
    # Crinquième ligne : B_tor (T)
    latex_code += r"\multicolumn{2}{|c|}{\textbf{$B_{tor}$ (T)}}"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.normalizing_quantities.b_field_tor)
    latex_code += r" \\ \hline" + "\n"

    # Partie Géométrie
    latex_code += r"\multirow{5}{*}{\textbf{Geometry}} & $q$"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.flux_surface.q)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\hat{s}$"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.flux_surface.magnetic_shear_r_minor)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\kappa$"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.flux_surface.elongation)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $r/R$"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.flux_surface.r_minor_norm)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\beta$ (\%)"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.species_all.beta_reference*100)
    latex_code += r" \\ \hline" + "\n"

    for sp_name in sp_list:
        if sp_name=='Electron':
             # Partie Electron
            latex_code += r"\multirow{3}{*}{\textbf{Electron}} & R/L$_T$"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(sp.temperature_log_gradient_norm)
                    
            # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
            latex_code += r" \\ " + "\n"

            latex_code += r"& R/L$_n$"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(sp.density_log_gradient_norm)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $T_e$ (keV)"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.normalizing_quantities.t_e*1e-3)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $n_e$ ($10^{-19}m^{-3}$)"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.normalizing_quantities.n_e*1e-19)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $\nu_{e/e}$"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,0])
                    
            latex_code += r" \\ \hline" + "\n"
        
        else:
            # Partie Ions

                latex_code += r"\multirow{7}{*}{\textbf{"+sp_name+r"}} & R/L$_T$"

                for gk_ids in gk_ids_list:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.temperature_log_gradient_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& R/L$_n$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_log_gradient_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $T_i/T_e$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.temperature_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $n_i/n_e$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_norm)
                    if not sp_find:
                        latex_code += " & {} "
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $\nu_{e/i}$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,ii])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/e}$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,0])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/i}$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,ii])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "


                latex_code += r" \\ \hline" + "\n"
    
    
    
    # Fin du tableau
    latex_code += r"""\end{tabular}}
    \end{center}
    \end{table}
    """
    print(latex_code)
    return latex_code

def generate_latex_table_radii(shot, time,  rho_tor_norm_list,  gk_ids_list, cp=None, eq=None, significant_figures=2):
    """
    Génère un tableau LaTeX en prenant en entrée plusieurs colonnes de valeurs.
    
    Chaque colonne correspond à une liste de valeurs, et le nombre de colonnes peut varier.
    """
    nrad=len(rho_tor_norm_list)
    


    sp_list = []
    for gk_ids in gk_ids_list:
        for sp in gk_ids.species:
            if sp.charge_norm <0:
                if not 'Electron' in sp_list:
                    sp_list.append('Electron')
            else:
                if not element_name_from_mass(sp.mass_norm*2) in sp_list:
                    sp_list.append(element_name_from_mass(sp.mass_norm*2))
    
    # Commence la chaîne LaTeX
    latex_code = r"""\begin{table}[H]
    \begin{center}
    \caption{Shot:"""+str(shot)+r""" and time slice:"""+str(time)+r""" s.}
    \label{tab:table_transposed1}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{|c|c|""" + "c|" * len(gk_ids_list) + r"""}
    \hline
    """
    
    # Première ligne (titres de colonnes) : Shot

    latex_code += r"\multicolumn{2}{|c|}{\textbf{Shot}}"
    latex_code += " & " +  r"\multicolumn{"+str(nrad)+r"}{c|}{"+str(shot)+"}"
    latex_code += r" \\ \hline" + "\n"

    latex_code += r"\multicolumn{2}{|c|}{\textbf{Time}}"
    latex_code += " & " +  r"\multicolumn{"+str(nrad)+r"}{c|}{"+str(time)+"}"
    latex_code += r" \\ \hline" + "\n"
    
    if eq!=None:
        # Troisième ligne : Ip (MA)
        latex_code += r"\multicolumn{2}{|c|}{\textbf{$I_P$ (MA)}}"
        latex_code += " & " +  r"\multicolumn{"+str(nrad)+r"}{c|}{"+format_number(np.abs(eq.time_slice[0].global_quantities.ip)*1e-6)+"}"
        latex_code += r" \\ \hline" + "\n"

    if cp!=None:
        # Quatrième ligne : Zeff
        latex_code += r"\multicolumn{2}{|c|}{\textbf{$Z_{eff,resistive}$}}"
        latex_code += " & " +  r"\multicolumn{"+str(nrad)+r"}{c|}{"+format_number(cp.profiles_1d[0].zeff[0])+"}"
        latex_code += r" \\ \hline" + "\n"

    latex_code += r"\multicolumn{2}{|c|}{$\rho_{tor,N}$}"
    for rhotor in rho_tor_norm_list:
        latex_code += " & " + str(rhotor)
    latex_code += r" \\ \hline" + "\n"
    
    # Crinquième ligne : B_tor (T)
    latex_code += r"\multicolumn{2}{|c|}{\textbf{$B_{tor}$ (T)}}"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.normalizing_quantities.b_field_tor)
    latex_code += r" \\ \hline" + "\n"

    # Partie Géométrie
    latex_code += r"\multirow{5}{*}{\textbf{Geometry}} & $q$"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.flux_surface.q)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\hat{s}$"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.flux_surface.magnetic_shear_r_minor)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\kappa$"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.flux_surface.elongation)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $r/R$"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.flux_surface.r_minor_norm)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\beta$ (\%)"
    for gk_ids in gk_ids_list:
        latex_code += " & " + format_number(gk_ids.species_all.beta_reference*100)
    latex_code += r" \\ \hline" + "\n"

    for sp_name in sp_list:
        if sp_name=='Electron':
             # Partie Electron
            latex_code += r"\multirow{3}{*}{\textbf{Electron}} & R/L$_T$"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(sp.temperature_log_gradient_norm)
                    
            # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
            latex_code += r" \\ " + "\n"

            latex_code += r"& R/L$_n$"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(sp.density_log_gradient_norm)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $T_e$ (keV)"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.normalizing_quantities.t_e*1e-3)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $n_e$ ($10^{-19}m^{-3}$)"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.normalizing_quantities.n_e*1e-19)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $\nu_{e/e}$"
            for gk_ids in gk_ids_list:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,0])
                    
            latex_code += r" \\ \hline" + "\n"
        
        else:
            # Partie Ions

                latex_code += r"\multirow{7}{*}{\textbf{"+sp_name+r"}} & R/L$_T$"

                for gk_ids in gk_ids_list:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.temperature_log_gradient_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& R/L$_n$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_log_gradient_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $T_i/T_e$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.temperature_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $n_i/n_e$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_norm)
                    if not sp_find:
                        latex_code += " & {} "
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $\nu_{e/i}$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,ii])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/e}$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,0])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/i}$"
                for gk_ids in gk_ids_list:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,ii])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "


                latex_code += r" \\ \hline" + "\n"
    
    
    
    # Fin du tableau
    latex_code += r"""\end{tabular}}
    \end{center}
    \end{table}
    """
    print(latex_code)
    return latex_code

def generate_latex_table_COMPARISON(shot_list, time_list,  rho_tor_norm,  gk_ids_list1, gk_ids_list2, cp_list=[], eq_list=[], significant_figures=2):
    """
    Génère un tableau LaTeX en prenant en entrée plusieurs colonnes de valeurs.
    
    Chaque colonne correspond à une liste de valeurs, et le nombre de colonnes peut varier.
    """
    ngkids = len(gk_ids_list1)
    


    sp_list = []
    for gk_ids in gk_ids_list1:
        for sp in gk_ids.species:
            if sp.charge_norm <0:
                if not 'Electron' in sp_list:
                    sp_list.append('Electron')
            else:
                if not element_name_from_mass(sp.mass_norm*2) in sp_list:
                    sp_list.append(element_name_from_mass(sp.mass_norm*2))
    
    # Commence la chaîne LaTeX
    latex_code = r"""\begin{table}[H]
    \begin{center}
    \caption{$\rho_{tor,N}="""+str(rho_tor_norm)+r"""$}
    \label{tab:table_transposed1}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{|c|c|""" + "c|" * len(gk_ids_list1) + r"""}
    \hline
    """
    
    # Première ligne (titres de colonnes) : Shot
    latex_code += r"\multicolumn{2}{|c|}{\textbf{Shot}}"
    for shot in shot_list:
        latex_code += " & " + str(shot)
    latex_code += r" \\ \hline" + "\n"
    
    # Deuxième ligne : Time (s)
    latex_code += r"\multicolumn{2}{|c|}{\textbf{Time (s)}}"
    for time in time_list:
        latex_code += " & " + str(time)
    latex_code += r" \\ \hline" + "\n"

    campaign_list = corresponding_campaign(shot_list)
    # Troisième ligne : Ip (MA)
    latex_code += r"\multicolumn{2}{|c|}{\textbf{WEST Campaign}}"
    for cc in campaign_list:
        latex_code += " & " + cc
    latex_code += r" \\ \hline" + "\n"
    
    if len(eq_list)!=0:
        # Troisième ligne : Ip (MA)
        latex_code += r"\multicolumn{2}{|c|}{\textbf{$I_P$ (MA)}}"
        for eq in eq_list:
            latex_code += " & " + format_number(np.abs(eq.time_slice[0].global_quantities.ip)*1e-6)
        latex_code += r" \\ \hline" + "\n"

    if len(cp_list)!=0:
        # Quatrième ligne : Zeff
        latex_code += r"\multicolumn{2}{|c|}{\textbf{$Z_{eff,resistive}$}}"
        for cp in cp_list:
            latex_code += " & " + format_number(cp.profiles_1d[0].zeff[0])
        latex_code += r" \\ \hline" + "\n"
    
    # Crinquième ligne : B_tor (T)
    latex_code += r"\multicolumn{2}{|c|}{\textbf{$B_{tor}$ (T)}}"
    for gk_ids in gk_ids_list1:
        latex_code += " & " + format_number(gk_ids.normalizing_quantities.b_field_tor)
    latex_code += r" \\ \hline" + "\n"

    # Partie Géométrie
    latex_code += r"\multirow{5}{*}{\textbf{Geometry}} & $q$"
    for gk_ids in gk_ids_list1:
        latex_code += " & " + format_number(gk_ids.flux_surface.q)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\hat{s}$"
    for gk_ids in gk_ids_list1:
        latex_code += " & " + format_number(gk_ids.flux_surface.magnetic_shear_r_minor)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\kappa$"
    for gk_ids in gk_ids_list1:
        latex_code += " & " + format_number(gk_ids.flux_surface.elongation)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $r/R$"
    for gk_ids in gk_ids_list1:
        latex_code += " & " + format_number(gk_ids.flux_surface.r_minor_norm)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\beta$ (\%)"
    for gk_ids in gk_ids_list1:
        latex_code += " & " + format_number(gk_ids.species_all.beta_reference*100)
    latex_code += r" \\ \hline" + "\n"

    for sp_name in sp_list:
        if sp_name=='Electron':
             # Partie Electron
            latex_code += r"\multirow{4}{*}{\textbf{Electron}} & R/L$_T$"
            for gk_ids in gk_ids_list1:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(sp.temperature_log_gradient_norm)
                    
            # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
            latex_code += r" \\ " + "\n"

            latex_code += r"& R/L$_n$ 1"
            for gk_ids in gk_ids_list1:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(sp.density_log_gradient_norm)

            latex_code += r" \\ " + "\n"

            latex_code += r"& R/L$_n$ 2"
            for gk_ids in gk_ids_list2:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(sp.density_log_gradient_norm)

            latex_code += r" \\ " + "\n"

            latex_code += r"& $T_e$ (keV)"
            for gk_ids in gk_ids_list1:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.normalizing_quantities.t_e*1e-3)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $n_e$ 1 ($10^{-19}m^{-3}$)"
            for gk_ids in gk_ids_list1:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.normalizing_quantities.n_e*1e-19)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $n_e$ 2 ($10^{-19}m^{-3}$)"
            for gk_ids in gk_ids_list2:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.normalizing_quantities.n_e*1e-19)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $\nu_{e/e}$ 1"
            for gk_ids in gk_ids_list1:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,0])

            latex_code += r" \\ " + "\n"

            latex_code += r"& $\nu_{e/e}$ 2"
            for gk_ids in gk_ids_list2:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,0])
                    
            latex_code += r" \\ \hline" + "\n"
        
        else:
            # Partie Ions

                latex_code += r"\multirow{12}{*}{\textbf{"+sp_name+r"}} & R/L$_T$"

                for gk_ids in gk_ids_list1:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.temperature_log_gradient_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& R/L$_n$ 1"
                for gk_ids in gk_ids_list1:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_log_gradient_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"

                latex_code += r"& R/L$_n$ 2"
                for gk_ids in gk_ids_list2:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_log_gradient_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $T_i/T_e$"
                for gk_ids in gk_ids_list1:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.temperature_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $n_i/n_e$ 1"
                for gk_ids in gk_ids_list1:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_norm)
                    if not sp_find:
                        latex_code += " & {} "
                latex_code += r" \\ " + "\n"

                latex_code += r"& $n_i/n_e$ 2"
                for gk_ids in gk_ids_list2:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_norm)
                    if not sp_find:
                        latex_code += " & {} "
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $\nu_{e/i} 1$"
                for gk_ids in gk_ids_list1:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,ii])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "
                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{e/i} 2$"
                for gk_ids in gk_ids_list2:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,ii])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/e} 1$"
                for gk_ids in gk_ids_list1:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,0])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/e} 2$"
                for gk_ids in gk_ids_list2:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,0])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/i} 1$"
                for gk_ids in gk_ids_list1:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,ii])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/i} 2$"
                for gk_ids in gk_ids_list2:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,ii])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "
                
                latex_code += r" \\ \hline" + "\n"
    
    # Fin du tableau
    latex_code += r"""\end{tabular}}
    \end{center}
    \end{table}
    """
    print(latex_code)
    return latex_code

def generate_latex_table_radii_COMPARISON(shot, time,  rho_tor_norm_list,  gk_ids_list1, gk_ids_list2, cp=None,  eq=None, significant_figures=2):
    """
    Génère un tableau LaTeX en prenant en entrée plusieurs colonnes de valeurs.
    
    Chaque colonne correspond à une liste de valeurs, et le nombre de colonnes peut varier.
    """
    nrad=len(rho_tor_norm_list)
    


    sp_list = []
    for gk_ids in gk_ids_list1:
        for sp in gk_ids.species:
            if sp.charge_norm <0:
                if not 'Electron' in sp_list:
                    sp_list.append('Electron')
            else:
                if not element_name_from_mass(sp.mass_norm*2) in sp_list:
                    sp_list.append(element_name_from_mass(sp.mass_norm*2))
    
    # Commence la chaîne LaTeX
    latex_code = r"""\begin{table}[H]
    \begin{center}
    \caption{Shot:"""+str(shot)+r""" and time slice:"""+str(time)+r""" s.}
    \label{tab:table_transposed1}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{|c|c|""" + "c|" * len(gk_ids_list1) + r"""}
    \hline
    """
    
    # Première ligne (titres de colonnes) : Shot

    latex_code += r"\multicolumn{2}{|c|}{\textbf{Shot}}"
    latex_code += " & " +  r"\multicolumn{"+str(nrad)+r"}{c|}{"+str(shot)+"}"
    latex_code += r" \\ \hline" + "\n"

    latex_code += r"\multicolumn{2}{|c|}{\textbf{Time}}"
    latex_code += " & " +  r"\multicolumn{"+str(nrad)+r"}{c|}{"+str(time)+"}"
    latex_code += r" \\ \hline" + "\n"
    
    if eq!=None:
        # Troisième ligne : Ip (MA)
        latex_code += r"\multicolumn{2}{|c|}{\textbf{$I_P$ (MA)}}"
        latex_code += " & " +  r"\multicolumn{"+str(nrad)+r"}{c|}{"+format_number(np.abs(eq.time_slice[0].global_quantities.ip)*1e-6)+"}"
        latex_code += r" \\ \hline" + "\n"

    if cp!=None:
        # Quatrième ligne : Zeff
        latex_code += r"\multicolumn{2}{|c|}{\textbf{$Z_{eff,resistive}$}}"
        latex_code += " & " +  r"\multicolumn{"+str(nrad)+r"}{c|}{"+format_number(cp.profiles_1d[0].zeff[0])+"}"
        latex_code += r" \\ \hline" + "\n"

    latex_code += r"\multicolumn{2}{|c|}{$\rho_{tor,N}$}"
    for rhotor in rho_tor_norm_list:
        latex_code += " & " + str(rhotor)
    latex_code += r" \\ \hline" + "\n"
    
    # Crinquième ligne : B_tor (T)
    latex_code += r"\multicolumn{2}{|c|}{\textbf{$B_{tor}$ (T)}}"
    for gk_ids in gk_ids_list1:
        latex_code += " & " + format_number(gk_ids.normalizing_quantities.b_field_tor)
    latex_code += r" \\ \hline" + "\n"

    # Partie Géométrie
    latex_code += r"\multirow{5}{*}{\textbf{Geometry}} & $q$"
    for gk_ids in gk_ids_list1:
        latex_code += " & " + format_number(gk_ids.flux_surface.q)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\hat{s}$"
    for gk_ids in gk_ids_list1:
        latex_code += " & " + format_number(gk_ids.flux_surface.magnetic_shear_r_minor)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\kappa$"
    for gk_ids in gk_ids_list1:
        latex_code += " & " + format_number(gk_ids.flux_surface.elongation)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $r/R$"
    for gk_ids in gk_ids_list1:
        latex_code += " & " + format_number(gk_ids.flux_surface.r_minor_norm)
    # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
    latex_code += r" \\ " + "\n"
    
    latex_code += r"& $\beta$ (\%)"
    for gk_ids in gk_ids_list1:
        latex_code += " & " + format_number(gk_ids.species_all.beta_reference*100)
    latex_code += r" \\ \hline" + "\n"

    for sp_name in sp_list:
        if sp_name=='Electron':
             # Partie Electron
            latex_code += r"\multirow{4}{*}{\textbf{Electron}} & R/L$_T$"
            for gk_ids in gk_ids_list1:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(sp.temperature_log_gradient_norm)
                    
            # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
            latex_code += r" \\ " + "\n"

            latex_code += r"& R/L$_n$ 1"
            for gk_ids in gk_ids_list1:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(sp.density_log_gradient_norm)

            latex_code += r" \\ " + "\n"

            latex_code += r"& R/L$_n$ 2"
            for gk_ids in gk_ids_list2:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(sp.density_log_gradient_norm)

            latex_code += r" \\ " + "\n"

            latex_code += r"& $T_e$ (keV)"
            for gk_ids in gk_ids_list1:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.normalizing_quantities.t_e*1e-3)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $n_e$ 1 ($10^{-19}m^{-3}$)"
            for gk_ids in gk_ids_list1:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.normalizing_quantities.n_e*1e-19)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $n_e$ 2 ($10^{-19}m^{-3}$)"
            for gk_ids in gk_ids_list2:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.normalizing_quantities.n_e*1e-19)
                    # else:
                    #     latex_code += " & {} "

            latex_code += r" \\ " + "\n"

            latex_code += r"& $\nu_{e/e}$ 1"
            for gk_ids in gk_ids_list1:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,0])

            latex_code += r" \\ " + "\n"

            latex_code += r"& $\nu_{e/e}$ 2"
            for gk_ids in gk_ids_list2:
                for sp in gk_ids.species:
                    if sp.charge_norm <0:
                        latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,0])
                    
            latex_code += r" \\ \hline" + "\n"
        
        else:
            # Partie Ions

                latex_code += r"\multirow{12}{*}{\textbf{"+sp_name+r"}} & R/L$_T$"

                for gk_ids in gk_ids_list1:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.temperature_log_gradient_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& R/L$_n$ 1"
                for gk_ids in gk_ids_list1:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_log_gradient_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"

                latex_code += r"& R/L$_n$ 2"
                for gk_ids in gk_ids_list2:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_log_gradient_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $T_i/T_e$"
                for gk_ids in gk_ids_list1:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.temperature_norm)
                    if not sp_find:
                        latex_code += " & {} "
                # latex_code += r" \\ \cline{2-"+str(2+ngkids)+r"}" + "\n"
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $n_i/n_e$ 1"
                for gk_ids in gk_ids_list1:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_norm)
                    if not sp_find:
                        latex_code += " & {} "
                latex_code += r" \\ " + "\n"

                latex_code += r"& $n_i/n_e$ 2"
                for gk_ids in gk_ids_list2:
                    sp_find = False
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(sp.density_norm)
                    if not sp_find:
                        latex_code += " & {} "
                latex_code += r" \\ " + "\n"
                
                latex_code += r"& $\nu_{e/i} 1$"
                for gk_ids in gk_ids_list1:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,ii])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "
                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{e/i} 2$"
                for gk_ids in gk_ids_list2:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[0,ii])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/e} 1$"
                for gk_ids in gk_ids_list1:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,0])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/e} 2$"
                for gk_ids in gk_ids_list2:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,0])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/i} 1$"
                for gk_ids in gk_ids_list1:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,ii])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "

                latex_code += r" \\ " + "\n"

                latex_code += r"& $\nu_{i/i} 2$"
                for gk_ids in gk_ids_list2:
                    sp_find = False
                    ii=0
                    for sp in gk_ids.species:
                        if element_name_from_mass(sp.mass_norm*2) == sp_name:
                            sp_find = True
                            latex_code += " & " + format_number(gk_ids.collisions.collisionality_norm[ii,ii])

                        ii+=1
                    if not sp_find:
                        latex_code += " & {} "
                
                latex_code += r" \\ \hline" + "\n"
    
    # Fin du tableau
    latex_code += r"""\end{tabular}}
    \end{center}
    \end{table}
    """
    print(latex_code)
    return latex_code

