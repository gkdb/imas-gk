"""
10/11/2024
author: enzo.vergnaud@cea.fr
Description: useful for debugging the WEST_GKDB_constructor function by checking specific shot and plateau
Work only on the Enzo computer. Cehck WEST_GKDB_paralllelisation to have the same function used west2cpeq_noRAMpb...
"""

import numpy as np
import imas_west as iw
import statistics_west as sw 
import sys 
import GKDB_plateaus_constructor as GKDBcreator
import os

sys.path.append("/Home/EV273809/Documents/These/Python/mes_codes/enzo_utilitaries")
from WESTtools import west2cpeq_noRAMpb
print("Enzo's computer detected, switch on function on enzo_utilitaries")

campaign = 'C7'
delta_t_allowed=0.05
shot = 57323
plat = 0
save_ids_path=f'/Home/EV273809/These/Python/GKDB_construction/testdebug/'
intref=False

rhotor_target = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

# We retrieve all the plateau stat of the campaign
stats =  sw.read_stats(campaign)
all_shot_list = np.array((stats['shot']))
all_shot_list = np.unique(all_shot_list)
all_shot_list = all_shot_list.astype(int) # BE CAREFUL TO HAVE int LIST and NOT NP.ARRAY

time_ini =stats['tIni_plto']
time_end =stats['tEnd_plto']
time_igni = stats['t_ignitron']
plateau = stats['plateau']

if intref:
    name_save += '_intref'

def west2cpeq_fit(shot, t0=None, t1=None):
    return west2cpeq_noRAMpb(shot, t0, t1, delta_t_allowed=delta_t_allowed, intref2ne_flag=intref, doplots=False)


try:
    GKDBcreator.debug_west_plat_constructor_ids_open(shot,rhotor_target, machine="debugWESTGKDB", user="EV273809", run=0, occurrence=plat)
except:
    GKDBcreator.debug_west_plat_constructor_ids_creation(shot, plat, rhotor_target, delta_t_allowed, west2cpeq_fit, plateau, time_igni, time_ini, time_end, machine="debugWESTGKDB", user="EV273809", run=0, occurrence=plat, verbose=False)
