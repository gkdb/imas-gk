'''
06/11/2024
Author: Enzo Vergnaud
enzo.vergnaud@cea.fr
Description: This code is not machine dependant. Based on the WEST plateau structure, this code constructs the gyrokinetic_local plateaus of a shot_list given by the user.
The philosophy behind this code is to be modular: if you have access to core_profiles and equilibrium IDS of a machine with IMAS, you can construct the GKDB easily.
TO DO if necessary...:
    -   increase log error msg for better debug
    -   read every functions to be sure all variables are well used
    -   with its modularity, it can be created other plateaus from other IDS (different than the gyrokinetic_local one) if you need other plateau creation... (maybe rename this script code "IMAS_plateau_creator" in this case when the configuration will be robust)
'''
import time 
import numpy as np
import pandas as pd
import glob
import sys
import os
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../../gkids/tools")))
from cpeq2gk import cpeq2gk
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "..")))
import IMASPYtools as imaspytools

# Utilitary functions
def msg_log(msg,log,verbose,critical=False): 
    """ Generic routine to store and optionally display warning/error messages 
    Inputs    
        msg         message to be stored/displayed
        log         list to store the messages
        verbose     if True, displays the message on screen
        critical    if True, exits the code
        
    """
    if critical:
        log.append("Error: "+msg)
        raise Exception(msg)
    else:
        log.append("Warning: "+msg)
        if verbose is True:
            print(msg)

def float_str(number):
    chaine = str(number)
    chaine_list = chaine.split('.')
    return chaine_list[0] + 'P' + chaine_list[1]

class GKDicoPlateau():
    """ Object containing dico where are stored all the gyrokinetic_local data
    INPUTS:
        -   shot
        -   plat
        -   radius
        -   name
    
    """
    def __init__(self, shot, plat, rayon, name='mean'):
        self.rayon = rayon 
        self.name = name
        self.plat_dico = {
            'shot': shot, 
            'plateau' : plat,
            't_e_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'n_e_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'b_field_tor_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'r_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'r_minor_norm_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'q_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'magnetic_shear_r_minor_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'pressure_gradient_norm_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            # 'dgeometric_axis_r_dr_minor_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            # 'dgeometric_axis_z_dr_minor_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'elongation_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'delongation_dr_minor_norm_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            # 'shape_coefficients_c_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            # 'shape_coefficients_s_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            # 'dc_dr_minor_norm_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            # 'ds_dr_minor_norm_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'beta_reference_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'velocity_tor_norm_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'shearing_rate_norm_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'debye_length_norm_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'charge_norm_e_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'mass_norm_e_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'density_norm_e_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'density_log_gradient_norm_e_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'temperature_norm_e_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'temperature_log_gradient_norm_e_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'velocity_tor_gradient_norm_e_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'charge_norm_i_' + float_str(rayon) + '_'+name+'_plto'                  : np.nan,
            'mass_norm_i_' + float_str(rayon) + '_'+name+'_plto'                    : np.nan,
            'density_norm_i_' + float_str(rayon) + '_'+name+'_plto'                 : np.nan,
            'density_log_gradient_norm_i_' + float_str(rayon) + '_'+name+'_plto'    : np.nan,
            'temperature_norm_i_' + float_str(rayon) + '_'+name+'_plto'             : np.nan,
            'temperature_log_gradient_norm_i_' + float_str(rayon) + '_'+name+'_plto': np.nan,
            'velocity_tor_gradient_norm_i_' + float_str(rayon) + '_'+name+'_plto'   : np.nan,
            'collisionality_norm_ee_'+ float_str(rayon) + '_'+name+'_plto' : np.nan,
            'collisionality_norm_ei_'+ float_str(rayon) + '_'+name+'_plto' : np.nan,
            'collisionality_norm_ie_'+ float_str(rayon) + '_'+name+'_plto' : np.nan,
            'collisionality_norm_ii_'+ float_str(rayon) + '_'+name+'_plto' : np.nan
        }
    
    def fill_plat_dico(self, gkids):
        self.plat_dico['t_e_' + float_str(self.rayon) + '_'+self.name+'_plto']       =float(gkids.normalizing_quantities.t_e),
        self.plat_dico['n_e_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.normalizing_quantities.n_e)
        self.plat_dico['b_field_tor_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.normalizing_quantities.b_field_tor)
        self.plat_dico['r_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.normalizing_quantities.r)
        self.plat_dico['r_minor_norm_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.flux_surface.r_minor_norm)
        self.plat_dico['q_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.flux_surface.q)
        self.plat_dico['magnetic_shear_r_minor_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.flux_surface.magnetic_shear_r_minor)
        self.plat_dico['pressure_gradient_norm_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.flux_surface.pressure_gradient_norm)
        # self.plat_dico['dgeometric_axis_r_dr_minor_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.flux_surface.dgeometric_axis_r_dr_minor)
        # self.plat_dico['dgeometric_axis_z_dr_minor_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.flux_surface.dgeometric_axis_z_dr_minor)
        self.plat_dico['elongation_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.flux_surface.elongation)
        self.plat_dico['delongation_dr_minor_norm_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.flux_surface.delongation_dr_minor_norm)
        # self.plat_dico['shape_coefficients_c_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.flux_surface.shape_coefficients_c)
        # self.plat_dico['shape_coefficients_s_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.flux_surface.shape_coefficients_s)
        # self.plat_dico['dc_dr_minor_norm_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.flux_surface.dc_dr_minor_norm)
        # self.plat_dico['ds_dr_minor_norm_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.flux_surface.ds_dr_minor_norm)
        self.plat_dico['beta_reference_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.species_all.beta_reference)
        self.plat_dico['velocity_tor_norm_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.species_all.velocity_tor_norm)
        self.plat_dico['shearing_rate_norm_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.species_all.shearing_rate_norm)
        self.plat_dico['debye_length_norm_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.species_all.debye_length_norm)
        self.plat_dico['charge_norm_e_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.species[0].charge_norm)
        self.plat_dico['mass_norm_e_' + float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.species[0].mass_norm)
        self.plat_dico['density_norm_e_' + float_str(self.rayon) + '_'+self.name+'_plto']                    = float(gkids.species[0].density_norm)
        self.plat_dico['density_log_gradient_norm_e_' + float_str(self.rayon) + '_'+self.name+'_plto']       = float(gkids.species[0].density_log_gradient_norm)
        self.plat_dico['temperature_norm_e_' + float_str(self.rayon) + '_'+self.name+'_plto']                = float(gkids.species[0].temperature_norm)
        self.plat_dico['temperature_log_gradient_norm_e_' + float_str(self.rayon) + '_'+self.name+'_plto']   = float(gkids.species[0].temperature_log_gradient_norm)
        self.plat_dico['velocity_tor_gradient_norm_e_' + float_str(self.rayon) + '_'+self.name+'_plto']      = float(gkids.species[0].velocity_tor_gradient_norm)
        self.plat_dico['charge_norm_i_' + float_str(self.rayon) + '_'+self.name+'_plto']                     = float(gkids.species[1].charge_norm)
        self.plat_dico['mass_norm_i_' + float_str(self.rayon) + '_'+self.name+'_plto']                       = float(gkids.species[1].mass_norm)
        self.plat_dico['density_norm_i_' + float_str(self.rayon) + '_'+self.name+'_plto']                    = float(gkids.species[1].density_norm)
        self.plat_dico['density_log_gradient_norm_i_' + float_str(self.rayon) + '_'+self.name+'_plto']       = float(gkids.species[1].density_log_gradient_norm)
        self.plat_dico['temperature_norm_i_' + float_str(self.rayon) + '_'+self.name+'_plto']                = float(gkids.species[1].temperature_norm)
        self.plat_dico['temperature_log_gradient_norm_i_' + float_str(self.rayon) + '_'+self.name+'_plto']   = float(gkids.species[1].temperature_log_gradient_norm)
        self.plat_dico['velocity_tor_gradient_norm_i_' + float_str(self.rayon) + '_'+self.name+'_plto']      = float(gkids.species[1].velocity_tor_gradient_norm)
        self.plat_dico['collisionality_norm_ee_'+ float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.collisions.collisionality_norm[0,0]) 
        self.plat_dico['collisionality_norm_ei_'+ float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.collisions.collisionality_norm[0,1]) 
        self.plat_dico['collisionality_norm_ie_'+ float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.collisions.collisionality_norm[1,0]) 
        self.plat_dico['collisionality_norm_ii_'+ float_str(self.rayon) + '_'+self.name+'_plto'] = float(gkids.collisions.collisionality_norm[1,1]) 

def shot_list_plateaus(shot_list, rhotor_target, delta_t_allowed, name_save, your_method_for_cp_eq_from_shot, plateau, time_igni, time_ini, time_end, verbose=False, return_df=False):
    """ From a list of shots and other necessary data of these correponding shots, create a pandas dataframe for every shot containing the plateaus and are automatically save under the .h5 format for avoid memory problem.
    INPUTS:
        -   shot                                    int: shot     
        -   rhotor_target                           args of the gyrokinetic_local function creator: need to be passed as args
        -   delta_t_allowed                         args of the gyrokinetic_local function creator: need to be passed as args
        -   name_save                               str: full path name of the h5 file folder where they will be stored
        -   your_method_for_cp_eq_from_shot         function: method that will take (shot, t0=t0, t1=t1)-> cp,eq with t0 and t1 the corresponding initial and end times of your plateaus.  Please take care to return False, False with this method if it encounters error sometimes (like if it missing data sometimes)
        -   time_ini                                matrix shot x plateau of the initial time of the plateau
        -   time_end                                matrix shot x plateau of the end time of the plateau
        -   time_igni                               matrix shot x plateau of the ignition time of the shot
        -   plateau                                 list of plateau number
        -   verbose                                 bool: need to be implemented to have the error. In either case, you will have the log.txt file at the end of the code.
        -   return_def                              bool: if the user wants the full dataframe created at the end(useful for debug, in reality it is advised to not use it to avoid to use too much memory...)
    OUTPUTS:
        -   if return_df=True: df of the all shot_list. Else: no outputs
    """
    log = []
    if type(shot_list)!=list:
        shot_list.tolist()
    if return_df:
        index_empty = pd.MultiIndex.from_arrays([[], []], names=[None, None])
        df = pd.DataFrame(index=index_empty)
    compteur_shot = 1
    start=time.time()
    for shot in shot_list:
        sublog=[]
        print("*********************")
        print(f"*SHOT : {compteur_shot}/{len(shot_list)}       *")
        print(f"*SHOT NUMBER : {shot}*")
        print("*********************")
        compteur_shot+=1
        if os.path.isfile(name_save+f'_shot_{shot}'+ ".h5"):
            print("This shot exists already in the GKDB folder.")
            print("Reading it from its .h5 format...")
            pre_name=os.path.basename(name_save)
            path_gkdb=os.path.normpath(name_save)
            filepath_components=path_gkdb.split(os.sep)
            path_gkdb = os.sep.join(filepath_components[:-1])
            df_shot =read_shot_GKDB(path_gkdb,pre_name, shot )
        else:
            df_shot = shot_plateaus(shot, rhotor_target,delta_t_allowed, sublog, your_method_for_cp_eq_from_shot, plateau, time_igni, time_ini, time_end, verbose=False)
            df_shot.to_hdf(name_save+f'_shot_{shot}'+ ".h5", key='df', mode='w')
        with open(f"{name_save}_log", "a") as file:
            for element in sublog:
                file.write(element + "\n")
        if return_df:
            df = pd.concat([df, df_shot])
        del df_shot # to save memory
    end=time.time()
    log.append(f"COMPUTATION TIME: {end-start}")
    with open(f"{name_save}_log", "a") as file:
        for element in log:
            file.write(element + "\n")
    if return_df:
        return df

def shot_plateaus(shot, rhotor_target, delta_t_allowed, log, your_method_for_cp_eq_from_shot, plateau, time_igni, time_ini, time_end, verbose=False):
    """ For a shot and other necessary data of this correponding shot, create a pandas dataframe containing the plateaus
    INPUTS:
        -   shot                                    int: shot     
        -   rhotor_target                           args of the gyrokinetic_local function creator: need to be passed as args
        -   delta_t_allowed                         args of the gyrokinetic_local function creator: need to be passed as args
        -   name_save                               str: full path name of the h5 file folder where they will be stored
        -   your_method_for_cp_eq_from_shot         function: method that will take (shot, t0=t0, t1=t1)-> cp,eq with t0 and t1 the corresponding initial and end times of your plateaus.  Please take care to return False, False with this method if it encounters error sometimes (like if it missing data sometimes)
        -   time_ini                                matrix shot x plateau of the initial time of the plateau
        -   time_end                                matrix shot x plateau of the end time of the plateau
        -   time_igni                               matrix shot x plateau of the ignition time of the shot
        -   plateau                                 list of plateau number
        -   verbose                                 bool: need to be implemented to have the error. In either case, you will have the log.txt file at the end of the code.
        -   return_def                              bool: if the user wants the full dataframe created at the end(useful for debug, in reality it is advised to not use it to avoid to use too much memory...)
    OUTPUTS:
        -   df                          dataframe of the shot
    """
    df= pd.DataFrame(columns=['shot', 'plateau'])
    # df = pd.concat([df, method(shot, rhotor_target)], ignore_index=True, sort=False)
    df= pd.merge(df, get_global_machine_gkids_mean_on_cpeq_nointerp(shot, rhotor_target, delta_t_allowed, log, your_method_for_cp_eq_from_shot, plateau, time_igni, time_ini, time_end, verbose=False), on=['shot', 'plateau'], how='outer')
    # To have the same stucture as stats = read_stats(campaign)
    df = df.set_index(['shot', 'plateau'], drop=False)
    # Conversion des niveaux de l'index en entiers
    df.index.names = [None, None]
    df.index = df.index.set_levels(df.index.levels[0].astype(int), level=0)
    df.index = df.index.set_levels(df.index.levels[1].astype(int), level=1)

    return df

def append_dico_to_df(df, dico, shot, plateau):
    """convert a dico into a dataframe well suited to be concatenated with the dataframe created to store the plateaus
    INPUTS:
        -   df
        -   dico
        -   shot
        -   plateau
    OUPUTS:
        -   df
    """
    # Check if shot + plateau combination already exists in the dataframe
    if ((df['shot'] == shot) & (df['plateau'] == plateau)).any():
        # If the line exists, adding the data
        for key, value in dico.items():
            df.loc[(df['shot'] == shot) & (df['plateau'] == plateau), key] = value
    else:
        # if the line doesn't exist, creation of the DataFrame
        new_row = pd.DataFrame([{**{'shot': shot, 'plateau': plateau}, **dico}])
        # add a line with pd.concat
        df = pd.concat([df, new_row], ignore_index=True)
    
    return df 

def get_global_machine_gkids_mean_on_cpeq_nointerp(shot, rhotor_target, delta_t_allowed, log, your_method_for_cp_eq_from_shot, plateau, time_igni, time_ini, time_end, verbose=False):
    ''' THE FUNCTION THAT COMPUTE THE DATA THAT WILL BE STORED : it is the function where we could increase the modularity... think about it later
    INPUTS:
        -   your_method_for_cp_eq_from_shot         method use that NEED to depends on time_ini, time_end and time_igni matrix, please take care to return False, False with this method if it encounters error sometimes (like if it missing data sometimes)
        -   time_ini                                matrix shot x plateau of the initial time of the plateau
        -   time_end                                matrix shot x plateau of the end time of the plateau
        -   time_igni                               matrix shot x plateau of the ignition time of the shot
        -   plateau                                 list of plateau number
    OUTPUTS:
    '''
    plateau_number  =   len(plateau[shot])
    opt             =   {   'species':              'main+imp',
                            'toroidal flow':        'none',         
                            'ExB shear':            'none',   
                            'parallel flow shear':  'none'
                        }

    gkids_mean_list = []
    for plat in range(plateau_number):
        t0=time_ini[shot][plat] - time_igni[shot][plat]
        t1=time_end[shot][plat] - time_igni[shot][plat]
        print(f"=========PLATEAU {plat+1}/{plateau_number}=========")
        print(f"FROM t={t0}s TO t={t1}s")
        print(f"WITH T IGNI:FROM t={time_ini[shot][plat]}s TO t={time_end[shot][plat]}s")
        print(f"=====================================")
        try:
            cp_mean, eq_mean = your_method_for_cp_eq_from_shot(shot, t0=t0, t1=t1)
            if (cp_mean, eq_mean) != (False, False):
                eq_mean.time = cp_mean.time # I do that because else the cpeq2gk function doesn't work: maybe modify the cpeq2gk function to let a delta_t allowed between the cp and eq IDS.time
                gkids_mean, log = cpeq2gk(cp_mean, eq_mean, rhotor_target, opt, doplots=False)
            else:
                msg=f"No cp or eq available with time discrepancy smaller than dt={delta_t_allowed}s in this shot for every time step"
                msg_log(msg,log,verbose=verbose, critical=False)
                gkids_mean = False
        except Exception as e:
            gkids_mean = False
            msg=f"Problem with your cp eq construction method OR cpeq2gk scripts shot {shot} plto {plat}: {e}"
            msg_log(msg, log, verbose=verbose, critical=False)

        gkids_mean_list.append(gkids_mean)
        
    df= pd.DataFrame(columns=['shot', 'plateau'])
    
    # Compute full plateau values
    for rr in range(len(rhotor_target)):
        radius = rhotor_target[rr]
        for plat in range(plateau_number):
            plateau_radius_dico_mean_obj    = GKDicoPlateau(shot, plat, radius, name='mean')
            plateau_radius_dico_std_obj     = GKDicoPlateau(shot, plat, radius, name='std')
            if not isinstance(gkids_mean_list[plat],bool):
                plateau_radius_dico_mean_obj.fill_plat_dico(gkids_mean_list[plat][rr])
            df = append_dico_to_df(df, plateau_radius_dico_mean_obj.plat_dico, shot, plat)
            df = append_dico_to_df(df, plateau_radius_dico_std_obj.plat_dico, shot, plat)

    df = df.set_index(['shot', 'plateau'], drop=False)
    # Convert index in int
    df.index.names = [None, None]
    df.index = df.index.set_levels(df.index.levels[0].astype(int), level=0)
    df.index = df.index.set_levels(df.index.levels[1].astype(int), level=1)

    return df

# READ SHOTS
def read_shot_GKDB(path_gkdb, pre_name, shot):
    """ Read a shot from the .h5 format save 
    INPUTS:
        -   path_gkdb       str: path of the folder where the shot is stored
        -   pre_name        str: pre_name of the shot if there is one
        -   shot            int: shot
    OUPUTS:
        -   df
    """
    if path_gkdb[-1]!='/':
        path_gkdb=path_gkdb+'/'
    shot = str(shot)
    full_shot_path = path_gkdb + pre_name + '_shot_' + shot + '.h5'
    df_hdf5 = pd.read_hdf(full_shot_path, key='df')

    return df_hdf5

def read_all_shot_GKDB(path_gkdb, pre_name):
    """ Read all shots from the .h5 format save 
    INPUTS:
        -   path_gkdb       str: path of the folder where the shots are stored
        -   pre_name        str: pre_name of the shot if there is one
    OUPUTS:
        -   df
    """
    if path_gkdb[-1]!='/':
        path_gkdb=path_gkdb+'/'
    file_pattern = '*.h5' 
    shot_h5_files_list = glob.glob(path_gkdb+'/'+pre_name + file_pattern)
    index_empty = pd.MultiIndex.from_arrays([[], []], names=[None, None])
    df_concat = pd.DataFrame(index=index_empty)
    for shot_str in shot_h5_files_list:
        shot = shot_str.split('_')[-1]
        shot = shot.split('.')[0]
        shot = int(shot)
        print(f'Reading {shot} h5 GKDB data...')
        df_hdf5 = read_shot_GKDB(path_gkdb, pre_name, shot)
        df_concat = pd.concat([df_concat,df_hdf5]) # we concatenate with the two index (shot, plateau)
    
    return df_concat

def read_h5_to_df(path_gkdb, save_name):
    """ Read a hdf5 file and return the corresponding dataframe
    INPUTS:
        -   path_gkdb
        -   save_name
        -   new_save_name
    OUTPUTS:

    """
    if path_gkdb[-1]!='/':
        path_gkdb=path_gkdb+'/'
    full_shot_path = path_gkdb + save_name + '.h5'
    df_hdf5 = pd.read_hdf(full_shot_path, key='df')

    return df_hdf5

def save_df_to_h5(df, path_gkdb, save_name):
    """ a save of a dataframe to hdf5 format
    INPUTS:
        -   df              dataframe of the shot(s)
        -   path_gkdb       str: path of the save
        -   save_name       str: name of the save
    OUTPUTS: None (a .h5 file is created in the path_gkdb repository with save_name name)
    """
    if path_gkdb[-1]!='/':
        path_gkdb=path_gkdb+'/'
    df.to_hdf(path_gkdb+'/'+save_name+ ".h5", key='df', mode='w')

def concatenate_part(path_gkdb, pre_name,part_number,  new_save_name):
    """Very specific function: aims to concatenate different group of hdf5 save with partX name with X a natural number
    INPUTS:
        -   pre_name            str pre_name of the path where the save is
        -   save_name           str : name of the save
    OUTPUTS:None (create a save of the h5 file)

    """
    log=[]
    if path_gkdb[-1]!='/':
        path_gkdb=path_gkdb+'/'
    index_empty = pd.MultiIndex.from_arrays([[], []], names=[None, None])
    df_concat = pd.DataFrame(index=index_empty)
    for ii in range(part_number):
        pre_name_part = pre_name+'_part'+str(ii)
        try:
            df_part = read_all_shot_GKDB(path_gkdb, pre_name_part)
            df_concat = pd.concat([df_concat,df_part]) 
        except:
            msg=f"No part {ii} existing for {pre_name} files in repository {path_gkdb}"
            msg_log(msg, log=log, verbose=True, critical=False)
    save_df_to_h5(df_concat, path_gkdb, new_save_name)

def extract_float_from_tuple(df, column_name):
    """  Very specific : for dark raison, there is some columns with tuple numbers instead of just the number
    """
    if column_name in df.columns:
        df[column_name] = df[column_name].apply(lambda x: x[0] if isinstance(x, tuple) else x)
    else:
        raise ValueError(f"The column '{column_name}' doesn't exist in the DataFrame.")
    return df

 
# for debugg
def debug_west_plat_constructor_ids_creation(shot, plat, rhotor_target, delta_t_allowed, your_method_for_cp_eq_from_shot, plateau, time_igni, time_ini, time_end, machine="debugWESTGKDB", user="EV273809", run=0, occurrence=0, verbose=False):
    """Create the cp and eq ids in a path to debug easily problem repertoried in the log file of shot_list_plateau
    INPUTS:
        -   shot                int:shot
        -   plateau             int: plateau
        -   ids_save_path       str: path of the ids save
        -   others              exactly same inputs as the shot_list_plateau() above
    OUPTUS:None (see save)
    """
    log = []
    opt             =   {   'species':              'main+imp',
                            'toroidal flow':        'none',         
                            'ExB shear':            'none',   
                            'parallel flow shear':  'none'
                        }

    t0=time_ini[shot][plat] - time_igni[shot][plat]
    t1=time_end[shot][plat] - time_igni[shot][plat]
    print(f"=========PLATEAU {plat+1}=========")
    print(f"FROM t={t0}s TO t={t1}s")
    print(f"WITH T IGNI:FROM t={time_ini[shot][plat]}s TO t={time_end[shot][plat]}s")
    print(f"=====================================")
    # try:
    cp_mean, eq_mean = your_method_for_cp_eq_from_shot(shot, t0=t0, t1=t1)
    imaspytools.ids_save_shot_hdf5(cp_mean, shot, machine, run, user, occurrence=occurrence)
    imaspytools.ids_save_shot_hdf5(eq_mean, shot, machine, run, user, occurrence=occurrence)
    if (cp_mean, eq_mean) != (False, False):
        eq_mean.time = cp_mean.time # I do that because else the cpeq2gk function doesn't work: maybe modify the cpeq2gk function to let a delta_t allowed between the cp and eq IDS.time
        gkids_mean, log = cpeq2gk(cp_mean, eq_mean, rhotor_target, opt, doplots=False)
    else:
        msg=f"No cp or eq available with time discrepancy smaller than dt={delta_t_allowed}s in this shot for every time step"
        msg_log(msg,log,verbose=verbose, critical=False)
        gkids_mean = False
    print(gkids_mean)

def debug_west_plat_constructor_ids_open(shot,rhotor_target, machine="debugWESTGKDB", user="EV273809", run=0, occurrence=0):
    """Do the same as the debug_west_palat_constructor_ids_create but use directly ids saved under your local machine
    INPUTS:
        -   shot                int:shot
        -   occurrence          int: occurence is the plateau here
        -   others              exactly same inputs as the shot_list_plateau() above
    OUPTUS:None (see save)
    """
    cp_mean = imaspytools.ids_open_shot_hdf5(shot, 'core_profiles', run=run, occurrence=occurrence, machine=machine, user=user)
    eq_mean = imaspytools.ids_open_shot_hdf5(shot, 'equilibrium', run=run, occurrence=occurrence, machine=machine, user=user)
    print("Save of the IDS cp and eq found: there are automatically selected.")
    opt             =   {   'species':              'main+imp',
                            'toroidal flow':        'none',         
                            'ExB shear':            'none',   
                            'parallel flow shear':  'none'
                        }
    eq_mean.time = cp_mean.time # I do that because else the cpeq2gk function doesn't work: maybe modify the cpeq2gk function to let a delta_t allowed between the cp and eq IDS.time
    try:
        gkids_mean, log = cpeq2gk(cp_mean, eq_mean, rhotor_target, opt, doplots=False)
    except Exception as e:
        print(e)