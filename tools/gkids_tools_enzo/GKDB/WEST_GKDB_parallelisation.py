"""
06/11/2024
Author: Enzo Vergnaud
enzo.vergnaud@cea.fr
Description: This code use parallelisation to have time gain and memory management. If you don't need parallelisation, you can directly use GKDB_plateaus_constructor shot_list_plateau() function and read what you need to put as inputs. You will need to know them for this code also and modify gkdb_by_part_creation() shot selection list/ time plateaus matrix corrresponding to this shot list ...
This code is not machine dependant and use GKDB_plateaus_constructor file + the function that you will use for the obtention of core_profiles and equilibrium IDS from your shot. The function needs to be under the form : 
INPUTS:
    -   shot        int: the shot number
    -   t_init      float: initial time of the plateau
    -   t_end       float: final time of the plateau
OUPUTS: 
    -   cp, eq      core_profiles, equilibrium averaged on the plateau IDS
To launch the script directly in the terminal please use the following command at the level of this script to have help on the settings:
python WEST_GKDB --help 
And as an example working on WEST:
python WEST_GKDB.py --subdivision 50 --timeout 3600 --part_start 15 --part_end 35 --campaign 'C7'

You will need in the __main__ some parser, like campaign that is specific to WEST. Be sure to use the shot_list you want.
"""

import numpy as np
import imas_west as iw
import statistics_west as sw 
import sys 
import GKDB_plateaus_constructor as GKDBcreator
import os
import time
import multiprocessing
import argparse # To have directly command in bash terminal

try:
    sys.path.append("/Home/EV273809/Documents/These/Python/mes_codes/enzo_utilitaries")
    from WESTtools import west2cpeq_noRAMpb
    print("Enzo's computer detected, switch on function on enzo_utilitaries")
except:
    try:
        import imaspy
    except:
        raise ValueError("No imaspy on this computer...")
    from imas import imasdef
    import pywed as pw
    import copy
    sys.path.append(os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "..")))
    from IMASPYtools import ids_mean, ids_mean_std, ids_sum, ids_multiplication, ids_diff

    def msg_log(msg,log,verbose,critical=False): 
        """ Generic routine to store and optionally display warning/error messages 
        Inputs    
            msg         message to be stored/displayed
            log         list to store the messages
            verbose     if True, displays the message on screen
            critical    if True, exits the code
            
        """
        if critical:
            log.append("Error: "+msg)
            raise Exception(msg)
        else:
            log.append("Warning: "+msg)
            if verbose is True:
                print(msg)

    def t_ignitron_retrievement(shot):
        ## t_ignitron retrievement
        log=[]
        verbose=True
        t_igni_found = False
        n_conn_try = 0
        N_CONN_MAX = 50
        while not(t_igni_found) and  n_conn_try < N_CONN_MAX:
            try:
                t_ignitron = pw.tsbase(shot, 'RIGNITRON')[0][0][0]
            except:
                n_conn_try += 1  
            else:
                t_igni_found = True
        if n_conn_try == N_CONN_MAX:
            msg = 'tsbase doesnt work, retry...'
            msg_log(msg,log,verbose,critical=True)
        
        return t_ignitron

    def west2cpeq_noRAMpb(shot, t0, t1=None, delta_t_allowed=0.05, verbose = True, intref2ne_flag = False, smooth = 1e-5, doplots = False):
        """ Generates one or several gyrokinetics_local IDS from a core_profile and an equilibrium IDS
        Inputs
            shot            shot number of a WEST discharger
            t0              time where the begin of the slice on the IDS will be done
            t1              time where the end  slice on the IDS will be done
            delta_t_allowed difference time allowed between to temporal point of the cp and eq ids to do the assumption that we are at the "same temporal point"
            option          option of the interpolations (TO DO)
            verbose         if True displays error messages (default: True)
            intref2ne_flag  use INTREF2ne function for ne storage in cp
            smooth          if non-zero, applies smoothing when computing the radial derivatives (default: 1e-5)
                            The higher the value, the stronger the smoothing
                            (check carefully the outputs if no smoothing is applied)
            doplots         for check interpolation plots

        Outputs
            cp           core_profiles IDS with 1 time slice
            eq           equilibrium IDS with 1 time slice
        """
        def ids_with_the_less_time_point(t_ids1, t_ids2):
            if len(t_ids1)>len(t_ids2):
                return np.unique(t_ids2).tolist()
            else:
                return np.unique(t_ids1).tolist()


        def get_closest_index(value, input_list):
            # "RÃ©cupÃ¨re l'indice le plus proche d'une valeur dans une liste stictement croissante."
            lst = np.abs(np.array(input_list)-value)
            min = np.min(lst)
            
            return np.where(lst == min)[0][0]

        def subtime_array(time_array, tini, tend):
            indini = get_closest_index(tini, time_array)
            indend=get_closest_index(tend, time_array)
            return time_array[indini:indend+1]
        
        if t1 == None:
            t1 = t0

        
        log         = []
        run         = 0
        occ_core    = 0
        occ_eq      = 1
        user_public = 'imas_public'       #for T_e
        user_simu   = 'imas_simulation'   #for species
        machine     = 'west'

        t_ignitron = t_ignitron_retrievement(shot)

        t0_slice = t0 + t_ignitron
        t1_slice = t1 + t_ignitron

        t_equi_array = iw.partialGet(shot, "equilibrium/time", run, occ_eq, user_public, machine)
        t_core_array = iw.partialGet(shot, "core_profiles/time", run, occ_core, user_public, machine)
        ind_t0_equi = get_closest_index(t0 + t_ignitron, t_equi_array)
        ind_t0_core = get_closest_index(t0 + t_ignitron, t_core_array)
        t0_abs_equi = t_equi_array[ind_t0_equi]
        ind_t1_equi = get_closest_index(t1 + t_ignitron, t_equi_array)
        ind_t1_core = get_closest_index(t1 + t_ignitron, t_core_array)
        t1_abs_equi = t_equi_array[ind_t1_equi]

        t_ids_less_accurate=ids_with_the_less_time_point(t_equi_array, t_core_array)
        time_array=subtime_array(t_ids_less_accurate, t0_slice, t1_slice)

        
        interp_type = imasdef.CLOSEST_INTERP # =1

        imas_entry_public = imaspy.DBEntry(imasdef.HDF5_BACKEND, machine, shot, run, user_public)
        imas_entry_public.open()
        imas_entry_simu = imaspy.DBEntry(imasdef.HDF5_BACKEND, machine, shot, run, user_simu)
        imas_entry_simu.open()

        if t0 == t1 or len(time_array)==1: # MAYBE USELESS IF CASE : Case where the user wants only ont time slice for his cp/eq creation
            eq_west_mean = imas_entry_public.get_slice("equilibrium", t0_slice, interp_type, occurrence = 1)
            cp_west_mean = imas_entry_public.get_slice("core_profiles", t0_slice, interp_type)
            cp_simu_mean = imas_entry_simu.get_slice("core_profiles", t0_slice, interp_type)
        else:
            cp_west_list, eq_west_list, cp_simu_list = [], [], []
            
            Nt = len(time_array)
            for tt in time_array:
                print("Time slice:", tt)
                cpw=imas_entry_public.get_slice("core_profiles", tt, interp_type)
                eqw=imas_entry_public.get_slice("equilibrium", tt, interp_type, occurrence=1)
                cps=imas_entry_simu.get_slice("core_profiles", tt, interp_type)
                if np.abs(cpw.time[0] - eqw.time[0])<delta_t_allowed:
                    cp_west_list.append(cpw)
                    eq_west_list.append(eqw)
                    cp_simu_list.append(cps)
                cp_west_sum = ids_sum(cp_west_list)
                eq_west_sum = ids_sum(eq_west_list)
                cp_simu_sum = ids_sum(cp_simu_list)
                del cpw 
                del eqw 
                del cps
                cp_west_list=[cp_west_sum]
                eq_west_list=[eq_west_sum]
                cp_simu_list=[cp_simu_sum]
                
            if cp_west_list!=[] and eq_west_list!=[] and cp_simu_list!=[]:
                cp_west_mean, err_cpm=ids_multiplication(cp_west_list[0], 1/Nt)
                eq_west_mean, err_eqm=ids_multiplication(eq_west_list[0], 1/Nt)
                cp_simu_mean, err_cps=ids_multiplication(cp_simu_list[0], 1/Nt)
                if err_cpm != '':
                    log.append(err_cpm)
                if err_eqm != '':
                    log.append(err_eqm)
                if err_cps != '':
                    log.append(err_cps)
            else:
                msg=f"No cp or eq available with time discrepancy smaller than dt={delta_t_allowed}s in this shot"
                msg_log(msg,log,verbose=True, critical=False)
                return False, False

        def cp_construction(cp_west, cp_simu):
            """In order:
                - time
                - profiles_1d
                - """
            nions = len(cp_simu.profiles_1d[0].ion)
            lst_ion_struct_dens_non_null = []
            for iion in range(nions):
                if np.all(cp_simu.profiles_1d[0].ion[iion].density > 0):
                    lst_ion_struct_dens_non_null.append(copy.deepcopy(cp_simu.profiles_1d[0].ion[iion]))
            nnions = len(lst_ion_struct_dens_non_null)
            cp_west.profiles_1d[0].ion.resize(nnions, keep = False)
            for iion in range(nnions):
                cp_west.profiles_1d[0].ion[iion] = lst_ion_struct_dens_non_null[iion]

            # Remplacement of z_eff computed with bremshtralung effect with z_eff resitiv
            zeff_resisitive = iw.partialGet(shot, 'core_profiles/global_quantities/z_eff_resistive', 0, 0, 'imas_simulation', 'west_simu_preparation')
            if ind_t0_core != ind_t1_core:
                cp_west.profiles_1d[0].zeff = [np.mean(zeff_resisitive[ind_t0_core:ind_t1_core]) for _ in  cp_west.profiles_1d[0].grid.rho_tor_norm]
                cp_west.global_quantities.z_eff_resistive = [np.mean(zeff_resisitive[ind_t0_core:ind_t1_core]) for _ in  cp_west.profiles_1d[0].grid.rho_tor_norm]
            else:
                cp_west.profiles_1d[0].zeff = [np.mean(zeff_resisitive[ind_t0_core]) for _ in  cp_west.profiles_1d[0].grid.rho_tor_norm]
                cp_west.global_quantities.z_eff_resistive = [np.mean(zeff_resisitive[ind_t0_core]) for _ in  cp_west.profiles_1d[0].grid.rho_tor_norm]

            if intref2ne_flag:
                ne_cp_old = copy.deepcopy(cp_west.profiles_1d[0].electrons.density)
                # Remplacement of ne from cp by ne of INTREF2ne routine (Fèvre routine)
                try:
                    outputs = intref_outputs(shot, tini= t0, tend= t1)
                    if outputs==False:
                        msg = "No data from DREFRAP or DREFLUC: fail of the INTREF2ne function"
                        msg_log(msg, log, verbose, critical= False)
                        cp_west.profiles_1d[0].electrons.density = np.asarray([np.nan for _ in cp_west.profiles_1d[0].electrons.density]) #I 
                    else:
                        ne_intref = outputs['ne_intref']
                        psin_intref = outputs['psin_intref']
                        frap_flag = outputs['flag_drefrap']
                        fluc_flag = outputs['flag_drefluc']
                        if frap_flag == 1:
                            frap_flag = True 
                        else:
                            frap_flag = False
                        if fluc_flag == 1:
                            fluc_flag = True 
                        else:
                            fluc_flag = False

                        if frap_flag or fluc_flag:
                            #interpolation
                            rhotor_cp=cp_west.profiles_1d[0].grid.rho_tor_norm
                            psi_cp= cp_west.profiles_1d[0].grid.psi
                            psi_axis=eq_west_mean.time_slice[0].global_quantities.psi_axis
                            psi_bnd=eq_west_mean.time_slice[0].global_quantities.psi_boundary
                            psin_cp=(psi_cp-psi_axis)/(psi_bnd-psi_axis)
                            tens=(rhotor_cp.size-np.sqrt(2*rhotor_cp.size))*smooth/100
                            # electrons
                            ne_spl_psin=splrep(psin_intref,ne_intref,s=tens*np.max(ne_intref)**2)
                            ne_cp = splev(psin_cp, ne_spl_psin)

                            cp_west.profiles_1d[0].electrons.density = ne_cp
                            cp_west.profiles_1d[0].electrons.density_thermal = ne_cp
                            # cp_west.profiles_1d[0].electrons.density_fast = ne_cp ???

                            if doplots:
                                plt.plot(psin_cp, ne_cp_old, color = 'blue', label = 'cp')
                                plt.plot(psin_cp, ne_cp, color = 'red', label = 'intref2ne')
                                plt.xlabel("PSIN", fontsize = 16)
                                plt.ylabel(r"$n_e$ ($m^{-3}$)", fontsize = 16)
                                plt.grid(True)
                                plt.legend(fontsize = 16)
                                plt.title(f"Shot {shot} | Time {t0}")
                                plt.show()
                except: # I need to erase this except but some issue when I go through the terminal to run matlab...
                    msg = "No data from DREFRAP or DREFLUC: fail of the INTREF2ne function"
                    msg_log(msg, log, verbose, critical= False)
                    cp_west.profiles_1d[0].electrons.density = np.asarray([np.nan for _ in cp_west.profiles_1d[0].electrons.density]) #I return Nan if no INTREF2ne data
                
                        
            return cp_west
        
        def eq_construction(eq_west):
            """In order:
                - time_slice
                - vacuum_toroidal_field
                - """

            ## ** profiles_2d **
            eq_west.time_slice[0].profiles_2d.resize(1)
            equil = iw.get(shot, "equilibrium", 0, 1)
            try:
                eq_west.time_slice[0].profiles_2d[0].grid.dim1 = equil.interp2D.r[:,0]# don't work: iw.partialGet(shot, "equilibrium/interp2D/r", 0,1)
                eq_west.time_slice[0].profiles_2d[0].grid.dim2 = equil.interp2D.z[0,:]# don't work:iw.partialGet(shot, "equilibrium/interp2D/z", 0,1)
                eq_west.time_slice[0].profiles_2d[0].grid_type.name='rectangular'
                ii_t0_equi = get_closest_index(t0_abs_equi, t_equi_array)
                ii_t1_equi = get_closest_index(t1_abs_equi, t_equi_array)
                if ii_t0_equi != ii_t1_equi:
                    eq_west.time_slice[0].profiles_2d[0].psi = equil.interp2D.psi[int((ii_t0_equi+ii_t1_equi)/2)]
                else:
                    eq_west.time_slice[0].profiles_2d[0].psi = equil.interp2D.psi[ii_t0_equi]# don't work:iw.partialGet(shot, f"equilibrium/interp2D/psi({ii_t_equi})")
            except:
                msg = "No data in equilibrium profiles_2d"
                msg_log(msg, log, verbose=True, critical=False)


            # ## ** profiles_1d **
            if ii_t0_equi != ii_t1_equi:
                eq_west.time_slice[0].profiles_1d.r_inboard = equil.profiles_1d.r_inboard[int((ii_t0_equi+ii_t1_equi)/2)]
                eq_west.time_slice[0].profiles_1d.r_outboard =equil.profiles_1d.r_outboard[int((ii_t0_equi+ii_t1_equi)/2)]
            else:
                eq_west.time_slice[0].profiles_1d.r_inboard = equil.profiles_1d.r_inboard[ii_t0_equi]
                eq_west.time_slice[0].profiles_1d.r_outboard =equil.profiles_1d.r_outboard[ii_t0_equi]


            ## ** boundary_separatrix **
            if ii_t0_equi != ii_t1_equi:
                eq_west.time_slice[0].boundary_separatrix.outline.r = equil.boundary.outline.r[int((ii_t0_equi+ii_t1_equi)/2)][0]
                eq_west.time_slice[0].boundary_separatrix.outline.z = equil.boundary.outline.z[int((ii_t0_equi+ii_t1_equi)/2)][0]
            else:
                eq_west.time_slice[0].boundary_separatrix.outline.r = equil.boundary.outline.r[ii_t0_equi][0]
                eq_west.time_slice[0].boundary_separatrix.outline.z = equil.boundary.outline.z[ii_t0_equi][0]

            return eq_west
        
        return cp_construction(cp_west_mean, cp_simu_mean), eq_construction(eq_west_mean)
    print("Take into account the function that you will use to create your cp, eq averaged IDS. You can modify it in the WEST_GKDB_parallelisation.py script.")
    print("If you don't modified anything, it is the west2cpeq_noRAMpb that is used by default. Specific to WEST.")
        

def gkdb_by_part_creation(PART, subdivision_of_all_shot_list, campaign, save_path, intref):
    rhotor_target = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
    name_save = save_path + f"GKDB{campaign}_part{PART}"

    # We retrieve all the plateau stat of the campaign
    stats =  sw.read_stats(campaign)
    all_shot_list = np.array((stats['shot']))
    all_shot_list = np.unique(all_shot_list)
    all_shot_list = all_shot_list.astype(int) # BE CAREFUL TO HAVE int LIST and NOT NP.ARRAY

    time_ini =stats['tIni_plto']
    time_end =stats['tEnd_plto']
    time_igni = stats['t_ignitron']
    plateau = stats['plateau']

    def subshot(shot_list, PART=0, subdivision_of_all_shot_list=30):
        sub_shot_list = []
        cut_number  = int(len(shot_list)/subdivision_of_all_shot_list)
        for ii in range(subdivision_of_all_shot_list):
            sub_shot_list.append(shot_list[0+ii*cut_number:(ii+1)*cut_number])
        sub_shot_list.append(shot_list[(cut_number+1)*cut_number:-1])

        return sub_shot_list[PART].tolist()


    if intref:
        name_save += '_intref'
    delta_t_allowed=0.05

    def west2cpeq_fit(shot, t0=None, t1=None):
        return west2cpeq_noRAMpb(shot, t0, t1, delta_t_allowed=delta_t_allowed, intref2ne_flag=intref, doplots=False)
    subshot_list = subshot(all_shot_list, PART=PART, subdivision_of_all_shot_list=subdivision_of_all_shot_list)
    
    GKDBcreator.shot_list_plateaus(subshot_list, rhotor_target, delta_t_allowed, name_save, west2cpeq_fit, plateau, time_igni, time_ini, time_end, verbose=False, return_df=False)

def multiprocessing_subdivision_database(subdivision_of_all_shot_list, timeout, part_start, part_end, campaign, save_path, intref):
    PART_list = range(part_start, part_end)
    processes = []
    if save_path[-1] != '/':
        save_path+= '/'
    if not os.path.exists(save_path):
        raise ValueError(f"{save_path} for the save_path doesn't exist... Stop script.")
    print("******************************")
    print("******************************")
    print("ADVERTISEMENT: some of the processes can crash. Be sure to cancel them (manually with ctrl+c) and that their load on your computer is also canceled (htop -> if you see process continuing you do F9 then enter)")
    input("Please press Enter to begin the script...")
    print("******************************")
    print("******************************")

    # Parallel process for eash part/subset
    for part in PART_list:
        process = multiprocessing.Process(target=gkdb_by_part_creation, args=(part, subdivision_of_all_shot_list, campaign, save_path, intref))
        processes.append((part, process))  # We keep the ID reference of the process
        process.start()
        print(f"Task for the part {part} launched.")

    # Use a timeout to avoid unlimmited time process
    for part, process in processes:
        process.join(timeout=timeout)  # Wait for the timeout for each process individually

        # Check if processes are alive after the timeout
        if process.is_alive():
            print(f"Task for the part {part} has exceeded the deadline {timeout} seconds and will be stopped.")
            process.terminate()  # Force stop the process
            process.join()       # Be sure it has been stopped
        else:
            print(f"Task for the part {part} completed within the allotted time.")

    print("******************************************")
    print("Every processes are finished or stopped.")
    print("******************************************")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Parallel task processing.')
    parser.add_argument('--subdivision', type=int, help="Number of subdivisions to process. Note: there will be 'number of subdivisions + 1' parts in total, as the last part represents the remaining shots from the division.")
    parser.add_argument('--timeout', type=int, default=10, help='Maximum timeout for each task (in seconds).')
    parser.add_argument('--part_start', type=int, default=0, help='Subdivision number where parallelization will start.')
    parser.add_argument('--part_end', type=int, default=10, help='Subdivision number where parallelization will end.')
    parser.add_argument('--campaign', type=str, default='C7', help="Campaign, by default C7.")
    parser.add_argument('--save_path', type=str, default=os.path.dirname(os.path.abspath(__file__)), help='Folder where is saved the GKDB.')
    parser.add_argument('--intref', type=bool, default=False, help="If set at True, uses the command intref2ne to construct the density profiles core_profiles. By default set at False.")
    
    args = parser.parse_args()

     # If --part_end is not provided, use subdivision_of_all_shot_list
    part_end = args.part_end if args.part_end is not None else args.subdivision

    # Executing all tasks with a timeout for each task in parallel
    multiprocessing_subdivision_database(args.subdivision, args.timeout, args.part_start, args.part_end, args.campaign, args.save_path, args.intref)
