"""
06/11/2024
Author: Enzo Vergnaud
enzo.vergnaud@cea.fr
Generate the latex table of a corresponding gyrokinetic_local IDS.
"""
from GASTD2IMAS import gastd_gkids_init
import GKIDStools as gkidstools 
import copy

ga_ids1                             = gastd_gkids_init(ky=0.5)
ga_ids2                             = copy.deepcopy(ga_ids1)
ga_ids2.species_all.beta_reference  = 5e-3


gk_ids_list                         = [ga_ids1  , ga_ids2 ]
shot_list                           = [565      , 435     ]
time_list                           = [50       , 10      ]

label_dico_for_each_gkids           = {
    "Shot"                          :   shot_list,
    "Time"                          :   time_list
}
label_dico_for_every_gkids          = {
    r"$\rho_{tor,N}$"               : 0.5,
    "Machine"                       : "Theory"
}

gkidstools.generate_latex_table(gk_ids_list, label_dico_for_each_gkids, label_dico_for_every_gkids, significant_figures=4)