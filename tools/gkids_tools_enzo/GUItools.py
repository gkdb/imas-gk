"""
06/11/2024
Author: Enzo Vergnaud
enzo.vergnaud@cea.fr
GUI modules to help user to create his/her own interface. 
"""
import sys
from PyQt5.QtWidgets import QApplication, QFileDialog, QWidget, QVBoxLayout, QListWidget, QPushButton, QMessageBox, QHBoxLayout, QLabel, QLineEdit

# Instance de QApplication - à créer ici si nécessaire
app = None

def init_app():
    global app
    if app is None:  # Vérifie si l'application a déjà été initialisée
        app = QApplication(sys.argv)

def msg_log(msg,log,verbose,critical=False): 
    """ Generic routine to store and optionally display warning/error messages 
    Inputs    
        msg         message to be stored/displayed
        log         list to store the messages
        verbose     if True, displays the message on screen
        critical    if True, exits the code
        
    """
    if critical:
        log.append("Error: "+msg)
        raise Exception(msg)
    else:
        log.append("Warning: "+msg)
        if verbose is True:
            print(msg)

def GUI_files_selector():
    """
    INPUTS:
    None
    OUTPUTS:
        -   files       list of path_file that the user selected in the GUI
    """

    init_app()
    log = []
    files, _ = QFileDialog.getOpenFileNames(None, "Select files", "", "Every files (*)")  
    if files:  # if files were selected
        return files
    else:
        msg = "No file(s) selected"
        msg_log(msg, log, verbose=True, critical=True)  # Assume msg_log is defined somewhere
        return []  # Retourne une liste vide en cas d'absence de fichiers
    
def GUI_dico_param_list(dico_param):
    """
    Opens a window to select a parameter from a dictionary.
    Returns the selected parameter.
    """
    init_app()  # Initialization of QApplication

    # Main Windows
    window = QWidget()
    window.setWindowTitle("Select a parameter")
    # Main layout disposition
    layout_principal = QVBoxLayout()
    # Parameter list and their value
    parameters_list = QListWidget()
    for param, value in dico_param.items():
        parameters_list.addItem(f"{param} : {value}")  # Display param and value

    # Add the list to the main layout
    layout_principal.addWidget(parameters_list)
    # Add button "OK"
    validate_button = QPushButton("OK")
    validate_button.setEnabled(False)  # By default in grey
    # Variable that will store the selected parameter
    parameter_selected = None

    # Activation of the button when a parameter is selected
    def on_param_selected():
        validate_button.setEnabled(True)  # Trigger the "OK" button

    # Connect the selection in the list to the on_param_selected function
    parameters_list.itemClicked.connect(on_param_selected)

    def OK_selection():
        nonlocal parameter_selected  # Use an extern variable
        selected_item = parameters_list.currentItem()
        if selected_item:
            parameter_selected = selected_item.text().split(" : ")[0]  # Retrieve the parameter
            QMessageBox.information(window, "Selected parameter", f"You selected: {parameter_selected}")
            window.close()

    validate_button.clicked.connect(OK_selection)
    # Add the button to the main layout
    layout_principal.addWidget(validate_button)
    # Apply the layout to the window
    window.setLayout(layout_principal)
    # Display the window
    window.show()
    # Run and wait for the window closure
    app.exec_()

    return parameter_selected

def GUI_dicodico_param_list(dico_param):
    """
    Opens a window to select a parameter from a dictionary of dictionaries.
    Returns the selected parameter and subparameter.
    """
    init_app()  # Check if the QApplication is welle initialized

    window = QWidget()
    window.setWindowTitle("Sélectionnez un Paramètre")

    layout_principal = QHBoxLayout()

    # First level parameter layout
    parameters_list = QListWidget()
    for param in dico_param.keys():
        parameters_list.addItem(param)  # Only the param is displayed

    layout_principal.addWidget(parameters_list)
    h_layout = QVBoxLayout()
    sub_parameters = QListWidget()
    h_layout.addWidget(sub_parameters)
    # Add the horizontal disposition
    layout_principal.addLayout(h_layout)
    # "OK" button
    validate_button = QPushButton("OK")
    validate_button.setEnabled(False)  # Grey by default

    parameter_selected = None
    sub_parameter_selected = None

    def on_param_selected():
        selected_item = parameters_list.currentItem()
        if selected_item:
            param_key = selected_item.text()
            # update the list of the sub_parameters and their values
            sub_parameters.clear()  # Erase previous sub parameters
            for sous_param, valeur in dico_param[param_key].items():
                sub_parameters.addItem(f"{sous_param} : {valeur}")  # display sub_parameters and their values

            validate_button.setEnabled(False)  # Unactivate the "OK" button until a sub_parameter is selected

    parameters_list.itemClicked.connect(on_param_selected)

    def OK_selection():
        nonlocal parameter_selected, sub_parameter_selected 
        selected_param_item = parameters_list.currentItem()
        selected_sous_param_item = sub_parameters.currentItem()

        if selected_param_item and selected_sous_param_item:
            parameter_selected = selected_param_item.text()  # Retrieve main parameter
            sub_parameter_selected = selected_sous_param_item.text().split(" : ")[0]  # retrieve sub parameter
            QMessageBox.information(window, "Selection", f"You selected : {parameter_selected}\nSub parameter: {sub_parameter_selected}")
            window.close() 

    validate_button.clicked.connect(OK_selection)

    # Connect subparameter list selection to enable button
    sub_parameters.itemClicked.connect(lambda: validate_button.setEnabled(True))

    layout_principal.addWidget(validate_button)
    window.setLayout(layout_principal)
    window.show()
    app.exec_()

    return parameter_selected, sub_parameter_selected

def GUI_scan_list_creator():
    """
    Opens a window and let the user set a scan by putting float values manually and return the list of float created
    """
    init_app()

    window = QWidget()
    window.setWindowTitle("Add float to the list")

    layout_principal = QHBoxLayout()

    # Layout for input box and buttons on the left
    layout_gauche = QVBoxLayout()
    
    label = QLabel("Write a float :")
    float_field = QLineEdit()
    
    # List to display added floats
    floats_list = QListWidget()

    add_button = QPushButton("Add")
    add_button.setEnabled(False)  # Unactivated by default

    delete_button = QPushButton("Delete")
    delete_button.setEnabled(False)  # Unactivated by default

    floats=None

    # function to activate or unactivate the "Add" button
    def check_float():
        try:
            float(float_field.text())  # check if it is a float 
            add_button.setEnabled(True)  # Activate if it is
        except ValueError:
            add_button.setEnabled(False)  # else unactivate

    # Function to add a float to the list
    def add_float():
        try:
            valeur = float(float_field.text())
            floats_list.addItem(str(valeur))
            float_field.clear()
            delete_button.setEnabled(True)  # activate Delete button
            add_button.setEnabled(False)  # Unactivate after adding
        except ValueError:
            QMessageBox.warning(window, "Error", "Plz put a valide number")

    # fucntion to erase from the list the last float
    def supprimer_float():
        if floats_list.count() > 0:
            floats_list.takeItem(floats_list.count() - 1)
            if floats_list.count() == 0:
                delete_button.setEnabled(False)  # Unactivate if the list is empty

    add_button.clicked.connect(add_float)
    delete_button.clicked.connect(supprimer_float)
    float_field.returnPressed.connect(add_float)
    float_field.textChanged.connect(check_float)

    layout_gauche.addWidget(label)
    layout_gauche.addWidget(float_field)
    layout_gauche.addWidget(add_button)
    layout_gauche.addWidget(delete_button)

    layout_principal.addLayout(layout_gauche)
    layout_principal.addWidget(floats_list)

    validate_button = QPushButton("OK")

    def OK_selection():
        nonlocal floats
        floats = [float(floats_list.item(i).text()) for i in range(floats_list.count())]
        QMessageBox.information(window, "Added values", f"Values : {floats}")
        window.close()  

    validate_button.clicked.connect(OK_selection)
    layout_principal.addWidget(validate_button)
    window.setLayout(layout_principal)
    window.show()
    app.exec_()

    return floats


