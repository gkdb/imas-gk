#!/usr/bin/env python3
import argparse
import geneimas
import sys,os
sys.path.append(os.path.join(os.path.dirname(__file__), '../../idspy_toolkit'))
import idspy_toolkit as idspy
sys.path.append(os.path.join(os.path.dirname(__file__), \
        '../../idspy_dictionaries_packages/idspy_dictionaries'))
from idspy_dictionaries import ids_gyrokinetics_local
import pydiag.utils.comm as comm
import pydiag.data.datafiles as datafiles
import pydiag.utils.generun as generun

# Parse input arguments
parser = argparse.ArgumentParser(description="command line tool to convert GENE outputs to GK IDS hdf5 files")

# Mandatory argument(s)
parser.add_argument("file", metavar="file", nargs="+", help="GENE output parameters file(s)")

# Input options
inputargs = parser.add_argument_group("Input options")
inputargs.add_argument("--time", "-t", default="f:l", help="Time window for analysis of nonlinear simulation in the form start:end. "
                                 "Use f/l for the first/last time in the data file.")
# Output options
outputargs = parser.add_argument_group("Output options")
outputargs.add_argument("--provider", "-p", default="", help="Set data provider string to be stored in GK IDS")
outputargs.add_argument("--outfilebase", "-o", default="geneids", help="Set filename base for output files. The original file extension and .h5 will be added")

args = parser.parse_args()

timewin = []
for timeelem in args.time.split(':'):
    if timeelem == 'f':
        timewin += [-1] #-1 is first time
    elif timeelem == "l":
        timewin += [-2] #-2 is last time
    else:
        timewin += [float(timeelem)]

for parfile in args.file:
    if not os.path.isfile(parfile):
        print ("skipping non-existing", parfile)
        continue

    print("Reading GENE run associated to {} ...".format(parfile))
    fext = parfile.split("parameters")[1]

    #Read GENE data (mainly parameters file)
    common = comm.CommonData(fext, timewin[0], timewin[1],ftype='native', verbose=False)

    #overwrite start/end time for linear runs and reset common data structures
    if not common.nonlinear:
        if common.pars['comp_type']=="'EV'":
            #load all EVs (first to last time step)
            timewin = [-1,-2]
        else:
            #read only last time step of linear IV runs
            timewin = [-2,-2]
        common = comm.CommonData(fext, timewin[0], timewin[1],ftype='native', verbose=False)

    rundatafiles = datafiles.RunDataFiles(common)

    generun_obj = generun.GENERUN(common, rundatafiles)
    IDS_gene = geneimas.gene2ids(generun_obj, provider=args.provider)

    outfilename = '{}{}.h5'.format(args.outfilebase,fext)
    g,k = idspy.ids_to_hdf5(IDS_gene,outfilename,overwrite=True)
    print("Finished writing {} ...".format(outfilename))

