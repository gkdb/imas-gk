#!/usr/bin/env python3
import argparse
import geneimas
import sys,os
sys.path.append(os.path.join(os.path.dirname(__file__), '../../idspy_toolkit'))
import idspy_toolkit as idspy
sys.path.append(os.path.join(os.path.dirname(__file__), \
        '../../idspy_dictionaries_packages/idspy_dictionaries'))
from idspy_dictionaries import ids_gyrokinetics_local

#parse input arguments
parser = argparse.ArgumentParser(description="command line tool to convert GK IDS hdf5 files to GENE input parameters files")
parser.add_argument("file", metavar="file", nargs="+", help="GK IDS hdf5 file(s)")

# Input options
inputargs = parser.add_argument_group("Input options")
inputargs.add_argument("--inpars", "-i", default="", help="Optional: Use GENE parameters inpars file as template")
# Output options
outputargs = parser.add_argument_group("Output options")
outputargs.add_argument("--outpars", "-o", default="parameters", help="Set filename base for GENE input file(s) to be written")

args = parser.parse_args()

for ii in range(len(args.file)):
    dum=ids_gyrokinetics_local.GyrokineticsLocal()
    idspy.fill_default_values_ids(dum)
    idspy.hdf5_to_ids(args.file[ii],dum,todict=False)
    testgene = geneimas.ids2gene(dum,"linear",parameters_in=args.inpars,parameters_out=args.outpars)
