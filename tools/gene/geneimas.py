""" geneimas.py: module handling data conversion between
IMAS 'gyrokinetics' IDS and the gyrokinetics turbulence code GENE (genecode.org).

Functions:

- ids2gene(gkids, verbose=False):
    Convert an IMAS 'gyrokinetics' IDS to GENE inputs
    Inputs
        gkids          Instance of the idspy GKIDS class with the reference 'gyrokinetics' IDS
        runtype        'linear' or 'non-linear'
        parameters_in  GENE input file to be considered as template (optional)
        parameters_out GENE output file in which parameters are written (default: "parameters")
    Outputs
        None

- gene2ids(generun, Nsh=10, provider=''):
    Convert GENE inputs and outputs to an IMAS 'gyrokinetics' IDS created with idspy
    Inputs:
        generun         Instance of the GENErun class containing GENE input and output data
        Nsh             Number of moments for flux surface Fourier parametrisation (default Nsh=10)
        provider        Person who ran this GENE simulation (default: global git user name)
    Outputs:
        gkids           Instance of the GKIDS class containing the generated 'gyrokinetics' IDS

Note: This file requires the idspy library for IMAS data manipulation.

For clarity and comparability, it roughly follows the corresponding routines developed by
Yann Camenen (lead developer of IDS GK) for the GKW code. This approach had also been
taken by TGLF, see imas-gk/tools/tglf
"""
### required modules
import sys,os
sys.path.append(os.path.join(os.path.dirname(__file__), '../../idspy_toolkit'))
import idspy_toolkit as idspy
sys.path.append(os.path.join(os.path.dirname(__file__), \
                             '../../idspy_dictionaries_packages/idspy_dictionaries'))
from idspy_dictionaries import ids_gyrokinetics_local
try:
    import pydiag.utils.generun as generun
except:
    print ("ERROR: Couldn't import generun, please add a GENE python-diag folder to PYTHONPATH")
    print ('       Example: export PYTHONPATH="${PYTHONPATH}:${YOURGENEDIR}/python-diag"')
    exit(-1)
import pydiag.utils.ParIO as geneParIO
try:
    from dicttoxml import dicttoxml
except:
    print ("ERROR: Couldn't import dicttoxml, please run: 'pip install dicttoxml' first")
    exit(-1)
try:
    import gkids.tools.FS_param as FS
except:
    print ("ERROR: Couldn't import FS_param, please add the imas-gk folder to PYTHONPATH")
    print ('       Example: export PYTHONPATH="${PYTHONPATH}:${YOURGKIMASDIR}"')
    exit(-1)
import datetime as dt           #needed for saving data/time
import matplotlib.pyplot as plt #remove later
import numpy as np
import re          #regular expressions with strings
from scipy.interpolate import interpolate, CubicSpline
import scipy.constants as codata
import subprocess  #needed to obtain GIT hash
import warnings

def pars_nml_to_dict(pardict,nmldict):
    " Function to combine pardict and nmldict into a nested dict "
    dd=dict()
    for param, nml in nmldict.items():
        if nml not in dd:
            dd[nml] = {}
        if nml[:-1] == 'species':
            dd[nml][param[:-1]] = pardict[param]
        else:
            dd[nml][param] = pardict[param]
    return dd

def ids2gene(gkids,runtype,parameters_in='',parameters_out="parameters"):
    """ Convert an IMAS 'gyrokinetics' IDS to GENE inputs
    Inputs
        gkids          Instance of the idspy GKIDS class with the reference 'gyrokinetics' IDS
        runtype        'linear' or 'non-linear'
        parameters_in  GENE input file to be considered as template (optional)
        parameters_out GENE output file in which parameters are written (default: "parameters")
    Outputs
        None
    """

    assert (runtype in ('linear','non_linear')), \
        "'linear' and 'non_linear' are the only accepted values for runtype"

    # compute IMAS_ref/GENE_ref ratio of reference quantities for GENE to IMAS conversion
    # the conversions are stored in a (modified) dictionary to allow, e.g., loops over fields
    class AttrDict(dict):  #enables addressing keys as attributes, e.g., rat.T instead of rat["T"]
        def __init__(self, *args, **kwargs):
            super(AttrDict, self).__init__(*args, **kwargs)
            self.__dict__ = self
    rat = AttrDict()
    # reference charge ratio
    # q_GENE = codata.physical_constants['elementary charge'][0]
    # q_IMAS = codata.physical_constants['elementary charge'][0] #(IMAS follows CODATA from NIST)
    rat.q = 1.0 #q_GENE/q_IMAS
    # reference mass ratio
    m_GENE = codata.physical_constants['proton mass'][0]
    m_IMAS = codata.physical_constants['deuteron mass'][0] #(IMAS follows CODATA from NIST)
    rat.m = 1.0 #but m_IMAS/m_GENE has to be applied to mref
    rat.T = 1.0
    rat.n = 1.0
    rat.L = 1.0 #R0_IMAS/L_GENE
    rat.B = 1.0
    # reference thermal velocity ratio
    rat.vth = np.sqrt(2.0*rat.T/rat.m) #vth_IMAS/cref_GENE
    # reference Larmor radius ratio
    rat.rho = (rat.m*rat.vth)/(rat.q*rat.B)
    # reference Larmor radius / reference length ratio
    rat.rho_L = rat.rho/rat.L
    # GENE to IMAS radial coordinate conversion
    rat.dr = 1.0

    genepars = geneParIO.Parameters()

    #now read parameters_in if provided to fix order of namelists
    if parameters_in:
        genepars.Read_Pars(parameters_in)

    #add some defaults:
    genepars.add('box','nv0',32)
    genepars.add('box','nw0',16)
    genepars.add('box','lv0',9)
    genepars.add('box','lw0',3)

    genepars.add('in_out','diagdir','./')
    if (runtype=='linear'):
        genepars.add('in_out','istep_field',2000)
        genepars.add('in_out','istep_mom',2000)
    else:
        genepars.add('in_out','istep_field',100)
        genepars.add('in_out','istep_mom',400)

    genepars.add('general','timelim',86000)
    genepars.add('general','calc_dt',True)
    genepars.add('general','hyp_z',2)

    #now reread parameters_in if provided and overwrite defaults
    if parameters_in:
        genepars.Read_Pars(parameters_in)

    genepars.add('general','nonlinear',bool(runtype=="non_linear"))


    ## model
    # gkids.model.adiabatic_electrons #nothing to be done - autodetected by GENE
    # gkids.model.include_a_field_parallel  #nothing to be done - autodetected by GENE
    genepars.add('general','bpar',bool(gkids.model.include_b_field_parallel))
    #genepars.add('geometry',"dpdx_term","'full_drift'" if gkids.model.include_full_curvature_drift else "'gradB_eq_curv'")
    genepars.add('geometry',"dpdx_term","'full_drift'" if not gkids.model.use_mhd_approximation else "'gradB_eq_curv'")
    genepars.add('external_contr',"with_coriolis",bool(gkids.model.include_coriolis_drift))
    genepars.add('external_contr',"with_centrifugal",bool(gkids.model.include_centrifugal_effects))
    genepars.add('external_contr',"with_bxphi0",bool(gkids.model.include_centrifugal_effects))
    genepars.add('external_contr',"with_comoving_other",bool(gkids.model.include_centrifugal_effects))
    if gkids.model.collisions_pitch_only:
        genepars.add('general','collision_op',"'pitch-angle'")
    elif gkids.model.collisions_finite_larmor_radius:
        genepars.add('general','collision_op',"'sugama'") #or exact
    else:
        genepars.add('general','collision_op',"'landau'")
    genepars.add('general','coll_FLR',bool(gkids.model.collisions_finite_larmor_radius))
    if gkids.model.collisions_momentum_conservation != gkids.model.collisions_energy_conservation:
        print("Different collision operator settings for momentum and energy conservation currently unsupported")
        exit(-1)
    if not gkids.model.collisions_momentum_conservation:
        genepars.add('general','coll_cons_model',"'none'")

    ## flux surface
    genepars.add('geometry','magn_geometry',"'miller_mxh'")
    genepars.add('geometry','trpeps',gkids.flux_surface.r_minor_norm)
    genepars.add('geometry','major_R',1.0)
    genepars.add('geometry','major_Z',0.0)
    genepars.add('geometry','q0',np.abs(gkids.flux_surface.q)) #sign set via sign variables
    genepars.add('geometry','shat',gkids.flux_surface.magnetic_shear_r_minor)
    genepars.add('geometry','dpdx_pm',gkids.flux_surface.pressure_gradient_norm)
    genepars.add('geometry','amhd',genepars.pardict['dpdx_pm']*genepars.pardict['q0']**2*\
                 genepars.pardict['major_R'])
    genepars.add('geometry','sign_Bt_CW',-gkids.flux_surface.b_field_tor_sign)
    genepars.add('geometry','sign_Ip_CW',-gkids.flux_surface.ip_sign)
    genepars.add('geometry','drR',gkids.flux_surface.dgeometric_axis_r_dr_minor)
    genepars.add('geometry','drZ',gkids.flux_surface.dgeometric_axis_z_dr_minor)
    genepars.add('geometry','kappa',gkids.flux_surface.elongation)
    genepars.add('geometry','s_kappa',gkids.flux_surface.delongation_dr_minor_norm\
        *gkids.flux_surface.r_minor_norm/gkids.flux_surface.elongation)
    if len(gkids.flux_surface.shape_coefficients_c) > 1:
        genepars.add('geometry','cN_m',','.join(map(str, -gkids.flux_surface.shape_coefficients_c)))
        genepars.add('geometry','sN_m',','.join(map(str, gkids.flux_surface.shape_coefficients_s)))
        genepars.add('geometry','cNdr_m',','.join(map(str, -gkids.flux_surface.dc_dr_minor_norm*gkids.flux_surface.r_minor_norm)))
        genepars.add('geometry','sNdr_m',','.join(map(str, gkids.flux_surface.ds_dr_minor_norm*gkids.flux_surface.r_minor_norm)))
    else:
        genepars.add('geometry','cN_m',-gkids.flux_surface.shape_coefficients_c[0])
        genepars.add('geometry','sN_m',gkids.flux_surface.shape_coefficients_s[0])
        genepars.add('geometry','cNdr_m',-gkids.flux_surface.dc_dr_minor_norm[0]*gkids.flux_surface.r_minor_norm)
        genepars.add('geometry','sNdr_m',gkids.flux_surface.ds_dr_minor_norm[0]*gkids.flux_surface.r_minor_norm)

    ## normalizing quantities
    genepars.add('units','Tref',gkids.normalizing_quantities.t_e/1E3 if gkids.normalizing_quantities.t_e!=9E40 else 0.0)
    genepars.add('units','nref',gkids.normalizing_quantities.n_e/1E19 if gkids.normalizing_quantities.n_e!=9E40 else 0.0)
    genepars.add('units','Bref',gkids.normalizing_quantities.b_field_tor if gkids.normalizing_quantities.b_field_tor!=9E40 else 0.0)
    genepars.add('units','Lref',gkids.normalizing_quantities.r if gkids.normalizing_quantities.r!=9E40 else 0.0)
    genepars.add('units','mref',m_IMAS/m_GENE) #mass measured in units of deuterium mass
    genepars.add('geometry','rhostar',0.0 if ((genepars.pardict['Tref']*genepars.pardict['nref']*genepars.pardict['Bref']) <= 0.0) else -1.0)

    ## species all
    genepars.add('external_contr','Omega0_tor',-gkids.species_all.velocity_tor_norm*rat.vth/rat.L) #e_phi is opposite in GENE/IMAS
    # TO DO - SET SIGN_OMEGA_TOR?
    genepars.add('external_contr','ExBrate',gkids.species_all.shearing_rate_norm*rat.vth/rat.L*rat.dr) #TODO CHECK
    genepars.add('general','beta',gkids.species_all.beta_reference/(rat.n*rat.T)*rat.B**2)
    genepars.add('general','debye2',gkids.species_all.debye_length_norm*rat.rho*np.sqrt((rat.n*rat.q**2)/rat.T))

    ## species
    n_species = len(gkids.species)
    i_ion = 0
    first = True
    genepars.add('box','n_spec',n_species)
    for isp,sp in enumerate(gkids.species,1):
        if sp.charge_norm > 0:
            i_ion += 1
            genepars.add('species{}'.format(isp),'name{}'.format(isp),"'ions{}'".format(i_ion))
        else:
            genepars.add('species{}'.format(isp),'name{}'.format(isp),"'electrons'")
        genepars.add('species{}'.format(isp),'charge{}'.format(isp),sp.charge_norm)
        genepars.add('species{}'.format(isp),'mass{}'.format(isp),sp.mass_norm*rat.m)
        genepars.add('species{}'.format(isp),'dens{}'.format(isp),sp.density_norm)
        genepars.add('species{}'.format(isp),'omn{}'.format(isp),sp.density_log_gradient_norm)
        genepars.add('species{}'.format(isp),'temp{}'.format(isp),sp.temperature_norm)
        genepars.add('species{}'.format(isp),'omt{}'.format(isp),sp.temperature_log_gradient_norm)
        genepars.add('species{}'.format(isp),'passive{}'.format(isp),sp.density_norm==0.0)
        thispfsrate = gkids.flux_surface.ip_sign * sp.velocity_tor_gradient_norm * \
            gkids.flux_surface.r_minor_norm / np.abs(gkids.flux_surface.q) * rat.vth
        if (isp==1):
            pfsrate = thispfsrate
        elif pfsrate != thispfsrate:
            if first:
                print("WARNING: GENE uses a species-independent parallel flow shear")
                first = False
            print("Using pfsrate {} instead of {}".format(coll,thiscoll))
    genepars.add('external_contr','pfsrate',pfsrate)

    ## collisions
    first = True
    eps0 = 1./(4.0*np.pi)   #epsilon0 = 1/4pi or 1?
    for ispec in range(0,n_species):
        for jspec in range(0,n_species):
            thiscoll = np.round(gkids.collisions.collisionality_norm[ispec][jspec]*\
                    rat.vth/rat.L*4.0*np.pi**2*eps0**2\
                    /(gkids.species[jspec].density_norm*rat.n*\
                      gkids.species[ispec].charge_norm**2*\
                      gkids.species[jspec].charge_norm**2)\
                    *np.sqrt(gkids.species[ispec].mass_norm*rat.m*\
                            (gkids.species[ispec].temperature_norm*rat.T)**3),10)
            if (ispec==0 and jspec==0):
                coll = thiscoll
            if coll != thiscoll:
                if first:
                    print("WARNING: GENE uses a species-independent Coulomb Logarithm")
                    first = False
                print("Using collisionality_norm[0][0] value of {} instead of {}".format(coll,thiscoll))

    genepars.add('general','coll',coll)
    if coll>0.0:
        genepars.add('general','hyp_v',0.0)
    else:
        genepars.add('general','hyp_v',0.05)

    # reconstruct flux surface to obtain metric required for wave number conversion
    nth = 2000
    theta_grid = (np.arange(0.0,nth,1.0))*2.0*np.pi/nth
    R_check,Z_check,R_rho,Z_rho,a_check,B_new,gxx,gxx_th0,gxy_th0,gyy_th0,theta_pol,C_y=\
        get_flux_surface_from_mxh(theta_grid,gkids.flux_surface.r_minor_norm,gkids.flux_surface.q,\
                                  1.0,gkids.flux_surface.dgeometric_axis_r_dr_minor,\
                                  0.0,gkids.flux_surface.dgeometric_axis_z_dr_minor,\
                                  gkids.flux_surface.elongation,gkids.flux_surface.delongation_dr_minor_norm,\
                                  gkids.flux_surface.shape_coefficients_c,gkids.flux_surface.dc_dr_minor_norm,\
                                  gkids.flux_surface.shape_coefficients_s,gkids.flux_surface.ds_dr_minor_norm,\
                                  Nsh=len(gkids.flux_surface.shape_coefficients_c))

    #transformation of wavevector components on a z grid
    #following D. Told, PhD thesis, Sec. A.3, p. 167
    k1facx = np.sqrt(gxx_th0)
    #k1facy = np.round(gxy_th0/np.sqrt(gxx_th0),12) = 0.0 (since gxy_th0 = 0.0)
    k2fac = np.sqrt(gyy_th0) #-gxy_th0**2/gxx_th0) #gxy = 0
    #print('binormal wv, gyy_th0, k2fac, rat.rho = ', gkids.linear.wavevector[0].binormal_wavevector_norm,gyy_th0, k2fac, rat.rho)

    ## linear
    if (runtype=='linear'):
        genepars.add('box','nky0',1)
        genepars.add('box','ky0_ind',1)
        kylist = str(np.round(gkids.linear.wavevector[0].binormal_wavevector_norm/(k2fac*rat.rho),12))
        kxclist = str(np.round(gkids.linear.wavevector[0].radial_wavevector_norm/(k1facx*rat.rho),12))
        if gkids.linear.wavevector[0].eigenmode:
            nx0list = str(gkids.linear.wavevector[0].eigenmode[0].poloidal_turns)
            nz0list = str(int(len(gkids.linear.wavevector[0].eigenmode[0].angle_pol)/
                              gkids.linear.wavevector[0].eigenmode[0].poloidal_turns))
        else: #if no eigenmode is stored in h5 file
            nx0list = str(15)
            nz0list = str(24)
        if len(gkids.linear.wavevector)>1:
            kylist = kylist + " !scanlist:" + kylist
            kxclist = kxclist + " !scanwith:kymin(1)," + kxclist
            nx0list = nx0list + " !scanwith:kymin(1)," + nx0list
            nz0list = nz0list + " !scanwith:kymin(1)," + nz0list
        for i in range(1,len(gkids.linear.wavevector)):
            kylist += ","+str(gkids.linear.wavevector[i].binormal_wavevector_norm/(k2fac*rat.rho))
            kxclist += ","+str(gkids.linear.wavevector[i].radial_wavevector_norm/(k1facx*rat.rho))
            nx0list += ","+str(gkids.linear.wavevector[i].eigenmode[0].poloidal_turns)
            nz0list += ","+str(int(len(gkids.linear.wavevector[i].eigenmode[0].angle_pol)/
                               gkids.linear.wavevector[i].eigenmode[0].poloidal_turns))
        genepars.add('box','kymin',kylist)
        genepars.add('box','kx_center',kxclist)
        genepars.add('box','nx0',nx0list)
        genepars.add('box','nz0',nz0list)
    else:
        print ('ERROR: nonlinear runs not yet supported by ids2gene')
        exit(-1)


    genepars.Write_Pars(parameters_out)


def gene2ids(generun,Nsh=10,provider=''):
    """ Convert GENE inputs and outputs to an IMAS 'gyrokinetics' IDS created with idspy
    Inputs
     generun         Instance of the GENErun class containing GENE input and output data
     Nsh             Number of moments for flux surface Fourier parametrisation (default Nsh=10)
     provider        Person who ran this GENE simulation (default: global git user name)
    Outputs
     gkids           Instance of the GKIDS class containing the generated 'gyrokinetics' IDS
    """

    # some pre initialisation
    scriptpath=os.path.dirname(os.path.realpath(__file__))
    thiscommit = subprocess.check_output(['git','-C',scriptpath,'rev-parse','HEAD']).decode('ascii').strip()
    print("*** This is gene2ids - commit {0:57s} ***".format(thiscommit))
    print("*** DISCLAIMER: beta version - benchmarks of many features are still pending ...        ***")
    print("*** Translation of basic parameters such as flux surface parametrization / normalization***")
    print("*** should be fine. But Re/Im signs of ballooning mode structures need to be confirmed  ***")
    print("*** for the GK IDS COCOS=17 choice of coordinates. For nonlinear simulations, only      ***")
    print("*** phase-space averaged fluxes and ky spectra are currently stored.                    ***")

    # - determine index of electron species
    ispec_electrons = -1111
    for spec in generun.cm.specnames:
        ispec = generun.cm.specnames.index(spec) + 1
        if np.isclose(generun.cm.pars["charge{}".format(ispec)],-1.0, atol=1e-06):
            ispec_electrons = ispec
    assert not(ispec_electrons == -1111), \
         "Adiabatic electrons not allowed in a GK IDS" #TODO - Why is there an IDS variable if not allowed?

    # initialise an empty GK IDS structure
    gkids=ids_gyrokinetics_local.GyrokineticsLocal()
    idspy.fill_default_values_ids(gkids)

    ## ids_properties
    if not provider:
        # assuming that GIT is installed / used on most user systems, try to determine
        # provider name from git config user.name
        try:
            res = subprocess.run(["git", "config", "user.name"], stdout=subprocess.PIPE)
            provider = res.stdout.strip().decode()
        except:
            provider = ''
            #do nothing
    gkids.ids_properties.provider = provider #name of the person creating the IDS
    #gkids.ids_properties.creation_date = str(dt.date.today())
    if generun.pars['DATETIME']:
        datetime_obj = dt.datetime.strptime(generun.pars['DATETIME'], '%Y%m%d %H%M%S.%f')
        gkids.ids_properties.creation_date = datetime_obj.strftime('%Y-%m-%d')
    gkids.ids_properties.comment = 'IDS data creation time: '+str(dt.date.today())+\
        ", NOTE: GENE NRG-FILES DO NOT ALLOW FOR SEPARATION OF A/B_FIELD_PARALLEL CONTRIBUTIONS"+\
        " - CURRENTLY BOTH OF THEM ARE ATTRIBUTED TO A_FIELD_PARALLEL in GK IDS"

    gkids.ids_properties.homogeneous_time = 2 #set to 2 for local GK data model

    ## code
    gkids.code.name="GENE"
    gkids.code.version = generun.pars['RELEASE']
    gkids.code.commit = generun.pars['GIT_BRANCH'][0:9]
    gkids.code.repository="https://gitlab.mpcdf.mpg.de/GENE/gene"

    ## libraries
    if hasattr(generun, 'traceback'):
        dum=ids_gyrokinetics_local.Library()
        dum.name=generun.__module__
        dum.description="Python module to load the raw GENE data into a GENErun class"
        dum.commit=generun.traceback['git_commit']
        dum.repository=generun.traceback['git_url']
        gkids.code.library.append(dum)

    dum=ids_gyrokinetics_local.Library()
    dum.name=gene2ids.__module__+'.gene2ids'
    dum.description="Python function to map GENE data from a GENErun class into a 'gyrokinetics_local' IDS"
    dum.commit=thiscommit
    dum.repository=subprocess.check_output(['git','-C',scriptpath,'config','--get','remote.origin.url']).decode('ascii').strip()
    gkids.code.library.append(dum)


    ## model
    gkids.model.adiabatic_electrons=int(ispec_electrons < 0)
    gkids.model.include_a_field_parallel=int(generun.pars['n_fields']>1)
    gkids.model.include_b_field_parallel=int(generun.pars['n_fields']>2)

    #gkids.model.include_full_curvature_drift=int(generun.pars['dpdx_term']=="'full_drift'")
    gkids.model.use_mhd_approximation=int(not generun.pars['dpdx_term']=="'full_drift'")

    gkids.model.include_coriolis_drift=int(generun.pars['with_coriolis'])
    gkids.model.include_centrifugal_effects=int(generun.pars['with_centrifugal'])

    gkids.model.collisions_pitch_only = int(generun.pars['collision_op']=="'pitch-angle'")
    gkids.model.collisions_momentum_conservation = int(generun.pars['coll_cons_model']!='none'
                                                     if generun.pars['coll'] > 0.0 else False)
    gkids.model.collisions_energy_conservation = gkids.model.collisions_momentum_conservation

    gkids.model.collisions_finite_larmor_radius = int((generun.pars['collision_op']=='sugama' or
                                                       generun.pars['collision_op']=='exact') and
                                                      generun.cm.coll_FLR)

    ## flux surface (required for normalization conversion)
    if generun.pars['magn_geometry']=="'miller_mxh'":
        #directly use Miller MXH but denormalize for plotting etc
        Lref = generun.pars['Lref']
        R0=generun.pars['major_R']*Lref
        Z0=generun.pars['major_Z']*Lref
        dR0dr=generun.pars['drR']
        dZ0dr=generun.pars['drZ']
        r=generun.pars['trpeps']*R0
        kappa=generun.pars['kappa']
        dkappa_dr=generun.pars['s_kappa']*kappa/r
        cN=-np.fromstring(str(generun.pars['cN_m']),dtype=float,sep=',')
        cN_dr=-np.fromstring(str(generun.pars['cNdr_m']),dtype=float,sep=',')/r
        sN=np.fromstring(str(generun.pars['sN_m']),dtype=float,sep=',')
        sN_dr=np.fromstring(str(generun.pars['sNdr_m']),dtype=float,sep=',')/r
        dxdr = 1.0
        Brat = 1.0
    else:
        R0,dR0dr,Z0,dZ0dr,r,kappa,dkappa_dr,cN,cN_dr,sN,sN_dr,dxdr,Brat = \
            genegeom2mxh(generun.geom,generun.cm.pnt.q0,Nsh)

    # compute GENE_ref/IMAS_ref ratio of reference quantities for GENE to IMAS conversion
    # the conversions are stored in a (modified) dictionary to allow, e.g., loops over fields
    class AttrDict(dict):  #enables addressing keys as attributes, e.g., rat.T instead of rat["T"]
        def __init__(self, *args, **kwargs):
            super(AttrDict, self).__init__(*args, **kwargs)
            self.__dict__ = self
    rat = AttrDict()
    # reference charge ratio
    # q_GENE = codata.physical_constants['elementary charge'][0]
    # q_IMAS = codata.physical_constants['elementary charge'][0] #(IMAS follows CODATA from NIST)
    rat.q = 1.0 #q_GENE/q_IMAS
    # reference mass ratio
    m_GENE = generun.cm.pnt.mref*codata.physical_constants['proton mass'][0]
    m_IMAS = codata.physical_constants['deuteron mass'][0] #(IMAS follows CODATA from NIST)
    rat.m = m_GENE/m_IMAS
    # reference temperature ratio (T_GENE depends on user choice, T_IMAS = Te)
    if ispec_electrons == -1111.0: #No electrons found, assume ions are normalized to Te
        #TODO IMPROVE BY CONSIDERING TAU (IF ADIABATIC ELECTRONS ARE PERMITTED)
        rat.T = 1.0/generun.pars["temp{}".format(1)]

    else:
        rat.T = 1.0/generun.pars["temp{}".format(ispec_electrons)] #Tref_GENE/Tref_IMAS
    # reference density ratio
    if ispec_electrons == -1111.0: #No electrons found, assume nref=ne (TODO CHECK)
        rat.n = 1.0/generun.pars["dens{}".format(1)]
    else:
        rat.n = 1.0/generun.pars["dens{}".format(ispec_electrons)] #nref_GENE/nref_IMAS
    # reference length ratio
    L_GENE = generun.cm.pnt.Lref
    rat.L = L_GENE/R0
    # reference magnetic field ratio B_GENE / B_IMAS
    rat.B = Brat
    # reference thermal velocity ratio
    rat.vth = np.sqrt(0.5*rat.T/rat.m) #cs_GENE/vth_IMAS
    # reference Larmor radius ratio
    rat.rho = (rat.m*rat.vth)/(rat.q*rat.B)
    # reference Larmor radius / reference length ratio
    rat.rho_L = rat.rho/rat.L
    # GENE to IMAS radial coordinate conversion
    rat.dr = dxdr #here, x and r have dimension of length, dx/dr is dimensionless
    # field conversion
    rat.phi = rat.T/rat.q*rat.rho_L
    rat.apar = rat.L*rat.B*rat.rho_L**2
    rat.bpar = rat.B*rat.rho_L
    # species independent flux (particle, momentum, heat) conversion
    rat.pflux = rat.n*rat.vth*rat.rho_L**2/rat.dr
    rat.mflux = -rat.n*rat.m*rat.L*(rat.vth*rat.rho_L)**2/rat.dr #LREF? TODO CHECK, MINUS DUE TO TOROIDAL DIR
    rat.qflux = rat.T*rat.pflux
    rat.Gammaflux = rat.pflux
    rat.Qflux = rat.qflux
    rat.Pitorparflux = rat.mflux
    rat.Pitorperpflux = rat.mflux

    # string conversions
    imas_string = {"phi" : "phi_potential",
                   "apar" : "a_field_parallel",
                   "bpar" : "b_field_parallel",
                   "dens" : "density",
                   "upar" : "j_parallel",
                   "tpar" : "temperature_parallel",
                   "tperp" : "temperature_perpendicular",
                   "Gamma" : "particles",
                   "Q"     : "energy",
                   "Pitorpar"    : "momentum_tor_parallel",
                   "Pitorperp"   : "momentum_tor_perpendicular"}

    print ("Tref_GENE/Tref_IMAS = ", rat.T)
    print ("mref_GENE/mref_IMAS = ", rat.m)
    print ("nref_GENE/nref_IMAS = ", rat.n)
    print ("Bref_GENE/Bref_IMAS = ", rat.B)
    print ("Lref_GENE/Lref_IMAS = ", rat.L)
    print ("dx_GENE/dr_IMAS = ", rat.dr)
    print ("grad. conversion: ", rat.dr/rat.L)
    #For comparison with Miller script in GENE:
    #        print ("drho_tor/dr = ", rat.dr/generun.cm.pnt.Lref)
    print ("rho_Lref_GENE/rho_Lref_IMAS = ", rat.rho_L)

    # signs pointing in phi direction, i.e. cnt-clockwise from top
    # GENE also from top view
    s_Ip = -generun.pars['sign_Ip_CW']
    s_Bt = -generun.pars['sign_Bt_CW']
    gkids.flux_surface.r_minor_norm=r/R0
    gkids.flux_surface.q=s_Ip*s_Bt*np.abs(generun.pars['q0'])
    if generun.pars['magn_geometry']=="'chease'":
        #TODO: find a better way? redefine x0 for chease?
        gkids.flux_surface.magnetic_shear_r_minor=generun.pars['shat']*r\
            /(generun.pars['flux_pos']*generun.pars['minor_r']*generun.pars['Lref'])*rat.dr
    else:
        gkids.flux_surface.magnetic_shear_r_minor=generun.pars['shat']*r\
            /(generun.pars['x0']*generun.pars['Lref'])*rat.dr
    gkids.flux_surface.pressure_gradient_norm=generun.pars['dpdx_pm']\
        /rat.L*rat.B**2*rat.dr
    gkids.flux_surface.b_field_tor_sign=s_Bt
    gkids.flux_surface.ip_sign=s_Ip
    gkids.flux_surface.dgeometric_axis_r_dr_minor=float(dR0dr)
    gkids.flux_surface.dgeometric_axis_z_dr_minor=float(dZ0dr)
    gkids.flux_surface.elongation=float(kappa)
    gkids.flux_surface.delongation_dr_minor_norm=float(dkappa_dr)*R0
    gkids.flux_surface.shape_coefficients_c=cN
    gkids.flux_surface.shape_coefficients_s=sN
    gkids.flux_surface.dc_dr_minor_norm=cN_dr*R0
    gkids.flux_surface.ds_dr_minor_norm=sN_dr*R0

    #transformation of wavevector components on a z grid
    #following D. Told, PhD thesis, Sec. A.3, p. 167
    k1facx = np.sqrt(generun.geom.gxx)
    k1facy = np.round(generun.geom.gxy/np.sqrt(generun.geom.gxx),12)
    k2fac = np.sqrt(generun.geom.gyy-generun.geom.gxy**2/generun.geom.gxx)

    ## normalizing quantities (TODO - ONLY WRITE IF SET)
    gkids.normalizing_quantities.t_e = generun.cm.pnt.Tref/rat.T*1E3  #rat.T = Tref_GENE/Tref_IMAS
    gkids.normalizing_quantities.n_e = generun.cm.pnt.nref/rat.n*1E19 #rat.n = nref_GENE/nref_IMAS
    gkids.normalizing_quantities.b_field_tor = generun.cm.pnt.Bref/rat.B #rat.B = Bref_GENE/Bref_IMAS
    gkids.normalizing_quantities.r = R0

    ## species all
    gkids.species_all.velocity_tor_norm = -generun.pars["Omega0_tor"]*rat.vth/rat.L #e_phi is opposite in GENE/IMAS
    gkids.species_all.shearing_rate_norm = generun.pars["ExBrate"]*r/(generun.pars["x0"]*L_GENE)*\
        rat.vth/rat.L*rat.dr
    gkids.species_all.beta_reference = generun.pars['beta']/(rat.n*rat.T)*rat.B**2
    gkids.species_all.debye_length_norm = np.sqrt(generun.pars['debye2']) * \
        rat.rho*np.sqrt((rat.n*rat.q**2)/rat.T)
    #gkids.species_all.angle_pol = []

    ## species
    for ispec in range(1,generun.pars['n_spec']+1):
        dum=ids_gyrokinetics_local.Species()
        #dum.name = generun.pars["name{}".format(ispec)]
        dum.charge_norm = generun.pars["charge{}".format(ispec)]*rat.q
        dum.mass_norm = generun.pars["mass{}".format(ispec)]*rat.m
        dum.density_norm = (0.0 if generun.pars["passive{}".format(ispec)] else
                            generun.pars["dens{}".format(ispec)]*rat.n)
        dum.density_log_gradient_norm = generun.pars["omn{}".format(ispec)]/rat.L*rat.dr
        dum.temperature_norm = generun.pars["temp{}".format(ispec)]*rat.T
        dum.temperature_log_gradient_norm = generun.pars["omt{}".format(ispec)]/rat.L*rat.dr
        dum.velocity_tor_gradient_norm = - generun.pars['sign_Ip_CW']*generun.pars["pfsrate"]\
            *abs(generun.pars["q0"])/generun.pars["x0"]*rat.vth/rat.L**2*rat.dr
        gkids.species.append(dum)

    ## collisions
    if (generun.pars["Zeff"]!=1.0):
        print ("WARNING: Zeff is not yet considered in collisionality_norm!")
    eps0 = 1./(4.0*np.pi)   #epsilon0 = 1/4pi or 1?
    gkids.collisions.collisionality_norm=np.full((generun.pars['n_spec'],generun.pars['n_spec']),0.0)
    for ispec in range(0,generun.pars['n_spec']):
        for jspec in range(0,generun.pars['n_spec']):
            gkids.collisions.collisionality_norm[ispec,jspec] = \
                (0.0 if generun.cm.pars["passive{}".format(jspec+1)] else
                 rat.vth/(rat.L*4.0*np.pi**2*eps0**2)*\
                 gkids.species[ispec].charge_norm**2*\
                 gkids.species[jspec].charge_norm**2*\
                 gkids.species[jspec].density_norm/rat.n\
                 /np.sqrt(gkids.species[ispec].mass_norm/rat.m*\
                         (gkids.species[ispec].temperature_norm/rat.T)**3)*\
                  generun.pars['coll'])

    ### common arrays for linear / nonlinear
    res = get_theta_pol(R0,Z0,generun.geom.R,generun.geom.Z)
    thetapol_orig = res["theta"]
    thetapol_sort = res["theta_sort"]
    sort_ind = res["sort_ind"]

    #transformation of wavevector components on a z grid
    #following D. Told, PhD thesis, Sec. A.3, p. 167
    #k1facx = np.sqrt(generun.geom.gxx)
    #k1facy = np.round(generun.geom.gxy/np.sqrt(generun.geom.gxx),12)
    #k2fac = np.sqrt(generun.geom.gyy-generun.geom.gxy**2/generun.geom.gxx)

    #now interpolate to thetapol = 0
    #interpolate to equidist. grid with proper order (interpol requires increasing arr)
    gxx_th0 = np.interp(0.0,thetapol_orig[::s_Ip],generun.geom.gxx[::s_Ip])
    gxy_th0 = np.round(np.interp(0.0,thetapol_orig[::s_Ip],generun.geom.gxy[::s_Ip]),7)
    gyy_th0 = np.interp(0.0,thetapol_orig[::s_Ip],generun.geom.gyy[::s_Ip])
    #print("gxx_th0,gxy_th0,gyy_th0 = ", gxx_th0, gxy_th0, gyy_th0)
    k1facx_th0 = np.sqrt(gxx_th0)/rat.rho
    k1facy_th0 = np.round(gxy_th0/np.sqrt(gxx_th0)/rat.rho,7)
    k2fac_th0 = np.sqrt(gyy_th0-gxy_th0**2/gxx_th0)/rat.rho
    #print ("k1facx_th0, k1facy_th0, k2fac_th0: ", k1facx_th0, k1facy_th0, k2fac_th0)


    ## linear
    if not generun.cm.pnt.nonlinear:
        gkids.linear=ids_gyrokinetics_local.Linear()

        Cyq0_x0 = generun.geom.Cy * generun.cm.pnt.q0 / generun.cm.pnt.x0
        if generun.cm.pnt.adapt_lx:
            absnexc = 1
        else:
            absnexc = int(np.round(generun.cm.pnt.lx*generun.cm.pnt.n_pol*
                    np.abs(generun.cm.pnt.shat)*generun.cm.pnt.kymin*
                    np.abs(Cyq0_x0)))
        nconn = int(int(int(generun.cm.pnt.nx0-1)/2)/(absnexc*generun.cm.pnt.ky0_ind))*2+1

        theta_full = []
        for iconn in range(-int(nconn/2),int(nconn/2)+1):
            theta_full += list(thetapol_orig-np.sign(thetapol_orig[0])*iconn*2.0*np.pi)
        fullsort_ind = np.argsort(theta_full)
        thetafullsort = np.array(theta_full)[fullsort_ind]
        ntheta=len(theta_full)

        ky = generun.cm.pnt.kymin*generun.cm.pnt.ky0_ind

        ## wavevector
        dum=ids_gyrokinetics_local.Wavevector()
        dum.radial_wavevector_norm=(generun.cm.pnt.kx_center*k1facx_th0+ky*k1facy_th0)
        dum.binormal_wavevector_norm=ky*k2fac_th0
        gkids.linear.wavevector.append(dum)

        ## eigenmodes
        if generun.pars["comp_type"]=="'IV'":
            ievrange=[-1,]
        else:
            ievrange=range(len(generun.eigendat.growth_rate))

        for iev in range(generun.pars["n_ev"]):
            if generun.eigendat.growth_rate[iev] <= 0.0:
                continue

            dum=ids_gyrokinetics_local.Eigenmode()

            # grids
            dum.angle_pol=thetafullsort
            dum.poloidal_turns=nconn
#            dum.time_norm=
            dum.initial_value_run=int(generun.pars["comp_type"]=="'IV'")

            #code
            dum.code=ids_gyrokinetics_local.CodePartialConstant()
            dum.code.parameters=dicttoxml(pars_nml_to_dict(generun.pars,generun.cm.nmldict),
                                          return_bytes=False)
            dum.code.output_flag=0

            #eigenvalues
            dum.growth_rate_norm=generun.eigendat.growth_rate[iev]*rat.vth/rat.L
            dum.frequency_norm=generun.eigendat.frequency[iev]*rat.vth/rat.L
            dum.growth_rate_tolerance=generun.cm.pnt.omega_prec*rat.vth/rat.L

            # eigenfunctions

            # determine normalization amplitude A_f for parallel mode structures
            rat.Af = 0.0
            for field in generun.fields:
                rat.Af += np.trapz(np.abs(generun.fieldmodestruct.ballamps[field][iev][fullsort_ind]\
                                          *rat[field])**2,dum.angle_pol)
            rat.Af = np.sqrt(rat.Af/(2.0*np.pi))

            # determine rotation on fine grid
            th_fine=np.linspace(dum.angle_pol[0],dum.angle_pol[-1],len(dum.angle_pol)*100)
            phi_fine=np.interp(th_fine,dum.angle_pol,
                               adjust_y_direction(generun.fieldmodestruct.ballamps["phi"][iev][fullsort_ind],
                               gkids.flux_surface.ip_sign))
            phi_fine_absmax_ind = np.argmax(np.abs(phi_fine))
            rotate_imas = np.abs(phi_fine[phi_fine_absmax_ind]) / phi_fine[phi_fine_absmax_ind]
            #GENE's grad y points in -sign_Ip_CW * e_phi(COCOS=12) direction
            #that is sign_Ip_CW * e_phi(COCOS=17) = -Ip_sign * e_phi(COCOS=17)
            #USE CONJG FOR Ip_sign > 0?

            # write field eigenmode structure information (norm, weight, parity)
            dum.fields=ids_gyrokinetics_local.EigenmodeFields()
            for var in generun.fields: #phi (, apar (, bpar))
                setattr(dum.fields,imas_string[var]+"_perturbed_norm",
                        (adjust_y_direction(generun.fieldmodestruct.ballamps[var][iev][fullsort_ind]*\
                                            rat[var]*rotate_imas/rat.Af,gkids.flux_surface.ip_sign)).reshape(ntheta,1))
                setattr(dum.fields,imas_string[var]+"_perturbed_weight",
                        [np.sqrt(np.trapz(np.abs(getattr(dum.fields,imas_string[var]+"_perturbed_norm"))**2,
                                          dum.angle_pol,axis=0)/(2*np.pi))])
                setattr(dum.fields,imas_string[var]+"_perturbed_parity",
                        [np.abs(np.trapz(getattr(dum.fields,imas_string[var]+"_perturbed_norm"),dum.angle_pol,axis=0))
                         / np.trapz(np.abs(getattr(dum.fields,imas_string[var]+"_perturbed_norm")),dum.angle_pol,axis=0)])

            # TO DO: ADD MOMENTS - FOR NOW ONLY DENSITY, J_PARALLEL, PRESSURE_PARALLEL, PRESSURE_PERPENDICULAR
            ddum = ids_gyrokinetics_local.MomentsLinear()
            #define empty array to consider the time dimension (3rd dimension, not used)
            ddum.density=np.empty([generun.pars['n_spec'],len(fullsort_ind),1],dtype=ddum.density.dtype)
            ddum.j_parallel,ddum.pressure_parallel,ddum.pressure_perpendicular=(np.empty_like(ddum.density) for i in range(3))
            for isp,spec in enumerate(generun.cm.specnames):
                ispec = generun.cm.specnames.index(spec) #w/o +1
                ddum.density[isp,:,0] = adjust_y_direction(generun.mommodestruct[ispec].ballamps["dens"][iev][fullsort_ind]*\
                                                     rat.n*rat.rho_L*rotate_imas/rat.Af,gkids.flux_surface.ip_sign)
                ddum.j_parallel[isp,:,0] = adjust_y_direction(generun.mommodestruct[ispec].ballamps["upar"][iev][fullsort_ind]\
                                            #/(generun.pars["dens{}".format(ispec+1)]*rat.n)\
                                            *generun.pars["charge{}".format(ispec+1)]\
                                            *rat.vth*rat.rho_L*rotate_imas/rat.Af,gkids.flux_surface.ip_sign) #TODO CHECK Z MULTIPLICATION WITH YANN
                ddum.pressure_parallel[isp,:,0] = adjust_y_direction(generun.mommodestruct[ispec].ballamps["tpar"][iev][fullsort_ind]+
                                                                     generun.mommodestruct[ispec].ballamps["dens"][iev][fullsort_ind],
                                                                     gkids.flux_surface.ip_sign)*\
                                                                     1./3.*rat.n*rat.T*generun.pars["temp{}".format(ispec+1)]*rat.rho_L*\
                                                                     rotate_imas/rat.Af
                ddum.pressure_perpendicular[isp,:,0] = adjust_y_direction(generun.mommodestruct[ispec].ballamps["tperp"][iev][fullsort_ind]+
                                                                          generun.mommodestruct[ispec].ballamps["dens"][iev][fullsort_ind],
                                                                          gkids.flux_surface.ip_sign)*\
                                                                          2./3.*rat.n*rat.T*generun.pars["temp{}".format(ispec+1)]*rat.rho_L*\
                                                                          rotate_imas/rat.Af
            dum.moments_norm_particle = ddum


            # Linear weights (fluxes) per eigenmode, FS averaged
            # NOTE/TODO: GENE NRG-FILES DO NOT ALLOW FOR SEPARATION OF A/B_FIELD_PARALLEL CONTRIBUTIONS
            # ALTERNATIVE: RECOMPUTE FLUXES FROM MOM-FILES (Pullback will still contain both fields though)
            ddum = ids_gyrokinetics_local.Fluxes()
            labfluxfield = []

            use_nrg_data_for_linear_weights = False #can't get accurate toroidal angular momentum fluxes this way
            if use_nrg_data_for_linear_weights: #Use data from NRG files (faster)
                for spec in generun.cm.specnames:
                    ispec = generun.cm.specnames.index(spec) #w/o +1
                    ddum.particles_phi_potential += [generun.nrgdat.dataarray[iev,ispec,4]*rat.pflux*(1.0/rat.Af)**2]
                    ddum.particles_a_field_parallel += [generun.nrgdat.dataarray[iev,ispec,5]*rat.pflux*(1.0/rat.Af)**2]
                    #TODO - replace parallel momentum flux by parallel toroidal momentum flux
                    ddum.momentum_tor_parallel_phi_potential += [generun.nrgdat.dataarray[iev,ispec,8]*rat.mflux*(1.0/rat.Af)**2] # FIX THIS
                    ddum.momentum_tor_parallel_a_field_parallel += [generun.nrgdat.dataarray[iev,ispec,9]*rat.mflux*(1.0/rat.Af)**2]
                    #ddum.momentum_tor_perpendicular_phi_potential": None, #TODO -- ADD PERPENDICULAR MOMENTUM
                    #ddum.momentum_tor_perpendicular_a_field_parallel": None,
                    ddum.energy_phi_potential += [generun.nrgdat.dataarray[iev,ispec,6]*rat.qflux*(1.0/rat.Af)**2]
                    ddum.energy_a_field_parallel += [generun.nrgdat.dataarray[iev,ispec,7]*rat.qflux*(1.0/rat.Af)**2]
            else:
                for fluxfield in generun.fluxspectra[0].fluxes_ky.keys():
                    [flux,field]= fluxfield.split("_")
                    if flux == "Pi":
                        continue
                    if flux[0:4] == "LAB-":
                        labfluxfield += [fluxfield[4:]]
                        continue
                    if flux == "Pitorpar" and (field in ["bpar"]): # TODO check if IDS accepts apar again
                        continue
                    dddum = []
                    for isp,spec in enumerate(generun.cm.specnames):
                        dddum += [float(generun.fluxspectra[isp].fluxes_ky[flux+"_"+field][iev])*rat[flux+"flux"]*(1.0/rat.Af)**2]
                    setattr(ddum,imas_string[flux]+"_"+imas_string[field],dddum)

            #split into co-moving / lab frame if angular background velocity present
            if generun.cm.pars["Omega0_tor"] != 0 or generun.cm.pars["omegatorref"] != 0:
                dum.linear_weights_rotating_frame = ddum
                if labfluxfield:
                    ddum = ids_gyrokinetics_local.Fluxes()
                    for fluxfield in labfluxfield:
                        [flux,field]= fluxfield.split("_")
                        if flux == "Pitorpar" and (field in ["apar", "bpar"]): # TODO check if IDS accepts apar again
                            continue
                        dddum = []
                        for isp,spec in enumerate(generun.cm.specnames):
                            dddum += [float(generun.fluxspectra[isp].fluxes_ky["LAB-"+flux+"_"+field][iev])*rat[flux+"flux"]*(1.0/rat.Af)**2]
                        setattr(ddum,imas_string[flux]+"_"+imas_string[field],dddum)
                    dum.linear_weights = ddum
            else:
                dum.linear_weights = ddum
                dum.linear_weights_rotating_frame = ddum

            gkids.linear.wavevector[-1].eigenmode.append(dum)

    else:  #nonlinear

        gkids.non_linear=ids_gyrokinetics_local.NonLinear()

        kx = 2.0 * np.pi / generun.cm.pnt.lx * np.arange(generun.pars["nx0"],dtype=float)
        ky = generun.cm.pnt.kymin*np.arange(generun.pars["nky0"],dtype=float)
        if abs(k1facy_th0) > 1E-6:
            raise RuntimeError("ky not binormal at outboard midplane - not yet supported")
            #gkids.non_linear.radial_wavevector_norm = (kx*k1facx_th0+ky*k1facy_th0)
        else:
            gkids.non_linear.radial_wavevector_norm = kx*k1facx_th0
        gkids.non_linear.binormal_wavevector_norm = ky*k2fac_th0
        #gkids.non_linear.angle_pol=

        #TODO - CHECK WITH YANN WHETHER TIME ARRAYS COULD BE SPECIFIED PER SIGNAL
        #       FOR NOW USING HIGHEST RES SIGNAL BASED ON NRG FILES
        gkids.non_linear.time_norm=generun.nrgdat.timearray*rat.L/rat.vth
        #use high temporal resolution nrg data to determine actual time window
        gkids.non_linear.time_interval_norm=[generun.nrgdat.timearray[0]*rat.L/rat.vth,
                                             generun.nrgdat.timearray[-1]*rat.L/rat.vth]
        gkids.non_linear.quasi_linear=0

        # code
        gkids.non_linear.code=ids_gyrokinetics_local.CodePartialConstant()
        gkids.non_linear.code.parameters=dicttoxml(pars_nml_to_dict(generun.pars,generun.cm.nmldict),
                                                   return_bytes=False)
        gkids.non_linear.code.output_flag=0

        # fluxes

        # fluxes_2D_kx_ky_sum_rotating_frame (time traces)
        # NOTE/TODO: GENE NRG-FILES DO NOT ALLOW FOR SEPARATION OF A/B_FIELD_PARALLEL CONTRIBUTIONS
        # CURRENTLY BOTH OF THEM ARE ATTRIBUTED TO A_FIELD_PARALLEL
        # ALTERNATIVE: RECOMPUTE FLUXES FROM MOM-FILES (Pullback will still contain both fields though)
        ddum = ids_gyrokinetics_local.FluxesNl2DSumKxKy()
        #define empty array to consider the time dimension ( dimension, not used)
        ddum.particles_phi_potential=np.empty([generun.pars['n_spec'],len(generun.nrgdat.dataarray[:,0,4])],dtype=ddum.particles_phi_potential.dtype)
        ddum.particles_a_field_parallel,ddum.momentum_tor_parallel_phi_potential,ddum.momentum_tor_parallel_a_field_parallel,\
            ddum.energy_phi_potential,ddum.energy_a_field_parallel = (np.empty_like(ddum.particles_phi_potential) for i in range(5))
        for spec in generun.cm.specnames:
            ispec = generun.cm.specnames.index(spec) #w/o +1
            ddum.particles_phi_potential[ispec,:] = generun.nrgdat.dataarray[:,ispec,4]*rat.pflux
            ddum.particles_a_field_parallel[ispec,:] = generun.nrgdat.dataarray[:,ispec,5]*rat.pflux #FIX THIS (APAR IS FULL EM)
            #ddum.particles_b_field_parallel += [generun.nrgdat.dataarray[:,ispec,5]*rat.pflux*0.0] #FIX THIS -- SET TO ZERO
            ddum.momentum_tor_parallel_phi_potential[ispec,:] = generun.nrgdat.dataarray[:,ispec,8]*rat.mflux # FIX THIS
            ddum.momentum_tor_parallel_a_field_parallel[ispec,:] = generun.nrgdat.dataarray[:,ispec,9]*rat.mflux
            #ddum.momentum_tor_parallel_b_field_parallel += [generun.nrgdat.dataarray[:,ispec,9]*rat.mflux*0.0] #FIX THIS -- SET TO ZERO
            #ddum.momentum_tor_perpendicular_phi_potential": None, #TODO -- ADD PERPENDICULAR MOMENTUM
            #ddum.momentum_tor_perpendicular_a_field_parallel": None,
            #ddum.momentum_tor_perpendicular_b_field_parallel": None,
            ddum.energy_phi_potential[ispec,:] = generun.nrgdat.dataarray[:,ispec,6]*rat.qflux
            ddum.energy_a_field_parallel[ispec,:] = generun.nrgdat.dataarray[:,ispec,7]*rat.qflux  #FIX THIS (APAR IS FULL EM)
            #ddum.energy_b_field_parallel += [generun.nrgdat.dataarray[:,ispec,7]*rat.qflux*0.0] #FIX THIS -- SET TO ZERO

        gkids.non_linear.fluxes_2d_k_x_k_y_sum_rotating_frame = ddum
        gkids.non_linear.fluxes_1d_rotating_frame = \
            computeFluxesNl1D(gkids.non_linear.time_norm,gkids.non_linear.fluxes_2d_k_x_k_y_sum_rotating_frame)

        # fluxes_2D_kx_sum_rotating_frame (time-averaged ky spectrum)
        ddum = ids_gyrokinetics_local.FluxesNl2DSumKx()
        for fluxfield in generun.fluxspectra[0].fluxes_ky.keys():
            [flux,field]= fluxfield.split("_")
            if flux == "Pi":
                continue
            if flux[0:4] == "LAB-":
                labfluxfield += [fluxfield[4:]]
                continue
            if flux == "Pitorpar" and (field in ["bpar"]): # TODO check if IDS accepts apar again
                continue
            dddum = []
            for isp,spec in enumerate(generun.cm.specnames):
                dddum += [generun.fluxspectra[isp].fluxes_ky[flux+"_"+field]*rat[flux+"flux"]]
            setattr(ddum,imas_string[flux]+"_"+imas_string[field],dddum)


        #use linear weights if no background angular velocity is given and split into co-moving / lab frame else
        if generun.cm.pars["Omega0_tor"] != 0 or generun.cm.pars["omegatorref"] != 0:
            gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame = ddum
            if labfluxfield:
                ddum = ids_gyrokinetics_local.FluxesNl2DSumKxKy()
                for fluxfield in labfluxfield:
                    [flux,field]= fluxfield.split("_")
                    if flux == "Pitorpar" and (field in ["apar", "bpar"]): # TODO check if IDS accepts apar again
                        continue
                    dddum = []
                    for isp,spec in enumerate(generun.cm.specnames):
                        dddum += [float(generun.fluxspectra[isp].fluxes_ky["LAB-"+flux+"_"+field][iev])*rat[flux+"flux"]*(1.0/rat.Af)**2]
                    setattr(ddum,imas_string[flux]+"_"+imas_string[field],dddum)
                gkids.non_linear.fluxes_2d_k_x_sum = ddum
        else:
            gkids.non_linear.fluxes_2d_k_x_sum = ddum
            gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame = ddum


    return gkids

def computeFluxesNl1D(time,fluxes):
    dumavg = ids_gyrokinetics_local.FluxesNl1D()

    allfluxes = ['particles','momentum_tor_parallel','momentum_tor_perpendicular','energy']
    allfields = ['phi_potential','a_field_parallel','b_field_parallel']

    for fluxkey in allfluxes:
        for fieldkey in allfields:
            thisflux=getattr(fluxes,fluxkey+'_'+fieldkey)
            if len(thisflux)>0:
                setattr(dumavg,fluxkey+'_'+fieldkey,np.trapz(thisflux,time)/(time[-1]-time[0]))

    return dumavg

def adjust_y_direction(modestructure,Ip_sign):
    #GENE's grad y points in -sign_Ip_CW * e_phi(COCOS=12) direction
    #that is sign_Ip_CW * e_phi(COCOS=17) = -Ip_sign * e_phi(COCOS=17)
    #USE CONJG FOR Ip_sign > 0?  # TODO - CHECK
    if Ip_sign > 0:
        #return(np.conj(modestructure))
        return(modestructure)
    else:
        return(modestructure)

def genegeom2mxh(geom,q0,Nsh):
    """ Determines Miller MXH shape parameters for GK ids (COCOS=17) from GENE geom (COCOS=12)
    Inputs
    geom             Geometry data structure of generun class or pydiag, respectively
    Nsh              Number of moments for flux surface Fourier parametrisation (default Nsh=10)
    Outputs
    TBA              TBA

    Follows previous GENE to Fourier Miller implementation for GKDB (COCOS=11).
    COCOS=12: right handed (R,Z,phi), phi clockwise from top, theta cnt-clockwise from front,
              psi incr., sign(q)=+1, sign(dp/dpsi)=-1 for Ip, B0>0 in this coords
    COCOS=17: right handed (R,phi,Z), phi cnt-clockwise from top, theta clockwise from front,
              psi decr., sign(q)=+1, sign(dp/dpsi)=+1 for Ip, B0>0 in this coords.
    all COCOS > 10 have full poloidal flux (not divided by 2pi)
    """

    #print ("Determining flux surface shape from GENE (R,Z) and metric ...")
    nz0 = len(geom.R)

    #first create high resolution representation from coarse data flux surface
    #to determine minor_r_norm, R0, Z0 etc with high accuracy

    #coarse flux surface center
    R0_coarse = 0.5*(max(geom.R)+min(geom.R))
    Z0_coarse = 0.5*(max(geom.Z)+min(geom.Z))

    res_coarse=get_flux_surface_distance(R0_coarse,Z0_coarse,geom.R,geom.Z)
    theta_coarse=res_coarse["theta"]
    theta = theta_coarse  #poloidal angle of original data

    aN_coarse = res_coarse["aN"]
    theta_coarse_sort=res_coarse["theta_sort"]
    coarse_sort_ind=res_coarse["sort_ind"]

    #equidistant theta (COCOS=11/17) with somewhat higher resolution for fourier transforms etc
    nth = 2000 #10*nz0
    theta_grid = (np.arange(0.0,nth,1.0))*2.0*np.pi/nth

    #interpolate to this equidist. grid with proper order (interpol requires increasing arr)
    aN_fine = interpolate_periodic(theta_coarse_sort,theta_grid,aN_coarse)

    #finally, construct high resolution (R,Z) contour
    R_fine = R0_coarse+aN_fine*np.cos(theta_grid)*R0_coarse
    Z_fine = Z0_coarse-aN_fine*np.sin(theta_grid)*R0_coarse

    #determine max/min indices (used several times)
    maxRf_ind = np.argmax(R_fine)
    minRf_ind = np.argmin(R_fine)

    #determine high resolution flux surface center
    R0 = 0.5*(max(R_fine)+min(R_fine))
    Z0 = 0.5*(max(Z_fine)+min(Z_fine))

    #determine minor radius of flux surface (high res.)
    r = 0.5*(max(R_fine)-min(R_fine))

    dxdR_fine=interpolate_periodic(theta_coarse_sort,theta_grid,geom.dxdR[coarse_sort_ind])
    dxdZ_fine=interpolate_periodic(theta_coarse_sort,theta_grid,geom.dxdZ[coarse_sort_ind])
    gxx_fine=interpolate_periodic(theta_coarse_sort,theta_grid,geom.gxx[coarse_sort_ind])

    # Derivation of dimensionless gradient conv. factor dx/dr (while x & r are here of dim. length)
    # Let l be some length along the radial GENE coordinate
    # R(l) = R_psi0 + dx/dR / g^xx * l
    # r = 0.5 * (max[R_psi0] - min[R_psi0])
    # dr/dl = 0.5 * (dx/dR / g^xx|_max[R_psi0] - dx/dR / g^xx|_min[R_psi0])
    # => dl/dr = 2 / (dx/dR / g^xx|_max[R_psi0] - dx/dR / g^xx|_min[R_psi0])
    # now use x again instead of l (which was just used to avoid confusion)
    dxdr = 2.0/(dxdR_fine[maxRf_ind]/gxx_fine[maxRf_ind]-\
                dxdR_fine[minRf_ind]/gxx_fine[minRf_ind])

    dRdr_coarse = geom.dxdR/geom.gxx*dxdr
    dZdr_coarse = geom.dxdZ/geom.gxx*dxdr

    #Now determine Miller MXH parametrization
    #R(r,theta_mxh) = R0(r) + r cos (theta_R_mxh)
    #Z(r,theta_mxh) = Z0(r) + kappa * r * sin(theta_mxh)
    #theta_R_mxh = theta_mxh + sum_n=0^N [cN * cos (n theta_mxh) + sN sin (n theta_mxh)]

    #center result
    res=get_flux_surface_distance(R0,Z0,R_fine,Z_fine)
    theta_sort = res["theta_sort"]
    aN_th=interpolate_periodic(theta_sort,theta_grid,res["aN"])


    #now construct neighbouring high resolution flux surfaces with given metric
    Nr = 3
    #delta_r for finite differences (should be small)
    delta_r = 0.00001 #*r/R0
    dRdr_fine = dxdR_fine/gxx_fine*dxdr
    dZdr_fine = dxdZ_fine/gxx_fine*dxdr
    Rdum = np.full((Nr,nth),np.nan)
    Zdum = np.full((Nr,nth),np.nan)
    for ii in range(Nr):
        Rdum[ii,:] = R_fine+(ii-Nr//2)*delta_r*dRdr_fine
        Zdum[ii,:] = Z_fine+(ii-Nr//2)*delta_r*dZdr_fine

    r0,R0,dR0dr,Z0,dZ0dr,kappa,dkappa_dr,cN,cN_dr,sN,sN_dr,R_out,Z_out,err_out=\
        FS.rz2mxh(Rdum,Zdum,'imas',r,Nsh,doplots=False)

    #check parametrization
    #    R_check,Z_check,R_rho,Z_rho,a_check,B_new,gxx,gxx_th0,gxy_th0,gyy_th0,theta_pol,C_y=\
        #        get_flux_surface_from_mxh(theta_grid,r,q0,R0,dR0dr,Z0,dZ0dr,kappa,dkappa_dr,cN,cN_dr,sN,sN_dr)
    Lnorm = 1.0
    R_check,Z_check,R_rho,Z_rho,a_check,B_new,gxx,gxx_th0,gxy_th0,gyy_th0,theta_pol,C_y=\
        get_flux_surface_from_mxh(theta_grid,r/Lnorm,q0,R0/Lnorm,dR0dr,Z0/Lnorm,dZ0dr,kappa,\
                                  dkappa_dr*Lnorm,cN,cN_dr*Lnorm,sN,sN_dr*Lnorm)
    # interpolate original Bfield to same grid (via poloidal angle)
    B_old_fine=interpolate_periodic(theta_coarse_sort,theta_pol,geom.Bfield[coarse_sort_ind])

    # now compute Bfield conversion from B_new from Miller MXH and old Bfield
    Brat = np.sum(B_new/B_old_fine)/nth

    #comparisons:
    #print("Cy = ", C_y, geom.Cy)
    #print('gxx(0), gyy(0) = ', gxx_th0, gyy_th0)
    #plt.plot(theta_coarse_sort,geom.gxx[coarse_sort_ind]/dxdr**2)
    #plt.plot(theta_pol,gxx)
    #plt.show()

    #some plot diagnostics
    show_plots = False #True
    if show_plots:
        plt.figure("Flux surface parametrization")
        plt.subplot(131)
        plt.plot(theta_coarse_sort/np.pi,aN_coarse*R0/Lnorm,'o-',markersize=3,label='aN_orig')
#        plt.plot(theta_grid/np.pi,aN_dr_th,'x-',markersize=3,label='aN_dr_th')
        plt.plot(theta_pol/np.pi,a_check,label='aN_final')
#        plt.plot(theta_pol/np.pi,a_dr_check,label='aN_dr_final')
        plt.xlabel(r'$\theta/\pi$')
        plt.legend()

        dr = 0.05*r  #radial extension for testing
        #original data
        plt.subplot(132)
        plt.plot(geom.R[coarse_sort_ind]/Lnorm,geom.Z[coarse_sort_ind]/Lnorm,'o-',markersize=3,label="original")
        plt.plot((geom.R[coarse_sort_ind]+dr*dRdr_coarse[coarse_sort_ind])/Lnorm,
                 (geom.Z[coarse_sort_ind]+dr*dZdr_coarse[coarse_sort_ind])/Lnorm,
                     'x-',markersize=3,label="original+dr")

        #flux surface after interpolation
        #plt.plot(R_fine,Z_fine,label="interpolated")
        #flux surface from Fourier coefficients
        theta_pol_periodic = np.append(theta_pol,2.0*np.pi)
        a_check_periodic = np.append(a_check,a_check[0])
        R_check2 = R0/Lnorm+a_check_periodic*np.cos(theta_pol_periodic)
        Z_check2 = Z0/Lnorm-a_check_periodic*np.sin(theta_pol_periodic)
        plt.plot(R_check2,Z_check2,label="IMAS")
        R_check2 = R_check + R_rho*dr/Lnorm
        Z_check2 = Z_check + Z_rho*dr/Lnorm
        plt.plot(R_check2,Z_check2,label="IMAS+dr")
        #rest of plotting
        plt.plot(R0/Lnorm,Z0/Lnorm,'o',markersize=2,color='black')
        plt.xlabel('R/R0') #plt.xlabel('R/m')
        plt.ylabel('Z/R0') #plt.ylabel('Z/m')
        plt.axis('equal')
        plt.legend()

        plt.subplot(133)
        plt.plot(theta_coarse_sort,geom.Bfield[coarse_sort_ind]*Brat,
                 'o-',markersize=3,label='B_old*Brat')
        plt.plot(theta_pol,B_new,label='B_new')
        plt.legend()
        plt.show()

    return (
        R0,          #center of flux surface
        dR0dr,       #radial derivative
        Z0,          #center of flux surface
        dZ0dr,       #radial derivative
        r,           #minor radius of flux surface
        kappa,       #elongation kappa
        dkappa_dr,   #radial derivative of kappa
        cN,          #cosine Fourier coeffs
        cN_dr,       #radial derivative
        sN,          #sine Fourier coeffs
        sN_dr,       #radial derivative
        dxdr,        #GENE to r conversion
        Brat         #Bnew/Bold due to shifted flux surface
    )
#    return {"R0" : R0,              #center of flux surface
#            "dR0dr" : dR0dr,        #radial derivative
#            "Z0" : Z0,              #center of flux surface
#            "dZ0dr" : dZ0dr,        #radial derivative
#            "r"  : r,               #minor radius of flux surface
#            "kappa" : kappa,        #elongation kappa
#            "dkappa_dr" : dkappa_dr,#radial derivative of kappa
#            "cN" : cN,              #cosine Fourier coeffs
#            "cN_dr" : cN_dr,        #radial derivative
#            "sN" : sN,              #sine Fourier coeffs
#            "sN_dr" : sN_dr,        #radial derivative
#            "dxdr" : dxdr,          #GENE to r conversion
#            "Brat" : Brat,          #Bnew/Bold due to shifted flux surface
#            }

def genegeom2mxh_old(geom,q0,Nsh):
    """ Determines Miller MXH shape parameters for GK ids (COCOS=17) from GENE geom (COCOS=12)
    Inputs
    geom             Geometry data structure of generun class or pydiag, respectively
    Nsh              Number of moments for flux surface Fourier parametrisation (default Nsh=10)
    Outputs
    TBA              TBA

    Follows previous GENE to Fourier Miller implementation for GKDB (COCOS=11).
    COCOS=12: right handed (R,Z,phi), phi clockwise from top, theta cnt-clockwise from front,
              psi incr., sign(q)=+1, sign(dp/dpsi)=-1 for Ip, B0>0 in this coords
    COCOS=17: right handed (R,phi,Z), phi cnt-clockwise from top, theta clockwise from front,
              psi decr., sign(q)=+1, sign(dp/dpsi)=+1 for Ip, B0>0 in this coords.
    all COCOS > 10 have full poloidal flux (not divided by 2pi)
    """

    print("stop - shouldn't be here")
    exit(-1)

    print ("Determining flux surface shape from GENE (R,Z) and metric ...")
    nz0 = len(geom.R)

    #first create high resolution representation from coarse data flux surface
    #to determine minor_r_norm, R0, Z0 etc with high accuracy

    #coarse flux surface center
    R0_coarse = 0.5*(max(geom.R)+min(geom.R))
    Z0_coarse = 0.5*(max(geom.Z)+min(geom.Z))

    res_coarse=get_flux_surface_distance(R0_coarse,Z0_coarse,geom.R,geom.Z)
    theta_coarse=res_coarse["theta"]
    theta = theta_coarse  #poloidal angle of original data

    aN_coarse = res_coarse["aN"]
    theta_coarse_sort=res_coarse["theta_sort"]
    coarse_sort_ind=res_coarse["sort_ind"]

    #equidistant theta (COCOS=11/17) with somewhat higher resolution for fourier transforms etc
    nth = 10*nz0
    theta_grid = (np.arange(0.0,nth,1.0))*2.0*np.pi/nth

    #interpolate to this equidist. grid with proper order (interpol requires increasing arr)
    aN_fine = interpolate_periodic(theta_coarse_sort,theta_grid,aN_coarse)

    #finally, construct high resolution (R,Z) contour
    R_fine = R0_coarse+aN_fine*np.cos(theta_grid)*R0_coarse
    Z_fine = Z0_coarse-aN_fine*np.sin(theta_grid)*R0_coarse

    #determine max/min indices (used several times)
    maxRf_ind = np.argmax(R_fine)
    minRf_ind = np.argmin(R_fine)

    #determine high resolution flux surface center
    R0 = 0.5*(max(R_fine)+min(R_fine))
    Z0 = 0.5*(max(Z_fine)+min(Z_fine))
    #print ("(R0,Z0) = (", R0, ",", Z0,")")

    #determine minor radius of flux surface (high res.)
    r = 0.5*(max(R_fine)-min(R_fine))
    #print("r/R0 = {}".format(r/R0))
    dxdR_fine=interpolate_periodic(theta_coarse_sort,theta_grid,geom.dxdR[coarse_sort_ind])
    dxdZ_fine=interpolate_periodic(theta_coarse_sort,theta_grid,geom.dxdZ[coarse_sort_ind])
    gxx_fine=interpolate_periodic(theta_coarse_sort,theta_grid,geom.gxx[coarse_sort_ind])

    # Derivation of dimensionless gradient conv. factor dx/dr (while x & r are here of dim. length)
    # Let l be some length along the radial GENE coordinate
    # R(l) = R_psi0 + dx/dR / g^xx * l
    # r = 0.5 * (max[R_psi0] - min[R_psi0])
    # dr/dl = 0.5 * (dx/dR / g^xx|_max[R_psi0] - dx/dR / g^xx|_min[R_psi0])
    # => dl/dr = 2 / (dx/dR / g^xx|_max[R_psi0] - dx/dR / g^xx|_min[R_psi0])
    # now use x again instead of l (which was just used to avoid confusion)
    dxdr = 2.0/(dxdR_fine[maxRf_ind]/gxx_fine[maxRf_ind]-\
                dxdR_fine[minRf_ind]/gxx_fine[minRf_ind])

    #determine high resolution based elongation
    kappa = 0.5*(max(Z_fine)-min(Z_fine))/r

    dRdr_coarse = geom.dxdR/geom.gxx*dxdr
    dZdr_coarse = geom.dxdZ/geom.gxx*dxdr

    #Now determine Miller MXH parametrization
    #R(r,theta_mxh) = R0(r) + r cos (theta_R_mxh)
    #Z(r,theta_mxh) = Z0(r) + kappa * r * sin(theta_mxh)
    #theta_R_mxh = theta_mxh + sum_n=0^N [cN * cos (n theta_mxh) + sN sin (n theta_mxh)]

    #center result
    res=get_flux_surface_distance(R0,Z0,R_fine,Z_fine)
    theta_sort = res["theta_sort"]
    aN_th=interpolate_periodic(theta_sort,theta_grid,res["aN"])


    #now construct neighbouring high resolution flux surfaces with given metric
    Nr = 3
    #delta_r for finite differences (should be small)
    delta_r = 0.00001 #*r/R0
    dRdr_fine = dxdR_fine/gxx_fine*dxdr
    dZdr_fine = dxdZ_fine/gxx_fine*dxdr
    R0n = np.full(Nr,np.nan)
    Z0n = np.full(Nr,np.nan)
    kappan = np.full(Nr,np.nan)
    thdiff_mxh_th_n = np.full((Nr,nth),np.nan)
#    plt.figure()
    for ii in range(Nr):
        Rn = R_fine+(ii-Nr//2)*delta_r*dRdr_fine
        Zn = Z_fine+(ii-Nr//2)*delta_r*dZdr_fine
#        plt.plot(Rn,Zn)
        res=get_theta_pol(R0,Z0,Rn,Zn)
        theta_sort = res["theta_sort"]
        sort_ind = res["sort_ind"]
        resmxh = get_theta_mxh(R0,Z0,Rn,Zn)
        thdiff_mxh_th_n[ii,:] = interpolate_periodic(theta_sort,theta_grid,\
                                             resmxh["thetadiff_mxh"][sort_ind])
        kappan[ii] = resmxh["kappa"]
        R0n[ii] = resmxh["R0"]
        Z0n[ii] = resmxh["Z0"]
#    plt.show()

    #using 2nd order centered scheme for derivative
    dR0dr = (R0n[Nr-1]-R0n[0])/(2.0*delta_r)
    dZ0dr = (Z0n[Nr-1]-Z0n[0])/(2.0*delta_r)
    dkappadr = (kappan[Nr-1]-kappan[0])/(2.0*delta_r) #check *R0
    dthdiff_mxh_th_dr = (thdiff_mxh_th_n[Nr-1,:]-thdiff_mxh_th_n[0,:])/(2.0*delta_r) #check *R0
    print ("kappa, dkappa_dr = ", kappa, dkappadr)
    #Note: Using the following analytic expression for the derivative doesn't work
    #as it neglects variations of theta with increased R,Z
    #        aN_dr = ((geom.R[sort_ind]-R0)*dRdr[sort_ind]+
    #                 (geom.Z[sort_ind]-Z0)*dZdr[sort_ind])/\
    #                 (aN*R0)

    #now fourier transform
    fft_thdiff_mxh = np.fft.fft(thdiff_mxh_th_n[1,:])
    fft_thdiff_mxh_dr = np.fft.fft(dthdiff_mxh_th_dr)

    #keep up to nth_kept modes (typically around 10)
    nth_kept = Nsh
    cN = 2.0*np.real(fft_thdiff_mxh)[0:nth_kept]/nth
    cN[0] *= 0.5
    sN = -2.0*np.imag(fft_thdiff_mxh)[0:nth_kept]/nth
    sN[0] *= 0.5
    cN_dr = 2.0*np.real(fft_thdiff_mxh_dr)[0:nth_kept]/nth
    cN_dr[0] *= 0.5
    sN_dr = -2.0*np.imag(fft_thdiff_mxh_dr)[0:nth_kept]/nth
    sN_dr[0] *= 0.5

    print("cN: ", cN)
    print("sN: ", sN)
    print("cN_dr: ", cN_dr)
    print("sN_dr: ", sN_dr)

    #check parametrization
    #R(r,theta_mxh) = R0(r) + r cos (theta_R_mxh)
    #Z(r,theta_mxh) = Z0(r) + kappa * r * sin(theta_mxh)
    #theta_R_mxh = theta_mxh + sum_n=0^N [cN * cos (n theta_mxh) + sN sin (n theta_mxh)]

    nind = np.arange(0.0,nth_kept,1.0)
    a_check = []
    a_dr_check = []
    Rcheck = []
    Zcheck = []
    for iz in range(0,nth):
        theta_R = theta_grid[iz] + np.sum(cN*np.cos(nind*theta_grid[iz])+
                           sN*np.sin(nind*theta_grid[iz]))
        Rcheck += [R0+r*np.cos(theta_R)]
        Zcheck += [Z0+kappa*r*np.sin(theta_grid[iz])]
        a_check += [r*np.sqrt((np.cos(theta_R))**2+(kappa*np.sin(theta_grid[iz]))**2)/R0]
#        a_check += [np.sum(cN*np.cos(nind*theta_grid[iz])+
#                           sN*np.sin(nind*theta_grid[iz]))]
        a_dr_check += [np.sum(cN_dr*np.cos(nind*theta_grid[iz])+
                              sN_dr*np.sin(nind*theta_grid[iz]))]

    a_dr_check = np.array(a_dr_check)
    err = np.sum(np.abs(a_check-aN_th)/aN_th)/nth
    print ("Relative error of flux surface parametrization: {0:12.6E}".format(err))
    err = np.sum(np.abs(a_dr_check-aN_dr_th)/aN_dr_th)/nth
    print ("Relative error of flux surface derivative parametrization: {0:12.6E}".format(err))

    #Determine new Bref by constructing and comparing the generalized Miller Bfield(theta) with
    #the original one interpolated to the new theta grid
    B_old_fine=interpolate_periodic(theta_coarse_sort,theta_grid,geom.Bfield[coarse_sort_ind])

    a_dth_check = []
    for iz in range(0,nth):
        a_dth_check += [np.sum(-cN*nind*np.sin(nind*theta_grid[iz])+
                               sN*nind*np.cos(nind*theta_grid[iz]))]
    R_check = 1.0 + a_check*np.cos(theta_grid)

    #TODO ADJUST TO MXH
    R_rho = a_dr_check * np.cos(theta_grid)
    Z_rho = a_dr_check * np.sin(theta_grid)

    R_theta = a_dth_check*np.cos(theta_grid) - a_check * np.sin(theta_grid)
    Z_theta = a_dth_check*np.sin(theta_grid) + a_check * np.cos(theta_grid)

    J_r = R_check*(R_rho*Z_theta-R_theta*Z_rho)
    dlp = np.sqrt(R_theta**2+Z_theta**2)
    F = 1.0 #assume F=1 for now, compute new Bfield and then determine rescale factor
    th_ext = np.append(theta_grid,2.0*np.pi)
    J_r_ext = np.append(J_r,J_r[0])
    R_ext = np.append(R_check,R_check[0])
    drPsi = F/(2.*np.pi*np.abs(q0))*np.trapz((J_r_ext/R_ext**2),th_ext)

    B_new = np.sqrt((drPsi / J_r * dlp)**2 + (F/R_check)**2)
    Brat = np.sum(B_new/B_old_fine)/nth
#    B0 = cm.pnt.Bref/Brat


    #some plot diagnostics
    show_plots = False
    if show_plots:
        plt.figure("Flux surface parametrization")
        plt.subplot(121)
        plt.plot(theta_coarse_sort/np.pi,aN_coarse,'o-',markersize=3,label='aN_orig')
        plt.plot(theta_grid/np.pi,aN_dr_th,'x-',markersize=3,label='aN_dr_th')
        plt.plot(theta_grid/np.pi,a_check,label='aN_final')
        plt.plot(theta_grid/np.pi,a_dr_check,label='aN_dr_final')
        plt.xlabel(r'$\theta/\pi$')
        plt.legend()

        dr = 0.05*r  #radial extension for testing
        #original data
        plt.subplot(122)
        plt.plot(geom.R[coarse_sort_ind],geom.Z[coarse_sort_ind],'o-',markersize=3,label="original")
        plt.plot((geom.R[coarse_sort_ind]+dr*dRdr_coarse[coarse_sort_ind]),
                 (geom.Z[coarse_sort_ind]+dr*dZdr_coarse[coarse_sort_ind]),
                     'x-',markersize=3,label="original+dr")

        #flux surface after interpolation
        #plt.plot(R_fine,Z_fine,label="interpolated")
        #flux surface from Fourier coefficients
        theta_periodic = np.append(theta_grid,2.0*np.pi)
        a_check_periodic = np.append(a_check,a_check[0])
        R_check = R0+a_check_periodic*np.cos(theta_periodic)*R0
        Z_check = Z0-a_check_periodic*np.sin(theta_periodic)*R0
        plt.plot(R_check,Z_check,label="GKDB")
        a_dr_check_periodic = np.append(a_dr_check,a_dr_check[0])
        R_check = R0+(a_check_periodic+dr/R0*a_dr_check_periodic)*np.cos(theta_periodic)*R0
        Z_check = Z0-(a_check_periodic+dr/R0*a_dr_check_periodic)*np.sin(theta_periodic)*R0
        plt.plot(R_check,Z_check,label="GKDB+dr")

        #rest of plotting
        plt.plot(R0,Z0,'o',markersize=2,color='black')
        plt.xlabel('R/m')
        plt.ylabel('Z/m')
        plt.axis('equal')
        plt.legend()
        plt.show()

    return {"R0" : R0,              #center of flux surface
            "dR0dr" : dR0dr,        #radial derivative
            "Z0" : Z0,              #center of flux surface
            "dZ0dr" : dZ0dr,        #radial derivative
            "r"  : r,               #minor radius of flux surface
            "kappa" : kappa,        #elongation kappa
            "dkappa_dr" : dkappa_dr,#radial derivative of kappa
            "cN" : cN,              #cosine Fourier coeffs
            "cN_dr" : cN_dr,        #radial derivative
            "sN" : sN,              #sine Fourier coeffs
            "sN_dr" : sN_dr,        #radial derivative
            "dxdr" : dxdr,          #GENE to r conversion
            "Brat" : Brat,          #Bnew/Bold due to shifted flux surface
            }

def get_theta_pol(R0,Z0,R,Z,verbose=0):
    #poloidal theta (COCOS=11/17)
    nz0 = len(R)
    theta = np.round(np.arctan(-(Z-Z0)/(R-R0)),12)
    for iz in range(int(nz0/2),nz0-1):
        thetadiff = (theta[iz+1]-theta[iz])
        if thetadiff>0.5*np.pi:
            theta[iz+1]-=np.pi
        elif thetadiff<-0.5*np.pi:
            theta[iz+1]+=np.pi
        thetadiff = (theta[nz0-1-iz-1]-theta[nz0-1-iz])
        if thetadiff>0.5*np.pi:
            theta[nz0-1-iz-1]-=np.pi
        elif thetadiff<-0.5*np.pi:
            theta[nz0-1-iz-1]+=np.pi

    #sorted theta (starting at th=0)
    theta_sort = (theta+2.0*np.pi)%(2.0*np.pi)
    sort_ind = np.argsort(theta_sort)
    theta_sort = theta_sort[sort_ind]
    return {"theta" : theta,          #poloidal angle for original data
            "sort_ind" : sort_ind,    #sort index to be applied to original data (later)
            "theta_sort" : theta_sort #sorted theta in increasing order
            }

def get_flux_surface_distance(R0,Z0,R,Z):
    nz0 = len(R)
    #poloidal theta (COCOS=11/17)
    theta = np.arctan(-(Z-Z0)/(R-R0))
    for iz in range(int(nz0/2),nz0-1):
        thetadiff = (theta[iz+1]-theta[iz])
        if thetadiff>0.5*np.pi:
            theta[iz+1]-=np.pi
        elif thetadiff<-0.5*np.pi:
            theta[iz+1]+=np.pi
        thetadiff = (theta[nz0-1-iz-1]-theta[nz0-1-iz])
        if thetadiff>0.5*np.pi:
            theta[nz0-1-iz-1]-=np.pi
        elif thetadiff<-0.5*np.pi:
            theta[nz0-1-iz-1]+=np.pi

    #sorted theta (starting at th=0)
    theta=np.where(abs(theta)<1E-12,0.0,theta)
    theta_sort = (theta+2.0*np.pi)%(2.0*np.pi)
    sort_ind = np.argsort(theta_sort)
    theta_sort = theta_sort[sort_ind]
    aN = np.sqrt((R[sort_ind]-R0)**2+(Z[sort_ind]-Z0)**2)/R0

    return {"theta" : theta,          #poloidal angle for original data
            "sort_ind" : sort_ind,    #sort index to be applied to original data (later)
            "aN" : aN,                #sorted flux surface radius funct.
            "theta_sort" : theta_sort #sorted theta in increasing order
            }

def get_theta_mxh(R0in,Z0in,R,Z):
    """ Returns theta_mxh (Miller Extended Harmonics definition)
    Need high resolution R,Z as inputs!
    """

    maxR = max(R)
    minR = min(R)
    maxZ = max(Z)
    minZ = min(Z)

    #Now determine Miller MXH parametrization
    #R(r,theta_mxh) = R0(r) + r cos (theta_R_mxh)
    #Z(r,theta_mxh) = Z0(r) + kappa * r * sin(theta_mxh)
    #r = 0.5*(max(R)-min(R))
    #kappa = 0.5*(max(Z)-min(Z))/r
    #theta_R_mxh = theta_mxh + sum_n=0^N [cN * cos (n theta_mxh) + sN sin (n theta_mxh)]

    #theta_R_mxh = arccos ( (R-R0) / r ) but some values might be slightly larger than one
    #due to round off errors hence using np.round to avoid arccos(1+eps) = nan
    #theta_R_mxh = np.arccos(np.round(((R-R0)/r),14))
    #theta_mxh = arcsin ( (Z-Z0) / (kappa r) )
    #The FFT is later performed along the difference theta_R_mxh - theta_mxh
    r = 0.5*(maxR-minR)
    R0 = 0.5*(maxR+minR)
    Z0 = 0.5*(maxZ+minZ)
    kappa = (maxZ-minZ)/(maxR-minR)
    print("r, kappa in get_theta_mxh: ", r, kappa)
    thmxhR = np.arccos(np.round(((R-R0)/r),14))
    thmxhR_Rmax = thmxhR[R==maxR]
    thmxhR_Rmin = thmxhR[R==minR]

    print(thmxhR_Rmax, thmxhR_Rmin)
    if thmxhR_Rmax>thmxhR_Rmin:
      II = (thmxhR>=thmxhR_Rmin) & (thmxhR<thmxhR_Rmax)
    else:
      II = (thmxhR>=thmxhR_Rmin) | (thmxhR<thmxhR_Rmax)
    thmxhR[II]=2*np.pi-thmxhR[II]
    print(thmxhR)
    thetadiff_mxh = np.arccos(np.round(((R-R0)/r),14)) \
        - np.arcsin(np.round(((Z-Z0)/(kappa * r)),14))

    return {"kappa" : kappa,          #elongation of given flux surface
#            "theta_mxh" : theta_mxh,  #MXH theta for flux surface wrt. R0,Z0
            "thetadiff_mxh" : thetadiff_mxh,
            "R0" : R0,
            "Z0" : Z0
            }

def interpolate_periodic(theta_in,theta_out,var_in,kind='cubic'):
    #axis and data needs to be sorted
    #now extend var and theta_sort to definitely cover >= [0.0,2*pi]
    nth = len(theta_in)
    nb = 5
    theta_ext = np.concatenate([theta_in[nth-1-nb:nth-1]-2.0*np.pi,theta_in,theta_in[0:nb]+2.0*np.pi])
    var_ext = np.concatenate([var_in[nth-1-nb:nth-1],var_in,var_in[0:nb]])

    fc = interpolate.interp1d(theta_ext,var_ext,kind=kind) #,fill_value='extrapolate')
    return fc(theta_out)

def get_flux_surface_from_mxh(theta_grid,r,q0,R0,dR0dr,Z0,dZ0dr,kappa,dkappa_dr,cN,cN_dr,sN,sN_dr,Nsh=10):
    #Compute flux surface shape from Miller MXH coefficients
    #R(r,theta_mxh) = R0(r) + r cos (theta_R_mxh)
    #Z(r,theta_mxh) = Z0(r) - kappa * r * sin(theta_mxh)
    #theta_R_mxh = theta_mxh + sum_n=0^N [cN * cos (n theta_mxh) + sN sin (n theta_mxh)]
    #print(r,q0,R0,dR0dr,Z0,dZ0dr,kappa,dkappa_dr)
    #print(cN,cN_dr,sN,sN_dr)

    thR = theta_grid[:]
    thRdr = 0.0*theta_grid[:]
    thRdth = thRdr[:] + 1.0
    for nind in range(Nsh):
        #Warning: using += would change the contents of theta_grid?!
        thR = thR + cN[nind]*np.cos(nind*theta_grid[:]) + sN[nind]*np.sin(nind*theta_grid[:])
        thRdr = thRdr + cN_dr[nind]*np.cos(nind*theta_grid[:]) + sN_dr[nind]*np.sin(nind*theta_grid[:])
        thRdth = thRdth - cN[nind]*nind*np.sin(nind*theta_grid[:])+sN[nind]*nind*np.cos(nind*theta_grid[:])

    R = R0 + r*np.cos(thR)
    Z = Z0 - kappa*r*np.sin(theta_grid)
    R_rho = dR0dr + np.cos(thR) - r * np.sin(thR) * thRdr
    Z_rho = dZ0dr - (dkappa_dr*r + kappa) * np.sin(theta_grid[:])
    R_theta = - r * np.sin(thR) * thRdth
    Z_theta = - kappa * r * np.cos(theta_grid[:])

    a_check = r*np.sqrt((np.cos(thR))**2+(kappa*np.sin(theta_grid))**2)
    #a_dr_check = a_check[:]/r+r**2/a_check[:]*(-np.sin(thR)*thRdr*np.cos(thR)
    #                                           +dkappa_dr*kappa*np.sin(theta_grid)**2)

    dummy = np.divide(-(Z-Z0),(R-R0), out=np.zeros_like(R), where=(R!=R0))
    theta_pol = np.round(np.arctan(dummy),12)

    # correct pi periods to establish theta_pol in [0, 2*pi]
    theta_pol[(R-R0)==0] = np.pi/2
    theta_pol[(R-R0)<0] += np.pi
    theta_pol[np.logical_and((R-R0)>=0,(Z-Z0)>0)] += 2*np.pi

    # now compute new Bfield from Miller MXH
    J_r = R*(R_rho*Z_theta-R_theta*Z_rho)
    dlp = np.sqrt(R_theta**2+Z_theta**2)
    F = 1.0 #assume F=1 for now, compute new Bfield and then determine rescale factor
    th_ext = np.append(theta_grid,2.0*np.pi)
    J_r_ext = np.append(J_r,J_r[0])
    R_ext = np.append(R,R[0])
    drPsi = F/(2.*np.pi*np.abs(q0))*np.trapz((J_r_ext/R_ext**2),th_ext)
    # Poloidal field (Bp = Bvec * nabla l)
    Bp=drPsi / J_r * dlp #* sign_Ip_CW
    #toroidal field
    Bphi=F*R0/R
    B_new = np.sqrt(Bp**2 + Bphi**2)

    C_y=drPsi #*sign_Ip_CW
    psi1 = -R*Bp #*sign_Ip_CW
    nu1=0.0 #at theta_pol = 0
    dnu_drho=0.0 #nu1_s
    dnu_dl=-F*R0/(R*psi1)
    grad_nu=np.sqrt(dnu_drho**2+dnu_dl**2)
    gxx=(psi1/drPsi)**2
    gxy=-psi1/drPsi*C_y*nu1 #*sign_Ip_CW
    gyy=C_y**2*(grad_nu**2+(R0/R)**2)
    gxx_th0 = np.interp(0.0,theta_pol,gxx)
    gxy_th0 = 0.0
    gyy_th0 = np.interp(0.0,theta_pol,gyy)
    #print('C_y, gyy_th0 in from_mxh: ', C_y, gyy_th0,np.trapz((J_r_ext/R_ext**2),th_ext))

    return(R,Z,R_rho,Z_rho,a_check,B_new,gxx,gxx_th0,gxy_th0,gyy_th0,theta_pol,C_y)

def write_h5(gkids, path='./', fileextension=''):
    """ Dump the ids into an h5 file """
    filename = 'myids{}.h5'.format(fileextension)
    g,k = idspy.ids_to_hdf5(gkids,filename,overwrite=True)
    print("Finished writing {} ...".format(filename))
