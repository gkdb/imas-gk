#!/usr/bin/env python3

""" gkids_cmp.py: command line tool to compare GKIDS h5 file entries

Usage: gkids_cmp.py <file(s)>

Prints most of the gkids text entries in columns per input file
and visualizes some entries in separate plots

Current version would be in desparate need for a python code cleanup

"""

import argparse
import dataclasses
import sys,os
sys.path.append(os.path.join(os.path.dirname(__file__), '../../idspy_toolkit'))
import idspy_toolkit as idspy
sys.path.append(os.path.join(os.path.dirname(__file__), \
                             '../../idspy_dictionaries_packages/idspy_dictionaries'))
from idspy_dictionaries import ids_gyrokinetics_local
import numpy as np
import warnings
import matplotlib.pyplot as plt

#parse input arguments
parser = argparse.ArgumentParser(description="command line tool for basic comparison of GKIDS h5 files")
parser.add_argument("file", metavar="file", nargs="+", help="GKIDS h5 file(s)")

parser.add_argument('--plot_fluxsurface', action='store_true', help="Plot flux surface shape")
parser.add_argument('--no-plot_fluxsurface', dest='plot_fluxsurface', action='store_false')
parser.set_defaults(plot_fluxsurface=True)

parser.add_argument('--plot_growthrate_freq', action='store_true', help="Plot growth rate / frequency spectra (if more than one mode available")
parser.add_argument('--no-plot_growthrate_freq', dest='plot_growthrate_freq', action='store_false')
parser.set_defaults(plot_growthrate_freq=True)

parser.add_argument('--plot_eigenmode_fields', action='store_true', help="Plot eigenmode structure of maximum overlap of field entries")
parser.add_argument('--no-plot_eigenmode_fields', dest='plot_eigenmode_fields', action='store_false')
parser.set_defaults(plot_eigenmode_fields=True)

parser.add_argument('--plot_eigenmode_moments', action='store_true', help="Plot eigenmode structure of several moments")
parser.add_argument('--no-plot_eigenmode_moments', dest='plot_eigenmode_moments', action='store_false')
parser.set_defaults(plot_eigenmode_moments=False)

parser.add_argument('--plot_flux_timetraces', action='store_true', help="Plot timetraces of nonlinear fluxes if available")
parser.add_argument('--no-plot_flux_timetraces', dest='plot_flux_timetraces', action='store_false')
parser.set_defaults(plot_flux_timetraces=True)

parser.add_argument('--plot_flux_ky_spectra', action='store_true', help="Plot nonlinear flux ky spectra if available")
parser.add_argument('--no-plot_flux_ky_spectra', dest='plot_flux_ky_spectra', action='store_false')
parser.set_defaults(plot_flux_ky_spectra=True)

args = parser.parse_args()

markers = ["o","s","P","D","v","^","<",">","*","p","h"]

gkdict = []
for ii in range(len(args.file)):
    dum=ids_gyrokinetics_local.GyrokineticsLocal()
    idspy.hdf5_to_ids(args.file[ii],dum,todict=False)
    gkdict += [dataclasses.asdict(dum)]


minspec=10000  #minimum number of species
minwv=10000    #minimum number of wavevectors
minev=10000    #minimum number of eigenvalues
for ii in range(len(gkdict)):
    minspec=min(minspec,len(gkdict[ii]["species"]))
    if gkdict[ii]["linear"]["wavevector"]:
        minwv=min(minwv,len(gkdict[ii]["linear"]["wavevector"]))
        minev=min(minev,len(gkdict[ii]["linear"]["wavevector"][0]["eigenmode"]))
    else:
        minwv = 0
        minev = 0

#print code name(s)
print ("{0:35s}\t".format("code:"),end="")
for ii in range(len(gkdict)):
    print ("{0:8s}\t".format(gkdict[ii]["code"]["name"]),end="")
print ("")
print ("")

#print all model entries
for modelkey in gkdict[0]["model"].keys():
    if (modelkey=="version" or modelkey=="max_repr_length"): continue
    print ("{0:35s}\t".format(modelkey[:35]),end="")
    for ii in range(len(gkdict)):
        if modelkey in gkdict[ii]["model"]:
            if gkdict[ii]["model"][modelkey]==None:
                print ("{0:14s}\t".format("None"),end="")
            elif isinstance(gkdict[ii]["model"][modelkey],bool):
                print ("{0:<14}\t".format(gkdict[ii]["model"][modelkey]),end="")
#            elif len(gkdict[ii]["model"][modelkey])==0:
#                print ("{0:14s}\t".format("[]"),end="")
            else:
                print ("{0:8.6f}\t".format(gkdict[ii]["model"][modelkey]),end="")
        else:
            print ("{0:14s}\t".format("None"),end="")
    print ("")
print ("")

#print selected fluxsurface properties
fluxkeys = ["r_minor_norm", "q", "magnetic_shear_r_minor",
            "pressure_gradient_norm", "ip_sign","b_field_tor_sign"]
print ("flux_surface")
for fluxkey in fluxkeys:
    if (fluxkey=="version" or fluxkey=="max_repr_length"): continue
    print ("{0:35s}\t".format(fluxkey[:35]),end="")
    for ii in range(len(gkdict)):
        if fluxkey in gkdict[ii]["flux_surface"]:
            if gkdict[ii]["flux_surface"][fluxkey]==None:
                print ("{0:14s}\t".format("None"),end="")
            else:
                print ("{0:8.6f}\t".format(gkdict[ii]["flux_surface"][fluxkey]),end="")
        else:
            print ("{0:14s}\t".format("None"),end="")
    print ("")
print ("")

#print species_all properties
print ("Species_all")
for specallkey in gkdict[0]["species_all"].keys():
    if (specallkey in ["version","max_repr_length","angle_pol","angle_pol_equilibrium"]): continue
    print ("{0:35s}\t".format(specallkey[:35]),end="")
    for ii in range(len(gkdict)):
        if specallkey in gkdict[ii]["species_all"]:
            if gkdict[ii]["species_all"][specallkey]==None:
                print ("{0:14s}\t".format("None"),end="")
            else:
                print ("{0:8.6f}\t".format(gkdict[ii]["species_all"][specallkey]),end="")
        else:
            print ("{0:14s}\t".format("None"),end="")
    print ("")
print ("")

#now loop over species properties
for ispec in range(minspec):
    print ("Species {0:d}".format(ispec))
    for speckey in gkdict[0]["species"][ispec].keys():
        if (speckey in ["version","max_repr_length","potential_energy_norm",
                        "potential_energy_gradient_norm"]): continue
        print ("{0:35s}\t".format(speckey[:35]),end="")
        for ii in range(len(gkdict)):
            if speckey in gkdict[ii]["species"][ispec]:
                if speckey=="name":
                    print ("{0:14s}\t".format(gkdict[ii]["species"][ispec][speckey]),end="")
                else:
                    print ("{0:8.6f}\t".format(gkdict[ii]["species"][ispec][speckey]),end="")
        print ("")
    print ("")

## collisions
print ("Collisions")
for ispec in range(minspec):
    for jspec in range(minspec):
        print ("{0:19s}{1:2}{2:2}{3:13}\t".format("collisionality_norm",ispec,jspec,""),end="")
        for ii in range(len(gkdict)):
            print ("{0:8.6f}\t".format(gkdict[ii]["collisions"]["collisionality_norm"][ispec,jspec]),end="")
        print("")
print("")


### linear
if len(gkdict[0]["linear"]["wavevector"])>0:

    #loop over wave numbers
    printwvkeys = ["radial_wavevector_norm","binormal_wavevector_norm"]

    for iwv in range(minwv):
        print ("Wavevector No {0:d}".format(iwv))
        mismatch = False
        for wvkey in printwvkeys:
            print ("{0:35s}\t".format(wvkey[:35]),end="")
            refval = gkdict[0]["linear"]["wavevector"][iwv][wvkey]
            for ii in range(len(gkdict)):
                wvdum = gkdict[ii]["linear"]["wavevector"][iwv]
                print ("{0:8.6f}\t".format(wvdum[wvkey]),end="")
                if refval==0.0:
                    mismatch = abs(wvdum[wvkey]-refval)>0.05
                else:
                    mismatch = abs((wvdum[wvkey]-refval)/refval) > 0.05
                if mismatch:
                    print ("")
                    print ("{0}".format("deviations in wavevector - skipping entries"))
                    break
            print ("")
            if mismatch:
                break

        print ("")
        if mismatch:
            continue

        for wvkey in ["growth_rate_norm","frequency_norm","poloidal_turns"]:
            print ("{0:35s}\t".format(wvkey+":"),end="")
            for ii in range(len(gkdict)):
                wvdum = gkdict[ii]["linear"]["wavevector"][iwv]
                print ("{0:8.6f}\t".format(wvdum["eigenmode"][0][wvkey]),end="")
            print ("")
        print ("")

        #compare linear weights
        print ("Linear weights rotating frame")
        for ispec in range(minspec):
            print ("Species {0:d}".format(ispec))
            for fluxkey in gkdict[0]["linear"]["wavevector"][iwv]["eigenmode"][0]["linear_weights_rotating_frame"].keys():
                if (fluxkey in ["version","max_repr_length","angle_pol"]): continue
                print ("{0:35s}\t".format(fluxkey[:35]),end="")
                for ii in range(len(gkdict)):
                    wvdum = gkdict[ii]["linear"]["wavevector"][iwv]
                    if fluxkey in wvdum["eigenmode"][0]["linear_weights_rotating_frame"]:
                        if np.size(wvdum["eigenmode"][0]["linear_weights_rotating_frame"][fluxkey])==0:
                            print ("{0:14s}\t".format("None"),end="")
                        elif wvdum["eigenmode"][0]["linear_weights_rotating_frame"][fluxkey][ispec]==None:
                            print ("{0:14s}\t".format("None"),end="")
                        else:
                            print ("{0:8.6f}\t".format(wvdum["eigenmode"][0]["linear_weights_rotating_frame"][fluxkey][ispec]),end="")
                print ("")
        print ("")


        #compare field mode structures
        print ("Fields")
        plotfields = ["phi_potential_perturbed"]
        plotfieldlabel = [r"$\phi$"]
        if gkdict[0]["model"]["include_a_field_parallel"]:
            plotfields += ["a_field_parallel_perturbed"]
            plotfieldlabel += [r"$A_\|$"]
        if gkdict[0]["model"]["include_b_field_parallel"]:
            plotfields += ["b_field_parallel_perturbed"]
            plotfieldlabel += [r"$B_\|$"]

        if args.plot_eigenmode_fields:
            fig1,axs1 = plt.subplots(len(plotfields))
            fig1.suptitle('Field mode structures - wavevector #{0:d}'.format(iwv))
            fig1.canvas.manager.set_window_title('Field mode structures - wavevector #{0:d}'.format(iwv))
        for iplot,pltmod in enumerate(plotfields):
            print ("Maxabs {0:30s}\t".format(pltmod[:30]),end="")
            for ii in range(len(gkdict)):
                wvdum = gkdict[ii]["linear"]["wavevector"][iwv]
                th = wvdum["eigenmode"][0]["angle_pol"]
                EV1_pltmod = (wvdum["eigenmode"][0]["fields"][pltmod+"_norm"])
                print ("{0:8.6f}\t".format(np.max(np.abs(EV1_pltmod))),end="")
                if args.plot_eigenmode_fields:
                    if len(plotfields)>1:
                        #axs1[iplot].plot(th/np.pi,np.abs(EV1_pltmod),label="Abs[], "+gkdict[ii]["code"]["name"],\
                            #                 marker=markers[2*ii],markersize=3)
                        axs1[iplot].plot(th/np.pi,np.real(EV1_pltmod),label="Re[], "+gkdict[ii]["code"]["name"],\
                                         marker=markers[2*ii],markersize=3)
                        axs1[iplot].plot(th/np.pi,np.imag(EV1_pltmod),label="Im[], "+gkdict[ii]["code"]["name"],\
                                         marker=markers[2*ii+1],markersize=3)
                    else:
                        axs1.plot(th/np.pi,np.real(EV1_pltmod),label="Re[], "+gkdict[ii]["code"]["name"],\
                                  marker=markers[2*ii],markersize=3)
                        axs1.plot(th/np.pi,np.imag(EV1_pltmod),label="Im[], "+gkdict[ii]["code"]["name"],\
                                  marker=markers[2*ii+1],markersize=3)
            if args.plot_eigenmode_fields:
                if len(plotfields)>1:
                    axs1[iplot].set(xlabel=r"$\theta_{\rm pol}/\pi$",ylabel=plotfieldlabel[iplot])
                    axs1[0].legend(fontsize=6)
                else:
                    axs1.set(xlabel=r"$\theta_{\rm pol}/\pi$",ylabel=plotfieldlabel[iplot])
                    axs1.legend(fontsize=6)
            print ("")

            if gkdict[ii]["code"]["name"]=="QuaLiKiz":
                continue

            print ("Weight {0:30s}\t".format(pltmod),end="")
            for ii in range(len(gkdict)):
                print ("{0:8.6f}\t".format(gkdict[ii]["linear"]["wavevector"][iwv]\
                                           ["eigenmode"][0]["fields"][pltmod+"_weight"][0,0]),end="")
            print ("")

            print ("Parity {0:30s}\t".format(pltmod),end="")
            for ii in range(len(gkdict)):
                print ("{0:8.6f}\t".format(gkdict[ii]["linear"]["wavevector"][iwv]\
                                           ["eigenmode"][0]["fields"][pltmod+"_parity"][0,0]),end="")
            print ("")

            print ("Points {0:30s}\t".format(pltmod),end="")
            for ii in range(len(gkdict)):
                print ("{0:8.6f}\t".format(len(gkdict[ii]["linear"]["wavevector"][iwv]\
                                           ["eigenmode"][0]["fields"][pltmod+"_norm"])),end="")
            print ("")

        if args.plot_eigenmode_fields:
            plt.tight_layout()
        #plt.show()
        print ("")



        #compare field mode structures
        plotmommodes = ["density","j_parallel","pressure_parallel","pressure_perpendicular"]
        plotmomlabels = [r"$n$", r"$j_\|$",r"$p_\|$",r"$p_\perp$"]
        if args.plot_eigenmode_moments:
            fig2,axs2 = plt.subplots(minspec,len(plotmommodes))
            fig2.suptitle('Moments mode structures - wavevector #{0:d}'.format(iwv))
            fig2.canvas.manager.set_window_title('Moments mode structures - wavevector #{0:d}'.format(iwv))
        for ispec in range(minspec):
            print ("Species {0:d}".format(ispec))
            for imod,pltmod in enumerate(plotmommodes):
                print ("Maxabs {0:30s}\t".format(pltmod[:30]),end="")
                for ii in range(len(gkdict)):
                    if gkdict[ii]["code"]["name"]=="QuaLiKiz":
                        continue
                    wvdum = gkdict[ii]["linear"]["wavevector"][iwv]
                    th = wvdum["eigenmode"][0]["angle_pol"]
                    if len(wvdum["eigenmode"][0]["moments_norm_particle"][pltmod]) > 0:
                        EV1_pltmod = (wvdum["eigenmode"][0]["moments_norm_particle"][pltmod][ispec])

                        print ("{0:8.6f}\t".format(np.max(np.abs(EV1_pltmod))),end="")
                        if args.plot_eigenmode_moments:
                            axs2[ispec,imod].plot(th/np.pi,np.real(EV1_pltmod),label="Re[], "+gkdict[ii]["code"]["name"],\
                                              marker=markers[2*ii],markersize=3)
                            axs2[ispec,imod].plot(th/np.pi,np.imag(EV1_pltmod),label="Im[], "+gkdict[ii]["code"]["name"],\
                                              marker=markers[2*ii+1],markersize=3)
                    else:
                        print ("{0:14s}\t".format("None"),end="")
                print ("")
                if args.plot_eigenmode_moments:
                    #            axs2[ispec].set_title(pltmod+", species {0:2d}".format(ispec))
                    axs2[ispec,imod].set(xlabel=r"$\theta_{\rm pol}/\pi$",ylabel=plotmomlabels[imod]+
                                         ", species {0:2d}".format(ispec))
                    if (ispec+imod)==0:
                        axs2[ispec,imod].legend(fontsize=6)
                    plt.tight_layout()

        print("-------------------------------------------------------------------------------")
        print("")
    #plt.plot(gkdict[0]["wavevector"][0]["eigenvalues"][0]["eigenvector"]["poloidal_angle"])
    #plt.plot(gkdict[1]["wavevector"][0]["eigenvalues"][0]["eigenvector"]["poloidal_angle"])
    #plt.show()

### Linear growth rate / frequency spectra
if args.plot_growthrate_freq and minwv > 1 and gkdict[0]["linear"]:
    figgamom,axsgamom = plt.subplots(2)
    figgamom.suptitle('Growth rate / Frequency - eigenmode #{0:d}'.format(0))
    figgamom.canvas.manager.set_window_title('Growth rate / Frequency - eigenmode #{0:d}'.format(0))
    for ii in range(len(gkdict)):
        k2 = []
        gamma = []
        freq = []
        for iwv in range(minwv):
            k2 += [gkdict[ii]["linear"]["wavevector"][iwv]["binormal_wavevector_norm"]]
            gamma += [gkdict[ii]["linear"]["wavevector"][iwv]["eigenmode"][0]["growth_rate_norm"]]
            freq += [gkdict[ii]["linear"]["wavevector"][iwv]["eigenmode"][0]["frequency_norm"]]
        k2 = np.array(k2)
        gamma = np.array(gamma)
        freq = np.array(freq)
        k2_sortind=np.argsort(k2)
        axsgamom[0].plot(k2[k2_sortind],gamma[k2_sortind],label=gkdict[ii]["code"]["name"])
        axsgamom[1].plot(k2[k2_sortind],freq[k2_sortind],label=gkdict[ii]["code"]["name"])

    axsgamom[0].set(xlabel=r"$k_\theta \rho_{\rm ref}$",ylabel=r"$\gamma / [v_{\rm ref}/R_0]$")
    axsgamom[1].set(xlabel=r"$k_\theta \rho_{\rm ref}$",ylabel=r"$\omega / [v_{\rm ref}/R_0]$")
    axsgamom[1].legend(fontsize=6)

    plt.tight_layout()


### non_linear
if len(gkdict[0]["non_linear"]["time_norm"])>0:

    #compare time traces
    if args.plot_flux_timetraces:
        figflux,axsflux = plt.subplots(minspec)
        figflux.suptitle('Fluxes time traces')
    print ("Fluxes 1D")
    for ispec in range(minspec):
        print ("Species {0:d}".format(ispec))
        if args.plot_flux_timetraces:
            axsflux[ispec].set_title("species {0:2d}".format(ispec))
            axsflux[ispec].set(xlabel=r"$t / [R/v_{th}]$",ylabel="Fluxes species {0:2d}".format(ispec))
        for fluxkey in gkdict[0]["non_linear"]["fluxes_2d_k_x_k_y_sum_rotating_frame"].keys():
            if (fluxkey in ["version","max_repr_length"]): continue
            print ("{0:35s}\t".format(fluxkey[:35]),end="")
            for ii in range(len(gkdict)):
                if len(gkdict[ii]["non_linear"]["fluxes_2d_k_x_k_y_sum_rotating_frame"][fluxkey])>0:
                    flux_norm = gkdict[ii]["non_linear"]["fluxes_2d_k_x_k_y_sum_rotating_frame"][fluxkey][ispec]
                    time_norm = gkdict[ii]["non_linear"]["time_norm"]

                    if args.plot_flux_timetraces:
                        axsflux[ispec].plot(time_norm,flux_norm,label=fluxkey+" "+gkdict[ii]["code"]["name"])
                        axsflux[ispec].legend(fontsize=6)
                    if gkdict[ii]["non_linear"]["fluxes_1d_rotating_frame"][fluxkey][ispec]==None:
                        print ("{0:14s}\t".format("None"),end="")
                    else:
                        if args.plot_flux_timetraces:
                            axsflux[ispec].axhline(y=gkdict[ii]["non_linear"]["fluxes_1d_rotating_frame"][fluxkey][ispec],
                                               linestyle='-.',color=axsflux[ispec].get_lines()[-1].get_c())
                        print ("{0:8.6f}\t".format(gkdict[ii]["non_linear"]["fluxes_1d_rotating_frame"][fluxkey][ispec]),end="")
                else:
                    print ("{0:14s}\t".format("None"),end="")
            print("")
        print("")
    if args.plot_flux_timetraces:
        plt.tight_layout()

    #Flux vs ky
    if args.plot_flux_ky_spectra:
        figfluxky,axsfluxky = plt.subplots(minspec)
        figfluxky.suptitle('Rotating frame flux spectra')
        for ispec in range(minspec):
            axsfluxky[ispec].set_title("species {0:2d}".format(ispec))
            axsfluxky[ispec].set(xlabel=r"$k_y\rho_{\rm ref}$",ylabel=\
                                 "Spectra species {0:2d}".format(ispec))
            for fluxkey in gkdict[0]["non_linear"]["fluxes_2d_k_x_sum_rotating_frame"].keys():
                if (fluxkey in ["version","max_repr_length"]): continue
                for ii in range(len(gkdict)):
                    if len(gkdict[ii]["non_linear"]["fluxes_2d_k_x_sum_rotating_frame"][fluxkey])>0:
                        fluxky_norm = gkdict[ii]["non_linear"]["fluxes_2d_k_x_sum_rotating_frame"][fluxkey][ispec]
                        ky_norm = gkdict[ii]["non_linear"]["binormal_wavevector_norm"]
                        axsfluxky[ispec].plot(ky_norm,fluxky_norm,label=fluxkey+" "+gkdict[ii]["code"]["name"])
                        axsfluxky[ispec].legend(fontsize=6)
        plt.tight_layout()

#compare flux surfaces
if args.plot_fluxsurface:
    fig3 = plt.figure("Flux surface")
    for ii in range(len(gkdict)):
        nth = 128
        r = gkdict[ii]["flux_surface"]["r_minor_norm"]
        k = gkdict[ii]["flux_surface"]["elongation"]
        dkdr = gkdict[ii]["flux_surface"]["delongation_dr_minor_norm"]
        R0 = 1.0
        Z0 = 0.0
        dR0dr = gkdict[ii]["flux_surface"]["dgeometric_axis_r_dr_minor"]
        dZ0dr = gkdict[ii]["flux_surface"]["dgeometric_axis_z_dr_minor"]
        cN = gkdict[ii]["flux_surface"]["shape_coefficients_c"]
        sN = gkdict[ii]["flux_surface"]["shape_coefficients_s"]
        cN_dr = gkdict[ii]["flux_surface"]["dc_dr_minor_norm"]
        sN_dr = gkdict[ii]["flux_surface"]["ds_dr_minor_norm"]

        theta_grid = (np.arange(0.0,nth,1.0))*2.0*np.pi/(nth-1)
        thR = theta_grid[:]
        thRdr = 0.0*theta_grid[:]
        for nind in range(len(cN)):
            #Warning: using += would change the contents of theta_grid?!
            thR = thR + cN[nind]*np.cos(nind*theta_grid[:]) + sN[nind]*np.sin(nind*theta_grid[:])
            thRdr = thRdr + cN_dr[nind]*np.cos(nind*theta_grid[:]) + sN_dr[nind]*np.sin(nind*theta_grid[:])

        R = R0 + r*np.cos(thR)
        Z = Z0 - k*r*np.sin(theta_grid)
        R_rho = dR0dr + np.cos(thR) - r * np.sin(thR) * thRdr
        Z_rho = dZ0dr - (dkdr*r + k) * np.sin(theta_grid[:])

        plt.plot(R,Z,'-.',label=gkdict[ii]["code"]["name"]+"(R,Z)")
        dr = 0.04
        plt.plot(R+R_rho*dr,Z+Z_rho*dr,':',label=gkdict[ii]["code"]["name"]+"(R,Z)+dr")
    plt.legend()
    plt.axis('equal')
    plt.xlabel(r'$R/R_0$')
    plt.ylabel(r'$Z/R_0$')

plt.show()

