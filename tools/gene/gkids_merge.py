#!/usr/bin/env python3

""" gkids_merge.py: command line tool to merge GKIDS h5 files

Usage: gkids_cmp.py -o <output file> <input file(s)>

Current version would be in desparate need for a python code cleanup

"""

import argparse
import dataclasses
import sys,os
sys.path.append(os.path.join(os.path.dirname(__file__), '../../idspy_toolkit'))
import idspy_toolkit as idspy
sys.path.append(os.path.join(os.path.dirname(__file__), \
        '../../idspy_dictionaries_packages/idspy_dictionaries'))
from idspy_dictionaries import ids_gyrokinetics_local

import gkids.tools.gkids as gkidstools
#try:
#    import gkids.tools.gkids as gkidstools
#except:
#    print ("ERROR: Couldn't import gkids.tools.gkids, please add the imas-gk folder to PYTHONPATH")
#    print ('       Example: export PYTHONPATH="${PYTHONPATH}:${YOURGKIMASDIR}"')
#    exit(-1)

import numpy as np
import warnings
import matplotlib.pyplot as plt

#parse input arguments
parser = argparse.ArgumentParser(description="command line tool to merge GKIDS h5 files")
parser.add_argument("infile", metavar="infile", nargs="+", help="GKIDS h5 input file(s)")
parser.add_argument("--outfile", "-o", default="merged_ids.h5", help="Set output filename")

args = parser.parse_args()

first = True

for infile in args.infile:
    if not os.path.isfile(infile):
        print ("skipping non-existing", infile)
        continue

    dum=ids_gyrokinetics_local.GyrokineticsLocal()
    idspy.fill_default_values_ids(dum)
    idspy.hdf5_to_ids(infile,dum,todict=False)

    if first:
        outgkids = dum
        first = False
    else:
        gkidstools.add_eigenmode(outgkids,dum)

g,k = idspy.ids_to_hdf5(outgkids,args.outfile,overwrite=True)
print("Finished writing {} ...".format(args.outfile))
