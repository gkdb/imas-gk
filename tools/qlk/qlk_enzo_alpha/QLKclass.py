from qlk2ids import *
import numpy as np
import inspect
import pandas as pd
from idspy_dictionaries import ids_gyrokinetics_local as gkids
import idspy_toolkit as idspy
import datetime
import os
import subprocess
from pathlib import Path
from os.path import relpath

from qualikiz_tools.qualikiz_io.inputfiles import *
from qualikiz_tools.machine_specific.bash import Batch, Run

import shutil
import struct

np.seterr(all="raise",)

class QLK_class:
    """
    A class containing all the inputs and outputs necessary to run QuaLiKiZ
    """
    def __init__(self):
        # initialise QLK constant values
        self.qref = np.float64(1.602176565e-19)  # C, qlk charge
        self.mref = np.float64(1.672621777e-27)  # kg, qlk proton mass
        self.me = np.float64(9.10938291e-31)  # kg, qlk electron mass
        self.Tref = np.float64(1e3)  # eV
        self.nref = np.float64(1e19)  # m^-3
        self.cref = np.sqrt(self.Tref * np.float64(self.qref) / np.float64(self.mref))

        self.typee = 1 #adiabatic default
        self.rot_flag = 0 # no rotation default
        self.anis = 0 # no temperature anisotropy default
        self.danis = 0 # no temperature anisotropy gradient default

        # INIT VALUES BY DEFAULT
        # By default parameters
        self.dimx = 1  # Number of radial points or scans: set to 1 (no scan)
        self.dimn = 1  # Number of wavenumbers
        self.nions = 1  # Number of ions in the system
        self.phys_meth = 2  # Flag for additional computation (default 0.0)
        self.coll_flag = 0  # Flag for collisionality (default 0.0)
        self.write_primi = 1  # Flag for writing primitive outputs (default 1)
        self.rot_flag = 0  # Flag for rotation (default 0.0)
        self.verbose = 1  # Flag to set the verbosity level of the output
        self.numsols = 3  # Number of solutions requested
        self.relacc1 = 1e-3  # Relative accuracy in 1D integrals
        self.relacc2 = 2e-2  # Relative accuracy in 2D integrals
        self.absacc1 = 0  # Collisionality multiplier (for tests)
        self.absacc2 = 0  # Collisionality multiplier (for tests)
        self.maxruns = 1  # Number of iterations jumping directly to Newton between contour checks
        self.maxpts = 5e5  # Number of integrand evaluations performed in the 2D integral
        self.timeout = 600  # Upper time limit (s) beyond which solutions are not sought for a given wavenumber and radius
        self.ETGmult = 1  # ETG multiplier
        self.collmult = 1  # Collisionality multiplier

        # Geometry input
        self.kthetarhos = 0.2  # Wavenumber spectrum input: Vector (dimn)
        self.x = 0.5 / 3  # Normalized radial coordinate (midplane average)
        self.rho = 0.5 / 3  # Normalized toroidal flux coordinate
        self.rhomin = 0  # Normalized toroidal flux coordinate
        self.rhomax = 1  # Normalized toroidal flux coordinate
        self.Ro = 3  # Major radius. Radial profile due to Shafranov shift
        self.Rmin = 0.5  # Geometric minor radius. Assumed to be an average at the midplane at the LCFS. Currently a profile but probably should be shifted to a scalar
        self.Bo = 1  # Probably not very rigorous to use this sqrt(<B^2>) to calculate Larmor radius; fairly close to <Bphi> in practice though
        # self.R0 = self.R0  # Major geometric radius used for normalizations
        self.q = 2  # Vector (radial grid x(aa))
        self.smag = 1  # Vector (radial grid x(aa)); q is a flux surface quantity --> makes sense to consider s = self.rho / q dq/drho
        self.alpha = 0  # Vector (radial grid x(aa))

        # Rotation input
        self.Machtor = 0  # Vector (radial grid x(aa))
        self.Autor = 0  # Vector (radial grid x(aa))
        self.Machpar = 0  # Vector (radial grid x(aa))
        self.Aupar = 0  # Vector (radial grid x(aa))
        self.gammaE = 0  # Vector (radial grid x(aa))

        # Electron input
        self.Te = 1  # Vector (radial grid x(aa))
        self.ne = 1  # Vector (radial grid x(aa))
        self.Ate = 9  # Vector (radial grid x(aa))
        self.Ane = 3  # Vector (radial grid x(aa))
        self.typee = 1  # Kinetic or adiabatic
        self.anise = 1  # Tperp/Tpar at LFS
        self.danisdre = 0  # d/dr(Tperp/Tpar) at LFS

        # Ion inputs (can be for multiple species)
        self.Ai = [1]  # Ion mass
        self.Zi = [1]  # Ion charge
        self.Ti =  [1]  # Vector (radial grid x(aa))
        self.normni = [1]  # ni/ne Vector (radial grid x(aa))
        self.Ati = [9]  # Vector (radial grid x(aa))
        self.Ani = [3]  # Vector (radial grid x(aa)); Check calculation against Qualikiz's electroneutrality assumption
        self.typei = [1]  # Kinetic, adiabatic, tracer
        self.anisi = [1]  # Tperp/Tpar at LFS
        self.danisdri = [0]  # d/dr(Tperp/Tpar) at LFS
        self.separateflux = 1  # Separate flux output
        self.simple_mpi_only = 0  # Separate flux output
        self.integration_routine = 1  # NAG

        self.commit = 'qlskdfj'
        self.code_version = 'qmlkdjf'


    def load_QLK_runfolder(self, run_path=None):
        """Loads data from a QLK pythontools plan"""
        print('Nice try but .load_runfolder is not coded yet :)')
        pass


    def load_IMAS_ids(self, ids_path=None, ids=None, ):
        """Loads data from an ids file and converts it to QLK normalisations. Can pass an existing IDS or the path to one.
        :param ids_path: path to load IDS from
        :param ids: existing IDS to load, not used if ids_path is provided
        """
        if ids is None and ids_path is None:
            print('no ids or path provided')
            return
        elif ids is None and ids_path is not None:
            # load ids from file
            print('generating empty ids')
            ids = gkids.GyrokineticsLocal()
            # fill with default values
            print('filling with default values')
            idspy.fill_default_values_ids(ids)
            # update with values from file
            print('filling with values from file')
            idspy.hdf5_to_ids(ids_path, ids)

        


        # species related sorted and indexing to separate ions, electrons and find main ion
        e_count = 0
        i_indices = []
        max_ion_dens = 0
        for index, species in enumerate(ids.species):
            # round charge to nearest whole number to account for different normalising quantities
            charge = round(species.charge_norm, 0)
            if charge < 0:
                # TODO: fit an error in here if charge != -1?
                # index as not ion
                if charge == -1:
                    # running electron count, needs to be 1 for script to work
                    e_count += 1
                    e_index = index
                if charge != -1:
                    print('Non 1 negative charge on species (not an electron), conversion aborted.')
                    return
            elif charge > 0:
                # index as ion
                i_indices.append(index)
                # find main ion
                if species.density_norm > max_ion_dens:
                    max_ion_dens = species.density_norm
                    main_ion_index = index
            elif charge == 0:
                print('No charge on species, conversion aborted.')
                return
        if e_count != 1:
            print('Electron species count not equal to 1. Conversion aborted, please check input file.')
            return

        # rearrange ion indices to have main ion first
        i_indices_reordered = [main_ion_index]
        for index in i_indices:
            if index != main_ion_index:
                i_indices_reordered.append(index)

        # number of ions
        nions = len(ids.species) - e_count


        self.Ro = Ro_qlk(ids)
        self.Bo = Bo_qlk(ids)
        self.ne = ne_qlk(ids, self.nref)
        self.Te = Te_qlk(ids, e_index, self.Tref)

        self.Rref = Rref_qlk(self.Ro)
        self.Bref = Bref_qlk(self.Bo)

        # load values to calculated IMAS constants and Ratios
        IMAS = constantsIMAS(self.Ro, self.Bo, self.Te, self.Te, self.Tref, self.nref)
        RATIOS = constantsRATIO(self, IMAS, baseline=0)

        self.source = 'ids'


        self.x = x_qlk()
        self.Rmin = Rmin_qlk(ids, self.Ro, self.x)

        self.q = q_qlk(ids)
        self.smag = smag_qlk(ids)
        self.alpha = alpha_qlk(ids, self.q, RATIOS)
        self.Machtor = Machtor_qlk(ids, IMAS, RATIOS, self.cref)

        self.Machpar = Machpar_qlk()
        self.Aupar = Aupar_qlk()
        self.Autor = Autor_qlk(ids, main_ion_index, IMAS, RATIOS, self.cref)
        self.gammaE = gammaE_qlk(ids, IMAS, RATIOS, self.cref)


        self.vel_grads = vel_grads_qlk(ids)
        # Electron Properties
        # select electron species from ids for simplicity
        electron = ids.species[e_index]
        self.Ane = Ane_qlk(electron, RATIOS)
        # QLK Electron Logarithmic Temperature Gradient
        self.Ate = Ate_qlk(electron, RATIOS)
        e_charge = np.abs(electron.charge_norm)



        # Ion Properties
        qlk_Ions_props = {'Ti': [], 'normni': [], 'Ani': [], 'Ati': [], 'Ai': [], 'Zi': [], 'typei': []}
        for index in i_indices_reordered:
            # select ion species from ids for simplicity
            ion = ids.species[index]
            # QLK Ion Temperature
            Ti_QLK = Ti_qlk(ion, self.Te)
            qlk_Ions_props['Ti'].append(Ti_QLK)
            # QLK Ion Density
            normni_QLK = normni_qlk(ion)
            qlk_Ions_props['normni'].append(normni_QLK)
            # QLK Ion Logarithmic Density Gradient
            Ani_QLK = Ani_qlk(ion, RATIOS)
            qlk_Ions_props['Ani'].append(Ani_QLK)
            # QLK Ion Logarithmic Temperature Gradient
            Ati_QLK = Ati_qlk(ion, RATIOS)
            qlk_Ions_props['Ati'].append(Ati_QLK)
            # QLK Ion Mass
            Ai_QLK = Ai_qlk(ion, RATIOS)
            qlk_Ions_props['Ai'].append(Ai_QLK)
            # QLK Ion Charge (adjusted to be relative to the QLK electron charge of 1)
            Zi_QLK = Zi_qlk(ion, e_charge)
            qlk_Ions_props['Zi'].append(Zi_QLK)
            # QLK Ion type
            qlk_ion_type = ion_type_qlk(normni_QLK)
            qlk_Ions_props['typei'].append(qlk_ion_type)


        self.normni = np.array(qlk_Ions_props['normni'])
        self.Zi = np.array(qlk_Ions_props['Zi'])
        self.ni = ni_qlk(self.normni, self.ne)
        self.Ai = np.array(qlk_Ions_props['Ai'])
        self.Ti = np.array(qlk_Ions_props['Ti'])
        self.Ani = np.array(qlk_Ions_props['Ani'])
        self.Ati = np.array(qlk_Ions_props['Ati'])

        self.Ai0 = self.Ai[0]
        self.mi0 = mi_qlk(self.Ai0, self.mref)
        self.vth_i0 = vth_qlk(self.Te, self.Tref, self.qref, self.mi0)
        self.rho_i0 = rho_i_qlk(self.mi0, self.vth_i0, self.qref, self.Bref)


        # recalculate ratios now that we have all the main ion quantities
        RATIOS = constantsRATIO(self, IMAS)

        self.kthetarhos = kthetarhos_qlk(ids, RATIOS)

        # adiabatic electrons for both QLK-IMAS and IMAS-QLK
        self.typee = typee_qlk(ids)


        self.typei = typei_qlk(self.normni)

        # temperature anisotropy and gradient set to 0
        self.anis = anis_qlk()
        self.danisdr = danisdr_qlk()

        self.rot_flag = rot_flag_qlk(ids)


        # load remaining values not used for QLK (to be able to loop conversion back and forth)
        self.code_version = ids.code.version
        self.commit = ids.code.commit

        if hasattr(ids, "linear"):
            if hasattr(ids.linear, "wavevector"):
                dimn = len(ids.linear.wavevector)
        else:
            dimn = 0

        # check number of solutions for qlk numsols (max of 3)
        if dimn > 0:
            numsols = len([ids.linear.wavevector[nk].eigenmode for nk in range(dimn)])
        else:
            dimn=1
            numsols = 2
        # ?????????????????????? ASK MATISSE
        # if set(numsols) == {numsols[0]}:
        #     numsols = min(3, numsols[0])
        #     print(f'Constant numsol')
        # else:
        #     numsols = min(3, max(numsols))
        #     print(f'Number of eigenmodes per wavevector is different. Using largest <= 3 by default')

        self.numsols = numsols
        self.ndimn = dimn
        self.nnions = nions


        # outputs
        try:
            self.gam_SI = gam_SI_qlk(ids, IMAS)
        except:
            pass

        try:
            self.ome_SI = ome_SI_qlk(ids, IMAS)
        except:
            pass

        try:
            self.pfe_SI = pf_SI_qlk(ids, e_index, self.ne, IMAS)
        except:
            pass

        try:
            self.efe_SI = ef_SI_qlk(ids, e_index, self.ne, IMAS)
        except:
            pass

        try:
            self.vfe_SI = vf_SI_qlk(ids, e_index, self.ne, IMAS)
        # fluxes contain all particles in a specific order, but ni has already been reordered so need to iterate through both differently hence the n and i_index
        except:
            pass

        try:
            self.pfi_SI = pf_SI_qlk(ids, i_indices_reordered, self.ni, IMAS)
        except:
            pass

        try:
            self.efi_SI = ef_SI_qlk(ids, i_indices_reordered, self.ni, IMAS)
        except:
            pass

        try:
            self.vfi_SI = vf_SI_qlk(ids, i_indices_reordered, self.ni, IMAS)
        except:
            pass

        #
        # self.distan =
        # self.rmodewidth =
        # self.modeshift =
        # self.modewidth =


        try:
            self.thetas = np.array([[eigenmode.angle_pol for eigenmode in wavevector.eigenmode] for wavevector in ids.linear.wavevector])
        except:
            pass
        try:
            self.pol_turns = np.array([[eigenmode.poloidal_turns for eigenmode in wavevector.eigenmode] for wavevector in ids.linear.wavevector])
        except:
            pass
        try:
            self.phi = np.array([[eigenmode.fields.phi_potential_perturbed_norm for eigenmode in wavevector.eigenmode] for wavevector in ids.linear.wavevector])
        except:
            pass


    def to_IMAS_ids(self, provider, comment=''):
        """
        converts the existing QLK class to IMAS IDS format and returns the filled IDS
        :param provider: name of person doing conversion
        :param comment: optional comment
        """
        # initialise constants
        IMAS = constantsIMAS(self.Ro, self.Bo, self.Te, self.ne, self.Tref, self.nref)
        RATIOS = constantsRATIO(self, IMAS)

        # ids properties #
        ids_properties: gkids.IdsProperties = gkids.IdsProperties(
            provider=provider,
            creation_date=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            comment=f'Converted from {self.source}. {comment}',
            homogeneous_time=int(2),
        )

        # code #
        code: gkids.Code = gkids.Code(
            name="QuaLiKiz",
            repository="https://gitlab.com/qualikiz-group/QuaLiKiz",
            commit=str(self.commit),
            version=str(self.code_version),
            # library = , (not relevant)
        )

        # normalizing_quantities #
        normalizing_quantities: gkids.InputNormalizing = gkids.InputNormalizing(
            t_e=IMAS.Tref,
            n_e=IMAS.nref,
            r=np.float64(IMAS.Rref),
            b_field_tor=np.float64(IMAS.Bref),
        )

        # model #
        model: gkids.Model = gkids.Model(
            adiabatic_electrons=electron_type(self.typee),
            include_a_field_parallel=include_a_field_parallel(),
            include_b_field_parallel=include_b_field_parallel(),
            include_full_curvature_drift=include_full_curvature_drift(),
            #TODO include_coriolis_drift=,
            include_centrifugal_effects=include_centrifugal_effects(self.rot_flag, self.x),
            collisions_pitch_only=collisions_pitch_only(),
            collisions_momentum_conservation=collisions_momentum_conservation(),
            collisions_energy_conservation=collisions_energy_conservation(),
            collisions_finite_larmor_radius=collisions_finite_larmor_radius(),
        )
        # flux_surface #
        flux_surface: gkids.FluxSurface = gkids.FluxSurface(
            ip_sign=ip_sign(),
            b_field_tor_sign=b_field_tor_sign(),
            r_minor_norm=r_minor_norm(self.Rmin, self.x, self.Ro),
            q=q_IMAS(self.q),
            magnetic_shear_r_minor=magnetic_shear_r_minor(self.smag),
            pressure_gradient_norm=pressure_gradient_norm(self.alpha, self.q, RATIOS.Rrat, RATIOS.Brat),
            dgeometric_axis_r_dr_minor=dgeometric_axis_r_dr_minor(self.alpha),
            dgeometric_axis_z_dr_minor=dgeometric_axis_z_dr_minor(),
            elongation=elongation(),
            delongation_dr_minor_norm=delongation_dr_minor_norm(),
            shape_coefficients_c=shape_coefficients_c(),
            dc_dr_minor_norm=dc_dr_minor_norm(),
            shape_coefficients_s=shape_coefficients_s(),
            ds_dr_minor_norm=ds_dr_minor_norm(),
        )

        # species_all #
        species_all = gkids.InputSpeciesGlobal(
            velocity_tor_norm=velocity_tor_norm(self.Machtor, self.cref, IMAS.vth_ref, RATIOS.Rrat),
            shearing_rate_norm=shearing_rate_norm(self.gammaE, self.cref, IMAS.vth_ref, RATIOS.Rrat),
            beta_reference=beta_reference(),
            debye_length_norm=debye_length_reference(),
            #TODO angle_pol=,
        )

        # species #
        # electron first
        electron_temp = gkids.Species(
            charge_norm=charge_norm(np.float64(-1), RATIOS.qrat),
            mass_norm=mass_norm(self.me / self.mref, RATIOS.mrat),
            density_norm=density_norm_e(self.ne),
            density_log_gradient_norm=density_log_gradient(self.Ane, RATIOS.Rrat),
            temperature_norm=temperature_norm(self.Te, self.Te),
            temperature_log_gradient_norm=temperature_log_gradient(self.Ate, RATIOS.Rrat),
            velocity_tor_gradient_norm=velocity_tor_gradient_norm(self.Autor, self.cref, IMAS.vth_ref, RATIOS.Rrat),
            #TODO potential_energy_norm ?
            #TODO potential_energy_gradient_norm ?

        )



        # ions
        # precalculate arrays to access with loop over nions
        Charge_Norm = charge_norm(self.Zi, RATIOS.qrat)
        Mass_norm = mass_norm(self.Ai, RATIOS.mrat)
        # calculate density of ions accounting for tracer ions
        ion_density_mult = ion_type(self.typei) # 0 or 1 depending on ion type
        dens_norm = density_norm_i(self.normni) * ion_density_mult # sets tracer ion densities to 0

        dens_grad = density_log_gradient(self.Ani, RATIOS.Rrat)
        temperature_norm_i = temperature_norm(self.Ti, self.Te)
        temp_grad_i = temperature_log_gradient(self.Ati, RATIOS.Rrat)

        species: tuple[gkids.Species] = (electron_temp,) + tuple([gkids.Species(
            charge_norm=Charge_Norm[nion],
            mass_norm=Mass_norm[nion],
            density_norm=dens_norm[nion],
            density_log_gradient_norm=dens_grad[nion],
            temperature_norm=temperature_norm_i[nion],
            temperature_log_gradient_norm=temp_grad_i[nion],
            velocity_tor_gradient_norm=electron_temp.velocity_tor_gradient_norm
            #TODO potential_energy_norm ?
            #TODO potential_energy_gradient_norm?
        ) for nion in range(self.nnions)])

        # collisionality #
        coll_array = np.zeros((self.nnions + 1, self.nnions + 1))
        coll_norm = collisionality_norm(IMAS.Rref,
                                        IMAS.vth_ref,
                                        self.ne,
                                        dens_norm,
                                        self.Zi,
                                        RATIOS.qrat,
                                        self.Te,
                                        IMAS.vth_e)
        for nion in range(self.nnions):
            coll_array[0, nion + 1] = coll_norm[nion]

        collisions: gkids.Collisions = gkids.Collisions(collisionality_norm=coll_array)

        # linear #
        linear: gkids.GyrokineticsLinear() = gkids.GyrokineticsLinear()

        # wavevectors
        binormal_wavevector_norm = binormal_component_norm(RATIOS.rho_rat, self.kthetarhos)
        # calculate growth rate and frequency for whole set
        try:
            growth_rate_norms = growth_rate_norm_SI(self.gam_SI, IMAS.Rref, IMAS.vth_ref)
        except:
            growth_rate_norms = growth_rate_norm_GB(self.gam_GB, self.Rmin, IMAS.Rref, RATIOS.vth_rat)
        try:
            frequency_norms = frequency_norm_SI(self.ome_SI, IMAS.Rref, IMAS.vth_ref)
        except:
            frequency_norms = frequency_norm_GB(self.ome_GB, self.Rmin, IMAS.Rref, RATIOS.vth_rat)


        # loop over wavevectors
        for dimn in range(self.ndimn):
            try:
                # eigenmode
                d = self.distan[dimn]
                w = self.modewidth[dimn]
                x0 = self.modeshift[dimn]

                poloidal_angle, pol_turns = poloidal_angle_pturns(w, d, x0)
                phi_potential_perturbed_norm, Af = phi_potential_perturbed_norm_linear(poloidal_angle, w, x0, d)
                # generate default wavevector class
                wavevector_temp:gkids.Wavevector() = gkids.Wavevector(
                    radial_wavevector_norm=radial_component_norm(),
                    binormal_wavevector_norm=binormal_wavevector_norm[dimn],

                    # iterate over the number of solutions
                    eigenmode=[gkids.Eigenmode(poloidal_turns=pol_turns,
                                                angle_pol=poloidal_angle,
                                                #todo: time_norm = 1,?
                                                initial_value_run = 0,
                                                code=gkids.Code(),
                                                growth_rate_norm=growth_rate_norms[dimn, numsol],
                                                frequency_norm=frequency_norms[dimn, numsol],
                                                fields=gkids.EigenmodeFields(phi_potential_perturbed_norm=[phi_potential_perturbed_norm]),
                                                # moments=,
                                                # linear_weights=,
                                                )
                                for numsol in range(self.numsols)]
                    )
            except:
                try:
                    wavevector_temp: gkids.Wavevector() = gkids.Wavevector(
                        radial_wavevector_norm=radial_component_norm(),
                        binormal_wavevector_norm=binormal_wavevector_norm[dimn],

                        # iterate over the number of solutions
                        eigenmode=[gkids.Eigenmode(poloidal_turns=self.pol_turns[dimn, numsol],
                                                    angle_pol=self.thetas[dimn, numsol],
                                                    # todo: time_norm = 1,?
                                                    initial_value_run=0,
                                                    code=gkids.Code(),
                                                    growth_rate_norm=growth_rate_norms[dimn, numsol],
                                                    frequency_norm=frequency_norms[dimn, numsol],
                                                    fields=gkids.EigenmodeFields(
                                                        phi_potential_perturbed_norm=self.phi[dimn, numsol]),
                                                    # moments=,
                                                    # linear_weights=,
                                                    )
                            for numsol in range(self.numsols)]
                    )
                except:
                    wavevector_temp: gkids.Wavevector() = gkids.Wavevector(
                        radial_wavevector_norm=radial_component_norm(),
                        binormal_wavevector_norm=binormal_wavevector_norm[dimn],

                        # iterate over the number of solutions
                        eigenmode=[gkids.Eigenmode()
                                    for numsol in range(self.numsols)]
                    )
            linear.wavevector.append(wavevector_temp)

        # non-linear
        try:
            ion_particle_flux = normalise_integrated_particle_flux(self.pfi_SI, self.ni, IMAS.vth_ref, IMAS.rho_star, Af=1)
        except:
            ion_particle_flux = np.zeros(self.nnions)
        try:
            electron_particle_flux = normalise_integrated_particle_flux(self.pfe_SI, self.ne, IMAS.vth_ref, IMAS.rho_star, Af=1)
        except:
            electron_particle_flux = np.zeros(1)

        particle_flux = np.append(electron_particle_flux, ion_particle_flux)

        try:
            ion_energy_flux = normalise_integrated_energy_flux(self.efi_SI, self.ni, IMAS.vth_ref, IMAS.rho_star, IMAS.Tref, Af=1)
        except:
            ion_energy_flux = np.zeros(self.nnions)
        try:
            electron_energy_flux = normalise_integrated_energy_flux(self.efe_SI, self.ne, IMAS.vth_ref, IMAS.rho_star, IMAS.Tref, Af=1)
        except:
            electron_energy_flux = np.zeros(1)

        energy_flux = np.append(electron_energy_flux, ion_energy_flux)
        parallel_momentum_flux = np.append(np.array(0), np.zeros(self.nnions))
        # catch missing vfe from adiabatic electrons
        try:
            ion_perpendicular_momentum_flux = normalise_integrated_momentum_flux(self.vfi_SI, self.ni, IMAS.vth_ref, IMAS.rho_star, self.mref, IMAS.Rref, Af=1)
        except:
            ion_perpendicular_momentum_flux = np.zeros(self.nnions)
        try:
            electron_perpendicular_momentum_flux = normalise_integrated_momentum_flux(self.vfe_SI, self.ne, IMAS.vth_ref, IMAS.rho_star, self.mref, IMAS.Rref, Af=1)
        except:
            electron_perpendicular_momentum_flux = np.zeros(1)

        perpendicular_momentum_flux = np.append(electron_perpendicular_momentum_flux, ion_perpendicular_momentum_flux)
        non_linear: gkids.GyrokineticsNonLinear() = gkids.GyrokineticsNonLinear(binormal_wavevector_norm=binormal_wavevector_norm,
                                                                                radial_wavevector_norm=radial_component_norm(binormal_wavevector_norm),
                                                                                quasi_linear=1,
                                                                                code=gkids.CodePartialConstant(),
                                                                                fluxes_1d=gkids.FluxesNl1D(
                                                                                    particles_phi_potential=particle_flux,
                                                                                    energy_phi_potential=energy_flux,
                                                                                    momentum_tor_parallel_phi_potential=parallel_momentum_flux,
                                                                                    momentum_tor_perpendicular_phi_potential=perpendicular_momentum_flux)
                                                                                )


        ids = gkids.GyrokineticsLocal(
            ids_properties=ids_properties,
            code=code,
            normalizing_quantities=normalizing_quantities,
            model=model,
            flux_surface=flux_surface,
            species_all=species_all,
            species=species,
            collisions=collisions,
            linear=linear,
            non_linear=non_linear,
        )
        return ids


    def run_QLK(self, name, runsdir, qlk_root_path, CPU =32, verbose=False, template_path='./default_parameters.json'):
        '''ENZO CODE HERE'''
        qlk_path_abs = os.path.expanduser(qlk_root_path) #if run_path is relative (means use the "~") it will expand to the correct path (doesn't do anything if the user put an absolute path)
        run_path_abs = os.path.expanduser(runsdir)
        run_name_path_abs = run_path_abs+'/'+name
        
        qlkclass_path = os.path.abspath(os.path.dirname(__file__)) # check at the end of the run if it is useful
        

        # Change the current work path to the run folder path 
        os.chdir(run_path_abs)

        # SET STORAGE


        if not os.path.isdir(run_path_abs):
            os.mkdir(run_path_abs)

        if not os.path.isdir(run_name_path_abs):
            os.mkdir(run_name_path_abs)

        # don't forget the QuaLiKiz executable
        shutil.copy(qlk_path_abs + '/QuaLiKiz', f'{run_name_path_abs}/QuaLiKiz')

        # Pre build of the files containing input and outputs
        os.makedirs(f'{run_name_path_abs}/input', exist_ok=True)
        os.makedirs(f'{run_name_path_abs}/output', exist_ok=True)
        os.makedirs(f'{run_name_path_abs}/output/primitive', exist_ok=True)
        os.makedirs(f'{run_name_path_abs}/debug', exist_ok=True)

        
        # WRITE INPUTS
        ################## Write binary input files ################################

        # namearg = self.list_properties()
        # namearg_that_are_not_qlk_inputs = ['anis', 'danis', 'danisdr', 'ni', 'Ai0', 'mi0', 'mi0', 'rho_i0', 'vth_i0', 'code_version', 'source', 'commit', 'cref', 'me', 'mref', 'Tref', 'nref', 'qref', 'Rref', 'Bref']
        # namearg = [item for item in namearg if item not in namearg_that_are_not_qlk_inputs]
        
        namearg = ['Ai',
                'Ane',
                'Ani',
                'Ate',
                'Ati',
                'Aupar',
                'Autor',
                'Bo',
                'ETGmult',
                'Machpar',
                'Machtor',
                'Rmin',
                'Ro',
                'Te',
                'Ti',
                'Zi',
                'absacc1',
                'absacc2',
                'alpha',
                'anise',
                'anisi',
                'coll_flag',
                'collmult',
                'danisdre',
                'danisdri',
                'dimn',
                'dimx',
                'gammaE',
                'integration_routine',
                'kthetarhos',
                'maxpts',
                'maxruns',
                'ne',
                'nions',
                'normni',
                'numsols',
                'phys_meth',
                'q',
                'relacc1',
                'relacc2',
                'rho',
                'rhomax',
                'rhomin',
                'rot_flag',
                'separateflux',
                'simple_mpi_only',
                'smag',
                'timeout',
                'typee',
                'typei',
                'verbose',
                'write_primi',
                'x']
        nargu = len(namearg)
        stringind=[] #no string indexes. Kept if adding any future string inputs

        # Change the current work path to the run folder path 
        os.chdir(os.path.expanduser(run_name_path_abs))

        # Write binaries
        for ii in range(nargu):
            if ii in stringind:
                with open(f'input/p{ii}.txt', 'wb') as fid:
                    fid.write(getattr(self, namearg[ii]))
            else:
                with open(f'input/{namearg[ii]}.bin', 'wb') as fid:
                    if isinstance(getattr(self, namearg[ii]), int):
                        fid.write(struct.pack('d', getattr(self, namearg[ii])))  # 'i' for int
                    elif isinstance(getattr(self, namearg[ii]), float):
                        fid.write(struct.pack('d', getattr(self, namearg[ii])))  # 'd' for float
                    elif isinstance(getattr(self, namearg[ii]), list):
                        for item in getattr(self, namearg[ii]):
                            fid.write(struct.pack('d', item))  # float list by default
                    elif isinstance(getattr(self, namearg[ii]), np.ndarray):
                        for item in getattr(self, namearg[ii]):
                            fid.write(struct.pack('d', item))  # float list by default


        # RUN
        if verbose:
            subprocess.run(f'mpiexec -np {CPU} ./QuaLiKiz', shell=True)
        else:
            subprocess.run(f'mpiexec -np {CPU} ./QuaLiKiz', shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

        # os.system(f'mpiexec -np {CPU} ./QuaLiKiz')

        # Come back to the previous work path
        os.chdir(qlkclass_path)
        return

    def load_runfolder(self, run_name_path_abs):
        """Loads data from QLK output and debug files"""
        def read_dat_file(parent_path, filename):
            """
            Read the .dat file and return either a float, or a float list or a float matrix 
            """


            path = os.path.join(parent_path, filename+'.dat')
            with open(path, 'r') as f:
                lines = f.readlines()
                
               #if there is only one line
                if len(lines) == 1 : 
                    nombres = lines[0].split()
                    if len(nombres) == 1:
                        if filename in ['rmodeshift', 'rmodewidth', 'distan', 'kthetarhos', 'Zi', 'Ai', 'normni', 'Ani', 'Ti', 'Ati', 'Ani', 'typei']:
                            return np.asarray([float(nombres[0].strip())])
                        elif filename in ['gam_SI', 'ome_SI', 'gam_GB', 'ome_GB']:
                            return np.asarray([np.asarray([float(nombres[0].strip())])])
                        elif filename in ['nions', 'dimx', 'dimn', 'numsols']:
                            return int(nombres[0].strip())
                        else:
                            return float(nombres[0].strip())
                    else:
                        return np.asarray([float(nombre.strip()) for nombre in nombres]) #strip() is used to remove extra whitespaces
                
                
                # if several lines
                else:
                    return np.transpose(np.asarray([[float(val.strip()) for val in line.split()] for line in lines]))

        #--------------------------------------------------------------------#
        # First debug outputs to have the correct inputs used during the run #
        #--------------------------------------------------------------------#
        debug_path = os.path.expanduser(run_name_path_abs + '/debug/')
        for file in os.listdir(debug_path):
            if file.endswith(".dat"):
                filename = os.path.splitext(file)[0]  # Remove the .dat extension of the file name
                setattr(self, filename, read_dat_file(debug_path, filename))

        #------------------#
        # Then the outputs #
        #------------------#
        output_path = os.path.expanduser(run_name_path_abs + '/output/')
        for file in os.listdir(output_path):
            if file.endswith(".dat"):
                filename = os.path.splitext(file)[0]  # Remove the .dat extension of the file name
                setattr(self, filename, read_dat_file(output_path, filename))
        
        #----------------------------------------#
        # Then the primitive in the ouput folder #
        #----------------------------------------#
        output_primitive_path = os.path.expanduser(run_name_path_abs + '/output/primitive/')
        for file in os.listdir(output_primitive_path):
            if file.endswith(".dat"):
                filename = os.path.splitext(file)[0]  # Remove the .dat extension of the file name
                setattr(self, filename, read_dat_file(output_primitive_path, filename))

        self.Rref = Rref_qlk(self.Ro)
        self.Bref = Bref_qlk(self.Bo)
        self.Ai0 = self.Ai[0]
        self.mi0 = mi_qlk(self.Ai0, self.mref)
        self.vth_i0 = vth_qlk(self.Te, self.Tref, self.qref, self.mi0)
        self.rho_i0 = rho_i_qlk(self.mi0, self.vth_i0, self.qref, self.Bref)


        self.source = 'qlkrunfolder'

        self.nnions = self.nions
        self.ndimn = self.dimn

        self.modewidth = np.complex128(self.rmodewidth + 1j * self.imodewidth)
        self.modeshift = np.complex128(self.rmodeshift + 1j * self.imodeshift)


    def update(self, **kwargs):
        """Used to update the existing class from a dict. Useful for cases where not all the values of the class change for faster processing but ultimately not very useful unless dealing with large datasets"""
        for key, value in kwargs.items():
            setattr(self, key, value)


    def list_properties(self,mode='short'):
        """lists properties of the class for testing purposes"""
        proplist = []
        if mode == 'short':
            for i in inspect.getmembers(self):

                # to remove private and protected
                # functions
                if not i[0].startswith('_'):

                    # To remove other methods that
                    # doesnot start with a underscore
                    if not inspect.ismethod(i[1]):
                        print(f'{i[0], type(i[1])}: {i[1]}')
                        proplist.append(i[0])
        elif mode in ['-A','-a']:
            for i in inspect.getmembers(self):
                print(f'{i[0], type(i[1])}: {i[1]}')
                proplist.append(i[0])
        return proplist


    def compare(self,c2,mode='default'):
        """used to compare this current QLK class to a different QLK class (c2) for testing purposes"""

        if mode == 'IMAS_check':
            IMAS_fields = ['x','Rmin','Aupar','Machpar', 'danisdr','source']
            fields1 = [field[0] for field in inspect.getmembers(self) if ((not field[0].startswith('_') and not inspect.ismethod(field[1])) and (field[0] not in IMAS_fields))]
            fields2 = [field[0] for field in inspect.getmembers(c2) if ((not field[0].startswith('_') and not inspect.ismethod(field[1])) and (field[0] not in IMAS_fields))]
        else:
            fields1 = [field[0] for field in inspect.getmembers(self) if (not field[0].startswith('_') and not inspect.ismethod(field[1]))]
            fields2 = [field[0] for field in inspect.getmembers(c2) if (not field[0].startswith('_') and not inspect.ismethod(field[1]))]
        print(fields1)
        print(fields2)
        missing_fields = [field for field in fields2 if field not in fields1]
        extra_fields = [field for field in fields1 if field not in fields2]
        same_fields = [field for field in fields1 if field in fields2]
        different_entries = []
        for field_name in same_fields:
                    v1 = getattr(self, field_name)
                    v2 = getattr(c2, field_name)
                    try:
                        if not np.array_equal(v1, v2):
                            print(f'{field_name} 1: {np.shape(v1)} {type(v1)}\n{v1}')
                            print(f'{field_name} 2: {np.shape(v2)} {type(v2)}\n{v2}')
                            different_entries.append(field_name)
                    except:
                        if not np.all(v1 == v2):
                            print(f'{field_name} 1: {np.shape(v1)} {type(v1)}\n{v1}')
                            print(f'{field_name} 2: {np.shape(v2)} {type(v2)}\n{v2}')
                            different_entries.append(field_name)
        print(f'different_entries: {different_entries}')
        print(f'missing_fields: {missing_fields}')
        print(f'extra_fields: {extra_fields}')
        return different_entries, missing_fields, extra_fields

    #### FASTER PROJECT SPECIFIC ####
    def to_IMAS_pandas(self, df_to_append=None, column_names_list=None, FASTER_dataset='Edge10D', output='split_df'):
        """
        converts the existing QLK file to IMAS and outputs lines of a pandas dataframe specifically for machine learning
        purposes in the FASTER project. If given an existing dataframe, will add the line to the existing dataframe.
        Note that this output may not be suitable for use outside of this intended function.

        :param df_to_append: dataframe to which lines are appended, optional.
        :param column_names_list: if no dataframe provided, will use these as the variable names. Otherwise will default to the FASTER_dataset default value.
        :param FASTER_dataset: string indicating which dataset the data comes from (treats different with different number of columns in the output dataframe.
        Current/planned options: "Edge10D, JET_QLK, JET_GKW"
        :param output: ('dict'/'df'/'split_df') whether to output as a dataset or a dict (recommend dict for speed which can later be turned into a df)
        """
        # initialise constants
        IMAS = constantsIMAS(self.Ro, self.Bo, self.Te, self.ne, self.Tref, self.nref)
        RATIOS = constantsRATIO(self, IMAS)

        # calculate non loop quantities
        IMAS_quantities = {
        'density_log_gradient_e' : density_log_gradient(self.Ane, RATIOS.Rrat),
        'temperature_log_gradient_e' : temperature_log_gradient(self.Ate, RATIOS.Rrat),
        'q': q_IMAS(self.q),
        'magnetic_shear_r_minor': magnetic_shear_r_minor(self.smag),
        'binormal_component_norm': binormal_component_norm(RATIOS.rho_rat2, self.kthetarhos)
        }

        # loop over number of modes for growth rate and freqs
        try:
            full_growth_rate_norm = growth_rate_norm_SI(self.gam_SI, IMAS.Rref, IMAS.vth_ref)
        except:
            full_growth_rate_norm = growth_rate_norm_GB(self.gam_GB, self.Rmin, IMAS.Rref, RATIOS.vth_rat)

        try:
            full_frequency_norm = frequency_norm_SI(self.ome_SI, IMAS.Rref, IMAS.vth_ref)
        except:
            full_frequency_norm = frequency_norm_GB(self.ome_GB, self.Rmin, IMAS.Rref, RATIOS.vth_rat)

        for numsol in range(self.numsols):
            IMAS_quantities[f'growth_rate_norm_{numsol}'] = full_growth_rate_norm[:,numsol]
            IMAS_quantities[f'frequency_norm_{numsol}'] = full_frequency_norm[:,numsol]

        # loop over ions for ion quantities
        imas_density_mult = ion_type(self.typei)
        density_norm_ion = density_norm_i(self.normni) * imas_density_mult

        IMAS_quantities['collisionality_norm'] = np.sum(collisionality_norm(IMAS.Rref, IMAS.vth_ref, self.ne, density_norm_ion, self.Zi, RATIOS.qrat, self.Te,IMAS.vth_e))
        temperature_log_gradient_i = temperature_log_gradient(self.Ati, RATIOS.Rrat)


        # assign instability based on value of growth rate 0
        IMAS_quantities['instability'] = np.array([1 if gam0 > 0 else 0 for gam0 in IMAS_quantities['growth_rate_norm_0']])

        # output method defined by input dataset
        output_dicts = []

        # EDGE10D conversion specific
        if FASTER_dataset in ['Edge10D','Edge10D2']:

            particles_phi_potential_i = normalise_integrated_particle_flux(self.pfi_SI, self.ni, IMAS.vth_ref,
                                                                           IMAS.rho_star)
            energy_phi_potential_i = normalise_integrated_energy_flux(self.efi_SI, self.ni, IMAS.vth_ref, IMAS.rho_star,
                                                                      IMAS.Tref)
            momentum_tor_perpendicular_phi_potential_i = normalise_integrated_momentum_flux(self.vfi_SI, self.ni,
                                                                                            IMAS.vth_ref, IMAS.rho_star,
                                                                                            IMAS.mref, IMAS.Tref)
            IMAS_quantities['particles_phi_potential_e'] = normalise_integrated_particle_flux(self.pfe_SI, self.ne, IMAS.vth_ref, IMAS.rho_star),
            IMAS_quantities['energy_phi_potential_e'] = normalise_integrated_energy_flux(self.efe_SI, self.ne, IMAS.vth_ref, IMAS.rho_star, IMAS.Tref)
            for nion in range(self.nnions):
                IMAS_quantities[f'density_norm_i_{nion}'] = density_norm_ion[nion]
                IMAS_quantities[f'temperature_log_gradient_i_{nion}'] = temperature_log_gradient_i[nion]
                IMAS_quantities[f'particles_phi_potential_i_{nion}'] = particles_phi_potential_i[nion]
                IMAS_quantities[f'energy_phi_potential_i_{nion}'] = energy_phi_potential_i[nion]
                IMAS_quantities[f'momentum_tor_perpendicular_phi_potential_i_{nion}'] = momentum_tor_perpendicular_phi_potential_i[nion]

            # set column names for dataset
            if df_to_append is not None:
                column_names_list = df_to_append.columns.values
            elif column_names_list is not None:
                pass
            else:
                column_names_list = ['density_norm_i_0', 'density_log_gradient_e',
                                    'temperature_log_gradient_e', 'temperature_log_gradient_i_0', 'q', 'magnetic_shear_r_minor',
                                    'collisionality_norm', 'binormal_component_norm', 'growth_rate_norm_0',
                                    'frequency_norm_0', 'growth_rate_norm_1', 'frequency_norm_1',
                                    'particles_phi_potential_e', 'energy_phi_potential_e', 'particles_phi_potential_i_0',
                                    'energy_phi_potential_i_0', 'momentum_tor_perpendicular_phi_potential_i_0',
                                    'particles_phi_potential_i_1', 'energy_phi_potential_i_1',
                                    'momentum_tor_perpendicular_phi_potential_i_1', 'imodewidth', 'rmodewidth', 'distan',
                                    'instability']

            # split the column names into wavevector columns and shared columns
            wv_cols = [name for name in column_names_list if np.shape(IMAS_quantities[name]) == (self.ndimn,)]
            non_wv_cols = [name for name in column_names_list if name not in wv_cols]

            if FASTER_dataset == 'Edge10D':
                output_dicts = {name: [] for name in column_names_list}
                # fill non-wv fields
                for name in non_wv_cols:
                    output_dicts[name] = [IMAS_quantities[name] for x in range(self.ndimn)]
                # iterate over wavevectors to append into dict lists
                for nk in range(self.ndimn):
                    for name in wv_cols:
                        output_dicts[name].append(IMAS_quantities[name][nk])

            elif FASTER_dataset == 'Edge10D2':
                # prepare the shared dict
                dict_shared = {name:IMAS_quantities[name] for name in non_wv_cols}
                # iterate over wavevectors to create 1 dict per wavevector
                for nk in range(self.ndimn):
                    wv_dict = dict_shared
                    for name in wv_cols:
                        wv_dict[name] = IMAS_quantities[name][nk]
                    output_dicts.append(wv_dict)



        elif FASTER_dataset == 'JET_QLK':
            # set column names for dataset
            if df_to_append is not None:
                column_names_list = df_to_append.columns.values
            elif column_names_list is not None:
                pass
            else:
                column_names_list = ['density_log_gradient_e',
                                     'temperature_log_gradient_e',
                                     'velocity_tor_norm',
                                     'velocity_tor_gradient_norm',
                                     'shearing_rate_norm',
                                     'r_minor_norm',
                                     'q',
                                     'magnetic_shear_r_minor',
                                     'charge_norm_i_1', # charge0 =  1, charge2 = 28
                                     'density_norm_i_0',
                                     'density_norm_i_1',
                                     'density_norm_i_2',
                                     'mass_norm_i_0',
                                     'mass_norm_i_1', #mass2 = 58
                                     'collisionality_norm',
                                     'temperature_norm', #ion temps all equal
                                     'temperature_log_gradient_i_0',
                                     'temperature_log_gradient_i_12', # same for species 1 and 2
                                     'density_log_gradient_i_0',
                                     'density_log_gradient_i_12', # same for species 1 and 2
                                     'pressure_gradient_norm',
                                     'binormal_component_norm',
                                     'growth_rate_norm_0',
                                     'frequency_norm_0',
                                     'growth_rate_norm_1',
                                     'frequency_norm_1',
                                     'instability']
            charge_norm_ion = charge_norm(self.Zi, RATIOS.qrat)
            mass_norm_ion = mass_norm(self.Ai, RATIOS.mrat)
            density_log_gradient_ion = density_log_gradient(self.Ani, RATIOS.Rrat)
            IMAS_quantities[f'temperature_norm'] = temperature_norm(self.Ti[0], self.Te)
            IMAS_quantities[f'density_log_gradient_i_0'] = density_log_gradient_ion[0]
            IMAS_quantities[f'density_log_gradient_i_12'] = density_log_gradient_ion[1]
            IMAS_quantities[f'temperature_log_gradient_i_0'] = temperature_log_gradient_i[0]
            IMAS_quantities[f'temperature_log_gradient_i_12'] = temperature_log_gradient_i[1]
            IMAS_quantities[f'pressure_gradient_norm'] = pressure_gradient_norm(self.alpha, self.q, RATIOS.Rrat, RATIOS.Brat)
            for nion in range(self.nnions):
                IMAS_quantities[f'density_norm_i_{nion}'] = density_norm_ion[nion]
                IMAS_quantities[f'density_log_gradient_i_{nion}'] = density_log_gradient_ion[nion]
                IMAS_quantities[f'charge_norm_i_{nion}'] = charge_norm_ion[nion]
                IMAS_quantities[f'mass_norm_i_{nion}'] = mass_norm_ion[nion]

            IMAS_quantities['velocity_tor_norm'] = velocity_tor_norm(self.Machtor, self.cref, IMAS.vth_ref, RATIOS.Rrat)
            IMAS_quantities['velocity_tor_gradient_norm'] = velocity_tor_gradient_norm(self.Autor, self.cref, IMAS.vth_ref, RATIOS.Rrat)
            IMAS_quantities['shearing_rate_norm'] = shearing_rate_norm(self.gammaE, self.cref, IMAS.vth_ref, RATIOS.Rrat)
            IMAS_quantities['r_minor_norm'] = r_minor_norm(self.Rmin, self.x, self.Ro)

            # check
            # print('Missing::', [name for name in column_names_list if name not in IMAS_quantities.keys()])
            # print('Unused::', [name for name in IMAS_quantities.keys() if name not in column_names_list])
            # split the column names into wavevector columns and shared columns
            wv_cols = [name for name in column_names_list if np.shape(IMAS_quantities[name]) == (self.ndimn,)]
            non_wv_cols = [name for name in column_names_list if name not in wv_cols]

            if output == 'split_df':

                input_dicts = {}
                output_dicts = {name: [] for name in wv_cols}
                # fill non-wv fields
                for name in non_wv_cols:
                    input_dicts[name] = [IMAS_quantities[name]]
                # iterate over wavevectors to append into dict lists
                for nk in range(self.ndimn):
                    for name in wv_cols:
                        output_dicts[name].append(IMAS_quantities[name][nk])

                df_out = pd.DataFrame(output_dicts)
                df_inp = pd.DataFrame(input_dicts)
                return df_inp, df_out
            else:
                output_dicts = {name: [] for name in column_names_list}
                # fill non-wv fields
                for name in non_wv_cols:
                    output_dicts[name] = [IMAS_quantities[name] for x in range(self.ndimn)]
                # iterate over wavevectors to append into dict lists
                for nk in range(self.ndimn):
                    for name in wv_cols:
                        output_dicts[name].append(IMAS_quantities[name][nk])
        elif FASTER_dataset == 'JET GKW':
            pass
        else:
            pass

        # final output

        if output =='dict':
            return output_dicts
        elif output == 'df':
            df_out = pd.DataFrame(output_dicts)
            return df_out
        elif output == "both":
            df_out = pd.DataFrame(output_dicts)
            return df_out, output_dicts


    def load_xr(self, xr_df):
        '''
        loads a QLK class from an entry in an xarray dataset
        :param xr_df: xarray dataframe containing a single QLK run (i.e. dimx = 1).
        '''
        # load all the required variables as float 64 to circumvent the issues with loading as float64 initially and
        # avoid rounding errors especially for back conversion
        if xr_df.dimx.shape != ():
            print(
                'dimx has more than 1 value. The class only supports a single value. Select an individual dimx by using the following: xr_df.isel(dimx=i) for an index i')
            return
        self.source = 'xarray'
        self.Rmin = xr_df.Rmin.values.astype('float64')
        self.x = xr_df.x.values.astype('float64')
        self.Ro = xr_df.Ro.values.astype('float64')
        self.Bo = xr_df.Bo.values.astype('float64')

        self.ndimn = len(xr_df.dimn.values)
        self.numsols = len(xr_df.numsols.values)
        self.nnions = len(xr_df.nions.values)

        self.q = xr_df.q.values.astype('float64')
        self.smag = xr_df.smag.values.astype('float64')
        self.alpha = xr_df.alpha.values.astype('float64')
        self.Machtor = xr_df.Machtor.values.astype('float64')
        self.Machpar = xr_df.Machpar.values.astype('float64')
        self.Autor = xr_df.Autor.values.astype('float64')
        self.Aupar = xr_df.Aupar.values.astype('float64')
        self.gammaE = xr_df.gammaE.values.astype('float64')
        self.normni = xr_df.normni.values.astype('float64')
        self.Ze = 1
        self.Zi = xr_df.Zi.values.astype('float64')
        self.ne = np.float64(xr_df.ne.values.astype('float64'))
        self.ni = self.normni * self.ne
        self.Te = xr_df.Te.values.astype('float64')
        self.Ti = xr_df.Ti.values.astype('float64')
        self.Ane = xr_df.Ane.values.astype('float64')
        self.Ani = xr_df.Ani.values.astype('float64')
        self.Ate = xr_df.Ate.values.astype('float64')
        self.Ati = xr_df.Ati.values.astype('float64')
        self.Ae = self.me
        self.Ai = xr_df.Ai.values.astype('float64')
        try:
            self.pfe_SI = xr_df.pfe_SI.values.astype('float64')
        except:
            self.pfe_SI = np.zeros(1)
        try:
            self.pfi_SI = xr_df.pfi_SI.values.astype('float64')
        except:
            self.pfi_SI = np.zeros(self.nnions)
        try:
            self.efe_SI = xr_df.efe_SI.values.astype('float64')
        except:
            self.efe_SI = np.zeros(1)
        try:
            self.efi_SI = xr_df.efi_SI.values.astype('float64')
        except:
            self.efi_SI = np.zeros(self.nnions)
        try:
            self.vfi_SI = xr_df.vfi_SI.values.astype('float64')
        except:
            self.vfi_SI = np.zeros(self.nnions)
        try:
            self.vfe_SI = xr_df.vfe_SI.values.astype('float64')
        except:
            self.vfe_SI = np.zeros(1)

        self.kthetarhos = xr_df.kthetarhos.values.astype('float64')
        try:
            self.gam_SI = xr_df.gam_SI.values.astype('float64')
        except:
            try:
                self.gam_GB = xr_df.gam_GB.values.astype('float64')
            except:
                self.gam_SI = np.zeros(self.ndimn)
        try:
            self.ome_SI = xr_df.ome_SI.values.astype('float64')
        except:
            try:
                self.ome_GB = xr_df.gam_GB.values.astype('float64')
            except:
                self.ome_SI = np.zeros(self.ndimn)

        self.rmodewidth = xr_df.rmodewidth.values.astype('float64')
        self.modewidth = np.complex128(
            xr_df.rmodewidth.values.astype('float64') + 1j * xr_df.imodewidth.values.astype('float64'))
        self.modeshift = np.complex128(
            xr_df.rmodeshift.values.astype('float64') + 1j * xr_df.imodeshift.values.astype('float64'))
        self.distan = xr_df.distan.values.astype('float64')
        self.code_version = xr_df.QLK_CLOSEST_RELEASE
        self.commit = xr_df.QLK_GITSHAKEY
        try:
            self.typee = xr_df.typee.values
        except:
            self.typee = 2

        try:
            self.typei = xr_df.typei.values
        except:
            self.typei = np.ones(self.nnions)

        try:
            self.rot_flag = xr_df.rot_flag.values
        except:
            pass
        self.Rref = self.Ro
        self.Bref = self.Bo
        self.Ai0 = np.float64(xr_df.isel(nions=0).Ai.values)
        self.mi0 = self.Ai0 * self.mref
        self.vth_i0 = np.sqrt(np.float64(self.Te) * self.Tref * self.qref / self.mi0)
        self.rho_i0 = self.mi0 * self.vth_i0 / (self.qref * self.Bref)

        try:
            self.anis = xr_df.anis.values.astype('float64')
            self.danisdr = xr_df.danisdr.values.astype('float64')
        except:
            self.anis = 0
            self.danisdr = 0


    def load_JET_QLK_line(self, inputs, outputs, wavevectors):
        """Loads data from a line of the QLK jet dataset"""
        self.source = 'JET Pandas'
        #unused inputs: ['Zeff', 'R0', 'logNustar']
        self.Rmin = inputs['Rmin']
        self.x = inputs['x']
        self.Ro = inputs['Ro']
        self.Bo = inputs['Bo']
        self.nnions = 3

        self.q = inputs['q']
        self.smag = inputs['smag']
        self.alpha = inputs['alpha']
        self.Machtor = inputs['Machtor']
        self.Machpar = inputs['Machpar']
        self.Autor = inputs['Autor']
        self.Aupar = inputs['Aupar']
        self.gammaE = inputs['gammaE']
        self.normni = np.array([inputs[f'normni{n}'] for n in range(self.nnions)])
        self.Zi = np.array([inputs[f'Zi{n}'] for n in range(self.nnions)])
        self.ne = inputs['ne']
        self.ni = np.array([normni * self.ne for normni in self.normni])
        self.Te = 1
        self.Ti = np.array([inputs[f'Ti_Te{n}'] for n in range(self.nnions)])
        self.Ane = inputs['Ane']
        self.Ani = np.array([inputs[f'Ani{n}'] for n in range(self.nnions)])
        self.Ate = inputs['Ate']
        self.Ati = np.array([inputs[f'Ati{n}'] for n in range(self.nnions)])
        self.Ai = np.array([inputs[f'Ai{n}'] for n in range(self.nnions)])
        self.rho = inputs['rho']
        self.typei = np.array([1 for n in range(self.nnions)])

        self.kthetarhos = np.array(wavevectors['kthetarhos'])
        self.ndimn = len(self.kthetarhos)


        self.Rref = self.Ro
        self.Bref = self.Bo
        self.Ai0 = inputs['Ai0']
        self.mi0 = self.Ai0 * self.mref
        self.vth_i0 = np.sqrt(np.float64(self.Te) * self.Tref * self.qref / self.mi0)
        self.rho_i0 = self.mi0 * self.vth_i0 / (self.qref * self.Bref)


        self.numsols = 2

        self.gam_GB = np.array([[outputs[f'gam_GB_k{k}_s{n}'] for n in range(self.numsols)] for k in range(self.ndimn)])
        self.ome_GB = np.array([[outputs[f'ome_GB_k{k}_s{n}'] for n in range(self.numsols)] for k in range(self.ndimn)])


    #### DEPRECATED PYTHONTOOLS FUNCTIONS ####
    def run_QLK_pythontools(self, name, runsdir, qlk_root_path, template_path='./default_parameters.json'):
        '''
        Deprecated.
        runs QLK in the specified directory using the current class as input. Creates .nc file with collated results.
        :param runs_relpath:
        :param run_name:
        :param qlk_relpath:
        :param overwrite:
        :return:
        '''
        cwd = os.getcwd()
        qlk_plan = self.to_QLK_plan(template_path=template_path)

        # TODO: fix so that path can have spacebar
        qlk_binpath = os.path.join(qlk_root_path, "QuaLiKiz")
        run_dir = str(os.path.join(runsdir,name))
        binreldir = Path(relpath(qlk_binpath, start=run_dir))


        run = Run(runsdir, name, binreldir, qualikiz_plan=qlk_plan)
        runlist = [run]

        batch = Batch(runsdir, name, runlist)

        batch.prepare()
        os.chdir(run_dir)
        batch.generate_input()

        batch.launch()

        batch.to_netcdf()

        # run_to_netcdf(run_dir)

        os.chdir(cwd)


    def to_QLK_plan(self, template_path='./default_parameters.json', **kwargs):
        '''deprecated'''
        # load up the default/given template:
        plan = QuaLiKizPlan.from_json(template_path)

        # fill in values from QLK Class
        plan['xpoint_base']['special']['kthetarhos'] = self.kthetarhos

        plan['xpoint_base']['elec'] = Electron(T=self.Te, n=self.ne, At=self.Ate, An=self.Ane, type=self.typee,
                                               anis=self.anis, danisdr=self.danisdr)
        # plan['xpoint_base']['elec'] = Particle(T=self.Te, n=self.ne, At=self.Ate, An=self.Ane, type=self.typee, anis=self.anis, danisdr=self.danisdr, A=self.Ae, Z=self.Ze)

        plan['xpoint_base']['ions'] = [
            Ion(T=self.Ti[nion], n=self.ni[nion], At=self.Ati[nion], An=self.Ani[nion], type=self.typei[nion],
                anis=self.anis, danisdr=self.danisdr, A=self.Ai[nion], Z=self.Zi[nion]) for nion in range(self.nnions)]
        try:
            plan['xpoint_base']['meta']['rot_flag'] = self.rot_flag
        except:
            pass
        plan['xpoint_base']['meta']['numsols'] = self.numsols

        plan['xpoint_base']['geometry']['x'] = self.x
        plan['xpoint_base']['geometry']['Ro'] = self.Ro
        plan['xpoint_base']['geometry']['Rmin'] = self.Rmin
        plan['xpoint_base']['geometry']['Bo'] = self.Bo
        plan['xpoint_base']['geometry']['q'] = self.q
        plan['xpoint_base']['geometry']['smag'] = self.smag
        plan['xpoint_base']['geometry']['alpha'] = self.alpha
        plan['xpoint_base']['geometry']['Machtor'] = self.Machtor
        plan['xpoint_base']['geometry']['Autor'] = self.Autor
        plan['xpoint_base']['geometry']['Machpar'] = self.Machpar
        plan['xpoint_base']['geometry']['Aupar'] = self.Aupar
        plan['xpoint_base']['geometry']['gammaE'] = self.gammaE

        # iterate through kwargs if any and adjust options
        meta = ["phys_meth",
                "coll_flag",
                "rot_flag",
                "verbose",
                "separateflux",
                "write_primi",
                "numsols",
                "relacc1",
                "relacc2",
                "absacc1",
                "absacc2",
                "maxruns",
                "maxpts",
                "ETGmult",
                "collmult",
                "rhomin",
                "rhomax",
                "simple_mpi_only",
                "integration_routine"]

        options = ["set_qn_normni",
                   "set_qn_normni_ion",
                   "set_qn_An",
                   "set_qn_An_ion",
                   "check_qn",
                   "x_eq_rho",
                   "recalc_Nustar",
                   "recalc_Ti_Te_rel",
                   "assume_tor_rot",
                   "puretor_abs_var",
                   "puretor_grad_var",
                   "recalc_rot_var",
                   "recalc_Ati"]

        if kwargs:
            print('Updating plan values with kwargs')
            for field, kwarg in kwargs.items():
                if field in meta:
                    plan['xpoint_base']['meta'][field] = kwarg
                elif field in options:
                    plan['xpoint_base']['options'][field] = kwarg
                else:
                    print(f'{field} not recognised')

        return plan


    def load_QLK_plan(self, json_path=None, QLKplan=None):
        """Loads data from a QLK pythontools plan"""
        print('Nice try but .load_QLKplan is not coded yet :)')
        pass




