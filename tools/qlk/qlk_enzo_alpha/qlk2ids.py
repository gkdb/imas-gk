import scipy.constants
import numpy as np
import inspect
from matplotlib import pyplot as plt
import idspy_toolkit as idspy

np.seterr(all="raise", )


class constantsIMAS:
    """
    Initialise an IMAS constants class from scipy.constants for use in the conversion functions

    :param ds: optional parameter required for conversion routine. If not present only the default values from scipy will be returned.
    """
    qref = np.float64(scipy.constants.physical_constants['electron volt'][0])  # C
    mref = np.float64(scipy.constants.physical_constants['deuteron mass'][0])  # kg
    me = np.float64(scipy.constants.physical_constants['electron mass'][0])  # kg

    def __init__(self, Ro, Bo, Te, ne, QLK_Tref, QLK_nref):
        # return eV value (given in keV)
        self.Tref = Te * QLK_Tref
        # return m^-3 value (given in 1e19 m^-3)
        self.nref = np.float64(ne) * QLK_nref
        self.Rref = Ro
        self.Bref = Bo
        qTe = self.Tref * self.qref
        self.vth_ref = np.sqrt(np.float64(2) * qTe / self.mref)
        self.rho_ref = self.mref * self.vth_ref / (self.qref * self.Bref)
        self.vth_e = np.sqrt(np.float64(2) * qTe / self.me)
        self.rho_star = self.rho_ref / self.Rref
        self.recip_rho_star = self.Rref / self.rho_ref

    def print_constants(self):

        print('\nIMAS Constants:')
        # members of an object
        for i in inspect.getmembers(self):

            # to remove private and protected
            # functions
            if not i[0].startswith('_'):

                # To remove other methods that
                # doesnot start with a underscore
                if not inspect.ismethod(i[1]):
                    var = i[1]
                    if type(var) == float:
                        print(f'{i[0]}: {var}, {type(var)}')
                    elif var.shape == ():
                        print(f'{i[0]}: {var}, {type(var)}')
                    else:
                        print(f'{i[0]} shape: {var.shape}')


class constantsRATIO():
    """
    Initialise a ratio constants class with QLK:IMAS ratios for use in the conversion functions
    :param QLK: a QLK class
    :param IMAS: an IMAS constants class
    """
    Trat = np.float64(1)

    def __init__(self, QLK, IMAS, baseline=1):
        # baseline !=1 used in cases where the main ion has not yet been identified and the ratios wanted are only using
        # reference quantities and this will be reloaded later
        # self.qrat = QLK.qref / IMAS.qref
        self.qrat = 1
        self.mrat = QLK.mref / IMAS.mref
        self.nrat = QLK.nref / IMAS.nref
        self.Rrat = QLK.Rref / IMAS.Rref
        self.Brat = QLK.Bref / IMAS.Bref
        if baseline == 1:
            self.vth_rat = QLK.vth_i0 / IMAS.vth_ref
            self.rho_rat = QLK.rho_i0 / IMAS.rho_ref
            # rho_rat 2 seems more consistent in outputting a single values, avoiding errors due to cancelling
            self.rho_rat2 = np.sqrt(QLK.mi0 / (2 * IMAS.mref * self.qrat * self.Brat))

    def print_constants(self):

        print('\nConstant Ratios:')
        # members of an object
        for i in inspect.getmembers(self):

            # to remove private and protected
            # functions
            if not i[0].startswith('_'):

                # To remove other methods that
                # doesnot start with a underscore
                if not inspect.ismethod(i[1]):
                    var = i[1]
                    if type(var) == float:
                        print(f'{i[0]}: {var}, {type(var)}')
                    elif var.shape == ():
                        print(f'{i[0]}: {var}, {type(var)}')
                    else:
                        print(f'{i[0]} shape: {var.shape}')


################################
##### QLK to IDS Functions #####
################################
def r_minor_norm(Rmin, x, Ro):
    """
    Calculates the IMAS normalised minor radius r_minor_norm given the following qualikiz inputs:
    :param Rmin: array or float of  QLK minor radius of LCS (ds.Rmin.values)
    :param x: array or float of  QLK normalised minor radius of flux surface (ds.x.values)
    :param Ro: array or float of QLK major radius (ds.Ro.values)

    :returns: array or float of IMAS minor radius normalised to major radius
    """
    return Rmin * x / Ro


def b_field_tor_sign():
    """
    Calculates the sign of the IMAS toroidal field direction:
    Always returns 1 for QLK
    """
    return 1


def ip_sign():
    """
    Calculates the sign of the IMAS toroidal current direction:
    Always returns 1 for QLK
    """
    return 1


def q_IMAS(q):
    """
    Calculates the IMAS safety factor:
    :param q: float or array of the qlk safety factor (ds.q.values)
    :return: float or array of the IMAS normalised safety factor
    """
    return np.float64(q)


def magnetic_shear_r_minor(smag):
    """
    Calculates the IMAS magnetic shear given the following qualikiz inputs:
    :param smag: array or float of QLK magnetic shear (ds.smag.values)

    :return: IMAS normalised magnetic shear
    """
    return np.float64(smag)


def pressure_gradient_norm(alpha, q, Rrat, Brat):
    """
    Calculates the IMAS normalised pressure gradient (pressure_gradient_norm) given the following qualikiz inputs:
    :param alpha: array or float of QLK MHD alpha (ds.alpha.values)
    :param q: array or float of  QLK safety factor (ds.q.values)
    :param Rrat: float of QLK:IMAS ratio of reference lengths
    :param Rrat: float of QLK:IMAS ratio of reference magnetic fields

    :returns: array or float of IMAS normalised pressure gradient
    """

    pres_grad_norm = alpha * Brat ** 2 / q ** 2 / Rrat
    return pres_grad_norm


def dgeometric_axis_r_dr_minor(alpha):
    """
    Calculates the IMAS derivative of R_0 wrt. r at r=r_0 given the following qualikiz inputs:
    :param alpha: array or float of QLK MHD alpha (ds.alpha.values)

    :return: array or float of IMAS derivative of R_0 wrt. r
    """
    return np.float64(-alpha)


def dgeometric_axis_z_dr_minor():
    """
    Returns the IMAS derivative of z wrt. r at r=r_0 which is always 0 for QLK

    :return: always 0 for QLK
    """
    return np.float64(0)


def elongation():
    """
    Returns the IMAS elongation which is always 1 for QLK

    :return: always 1 for QLK
    """
    return np.float64(1)


def delongation_dr_minor_norm():
    """
    Calculates the IMAS derivative of elongation wrt. r at r=r_0 which is always 0 for QLK

    :return: always 0 for QLK
    """
    return np.float64(0)


def shape_coefficients_c():
    """
    Calculates the IMAS MXH cosine shape coefficients which are always 0 for QLK

    :return: always 0 for QLK
    """
    return np.array([0], dtype=np.float64)


def shape_coefficients_s():
    """
    Calculates the IMAS MXH sine shape coefficients which are always 0 for QLK

    :return: always 0 for QLK
    """
    return np.array([0], dtype=np.float64)


def dc_dr_minor_norm():
    """
    Calculates the IMAS derivative of IMAS MXH cosine shape coefficients wrt. r at r=r_0 which is always 0 for QLK

    :return: always 0 for QLK
    """
    return np.array([0], dtype=np.float64)


def ds_dr_minor_norm():
    """
    Calculates the IMAS derivative of IMAS MXH sine shape coefficients wrt. r at r=r_0 which is always 0 for QLK

    :return: always 0 for QLK
    """
    return np.array([0], dtype=np.float64)


def charge_norm(Zi_QLK, q_rat):
    """
    Calculates the IMAS normalised charge of an ion species given the following inputs:
    Zi_QLK: array of the ion species charge number from QLK
    q_rat: float of the ratio of QLK:IMAS reference charge (ratioclass.qrat)
    """
    return Zi_QLK * q_rat


def mass_norm(Ai_QLK, m_rat):
    """
    Calculates the IMAS normalised charge of a species given the following inputs:
    Ai_QLK: ds.Ai, the species mass number from QLK
    m_rat: the ratio of QLK:IMAS reference masses (ratioclass.mrat)
    """
    return Ai_QLK * m_rat


def density_norm_e(ne_QLK):
    """
    Calculates the normalised IMAS electron density. Should always be 1
    :param ne_QLK: float or array of the electron density from QLK (ds.ne.values)
    :return: same dimension array as ne, filled with 1s
    """
    if np.isscalar(ne_QLK):
        return np.float64(1)
    else:
        return np.ones_like(ne_QLK, np.float64)


def density_norm_i(normni_QLK):
    """
    Calculates the normalised IMAS ion density. Should always be the values from ds.normni
    but needs to be split into one value for each ion species
    :param normni_QLK: array or float of QLK normalised ion densities ds.normni.values
    :return: input
    """
    return normni_QLK


def density_log_gradient(Ans_QLK, R_rat):
    """
    Calculates the IMAS normalised charge of a species given the following inputs:
    :param Ans_QLK: float or array of the species normalised logarithmic temperature gradient from QLK (ds.Ani.values / ds.Ane.values)
    :param L_rat: float of the ratio of QLK:IMAS reference masses (RATIOS.Rrat)

    :return: IMAS normalised logarithmic temperature gradient
    """
    Ans = Ans_QLK / R_rat
    return Ans


def temperature_norm(Ts_QLK, Te_QLK):
    """
    Calculates the IMAS normalised temperature of a species given the following inputs:
    Ts_QLK: float or array of (ds.Te.values or ds.Ti.values) the species temperature from QLK
    Te_QLK: float of (ds.Te.values) the electron temperature from QLK
    :return: normalised temperature, Ts / Te
    """
    T_norm = Ts_QLK / Te_QLK
    return T_norm


def temperature_log_gradient(Ats_QLK, R_rat):
    """
    Calculates the IMAS normalised temperature gradient of a species given the following inputs:
    Ans_QLK: ds.Ati/ds.Ate the species normalised temperature gradient from QLK
    R_rat: the ratio of QLK:IMAS reference lengths (ratioclass.Lrat)

    strictly speaking, should be Ans_QLK/L_rat but since L_rat is 1 (for now) it just saves processing time
    """
    return Ats_QLK / R_rat


def velocity_tor_norm(Machtor, cref_QLK, vth_ref_IMAS, Rrat):
    """
    Calculates the IMAS normalised toroidal veloctiy given the following qualikiz inputs:

    :param Machtor: array or float of toroidal mach number from QLK files (ds.Machtor.values)
    :param cref_QLK: float of normalising velocity for QLK normalisations (QLK.cref)
    :param vth_ref_IMAS: float of IMAS reference thermal velocity (IMAS.vth_ref)
    :param Rrat: float of ratio of QLK to IMAS major radius (RATIOS.Rrat)
    :return: IMAS normalised toroidal velocity
    """
    vel_tor_norm = Machtor * cref_QLK / (vth_ref_IMAS * Rrat)
    return vel_tor_norm


def velocity_tor_gradient_norm(Autor_QLK, cref_QLK, vth_ref_IMAS, Rrat_ratio):
    """
    Calculates the IMAS normalised toroidal veloctiy gradient (or parallel flow shear) given the following qualikiz inputs:
    :param Autor_QLK: array or float of the toroidal velocity gradient from QLK files (ds.Autor.values)
    :param cref_QLK: float of normalising velocity for QLK normalisations  (QLK.cref)
    :param vth_ref_IMAS: float of IMAS reference thermal velocity (IMAS.vth_ref)
    :param Rrat:  float of ratio of QLK to IMAS major radius (RATIOS.Rrat)
    :return: IMAS normalised toroidal velocity gradient
    """
    vel_tor_gradient_norm = cref_QLK * Autor_QLK / (vth_ref_IMAS * Rrat_ratio ** 2)
    return vel_tor_gradient_norm


def shearing_rate_norm(gammE_QLK, cref_QLK, vth_ref_IMAS, Rrat):
    """
    Calculates the IMAS normalised perpendicular flow shear given the following qualikiz inputs:

    :param gammE_QLK: array or float of (ds.gammaE.values) the QLK gammaE data
    :param cref_QLK:  array or float of (QLK.cref) QLK reference velocity for normalising velocities
    :param vth_ref_IMAS: array or float of (IMAS.vth_ref) IMAS thermal reference velocity
    :param Rrat: array or float of (RATIOS.Rrat) ratio of QLK to IMAS R (=1)
    :return: IMAS normalised perpendicular flow shear
    """
    shear_rate_norm = gammE_QLK * cref_QLK / (vth_ref_IMAS * Rrat)
    return shear_rate_norm


def beta_reference():
    """
    beta reference is set to 0 for electrostatic codes like QLk
    """
    return np.float64(0)


def debye_length_reference():
    """
    debye length is assumed to be 0 for simplifications to hold
    """
    return np.float64(0)


def radial_component_norm(kthetarho_QLK=None):
    """
    radial wavevector is set to 0
    """
    if kthetarho_QLK is None:
        return 0
    else:
        return np.zeros_like(kthetarho_QLK, dtype=np.float64)


def binormal_component_norm(rho_rat, kthetarho_QLK):
    """
    Calculates the IMAS binormal wavevector given the following inputs:

    :param kthetarho_QLK: float or array of the wavenumber from QLK (ds.kthetarhos.values)
    :param rho_rat: float of (RATIOS.rho_rat) the ratio of larmor radii of QLK to IMAS :return: IMAS binormal wavevector
    """
    if np.isscalar(rho_rat):
        kthetas = kthetarho_QLK / rho_rat
    else:
        kthetas = np.array([kthetarho_QLK / rho for rho in rho_rat])
    return kthetas


def collisionality_norm(IMAS_Rref, IMAS_vth_ref, QLK_n_e, QLK_n_b, QLK_Z_b, q_rat, QLK_t_e, IMAS_vth_a):
    """
    Calculates the IMAS normalised collisionality (only valid for electrons onto ions for QLK?). Splits the ion density
    array and calculates the collisionality for each ion type separately before summing. Uses the following inputs:
    IMAS_Rref: IMAS reference length (IMAS.Rref)
    IMAS_vth_ref: IMAS reference thermal velocity (IMAS.vth_ref)
    QLK_n_e: float or array of  qualikiz electron density in units of 1e19 (ds.ne.values)
    QLK_n_b: float or array of normalised qualikiz density of target ion (SI), (ds.normni.values)
    QLK_Z_b: float or array of charge number of target ion (ds.Zi.values)
    q_rat: float of QLK:IMAS refernce charge ratio
    QLK_t_e: float or array of QLK electron temperature (ds.Te.values)
    IMAS_vth_a: thermal velocity of incident particles i.e. electrons (IMAS.vth_e)

    returns: sum of the collisionality arrays for each ion
    """

    def coulomblog(QLK_ne, QLK_Te):
        coulog = np.float64(15.2 - 0.5 * np.log(QLK_ne / 10) + np.log(QLK_Te))
        return coulog

    # convert qualikiz normalised ion density to SI
    n_b = QLK_n_b * QLK_n_e * np.float64(1e19)
    Z_a = np.float64(-1) * q_rat  # incident particles are hardcoded to electrons in QLK
    Z_b = QLK_Z_b * q_rat
    e = scipy.constants.physical_constants['electron volt'][0]
    e_0 = scipy.constants.physical_constants['vacuum electric permittivity'][0]
    m_a = scipy.constants.physical_constants['electron mass'][0]  # incident particles are hardcoded to electrons in QLK
    coulomblog = coulomblog(QLK_n_e, QLK_t_e)
    collated_constants = Z_a ** 2 * e ** 4 / (4 * np.pi * e_0 ** 2 * m_a ** 2)
    collisionality = collated_constants * IMAS_Rref * n_b * (Z_b ** 2) * coulomblog / (IMAS_vth_ref * (IMAS_vth_a ** 3))
    return collisionality


def include_centrifugal_effects(rot_flag_QLK, x_QLK):
    """
    returns if centrifugal effects are taken into account in QLK.
    todo: check with someone the correct equality sign (currently assumed and would make a difference for runs at x=0.5)
    """
    if rot_flag_QLK == 0 or (rot_flag_QLK == 2 and x_QLK < 0.5):
        return 0
    elif rot_flag_QLK == 1 or (rot_flag_QLK == 2 and x_QLK >= 0.5):
        return 1


def include_a_field_parallel():
    return 0


def include_b_field_parallel():
    return 0


def include_full_curvature_drift():
    return 0


def collisions_pitch_only():
    """return true for QLK"""
    return 1


def collisions_momentum_conservation():
    return 0


def collisions_energy_conservation():
    return 0


def collisions_finite_larmor_radius():
    return 0


def growth_rate_norm_GB(gamma_GB_QLK, Rmin_QLK, Rref_IMAS, vth_rat_RATIO):
    """
    Calculates the IMAS normalised growth rate of a mode given the following inputs:
    gamma_GB_QLK: float or array of (ds.gam_GB.values)  GyroBohm normalised growth rate from QLK
    Rmin_QLK: float or array of (ds.Rmin.values) QLK minor radius of LCS
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_rat_RATIO: (RATIOS.vth_rat) ratio of QLK:IMAS thermal velocities
    """
    return gamma_GB_QLK * Rref_IMAS * vth_rat_RATIO / Rmin_QLK


def growth_rate_norm_SI(gamma_SI_QLK, Rref_IMAS, vth_ref_IMAS):
    """
    Calculates the IMAS normalised growth rate of a mode given the following inputs:
    gamma_SI_QLK: float or array of (ds.gam_SI.values)  SI growth rate from QLK
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_ref_IMAS: (IMAS.vth_ref) IMAS reference thermal velocity
    """
    return gamma_SI_QLK * Rref_IMAS / vth_ref_IMAS


def frequency_norm_GB(omega_GB_QLK, Rmin_QLK, Rref_IMAS, vth_rat_RATIO):
    """
    Calculates the IMAS normalised frequency of a mode given the following inputs:
    omega_GB_QLK: float or array of (ds.ome_GB.values) GyroBohm normalised frequency from QLK
    Rmin_QLK: (ds.Rmin) QLK minor radius of LCS
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_rat_RATIO: (RATIOS.vth_rat) ratio of QLK:IMAS thermal velocities
    """
    return -omega_GB_QLK * Rref_IMAS * vth_rat_RATIO / Rmin_QLK


def frequency_norm_SI(omega_SI_QLK, Rref_IMAS, vth_ref_IMAS):
    """
    Calculates the IMAS normalised frequency of a mode given the following inputs:
    omega_SI_QLK: float or array of (ds.ome_SI.values) SI normalised frequency from QLK
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_ref_IMAS: (IMAS.vth_ref) IMAS reference thermal velocity
    """
    return -omega_SI_QLK * Rref_IMAS / vth_ref_IMAS


def phi_potential_perturbed_norm_nonlinear():
    """
    Calculates the IMAS normalised perturbed potential for a nonlinear simulation given the following inputs:
    TODO: uneeded for our purposes, need for QLK.nc generic file conversion? not complete in doc yet
    """
    return


def phi_potential_perturbed_norm_linear(theta, modewidth, modeshift, d, phi_0=1, test=False):
    """
    Calculates the IMAS normalised perturbed potential for a linear simulation given the following inputs:

    :param theta: theta array over which to integrate/calculate potential
    :param w: modewidth from QLK. (Complex number made up of ds.imodewidth and ds.rmodewidth)
    :param x0: modeshift from QLK. (Complex number made up of ds.imodeshift and ds.rmodeshift)
    :param d: ds.distan, another scaling factor for the gaussian width scaling inversely to w. Represents the distance between rational flux surfaces
    :return: the normalised IMAS potential over the theta grid
    """

    exp1 = np.exp(-modewidth ** 2 * theta ** 2 / (2 * d ** 2))
    exp2 = np.exp(1j * theta * modeshift / d)
    phi_hat_N = exp1 * exp2

    phi_hat_abs_sq = np.abs(phi_hat_N) ** 2

    Af = np.sqrt(1 / (2 * np.pi) * np.trapz(phi_hat_abs_sq, theta))

    max_index = np.argmax(np.abs(phi_hat_N))
    phi_hat_N_max = np.abs(phi_hat_N)[max_index]

    e_i_alpha = phi_hat_N_max / phi_hat_N[max_index]

    phi_Nf = phi_hat_N * e_i_alpha / Af

    if test is True:
        plt.plot(theta, np.real(phi_hat_N), label='Re')
        plt.plot(theta, np.imag(phi_hat_N), label='Im')
        plt.plot(theta, np.abs(phi_hat_N), label='Abs')
        plt.legend()
        plt.show()

        plt.plot(theta, phi_Nf, label='Nf')
        plt.legend()
        plt.show()
    return phi_Nf, Af


def phi_potential_perturbed_norm_linear2(theta, w, x0, d, recip_rho_star, q_ref, T_ref, phi_0=1, test=False):
    """
    WIP to avoid underflow errors, circumvented by smarter coding (in theory) for now

    Calculates the IMAS normalised perturbed potential for a linear simulation given the following inputs:

    :param theta: theta array over which to integrate/calculate potential
    :param w: modewidth from QLK. (Complex number made up of ds.imodewidth and ds.rmodewidth)
    :param x0: modeshift from QLK. (Complex number made up of ds.imodeshift and ds.rmodeshift)
    :param d: ds.distan, another scaling factor for the gaussian width scaling inversely to w. Represents the distance between rational flux surfaces
    :param phi_0: normalising factor, wont affect the result
    :param recip_rho_star: reciprocal of rho_star, the normalised IMAS reference larmor radius
    :param q_ref: reference IMAS q
    :param T_ref: reference IMAS Temperature
    :return: the normalised IMAS potential over the theta grid
    """

    def try_exp(x):
        try:
            return np.exp(x)
        except:
            return 0

    exp_factor1 = -w ** 2 * theta ** 2 / (2 * d ** 2)
    exp1 = try_exp(exp_factor1)
    exp_factor2 = 1j * theta * x0 / d
    exp2 = try_exp(exp_factor2)

    def try_mult(a, b):
        try:
            return a * b
        except:
            return 0

    phi_hat_N = try_mult(exp1, exp2)

    plt.plot(theta, np.real(phi_hat_N), label='Re')
    plt.plot(theta, np.imag(phi_hat_N), label='Im')
    plt.plot(theta, np.abs(phi_hat_N), label='Abs')
    plt.legend()
    plt.show()

    def try_abs_sq(x):
        try:
            return np.abs(x) ** 2
        except:
            return 0

    phi_hat_abs_sq = try_abs_sq(phi_hat_N)

    def try_af(x, theta):
        try:
            return np.sqrt(1 / (2 * np.pi) * np.trapz(phi_hat_abs_sq, theta))
        except:
            return 0

    Af = np.sqrt(1 / (2 * np.pi) * np.trapz(phi_hat_abs_sq, theta))
    # Af2 = try_af(phi_hat_abs_sq, theta)

    max_index = np.argmax(np.abs(phi_hat_N))
    phi_hat_N_max = np.abs(phi_hat_N)[max_index]

    e_i_alpha = phi_hat_N_max / phi_hat_N[max_index]

    phi_Nf = phi_hat_N * e_i_alpha / Af

    if test is True:
        plt.plot(theta, phi_Nf, label='Nf')
        plt.legend()
        plt.show()

    return phi_Nf, Af


def normalise_integrated_particle_flux(particle_flux, n_s, v_thref, rho_star, Af=1):
    '''
    Normalises a QLK particle flux to IMAS standard.
    :param particle_flux: float or array of QLK particle flux (ds.pfe_SI.values or ds.pfi_SI.values)
    :param n_s: float or array of particle density (ds.ne.values or ds.normni.values * ds.ne.values) in units of 1e19
    :param v_thref: float of IMAS v_thref
    :param rho_star: float of IMAS rhostar (reference larmor radius normalised to reactor Lref)
    :param Af: Potential normalisation factor: 1 if non-linear, otherwise calculated from phi_potential_perturbed_norm_linear()

    :return: IMAS normalised particle flux
    '''
    denom = Af ** 2 * n_s * np.float64(1e19) * (v_thref * rho_star) ** 2
    particle_flux_norm = np.divide(particle_flux, denom, out=np.zeros_like(denom), where=denom != 0)
    return particle_flux_norm


def normalise_integrated_energy_flux(energy_flux, n_s, v_thref, rho_star, T_ref, Af=1):
    '''
    Normalises a QLK particle flux to IMAS standard.
    :param energy_flux: float or array of QLK energy flux (ds.efe_SI.values or ds.efi_SI.values)
    :param n_s: float or array of particle density (ds.ne.values or ds.normni.values * ds.ne.values) in units of 1e19
    :param v_thref: float of IMAS v_thref
    :param rho_star: float of IMAS rhostar (reference larmor radius normalised to reactor Lref)
    :param T_ref: float of IMAS reference temperature (IMAS.Tref)
    :param Af: Potential normalisation factor: 1 if non-linear, otherwise calculated from phi_potential_perturbed_norm_linear()

    :return: IMAS normalised energy flux
    '''
    denom = Af ** 2 * n_s * np.float64(1e19) * T_ref * v_thref * rho_star ** 2
    energy_flux_norm = np.divide(energy_flux, denom, out=np.zeros_like(denom), where=denom != 0)
    return energy_flux_norm


def normalise_integrated_momentum_flux(momentum_flux, n_s, v_thref, rho_star, m_ref, L_ref, Af=1):
    '''
    Normalises a QLK particle flux to IMAS standard.
    :param momentum_flux: float or array of QLK momentum flux (ds.vfe_SI.values or ds.vfi_SI.values)
    :param n_s: float or array of particle density (ds.ne.values or ds.normni.values * ds.ne.values) in units of 1e19
    :param v_thref: float of IMAS v_thref
    :param rho_star: float of IMAS rhostar (reference larmor radius normalised to reactor Lref)
    :param m_ref: float of IMAS reference mass (IMAS.mref)
    :param L_ref: float of IMAS reference major radius (IMAS.Rref)
    :param Af: Potential normalisation factor: 1 if non-linear, otherwise calculated from phi_potential_perturbed_norm_linear()

    :return: IMAS normalised energy flux
    '''
    denom = Af ** 2 * n_s * np.float64(1e19) * m_ref * L_ref * (v_thref * rho_star) ** 2
    # conditon for dealing with 0 density species, sets outputs of division to 0 if denom = 0
    momentum_flux_norm = np.divide(momentum_flux, denom, out=np.zeros_like(denom), where=denom != 0)
    return momentum_flux_norm


def electron_type(etype):
    # converts from QLK electron type (1: active, 2: adiabatic, 3: adiabatic passing at ion scales) to IMAS electron type (0: active, 1: adiabatic)
    if etype == 1:
        return 0
    elif etype == 2:
        return 1
    elif etype == 3:
        print('electron type not supported')
        return 'electron type not supported'
    else:
        print(f'electron type not recognised: {etype, type(etype)}')
        return f'electron type not recognised: {etype, type(etype)}'


def ion_type(itypes):
    imas_density_mult = []
    for itype in itypes:
        if itype == 1:
            imas_density_mult.append(1)
        elif itype == 3:
            imas_density_mult.append(0)
        elif itype == 2:
            print(f'Adiabatic ions are incompatible with IMAS')
            return
        elif itype == 4:
            print(f'Type 4 ions are incompatible with IMAS')
            return
        else:
            print(f'"{itype}" Ion type not recognised')
    return imas_density_mult


def poloidal_angle_pturns(w, d, x0, res=32, domain_mult=4, max_rot=6, test=False):
    """
    function to create a theta grid 4 FWHMs (maximum of 6pi, should never be reached due to strong ballooning) around the
    center of the potential, with a given resolution.
    :param w: ds.rmodewidth + ds.imodewidth
    :param x0: ds.rmodeshift + ds.imodeshift
    :param d: ds.distan
    :param res: resolution in points per FWHM (default of 4 FWHM plotted)
    :param domain_mult: controls number of FWHM to plot (recommend leaving default or smaller to avoid underflow errors)
    :param max_rot: maximum number of full rotations for theta, default to 6 rotations, should be more than big enough

    :return: poloidal angle grid
    """
    if res % 2 == 1:
        print('odd resolution detected, recommend using even numbers to include peak values')

    a = np.abs(w) ** 2 / (2 * d ** 2)
    b = np.imag(x0) / d
    center = -b / (2 * a)

    fwhm = d * np.sqrt(2 * np.log(2) / (np.real(w) ** 2 - np.imag(w) ** 2))
    domain_lim = max_rot * np.pi
    domain = min(domain_mult * fwhm, domain_lim)
    edges = (center - domain, center + domain)
    pol_turns = domain / np.pi
    int_pol_turns = int(np.ceil(pol_turns))
    if domain < domain_lim:
        pol_ang = np.linspace(edges[0], edges[1], (domain_mult * res) + 1)
    else:
        pol_ang = np.linspace(edges[0], edges[1], (domain_lim / (domain_mult * fwhm) * res) + 1)
    if test is True:
        print(w, x0)
        plt.axvline(center)
        y = np.exp(-w ** 2 * pol_ang ** 2 / (2 * d ** 2)) * np.exp(1j * x0 / d * pol_ang)
        plt.plot(pol_ang, np.real(y))
        plt.plot(pol_ang, np.imag(y))
        plt.plot(pol_ang, np.abs(y))
        edges = ((center - fwhm * (x + 1), center + fwhm * (x + 1)) for x in reversed(range(domain_mult)))
        cols = ('r', 'g', 'b', 'y')
        for n, mult in enumerate(edges):
            plt.hlines(0.5, mult[0], mult[1], colors=cols[n])
        plt.show()
    return pol_ang, int_pol_turns


################################
##### IDS to QLK Functions #####
################################

def Ro_qlk(ids=None):
    if ids is None:
        return 3
    if ids.normalizing_quantities.r==9e40:
        return 3
    else:
        return float(ids.normalizing_quantities.r)


def Bo_qlk(ids=None):
    if ids is None:
        return 5
    if ids.normalizing_quantities.b_field_tor==9e40:
        return 5
    else:
        return float(ids.normalizing_quantities.b_field_tor)


def ne_qlk(ids, self_nref):
    if ids.normalizing_quantities.t_e == 9e40:
        return 1
    else:
        return float(ids.normalizing_quantities.n_e) / self_nref


def Te_qlk(ids, e_index, self_Tref):
    if ids.normalizing_quantities.t_e == 9e40:
        return 10
    else:
        return float(ids.species[e_index].temperature_norm * ids.normalizing_quantities.t_e / self_Tref)


def Rref_qlk(self_Ro):
    return float(self_Ro)


def Bref_qlk(self_Bo):
    return float(self_Bo)


def x_qlk():
    return 1


def Rmin_qlk(ids, self_Ro, self_x):
    return float(ids.flux_surface.r_minor_norm * self_Ro / self_x)


def q_qlk(ids):
    return np.abs(ids.flux_surface.q)


def smag_qlk(ids):
    return float(ids.flux_surface.magnetic_shear_r_minor)


def alpha_qlk(ids, self_q, RATIOS):
    return float(ids.flux_surface.pressure_gradient_norm * self_q ** 2 * RATIOS.Rrat / RATIOS.Brat ** 2)


def Machtor_qlk(ids, IMAS, RATIOS, self_cref):
    if not ids.species_all.velocity_tor_norm==9e40:
        return float(ids.species_all.velocity_tor_norm * IMAS.vth_ref * RATIOS.Rrat / self_cref)
    else:
        return 0


def Machpar_qlk():
    return 0


def Aupar_qlk():
    return 0


def Autor_qlk(ids, main_ion_index, IMAS, RATIOS, self_cref):
    if not ids.species[main_ion_index].velocity_tor_gradient_norm==9e40:
        return float(ids.species[main_ion_index].velocity_tor_gradient_norm * IMAS.vth_ref * (RATIOS.Rrat ** 2) / self_cref)
    else:
        return 0


def gammaE_qlk(ids, IMAS, RATIOS, self_cref):
    if ids.species_all.shearing_rate_norm==9e40:
        return ids.species_all.shearing_rate_norm * IMAS.vth_ref * RATIOS.Rrat / self_cref
    else:
        print('No shearing_rate_norm field in IDS, falling back to default value of 0')
        return 0


def vel_grads_qlk(ids):
    # vel_grads = list({species.velocity_tor_gradient_norm for species in ids.species})
    # loop to check velocity gradients are equal and send warning if not.
    # if len(vel_grads) > 1:
    #     print('IDS Species have different velocity gradients. This is not supported in QLK and the main ion velocity gradient has been used by default.')
    # else:
    #     return vel_grads[0]
    return [0 for species in ids.species]


def Ane_qlk(electron, RATIOS):
    return float(electron.density_log_gradient_norm * RATIOS.Rrat)


def Ate_qlk(electron, RATIOS):
    return float(electron.temperature_log_gradient_norm * RATIOS.Rrat)


def Ti_qlk(ion, self_Te):
    return float(ion.temperature_norm * self_Te)


def normni_qlk(ion):
    return float(ion.density_norm)


def Ani_qlk(ion, RATIOS):
    return float(ion.density_log_gradient_norm * RATIOS.Rrat)


def Ati_qlk(ion, RATIOS):
    return float(ion.temperature_log_gradient_norm * RATIOS.Rrat)


def Ai_qlk(ion, RATIOS):
    return float(ion.mass_norm / RATIOS.mrat)


def Zi_qlk(ion, e_charge):
    return float(ion.charge_norm / e_charge)


def ion_type_qlk(normni_QLK):
    if normni_QLK == 0:
        # set to tracer if density = 0
        return 3
    else:
        # set to active otherwise
        return 1


def ni_qlk(self_normni, self_ne):
    # breakpoint()
    return np.asarray(self_normni) * self_ne


def mi_qlk(self_Ai, self_mref):
    return np.asarray(self_Ai) * self_mref


def vth_qlk(self_Te, self_Tref, self_qref, self_mi):
    return float(np.sqrt(np.float64(self_Te) * self_Tref * self_qref / self_mi))


def rho_i_qlk(self_mi, self_vth_i, self_qref, self_Bref):
    return float(self_mi * self_vth_i / (self_qref * self_Bref))


def kthetarhos_qlk(ids, RATIOS):
    return np.array([float(wavevector.binormal_wavevector_norm * RATIOS.rho_rat) for wavevector in ids.linear.wavevector])


def typee_qlk(ids):
    if ids.model.adiabatic_electrons == 1:
        # set to adibatic electrons if IMAS = 1
        return 2
    else:
        # set to active electrons if IMAS = 0
        return 1


def typei_qlk(self_normni):
    return np.array([3 if dens == 0 else 1 for dens in self_normni])


def anis_qlk():
    return 0


def danisdr_qlk():
    return 0


def rot_flag_qlk(ids):
    # check value in ids not default, if not set to same values, else set to 0
    # try:
    #     if not idspy.is_default_imas_value(ids.model, 'include_centrifugal_effects'):
    #         return ids.model.include_centrifugal_effects
    #     else:
    #         return 0
    # except:
    return 0


def gam_SI_qlk(ids, IMAS):
    return np.array([[float(eigenmode.growth_rate_norm * IMAS.vth_ref / IMAS.Rref) for eigenmode in wavevector.eigenmode] for wavevector in ids.linear.wavevector])


def ome_SI_qlk(ids, IMAS):
    return np.array([[float(-eigenmode.frequency_norm * IMAS.vth_ref / IMAS.Rref) for eigenmode in wavevector.eigenmode] for wavevector in ids.linear.wavevector])


def pf_SI_qlk(ids, index, self_n, IMAS):
    return np.array(float(ids.non_linear.fluxes_1d.particles_phi_potential[index] * self_n * np.float64(1e19) * (IMAS.vth_ref * IMAS.rho_star) ** 2))


def ef_SI_qlk(ids, index, self_n, IMAS):
    return np.array(float(ids.non_linear.fluxes_1d.energy_phi_potential[index] * self_n * np.float64(1e19) * IMAS.Tref * IMAS.vth_ref * IMAS.rho_star ** 2))


def vf_SI_qlk(ids, index, self_n, IMAS):
    return np.array(float(ids.non_linear.fluxes_1d.momentum_tor_perpendicular_phi_potential[index] * self_n * np.float64(1e19) * IMAS.mref * IMAS.Rref * (IMAS.vth_ref * IMAS.rho_star) ** 2))
