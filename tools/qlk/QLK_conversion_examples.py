from qlk2ids import *


#############################
# generating an empty class #
#############################
new_qlk_class = QLK_class()


###########################################################
# loading data into the class from local gyrokinetics ids #
###########################################################
# from a path
idspath = './test_ids'
new_qlk_class.load_ids(ids_path=idspath)

# from an ids object
test_ids = gkids.GyrokineticsLocal()
new_qlk_class.load_ids(ids=test_ids)


############################################################################
# loading data into the class from a QuaLiKiz pythontools plan obj or json #
############################################################################
# from json path
json_path = './default_parameters.json'
new_qlk_class.load_QLKplan(json_path=json_path)

# from an existing QuaLiKizPlan object
json_path = './default_parameters.json'
qualikiz_plan_base = QuaLiKizPlan.from_json(json_path)
new_qlk_class.load_QLKplan(QLKplan=qualikiz_plan_base)


##########################################################
# loading data into the class from a QuaLiKiz run folder #
##########################################################
'''WIP'''
path_to_run = './qualikizruns'
new_qlk_class.load_runfolder(run_path=path_to_run)


#################################
# converting data into IMAS IDS #
#################################
provider = 'your name'
comment = 'optional'
gk_ids = new_qlk_class.to_IMAS_ids(provider, comment=comment)


#################################
# converting data into QLK plan #
#################################
json_path = './default_parameters.json'
''' 
the template JSON serves as the basis for all the default settings and is provided along with this code. 
These default settings can either be changed by pointing to a modified json file or by providing a dictionary of 
**kwargs to change the QLKplan Parameters and Options outlined in 4.7 and 4.8 of the documentation.
'''
qlk_plan = new_qlk_class.to_QLK_plan(template_path=json_path)


####################################################
# running a QLK simulation directly from the class #
####################################################
'''WIP'''
new_qlk_class.run_QLK()