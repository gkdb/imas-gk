import os
import shutil
import struct # for the input conversion in binaries


class QLKrun:
    """ Load and manipulate data from a QLK run

        QLKrun.storage                    -> path of QLK input and outputs files
                                            filled by set_storage
            .input                      -> content of QLK input files input.tglf and out.tglf.globaldump
            .input_out                     filled by get_inputs

        Other methods:
            .set_storage      -> Set the path of QLK input and/or output files
            .write_input      -> write a QLK input file using the data in .input
            .read             -> load available inputs and outputs calling all the 'get' functions
            .run              -> run QLK using the input file located in self.storage['flpth']
    Other functions:
            convert_value     -> Function to convert values to appropriate types
    """
    def __init__(self):
        #self.set_storage(qlkpath, runpath, runname)
        #qualikiz_names
        # Dico initilization
        self.input = {}

        # By default parameters
        self.input['dimx'] = 1  # p{1} (-) Number of radial points or scans: set to 1 (no scan) 
        self.input['dimn'] = 1  # p{2} (-) Number of wavenumbers
        self.input['nions'] = 1  # p{3} (-) Number of ions in the system
        self.input['phys_meth'] = 2  # p{4} (-) Flag for additional computation (default 0.0)
        self.input['coll_flag'] = 0  # p{5} (-) Flag for collisionality (default 0.0)
        self.input['write_primi'] = 1  # Flag for writing primitive outputs (default 1)
        self.input['rot_flag'] = 0  # p{6} (-) Flag for rotation (default 0.0)
        self.input['verbose'] = 1  # p{7} (-) Flag to set the verbosity level of the output
        self.input['numsols'] = 3  # p{8} (-) Number of solutions requested
        self.input['relacc1'] = 1e-3  # p{9} (-) Relative accuracy in 1D integrals
        self.input['relacc2'] = 2e-2  # p{10} (-) Relative accuracy in 2D integrals
        self.input['absacc1'] = 0  # p{15} (-) Collisionality multiplier (for tests)
        self.input['absacc2'] = 0  # p{15} (-) Collisionality multiplier (for tests)
        self.input['maxruns'] = 1  # p{11} (-) Number of iterations jumping directly to Newton between contour checks
        self.input['maxpts'] = 5e5 # p{12} (-) Number of integrand evaluations performed in the 2D integral
        self.input['timeout'] = 600  # p{13} (-) Upper time limit (s) beyond which solutions are not sought for a given wavenumber and radius
        self.input['ETGmult'] = 1  # p{14} (-) ETG multiplier
        self.input['collmult'] = 1  # p{15} (-) Collisionality multiplier

        # Geometry input
        self.input['kthetarhos'] = 0.2  # p{16} (-) Wavenumber spectrum input: Vector (dimn)
        self.input['x'] = 0.5/3  # p{17} (-) Normalized radial coordinate (midplane average)
        self.input['rho'] = 0.5/3  # p{18} (-) Normalized toroidal flux coordinate
        self.input['rhomin'] = 0  # p{18} (-) Normalized toroidal flux coordinate
        self.input['rhomax'] = 1  # p{18} (-) Normalized toroidal flux coordinate
        self.input['Ro'] = 3  # p{19} (m) Major radius. Radial profile due to Shafranov shift
        self.input['Rmin'] = 0.5  # p{20} (m) Geometric minor radius. Assumed to be an average at the midplane at the LCFS. Currently a profile but probably should be shifted to a scalar
        self.input['Bo'] = 1  # p{21} (T) Probably not very rigorous to use this sqrt(<B^2>) to calculate Larmor radius # fairly close to <Bphi> in practice though
        #self.input['R0'] = self.R0  # p{22} (m) Major geometric radius used for normalizations
        self.input['q'] = 2  # p{23} (-) Vector (radial grid x(aa))
        self.input['smag'] = 1  # p{24} (-) Vector (radial grid x(aa)) q is a flux surface quantity --> makes sense to consider s  = self. rho/q dq/drho
        self.input['alpha'] = 0  # p{25} (-) Vector (radial grid x(aa))

        # Rotation input
        self.input['Machtor'] = 0  # p{26} (-) Vector (radial grid x(aa))
        self.input['Autor'] = 0  # p{27} (-) Vector (radial grid x(aa))
        self.input['Machpar'] = 0  # p{28} (-) Vector (radial grid x(aa))
        self.input['Aupar'] = 0  # p{29} (-) Vector (radial grid x(aa))
        self.input['gammaE'] = 0  # p{30} (-) Vector (radial grid x(aa))

        # Electron input
        self.input['Te'] = 1  # p{31} (keV) Vector (radial grid x(aa))
        self.input['ne'] = 1  # p{32} (10^19 m^-3) Vector (radial grid x(aa))
        self.input['Ate'] = 9  # p{33} (-) Vector (radial grid x(aa))
        self.input['Ane'] = 3  # p{34} (-) Vector (radial grid x(aa))
        self.input['typee'] = 1  # p{35} Kinetic or adiabatic
        self.input['anise'] = 1  # p{36} Tperp/Tpar at LFS
        self.input['danisdre'] = 0  # p{37} d/dr(Tperp/Tpar) at LFS

        # Ion inputs (can be for multiple species)
        self.input['Ai'] = 1  # p{38} (-) Ion mass
        self.input['Zi'] = 1  # p{39} (-) Ion charge
        self.input['Ti'] = 1  # p{40} (keV) Vector (radial grid x(aa))
        self.input['normni'] = 1  # p{41} ni/ne Vector (radial grid x(aa))
        self.input['Ati'] = 9  # p{42} (-) Vector (radial grid x(aa))
        self.input['Ani'] = 3  # p{43} (-) Vector (radial grid x(aa)) Check calculation against Qualikiz's electroneutrality assumption
        self.input['typei'] = 1  # p{44} Kinetic, adiabatic, tracer
        self.input['anisi'] = 1  # p{45} Tperp/Tpar at LFS
        self.input['danisdri'] = 0  # p{46} d/dr(Tperp/Tpar) at LFS
        self.input['separateflux'] = 1  # p{47} Separate flux output
        self.input['simple_mpi_only'] = 0  # p{48} Separate flux output
        self.input['integration_routine'] = 1  # p{15} (-) NAG



    def set_storage(self, qlkpath = '~/Documents/These/QuaLiKiz', runpath = '~/Documents/These/QuaLiKiz/runs', runname = 'my_run'):
        # Saving the path of the IMAS2QLK
        self.imas2qlkpath = os.path.abspath(os.path.dirname(__file__))

        # Absolute qlk path
        self.qlkpath_abs = os.path.expanduser(qlkpath) #if runpath is relative (means use the "~") it will expand to the correct path (doesn't do anything if the user put an absolute path)
        self.runpath_abs = os.path.expanduser(runpath)
        print("Absolute Path of QLK fold:", self.qlkpath_abs)

        if not os.path.isdir(self.runpath_abs):
            os.mkdir(self.runpath_abs)

        # Creates runs directory if it doesn't already exist 
        self.runfoldpath=self.runpath_abs+'/'+runname

        if not os.path.isdir(self.runfoldpath):
            os.mkdir(self.runfoldpath)

        # don't forget the QuaLiKiz executable
        shutil.copy(self.qlkpath_abs + '/QuaLiKiz', f'{self.runfoldpath}/QuaLiKiz')

        # Pre build of the files containing input and outputs
        os.makedirs(f'{self.runfoldpath}/input', exist_ok=True)
        os.makedirs(f'{self.runfoldpath}/output', exist_ok=True)
        os.makedirs(f'{self.runfoldpath}/output/primitive', exist_ok=True)
        os.makedirs(f'{self.runfoldpath}/debug', exist_ok=True)

        # Come back to the previous work path
        os.chdir(self.imas2qlkpath)


    def write_input(self):

        ################## Write binary input files ################################

        nargu = len(self.input)
        namearg = list(self.input.keys())
        valuearg = list(self.input.values())
        stringind=[] #no string indexes. Kept if adding any future string inputs

        # Change the current work path to the run folder path 
        os.chdir(self.runfoldpath)

        # Write binaries
        for ii in range(nargu):
            if ii in stringind:
                with open(f'input/p{ii}.txt', 'wb') as fid:
                    fid.write(valuearg[ii])
            else:
                with open(f'input/{namearg[ii]}.bin', 'wb') as fid:
                    if isinstance(valuearg[ii], int):
                        fid.write(struct.pack('d', valuearg[ii]))  # 'i' for int
                    elif isinstance(valuearg[ii], float):
                        fid.write(struct.pack('d', valuearg[ii]))  # 'd' for float
                    elif isinstance(valuearg[ii], list):
                        for item in valuearg[ii]:
                            fid.write(struct.pack('d', item))  # float list by default

        # Come back to the previous work path
        os.chdir(self.imas2qlkpath)


    def read(self):
        def read_dat_file(parent_path, filename):
            """
            Read the .dat file and return either a float, or a float list or a float matrix 
            """

            path = os.path.join(parent_path, filename+'.dat')
            with open(path, 'r') as f:
                lines = f.readlines()
                
               #if there is only one line
                if len(lines) == 1 : 
                    nombres = lines[0].split()
                    if len(nombres) == 1:
                        if filename in ['kthetarhos', 'Zi', 'Ai', 'normni', 'Ani', 'Ti', 'Ati', 'Ani']:
                            return [float(nombres[0].strip())]
                        else:
                            return float(nombres[0].strip())
                    else:
                        return [float(nombre.strip()) for nombre in nombres] #strip() is used to remove extra whitespaces
                
                
                # if several lines
                else:
                    return [[float(val.strip()) for val in line.split()] for line in lines]

        # Initialization of a dictionnary that will retrieve the data from debug and output folders of the run
        self.input_out = {}
        #--------------------------------------------------------------------#
        # First debug outputs to have the correct inputs used during the run #
        #--------------------------------------------------------------------#
        debug_path = self.runfoldpath + '/debug/'
        for file in os.listdir(debug_path):
            if file.endswith(".dat"):
                filename = os.path.splitext(file)[0]  # Remove the .dat extension of the file name
                self.input_out[filename] = read_dat_file(debug_path, filename)

        #------------------#
        # Then the outputs #
        #------------------#
        output_path = self.runfoldpath + '/output/'
        for file in os.listdir(output_path):
            if file.endswith(".dat"):
                filename = os.path.splitext(file)[0]  # Remove the .dat extension of the file name
                self.input_out[filename] = read_dat_file(output_path, filename)
        
        #----------------------------------------#
        # Then the primitive in the ouput folder #
        #----------------------------------------#
        output_primitive_path = self.runfoldpath + '/output/primitive/'
        for file in os.listdir(output_primitive_path):
            if file.endswith(".dat"):
                filename = os.path.splitext(file)[0]  # Remove the .dat extension of the file name
                self.input_out[filename] = read_dat_file(output_primitive_path, filename)




    def run(self, CPU = 32):
        # Change the current work path to the run folder path 
        os.chdir(self.runfoldpath)
        os.system(f'mpiexec -np {CPU} ./QuaLiKiz')
        # Come back to the previous work path
        os.chdir(self.imas2qlkpath)


