"""
This file contains functions to convert data between IMAS 'gyrokinetics' gkids
and QLK inputs and outputs.

Functions:
    
- ids2qlk(gkids, RMAJ_LOC=None, verbose=False): 
    Convert an IMAS 'gyrokinetics' gkids to a QLK input.
    Inputs:
        gkids: Instance of the idspy class.
        verbose: (Optional) Whether to print warnings. Defaults to False. TO DO 
    Outputs:
        qlkrun: Instance of the QLKrun class gathering all the QLK inputs generated from the gkids.
                 Inputs are stored in qlkrun.input

- qlk2ids(qlkrun, provider=''): 
    Convert QLK inputs and outputs to an IMAS 'gyrokinetics' gkids created with idspy.
    Inputs:
        qlkrun: Instance of the QLKrun class containing QLK input and output data.
        provider: (Optional) Provider information. Defaults to an empty string. TO DO 
    Outputs:
        gkids: Instance of the GKgkids class containing the generated 'gyrokinetics' gkids.
    
Note: This file requires the idspy library for IMAS data manipulation.
"""



import sys
import numpy as np
import scipy.constants as codata

import QLKrun as qlkrun
import idspy_toolkit as idspy
from idspy_dictionaries import ids_gyrokinetics_local

# Import the file FS_param.py from the repository IMAS-GK 
# that can be found at  https://gitlab.com/gkdb/imas-gk/-/blob/main/gkids/tools/FS_param.py
sys.path.append('imas-gk/gkids/tools')
import FS_param


def ids2qlk(gkids,verbose=False):
    """ Convert an IMAS 'gyrokinetics' gkids to a QLK input
    Inputs
    gkids       Instance of the idspy GKgkids class containing the reference 'gyrokinetics' gkids
    Outputs
    qlkrun     Instance of the QLKrun class gathering all the QLK inputs generated from the gkids
    """
    qlk_object = qlkrun.QLKrun()
    input = {}
    input['dimx'] = 1  #  Number of radial points or scans: set to 1 (no scan) 
    input['dimn'] = 1  #  Number of wavenumbers
    input['nions'] = 1  #  Number of ions in the system
    input['phys_meth'] = 2.0 
    if np.max(gkids.collisions.collisionality_norm)<1e-10:
        input['coll_flag'] = 0.0
    else:    
        input['coll_flag'] = 1.0# Flag for collisionless simulations if 0 no collision consdiered
    input['verbose'] = 1

    input['write_primi'] = 1.0# Do not write primitive outputs if 0
    input['numsols']   = 3  # Number of solutions in the output
    input['nprocs']    = 1  # Number of processors used in the calculation A ENLEVER
    input['maxruns']   = 1 #number of runs between contour checks
    input['maxpts']    = 5e5 #number of integrand evaluations done in 2D integral
    input['relacc1']   = 1e-3 #relative accuracy in 1D integrals
    input['relacc2']   = 2e-2 #relative accuracy in 2D integrals
    input['timeout'] = 600
    input['ETGmult'] = 1.0
    input['collmult'] = 1.0
    input['separateflux']=1.0
    input['simple_mpi_only']=0.0 #for debugging if 1 then no openMP used (F. Casson QLK development)
    input['write_primi'] = 1.0# Do not write primitive outputs if 0
    input['integration_routine'] = 1
    input['absacc1'] = 0
    input['absacc2'] = 0
    input['rhomin'] = 0
    input['rhomax'] = 1

    input['ntheta']=64 #resolution of parallel direction
    input['numecoefs'] = 13 #number of outputs in the ecoefs matrix

    input['set_normni1']=1      #flag for automating main ion concentration for maintaining QN
    input['set_Ani1']=1 #flag for automating main ion gradient for maintaining QN
    input['set_QN_grad']=1 #flag for maintaining quasineutrality of gradients.
    # First: to have correct QLK normalisation it is needed to retrieve the IMAS normalisation parameters 
    if gkids.normalizing_quantities.r >= 1e20:
        Lref_imas = 3
    else:
        Lref_imas = gkids.normalizing_quantities.r 
    if gkids.normalizing_quantities.b_field_tor >= 1e20:
        Bref_imas = 1
    else:
        Bref_imas = gkids.normalizing_quantities.b_field_tor
    if gkids.normalizing_quantities.n_e >= 1e20:
        nref_imas = 1e19
    else:
        nref_imas = gkids.normalizing_quantities.n_e 
    if gkids.normalizing_quantities.t_e >= 1e20:
        Tref_imas = 1e3 #eV
    else:
        Tref_imas = gkids.normalizing_quantities.t_e # eV
    qref_imas = 1.602176634e-19 #elementary charge (C)        
    mref_imas = 3.3435837724e-27 #Deuterium mass (kg)

    vthref_imas = np.sqrt(2*Tref_imas*qref_imas/mref_imas)
    rhoref_imas = mref_imas*vthref_imas/(qref_imas*Bref_imas)
    me_imas = 9.1093837015e-31 # electron mass (kg)

    # ... and the QLK normalisation parameters
    Lref_qlk = 3 # m
    Bref_qlk = 1 # T
    nref_qlk = 1e19 # m-3
    Tref_qlk = 1e3 # eV CAREFUL  : put the input temperature in keV
    qref_qlk = 1.602176565e-19 # C (according to Matisse, value taken from QuaLiKiz/src/datcal.f90) : weird to have a different value from IMAS...
    mref_qlk =1.672621777e-27 # proton mass (according to Matisse, value taken from QuaLiKiz/src/datcal.f90)

    ind_main_ion_qlk = 0 # we will find the index of the main ion (defined as the ion with the highest density) !!! MAIN ION = FIRST ION IN THE LIST APPARENTLY (cf qlk code src)
    density_ion_stack = 0
    for ind in range(len(gkids.species)):
        if gkids.species[ind].charge_norm>0:
            if gkids.species[ind].density_norm > density_ion_stack:
                density_ion_stack = gkids.species[ind].density_norm
                ind_main_ion_qlk = ind
    mass_norm_main_ion_qlk = gkids.species[ind_main_ion_qlk].mass_norm * mref_imas/mref_qlk
    mass_main_ion_qlk = mref_qlk * mass_norm_main_ion_qlk

    vthref_qlk = np.sqrt(Tref_qlk*1e3*qref_qlk/mass_main_ion_qlk) 
    rhoref_qlk = mref_qlk * vthref_qlk/(qref_qlk*Bref_qlk)


    # Ratios of these normalisation parameters
    Lrat = Lref_qlk/Lref_imas # =1
    Brat = Bref_qlk/Bref_imas # =1
    nrat = nref_qlk/nref_imas 
    Trat = Tref_qlk/Tref_imas 
    qrat = qref_qlk/qref_imas 
    mrat = mref_qlk/mref_imas # m_proton/m_main_ion = 0.5 if deuterium

    vthrat = vthref_qlk/vthref_imas 
    rhorat = rhoref_qlk/rhoref_imas

    input['kthetarhos'] = []
    for kk in range(len(gkids.linear.wavevector)):
        input['kthetarhos'].append(gkids.linear.wavevector[kk].binormal_wavevector_norm /rhorat)#list

    input['x']=[gkids.flux_surface.r_minor_norm /Lref_imas] #??? Matisse set it up to 1. but it as fully arbitrary bc it seems to be lost in the GK gkids
    input['rho']=input['x']

    input['Bo'] = Bref_qlk#magnetic field
    input['Ro'] = Lref_qlk#major radius in meter
    input['Rmin'] = gkids.flux_surface.r_minor_norm  #minor radius


    # Following depends on exp profiles
    input['q'] = gkids.flux_surface.q #set q-profile
    input['smag']=gkids.flux_surface.magnetic_shear_r_minor
    input['alpha']=gkids.flux_surface.pressure_gradient_norm*((input['q'])**2)*Lrat/(Brat)**2#MHD alpha ???

    # Ions set up list
    input['Ai'] = []
    input['Zi'] = []
    input['Ti']=[] # keV
    input['normni']=[]  # ni/ne arbitrary for main ions (will be rewritten to ensure quasineutrality)
    input['Ati']=[] #logarithmic gradient normalized to R
    input['Ani'] = [] #arbitrary for main ions, will be rewritten to ensure quasineutrality of gradients
    input['typei']=[] #1 for active, 2 for adiabatic, 3 for tracer (also won't be included in quasineutrality checks), 4 for tracer but included in Zeff
    input['anisi']=[] #Tperp/Tpar at LFS. Ti defined as Tpar
    input['danisdri'] = []
    #rotation inputs. Intrinsic assumption that all species rotate together
    if gkids.species_all.velocity_tor_norm >=1e20:
        input['Machtor'] = 0.0
    else:
        input['Machtor'] = gkids.species_all.velocity_tor_norm * vthref_imas * Lrat/vthref_qlk #* vthrat  #Main ion mach number of the toroidal rotation. All others are scaled to this.
    input['Machpar'] = input['Machtor'] #IMAS supposed it null bc species are asumed to have a purely toroidal flow [ref Yann Camenen]#Main ion mach number of the toroidal rotation. All others are scaled to this.

    if input['Machtor'] == 0: #Machtor  CHECK IF IT IS CONSISTENT
        input['rot_flag'] = 0.0
    else:
        input['rot_flag'] = 1.0 
    if gkids.species_all.shearing_rate_norm >=1e20:
        input['gammaE'] = 0.0
    else:
        input['gammaE'] = gkids.species_all.shearing_rate_norm * vthref_imas * Lrat/vthref_qlk #ExB shear. Defined with the sqrt(2)!
    if gkids.species[ind_main_ion_qlk].velocity_tor_gradient_norm >=1e20:
        input['Autor'] = 0.0
    else:
        input['Autor'] = gkids.species[ind_main_ion_qlk].velocity_tor_gradient_norm * vthref_imas * Lrat/vthref_qlk
    input['Aupar'] = input['Autor']

    # fill species parameters (Electron & Ions)
    input['nions'] = len(gkids.species) -1
    for sp in gkids.species:
        if sp.charge_norm< 0: #electron case
            if sp.temperature_norm >=1e20:
                input['Te']=Tref_qlk*1e-3# kev
            else:
                input['Te']=sp.temperature_norm*Tref_imas*1e-3 #/ input['Trat# kev
            input['ne']=sp.density_norm/nrat # ne is in units of 10^19
            input['Ate']=sp.temperature_log_gradient_norm
            input['Ane'] = sp.density_log_gradient_norm 
            if gkids.model.adiabatic_electrons ==0:
                input['typee'] = 1 # 1 for active, 2 for adiabatic, 3 for adiabatic passing at ion scales (kthetarhosmax<2) ??????????????
            elif gkids.model.adiabatic_electrons ==1:
                input['typee'] = 2
            input['anise'] = 1. # Temperature anisotropy ?????????
            input['danisdre'] = 0. # Radial gradient of temperature anisotropy ???????????
            
        else: # Ions case
            input['Ai'].append(sp.mass_norm / mrat) #UNIT : proton mass
            input['Zi'].append(sp.charge_norm / qrat) #UNIT : e
            if sp.temperature_norm >=1e20:
                input['Ti'].append(Tref_qlk*1e-3) # keV
            else:
                input['Ti'].append(sp.temperature_norm*Tref_imas*1e-3)# / input['Trat) # keV
            input['normni'].append(sp.density_norm/nrat)  # ni/ne arbitrary for main ions (will be rewritten to ensure quasineutrality) !!! nrat ????
            input['Ati'].append(sp.temperature_log_gradient_norm ) #logarithmic gradient normalized to R
            input['Ani'].append(sp.density_log_gradient_norm ) #arbitrary for main ions, will be rewritten to ensure quasineutrality of gradients
            input['typei'].append(1.) #1 for active, 2 for adiabatic, 3 for tracer (also won't be included in quasineutrality checks), 4 for tracer but included in Zeff   ??????????????????
            input['anisi'].append(1.) #Tperp/Tpar at LFS. Ti defined as Tpar   ??????????????????
            input['danisdri'].append(0.) #d/dr(Tperp/Tpar) at LFS. Ti defined as Tpar   ??????????????????
            # VELOCITY TOR
            # input['Aupar.append(0.) #IMAS supposed it null bc species are asumed to have a purely toroidal flow [ref Yann Camenen] #Parallel velocity shear. -1111 means that the consistent value with toroidal rotation is calculated.   
            # input['Autor.append(sp.velocity_tor_gradient_norm)  #Toroidal velocity shear. -1111 means that the consistent value with toroidal rotation is calculated. 
        
    qlk_object.input = input
  
    return qlk_object
  
  





def qlk2ids(qlkrun,provider=''):
    """ Convert GKW inputs and outputs to an IMAS 'gyrokinetics' gkids created with idspy
    Inputs
        qlkrun    Instance of the QLKrun class containing QLK input and output data
    Outputs
        gkids      Instance of the GKgkids class containing the generated 'gyrokinetics' gkids

    Warning and error logs are stored in ???
    """
    # initialise an empty GK gkids structure
    


 
    IDS_QLK  = ids_gyrokinetics_local.GyrokineticsLocal()
    idspy.fill_default_values_ids(IDS_QLK)

    '''******************************************************'''
    '''*********************---INPUTS---*********************'''
    '''******************************************************'''


    ### --------------------------------
    ### -------IDS.ids_properties-------
    ### --------------------------------
    IDS_QLK.ids_properties.provider = 'Name : '
    IDS_QLK.ids_properties.comment  = 'Converted from source'

    ### --------------------------------
    ### -----------IDS.code-------------
    ### --------------------------------
    IDS_QLK.code.name = 'QuaLiKiz'
    IDS_QLK.code.repository = 'https://gitlab.com/qualikiz-group/QuaLiKiz'
    IDS_QLK.code.commit = 'Latest commit'
    IDS_QLK.code.version = 'Latest QuaLiKiz version'
    IDS_QLK.code.library = 'no library'
    IDS_QLK.code.output_flag = 0


    ### --------------------------------
    ### ---IDS.normalizing_quantities---
    ### --------------------------------
    Lref_imas = 3
    Bref_imas = 1
    nref_imas = 1e19
    Tref_imas = 1e3 #eV
    qref_imas = 1.602176634e-19 #elementary charge (C)        
    mref_imas = 3.3435837724e-27 #Deuterium mass (kg)

    vthref_imas = np.sqrt(2*Tref_imas*qref_imas/mref_imas)
    rhoref_imas = mref_imas*vthref_imas/(qref_imas*Bref_imas)
    me_imas = 9.1093837015e-31 # electron mass (kg)

    # ... and the QLK normalisation parameters
    Lref_qlk = Lref_imas
    Bref_qlk = Bref_imas
    nref_qlk = 1e19 # m-3
    Tref_qlk = 1e3 # eV CAREFUL  : put the input temperature in keV
    qref_qlk = 1.602176565e-19 # C (according to Matisse, value taken from QuaLiKiz/src/datcal.f90) : weird to have a different value from IMAS...
    mref_qlk =1.672621777e-27 # proton mass (according to Matisse, value taken from QuaLiKiz/src/datcal.f90)

   
    ind_main_ion_qlk = 0 # we will find the index of the main ion (defined as the ion with the highest density)
    density_ion_stack = 0
    for ind in range(int(qlkrun.input_out['nions'])):
        if qlkrun.input_out['Zi'][ind] > density_ion_stack:
            density_ion_stack = qlkrun.input_out['Zi']
            ind_main_ion_qlk = ind
    mass_norm_main_ion_qlk = qlkrun.input_out['Ai'][ind_main_ion_qlk]* mref_imas/mref_qlk
    mass_main_ion_qlk = mref_qlk * mass_norm_main_ion_qlk

    vthref_qlk = np.sqrt(Tref_qlk*1e3*qref_qlk/mref_qlk)
    rhoref_qlk = mref_qlk * vthref_qlk/(qref_qlk*Bref_qlk)


    # Ratios of these normalisation parameters
    Lrat = Lref_qlk/Lref_imas # =1
    Brat = Bref_qlk/Bref_imas # =1
    nrat = nref_qlk/nref_imas 
    Trat = Tref_qlk/Tref_imas 
    qrat = qref_qlk/qref_imas 
    mrat = mref_qlk/mref_imas 

    vthrat = vthref_qlk/vthref_imas 
    rhorat = rhoref_qlk/rhoref_imas

    ### --------------------------------
    ### -----------IDS.model------------
    ### --------------------------------
    IDS_QLK.model.adiabatic_electrons = 0#qlkrun.input_out['typee'] # under which experimental considerations could elecrons be considered as adiabatic?
    IDS_QLK.model.include_a_field_parallel = 0 # same commentary
    IDS_QLK.model.include_b_field_parallel = 0 # same commentary
    #IDS_QLK.model.use_mhd_rule = 0 # same commentary NOT UPDATED 
    IDS_QLK.model.include_coriolis_drift = 0 # same commentary
    IDS_QLK.model.include_centrifugal_effects = 0 # same commentary
    IDS_QLK.model.collisions_pitch_only = 1 # same commentary
    IDS_QLK.model.collisions_momentum_conservation = 0 # same commentary
    IDS_QLK.model.collisions_energy_conservation = 0 # same commentary
    IDS_QLK.model.collisions_momentum_conservation = 0 # same commentary
    
    ### --------------------------------
    ### --------IDS.flux_surface--------
    ### --------------------------------
    IDS_QLK.flux_surface.ip_sign = 1 #according to J. Citrin (according to Matisse Lanzarone)
    IDS_QLK.flux_surface.b_field_tor_sign = 1
    IDS_QLK.flux_surface.r_minor_norm = qlkrun.input_out['Rmin'] # * qlkrun.input_out['x']/qlkrun.input_out['Ro'] / Lrat
    IDS_QLK.flux_surface.q = qlkrun.input_out['q']
    IDS_QLK.flux_surface.magnetic_shear_r_minor= qlkrun.input_out['smag'] # according to Matisse BUT maybe need normalisation with Rrat
    IDS_QLK.flux_surface.pressure_gradient_norm = qlkrun.input_out['alpha']*(Brat)**2/(Lrat * qlkrun.input_out['q'])**2
    IDS_QLK.flux_surface.dgeometric_axis_r_dr_minor = -qlkrun.input_out['alpha']
    IDS_QLK.flux_surface.dgeometric_axis_z_dr_minor = [0]
    IDS_QLK.flux_surface.elongation = [1] # the one of the flux surface under consideration
    IDS_QLK.flux_surface.delongation_dr_minor_norm = [0] # the one of the flux surface under consideration
    IDS_QLK.flux_surface.shape_coefficients_c = [0]
    IDS_QLK.flux_surface.shape_coefficients_s = [0]
    IDS_QLK.flux_surface.dc_dr_minor_norm = [0]
    IDS_QLK.flux_surface.ds_dr_minor_norm = [0]

    ### --------------------------------
    ### --------IDS.species_all---------
    ### --------------------------------
    IDS_QLK.species_all.velocity_tor_norm = qlkrun.input_out['Machtor']*vthref_qlk/(vthref_imas*Lrat)
    IDS_QLK.species_all.shearing_rate_norm = qlkrun.input_out['gammaE']*vthref_qlk/(vthref_imas*Lrat) 
    IDS_QLK.species_all.beta_reference = 0.0
    IDS_QLK.species_all.debye_length_norm = 0.0
    # IDS_QLK.species_all.angle_pol = ???

    ### --------------------------------
    ### ----------IDS.species-----------
    ### --------------------------------
    #velocity_tor_gradient_norm to update
    dum_e = ids_gyrokinetics_local.Species()
    dum_e.charge_norm = -1.0 # default param 
    dum_e.mass_norm = me_imas/mref_imas 
    dum_e.density_norm = 1.0
    dum_e.density_log_gradient_norm = qlkrun.input_out['Ane'] /Lrat
    dum_e.temperature_norm = qlkrun.input_out['Te'] / Trat 
    dum_e.temperature_log_gradient_norm = qlkrun.input_out['Ate'] /Lrat

    IDS_QLK.species.append(dum_e)

    nbr_ions = int(qlkrun.input_out['nions'])
    print(nbr_ions)
    for ii in range(nbr_ions):
        dum_i = ids_gyrokinetics_local.Species()
        dum_i.charge_norm = qlkrun.input_out['Zi'][ii] * qrat
        dum_i.mass_norm = qlkrun.input_out['Ai'][ii] * mrat
        dum_i.density_norm = qlkrun.input_out['normni'][ii] 
        dum_i.density_log_gradient_norm = qlkrun.input_out['Ani'][ii] /Lrat
        dum_i.temperature_norm = qlkrun.input_out['Ti'][ii] /Trat 
        dum_i.temperature_log_gradient_norm = qlkrun.input_out['Ati'][ii] /Lrat

        IDS_QLK.species.append(dum_i)

    

    ### --------------------------------
    ### --------IDS.collisions----------
    ### --------------------------------
    # Need to be updated
    IDS_QLK.collisions.collisionality_norm = [[0, 0],[0, 0]] #matrix number_species x number_species

    '''*******************************************************'''
    '''*********************---OUTPUTS---*********************'''
    '''*******************************************************'''
    # We don't consider the case where QuaLiKIz is used for scan then debug_parameter['dimx'] = 1

    ### --------------------------------
    ### ----------IDS.linear------------
    ### --------------------------------
    # useful for running GK codes such as QLK or QLK but it is stored in the 'outputs' region of the IDS
    nscan = int(qlkrun.input_out['dimx']) # =1 without scan and corresponds to len(x)
    nnumsols = int(qlkrun.input_out['numsols']) #seems to be the mode number
    nky = int(qlkrun.input_out['dimn'])
    for ii in range(nky):
        wavevect_ii = ids_gyrokinetics_local.Wavevector()
        wavevect_ii.binormal_wavevector_norm = qlkrun.input_out['kthetarhos'][ii] * rhorat #need main ion
        wavevect_ii.radial_wavevector_norm = 0

        # Matisse treatment
        # modewidth_module = np.abs(output_primitives_parameters['rmodewidth'] + 1j*output_primitives_parameters['imodewidth'])
        # center = -output_primitives_parameters['imodewidth'] * output_primitives_parameters['distan']/(modewidth_module**2)
        # fwhm = np.sqrt(2* np.log(2))*output_primitives_parameters['distan']/modewidth_module

        for jj in range(nnumsols):
            eigenmode_jj = ids_gyrokinetics_local.Eigenmode()
            # eigenmode_jj.angle_pol = np.linspace(2*np.round(center - 4*fwhm), int(2*np.round(center+4*fwhm)), 129) #not sure at all of the definition : see with Matisse
            # eigenmode_jj.poloidal_turns = np.round(4*fwhm/np.pi)
            # eigenmode_jj.initial_value_run = 0
            # eigenmode_jj.code.parameters = 'To do'
            # eigenmode_jj.code.output_flag = 0 #to do
            # WITH GAMMA SI
            eigenmode_jj.growth_rate_norm = qlkrun.input_out['gam_SI'][jj][ii]* Lref_imas/vthref_imas 
            eigenmode_jj.frequency_norm = -qlkrun.input_out['ome_SI'][jj][ii]* Lref_imas/vthref_imas 

            wavevect_ii.eigenmode.append(eigenmode_jj)

        IDS_QLK.linear.wavevector.append(wavevect_ii)


    return IDS_QLK
        
   