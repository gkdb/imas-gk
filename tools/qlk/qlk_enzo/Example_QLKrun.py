#--------------------------------------------------------
# This is a quick tutorial on how to use the QLKrun class
#--------------------------------------------------------

import QLKrun as qlkrun

# Define the path of the directory where QLK input and/or output files are located
# or specify the directory where you want your QLK simulation to be executed 
qlkpath = '~/path/to/QuaLiKiz'
runpath = '~/path/to/your/runs/folder'
runname = 'my_run_qlk'

qlkpath = '~/Documents/These/QuaLiKiz'
runpath = qlkpath + '/runs'

# Create an instance of the QLKrun class called 'test'
test = qlkrun.QLKrun()

# Set the storage path for QLK input and output files
test.set_storage(qlkpath = qlkpath, runpath = runpath, runname = runname)

# Modify input parameters as needed. For a comprehensive list of available input parameters in QLK,
# please refer to the official documentation at: https://gitlab.com/qualikiz-group/QuaLiKiz/-/wikis/QuaLiKiz/Input-and-output-variables
test.input['Ani'] = 10

# Write the modified input parameters to the input file
test.write_input()

# Run QLK using the input file located in the specified storage path
test.run()

# Read the available inputs and outputs from QLK files
test.read()

# Few remarks:
#
# in test.log is stored the warning/error messages generated when calling the class methods
# in test.input_out is stored all the input values used for the QLK simulation

