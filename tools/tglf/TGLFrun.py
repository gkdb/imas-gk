import os




class TGLFrun:
  """ Load and manipulate data from a TGLF run

     TGLFrun.storage                    -> path of TGLF input and outputs files
                                           filled by set_storage
            .log                        -> dict containing warning/error messages generated when calling the class methods
            .input                      -> content of TGLF input files input.tglf and out.tglf.globaldump
            .input_out                     filled by get_inputs
            .density_spectrum           -> content of out.tglf.density_spectrum
                                           filled by get_density_spectrum
            .eigenvalues                -> content of out.tglf.eigenvalue_spectrum
                                           filled by get_eigenvalue
            .field_spectrum             -> content of out.tglf.field_spectrum
                                           filled by get_field_spectrum
            .gbflux                     -> content of out.tglf.gbflux
                                           filled by get_gbflux
            .intensity_spectrum         -> content of out.tglf.intensity_spectrum
                                           filled by get_intensity_spectrum
            .ky                         -> content of out.tglf.ky_spectrum
                                           filled by get_ky_spectrum
            .nete_crossphase_spectrum   -> content of out.tglf.nete_crossphase_spectrum
                                           filled by get_nete_crossphase_spectrum
            .nsts_crossphase_spectrum   -> content of out.tglf.nsts_crossphase_spectrum
                                           filled by get_nsts_crossphase_spectrum
            .QL_flux_spectrum           -> content of out.tglf.QL_flux_spectrum
                                           filled by get_QL_flux_spectrum
            .QL_intensity_spectrum      -> content of out.tglf.QL_intensity_spectrum
                                           filled by get_QL_intensity_spectrum
            .sum_flux_spectrum          -> content of out.tglf.sum_flux_spectrum
                                           filled by get_sum_flux_spectrum
            .temperature_spectrum       -> content of out.tglf.temperature_spectrum
                                           filled by get_temperature_spectrum
            .version                    -> content of out.tglf.version
                                           filled by get_version
            .width                      -> content of out.tglf.width_spectrum
                                           filled by get_width_spectrum
            .wavefunction               -> content of out.tglf.wavefunction
                                           filled by get_wavefunction

     Other methods:
            .set_storage      -> Set the path of TGLF input and/or output files
            .write_input      -> write a TGLF input file using the data in .input
            .read             -> load available inputs and outputs calling all the 'get' functions
            .run              -> run TGLF using the input file located in self.storage['flpth']
    Other functions:
            convert_value     -> Function to convert values to appropriate types
  """
  def __init__(self):
    self.log={}
    self.settings={}
    self.settings['verbose']=True
    self.input = {}

 
  def convert_value(self, value):
    """ Function to convert values to appropriate types """
    if value.lower() in ['t', 'true', '.true.']:
      return True
    elif value.lower() in ['f', 'false', '.false.']:
      return False
    else:
      try:
        return float(value)
      except ValueError:
        return value  



  def set_storage(self,flpth):
    """ Get the path of TGLF input and output files 
    Inputs
      flpth        path to the run folder
    Outputs
      TGLFrun.storage['path']    dict containing the path to the various TGLF inputs and outputs
    """
    # checks
    if flpth[-1]!='/':
      flpth=flpth+'/'
    
    # store input values
    self.storage={}
    self.storage['flpth']=flpth
    
    # store path to TGLF inputs and outputs
    self.storage['path']={}
    self.storage['path']['input'] = flpth + 'input.tglf'
    self.storage['path']['globaldump']=flpth+ 'out.tglf.globaldump'
    self.storage['path']['density_spectrum']=flpth+ 'out.tglf.density_spectrum'
    self.storage['path']['eigenvalue_spectrum']=flpth+ 'out.tglf.eigenvalue_spectrum'
    self.storage['path']['field_spectrum']=flpth+ 'out.tglf.field_spectrum'
    self.storage['path']['gbflux']=flpth+ 'out.tglf.gbflux'
    self.storage['path']['intensity_spectrum']=flpth+ 'out.tglf.intensity_spectrum'
    self.storage['path']['ky_spectrum']=flpth+ 'out.tglf.ky_spectrum'
    self.storage['path']['nete_crossphase_spectrum']=flpth+ 'out.tglf.nete_crossphase_spectrum'
    self.storage['path']['nsts_crossphase_spectrum']=flpth+ 'out.tglf.nsts_crossphase_spectrum'
    self.storage['path']['QL_flux_spectrum']=flpth+ 'out.tglf.QL_flux_spectrum'
    self.storage['path']['QL_intensity_spectrum']=flpth+ 'out.tglf.QL_intensity_spectrum'
    self.storage['path']['spectral_shift_spectrum']=flpth+ 'out.tglf.spectral_shift_spectrum'
    self.storage['path']['sum_flux_spectrum']=flpth+ 'out.tglf.sum_flux_spectrum'
    self.storage['path']['temperature_spectrum']=flpth+ 'out.tglf.temperature_spectrum'
    self.storage['path']['version']=flpth+ 'out.tglf.version'
    self.storage['path']['width_spectrum']=flpth+ 'out.tglf.width_spectrum'
    self.storage['path']['wavefunction']=flpth+ 'out.tglf.wavefunction'
    self.storage['path']['scalar_saturation_parameters']=flpth+ 'out.tglf.scalar_saturation_parameters'
  

  def msg_log(self,parent,msg,verbose = None): 
    """ Generic routine to store and optionally display warning/error messages 
    Inputs    
      parent      name of the method for which a message needs to be stored/displayed 
      msg         message to be stored/displayed
      verbose     if True, displays the message on screen
    """
    if verbose is None:
        verbose = self.settings['verbose']
    if parent not in self.log.keys():
      self.log[parent]=[]
    self.log[parent].append(msg)
    if verbose:
      print(msg)


  def get_inputs(self):
      """ Load TGLF input files (both input.tglf and out.tglf.globaldump) in the following dicts:
          TGLFrun.input
          TGLFrun.input_out
      """
      self.input_out = {}
      # Read input.tglf file
      try:
        with open(self.storage['path']['input'], 'r') as f:
          for line in f:
            if '=' in line:
              key, value = line.strip().split('=')
              self.input[key.strip()] = self.convert_value(value.strip())
      except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_inputs', msg)
        
      # Read out.tglf.globaldump file
      try:
        with open(self.storage['path']['globaldump'], 'r') as f:
          for line in f:
            if '=' in line:
              key, value = line.strip().split('=')
              self.input_out[key.strip()] = self.convert_value(value.strip())
      except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_inputs', msg)


  def get_density_spectrum(self):
    """ Load density spectrum from out.tglf.density_spectrum in the following dict:
        TGLFrun.density_spectrum[species][ky]
    """
    self.density_spectrum = [[] for _ in range(int(self.input_out['NS']))]

    try:
        with open(self.storage['path']['density_spectrum'], 'r') as f:
            # Skip header lines
            for line in f:
                if not line.strip().startswith('('):
                    continue
                else:
                    break
            # Read data lines
            for line in f:
                if not line.strip():  # Skip empty lines
                    continue
                # Split the line and convert values to float
                values = [float(val) for val in line.split()]
                # Separate values into respective columns
                for i, val in enumerate(values):
                    self.density_spectrum[i].append(val)
    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_density_spectrum', msg)



  def get_eigenvalue(self):
    """ Load eigenvalue spectrum from out.tglf.eigenvalue_spectrum in the following dict:
        TGLFrun.eigenvalue['gamma'][mode][ky]
        TGLFrun.eigenvalue['freq'][mode][ky]
    """
    self.eigenvalue = {'gamma': [[] for _ in range(int(self.input_out['NMODES']))],
                       'freq': [[] for _ in range(int(self.input_out['NMODES']))]}

    try:
        with open(self.storage['path']['eigenvalue_spectrum'], 'r') as f:
            # Skip header lines
            for line in f:
                if not line.strip().startswith('('):
                    continue
                else:
                    break
            # Read data
            for line in f:
                data = line.strip().split()
                for i in range(len(data) // 2):
                    gamma = float(data[i * 2])
                    freq = float(data[i * 2 + 1])
                    self.eigenvalue['gamma'][i].append(gamma)
                    self.eigenvalue['freq'][i].append(freq)
    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_eigenvalue_spectrum', msg)



  def get_field_spectrum(self):
    """ Load field fluctuation intensity spectrum per mode from 
        out.tglf.field_spectrum in the following dict:
        TGLFrun.field_spectrum['A'][mode][ky]
        TGLFrun.field_spectrum['phi'][mode][ky]
        TGLFrun.field_spectrum['Bper'][mode][ky]
        TGLFrun.field_spectrum['Bpar'][mode][ky]
    """
    self.field_spectrum = {'A': [[] for _ in range(int(self.input_out['NMODES']))],
                           'phi': [[] for _ in range(int(self.input_out['NMODES']))],
                           'Bper': [[] for _ in range(int(self.input_out['NMODES']))],
                           'Bpar': [[] for _ in range(int(self.input_out['NMODES']))]}

    try:
        with open(self.storage['path']['field_spectrum'], 'r') as f:
            # Skip header lines
            header_read = False
            for line in f:
                if not header_read and line.strip().startswith('index limits:'):
                    nky, nmodes = map(int, next(f).strip().split())
                    header_read = True
                    break

            # Read data
            mode_idx = 0
            
            for line in f:
                if not 'b_par' in line.strip()  :
                    continue
                else:
                    break
            for line in f:
                data = line.strip().split()
                if mode_idx == nmodes:
                    mode_idx = 0
                self.field_spectrum['A'][mode_idx].append(float(data[0]))
                self.field_spectrum['phi'][mode_idx].append(float(data[1]))
                self.field_spectrum['Bper'][mode_idx].append(float(data[2]))
                self.field_spectrum['Bpar'][mode_idx].append(float(data[3]))
                mode_idx += 1

    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_field_spectrum', msg)



  def get_gbflux(self):
    """ Load gyro-Bohm normalised fluxes from out.tglf.gbflux in the following dict:
        TGLFrun.gbflux['Gamma'][species]
        TGLFrun.gbflux['Q'][species]
        TGLFrun.gbflux['Pi'][species]
        TGLFrun.gbflux['S'][species]
    """
    ns = int(self.input_out['NS'])
    flux_types = ['Gamma', 'Q', 'Pi', 'S']
    self.gbflux = {flux_type: [] for flux_type in flux_types}

    try:
        with open(self.storage['path']['gbflux'], 'r') as f:
            data = f.read().split()
            for i in range(len(data) // ns):
                for j in range(ns):
                    self.gbflux[flux_types[i]].append(float(data[i * ns + j]))
    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_gbflux', msg)




  def get_intensity_spectrum(self):
    """ Load gyro-bohm normalized fluctuation intensity spectra per mode from 
        out.tglf.intensity_spectrum in the following dict:
        TGLFrun.intensity_spectrum['density'][species][mode][ky]
        TGLFrun.intensity_spectrum['temperature'][species][mode][ky]
        TGLFrun.intensity_spectrum['parallel_velocity'][species][mode][ky]
        TGLFrun.intensity_spectrum['parallel_energy'][species][mode][ky]
    """
    self.intensity_spectrum = {'density': [[[] for _ in range(int(self.input_out['NMODES']))]
                                           for _ in range(int(self.input_out['NS']))],
                               'temperature': [[[] for _ in range(int(self.input_out['NMODES']))]
                                                for _ in range(int(self.input_out['NS']))],
                               'parallel_velocity': [[[] for _ in range(int(self.input_out['NMODES']))]
                                                     for _ in range(int(self.input_out['NS']))],
                               'parallel_energy': [[[] for _ in range(int(self.input_out['NMODES']))]
                                                   for _ in range(int(self.input_out['NS']))]}

    try:
        with open(self.storage['path']['intensity_spectrum'], 'r') as f:
            # Skip header lines
            header_read = False
            for line in f:
                if not header_read and line.strip().startswith('index limits:'):
                    ns, nky, nmodes = map(int, next(f).strip().split())
                    header_read = True
                    break

            # Read data
            specie_idx = 0
            mode_idx = 0
            ky_idx = 0
            for line in f:
                data = line.strip().split()
                self.intensity_spectrum['density'][specie_idx][mode_idx].append(float(data[0]))
                self.intensity_spectrum['temperature'][specie_idx][mode_idx].append(float(data[1]))
                self.intensity_spectrum['parallel_velocity'][specie_idx][mode_idx].append(float(data[2]))
                self.intensity_spectrum['parallel_energy'][specie_idx][mode_idx].append(float(data[3]))

                mode_idx += 1
                if mode_idx == nmodes:
                    mode_idx = 0
                    ky_idx += 1
                if ky_idx == nky:
                    specie_idx += 1
                    ky_idx = 0
    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_intensity_spectrum', msg)


  def get_ky_spectrum(self):
    """Load ky spectrum from out.tglf.ky_spectrum in the following dict:
        TGLFrun.ky
    """
    try:
        with open(self.storage['path']['ky_spectrum'], 'r') as f:
            # Skip header lines
            header_read = False
            for line in f:
                if not header_read and line.strip().startswith('index limits:'):
                    nky = int(next(f).strip())
                    header_read = True
                    break
            
            # Read ky values
            ky_values = [float(next(f).strip()) for _ in range(nky)]
            
            # Store ky values in self.ky
            self.ky = ky_values
            
    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_ky_spectrum', msg)
      


  def get_nete_crossphase_spectrum(self):
    """Load electron density-temperature cross phase spectra per mode from
       out.tglf.nete_crossphase_spectrum in the following dict:
       TGLFrun.nete_crossphase_spectrum[mode][ky]
    """
    self.nete_crossphase_spectrum = [[] for _ in range(int(self.input_out['NMODES']))]

    try:
        with open(self.storage['path']['nete_crossphase_spectrum'], 'r') as f:
            # Skip header lines
            for line in f:
                if not line.strip().startswith('('):
                    continue
                else:
                    break
            # Read data
            for line in f:
                data = line.strip().split()
                for i in range(len(data)):
                    self.nete_crossphase_spectrum[i].append(float(data[i]))

    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_nete_crossphase_spectrum', msg)




  def get_nsts_crossphase_spectrum(self):
    """Load density-temperature cross phase spectra per mode for each species from
       out.tglf.nsts_crossphase_spectrum in the following dict:
       TGLFrun.nsts_crossphase_spectrum[species][mode][ky]
    """
    self.nsts_crossphase_spectrum = [[[] for _ in range(int(self.input_out['NMODES']))] 
                                     for _ in range(int(self.input_out['NS']))]

    try:
        with open(self.storage['path']['nsts_crossphase_spectrum'], 'r') as f:
            species_idx = 0

            for line in f:
                if 'density-temperature' in line:
                    continue
                if 'species index' in line:
                    species_idx = int(line.strip().split('=')[1]) - 1
                    #self.nsts_crossphase_spectrum[species_idx] = [[] for _ in range(int(self.input_out['NMODES']))]
                    continue
                if line.strip().startswith('('):
                    continue
                # Read data
                data = line.strip().split()
                for i in range(len(data)):
                    self.nsts_crossphase_spectrum[species_idx][i].append(float(data[i]))


    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_nsts_crossphase_spectrum', msg)



  def get_QL_flux_spectrum(self):
    """Load QL flux spectrum from out.tglf.QL_flux_spectrum in the following dict:
       TGLFrun.QL_flux_spectrum[type][species][mode][field][ky]
       With : type = {'Gamma','Q', 'Pi_tor', 'Pi_par', 'S'}
              field = {'phi', 'Bper', 'Bpar'}
    """
    type_mapping = ['Gamma', 'Q', 'Pi_tor', 'Pi_par', 'S']
    field_mapping = ['phi', 'Bper', 'Bpar']

    self.QL_flux_spectrum = {}

    # Initialize the dictionary with empty lists for each combination
    for type_name in type_mapping:
        self.QL_flux_spectrum[type_name] = []
        for _ in range(int(self.input_out['NS'])):
            species_list = []
            for _ in range(int(self.input_out['NMODES'])):
                mode_dict = {}
                for field_name in field_mapping:
                    mode_dict[field_name] = []
                species_list.append(mode_dict)
            self.QL_flux_spectrum[type_name].append(species_list)

    try:
        with open(self.storage['path']['QL_flux_spectrum'], 'r') as f:
            # Skip header lines
            header_read = False
            for line in f:
                if not header_read and line.strip().startswith('index limits:'):
                    ntype, nspecies, nfields, nky, nmodes = map(int, next(f).strip().split())
                    header_read = True
                    break

            # Read data
            mode_idx = 0
            species_idx = 0
            field_idx = 0

            for line in f:
                if line.strip().startswith('species'):
                    species_idx = int(line.strip().split('=')[1].strip().split()[0]) - 1
                    field_idx = int(line.strip().split('=')[2].strip()) - 1
                    continue
                if line.strip().startswith('mode'):
                    mode_idx = int(line.strip().split('=')[1].strip()) - 1
                    continue

                # Read data
                data = line.strip().split()
                for i in range(len(data)):
                    type_name = type_mapping[i]
                    field_name = field_mapping[field_idx]
                    self.QL_flux_spectrum[type_name][species_idx][mode_idx][field_name].append(float(data[i]))

    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_QL_flux_spectrum', msg)




  def get_QL_intensity_spectrum(self):
    """Load QL flux spectrum from out.tglf.QL_intensity_spectrum in the following dict:
       TGLFrun.QL_intensity_spectrum[type][species][mode][ky]
       With : type = {'density','temperature', 'U', 'Q'}
    """
    type_mapping = ['density','temperature', 'U', 'Q']

    self.QL_intensity_spectrum = {}

    # Initialize the dictionary with empty lists for each combination
    for type_name in type_mapping:
        self.QL_intensity_spectrum[type_name] = []
        for _ in range(int(self.input_out['NS'])):
            species_list = []
            for _ in range(int(self.input_out['NMODES'])):
                mode = []
                species_list.append(mode)
            self.QL_intensity_spectrum[type_name].append(species_list)

    try:
        with open(self.storage['path']['QL_intensity_spectrum'], 'r') as f:
            # Skip header lines
            header_read = False
            for line in f:
                if not header_read and line.strip().startswith('index limits:'):
                    ntype, nspecies, nky, nmodes = map(int, next(f).strip().split())
                    header_read = True
                    break

            # Read data
            mode_idx = 0
            species_idx = 0

            for line in f:
                if line.strip().startswith('species'):
                    species_idx = int(line.strip().split('=')[1].strip()) - 1
                    continue
                if line.strip().startswith('mode'):
                    mode_idx = int(line.strip().split('=')[1].strip()) - 1
                    continue
                # Read data
                data = line.strip().split()
                for i in range(len(data)):
                    type_name = type_mapping[i]
                    self.QL_intensity_spectrum[type_name][species_idx][mode_idx].append(float(data[i]))

    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_QL_intensity_spectrum', msg)



     
  def get_sum_flux_spectrum(self):
    """Load Sum flux spectrum from out.tglf.sum_flux_spectrum in the following dict:
       TGLFrun.sum_flux_spectrum[type][species][field][ky]
       With : type = {'particle_flux', 'energy_flux', 'toroidal_stress','parallel_stress', 'exchange'}
              field = {'phi', 'Bper', 'Bpar'}
    """
    type_mapping = ['particle_flux', 'energy_flux', 'toroidal_stress',
                    'parallel_stress', 'exchange']
    field_mapping = ['phi', 'Bper', 'Bpar']
    self.sum_flux_spectrum = {}

    # Initialize the dictionary with empty lists for each combination
    for type_name in type_mapping:
        self.sum_flux_spectrum[type_name] = []
        for _ in range(int(self.input_out['NS'])):
            species_list = {}
            for field in field_mapping:
                species_list[field] = []
            self.sum_flux_spectrum[type_name].append(species_list)
    
    try:
        with open(self.storage['path']['sum_flux_spectrum'], 'r') as f:
            species_idx = 0
            field_idx = 0
            for line in f:
                if line.strip().startswith('particle'):
                    continue
                if line.strip().startswith('species'):
                    species_idx = int(line.strip().split('=')[1].strip().split()[0]) -1
                    field_idx = int(line.strip().split('=')[2].strip()) -1
                    continue
                # Read data
                data = line.strip().split()
                for i in range(len(data)):
                    type_name = type_mapping[i]
                    field_name = field_mapping[field_idx]
                    self.sum_flux_spectrum[type_name][species_idx][field_name].append(float(data[i]))

    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_sum_flux_spectrum', msg)



      
  def get_temperature_spectrum(self):
    """Load temperature spectrum from out.tglf.temperature_spectrum in the following dict:
       TGLFrun.temperature_spectrum[species][ky]
    """
    self.temperature_spectrum = [[] for _ in range(int(self.input_out['NS']))]

    try:
        with open(self.storage['path']['temperature_spectrum'], 'r') as f:
            for line in f:
                if not line.strip().startswith('('):
                    continue
                else:
                    break
            for line in f:
                # Read data
                data = line.strip().split()
                for i in range(len(data)):
                    self.temperature_spectrum[i].append(float(data[i]))

    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_temperature_spectrum', msg)



  def get_version(self):
    """Load temperature spectrum from out.tglf.version in the following dict:
       TGLFrun.version
    """
    self.version = {}
    nb_line = 0
    try:
        with open(self.storage['path']['version'], 'r') as f:
            for line in f:
                if nb_line == 0:
                    self.version['commit'] = line.strip()
                if nb_line == 1:
                    # Extract computer name
                    self.version['name_of_computer'] = line.strip()
                if nb_line == 2:
                    self.version['date'] = line
                nb_line += 1
    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_version', msg)


  

  def get_width_spectrum(self):
    """Load width spectrum from out.tglf.width_spectrum in the following dict:
       TGLFrun.width
    """
    self.width = []

    try:
        with open(self.storage['path']['width_spectrum'], 'r') as f:
            # Skip header lines
            header_read = False
            skip_next_line = False
            for line in f:
                if not header_read:
                    if line.strip().startswith('index limits:'):
                        header_read = True
                        skip_next_line = True
                    continue
                elif skip_next_line:
                    skip_next_line = False
                    continue
                else:
                    if line.strip():  # Check if the line is not empty
                        self.width.append(float(line.strip()))

    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_width_spectrum', msg)




  def get_wavefunction(self):
    """Load wavefunctions structures from out.tglf.wavefunction in the following dict:
       TGLFrun.wavefunction[field][mode][theta]
       With : field = {'phi', 'Bper', 'Bpar'} and the angle theta in :
              TGLFrun.wavefunction['theta']
    """
    self.wavefunction = {}

    try:
        with open(self.storage['path']['wavefunction'], 'r') as f:
            # Read the first line containing the numbers of modes, fields, and theta
            nmodes, nfields, ntheta = map(int, next(f).split())

            # Read the second line containing the field values
            field_line = next(f).split()
            fields = field_line[1::2]  # Extract field names from the line
            fields = []
            for j in field_line :
                if 'phi' in j and 'phi' not in fields:
                    fields.append('phi')
                if 'Bper' in j and 'Bper' not in fields:
                    fields.append('Bper')
                if 'Bpar' in j and 'Bpar' not in fields:
                    fields.append('Bpar')
            # Initialize wavefunction dictionary
            self.wavefunction['theta'] = []
            for field in fields:
                self.wavefunction[field] = [[] for _ in range(nmodes)]

            # Read data
            for line in f:
                data = list(map(float, line.split()))
                theta = data[0]
                self.wavefunction['theta'].append(theta)
                for i_mode in range(nmodes):
                    for i_field, field in enumerate(fields):
                        real_idx = 1 + 2 * (nfields * i_mode + i_field)
                        imag_idx = real_idx + 1
                        real_part = data[real_idx]
                        imag_part = data[imag_idx]
                        self.wavefunction[field][i_mode].append(complex(real_part, imag_part))

    except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        msg2 = "\n In TGLF to obtain the file out.tglf.wavefunction you need to \n\
 run TGLF with the input USE_TRANSPORT_MODEL set to false \n"
        self.msg_log('get_wavefunction', msg+msg2)

  def get_scalar_saturation_parameters(self):
      """Load saturation rule parameters from out.tglf.scalar_saturation_parameters in the following dict:
         TGLFrun.scalar_saturation_parameters
      """
      self.scalar_saturation_parameters = {}        
      # Read out.tglf.scalar_saturation_parameters file
      try:
        with open(self.storage['path']['scalar_saturation_parameters'], 'r') as f:
          for line in f:
            if '=' in line:
              key, value = line.strip().split('=')
              self.scalar_saturation_parameters[key.strip()] = self.convert_value(value.strip())
      except FileNotFoundError as err:
        msg = "Warning: file " + err.filename + " not found."
        self.msg_log('get_scalar_saturation_parameters', msg)

  def write_input(self):
    """Write a TGLF input file from the content of TGLFrun.input
    """
    
    # Check if folder exists, create it if it doesn't
    if not os.path.exists(self.storage['flpth']):
        os.makedirs(self.storage['flpth'])
        self.msg_log('write_input', f"Folder '{self.storage['flpth']}' created.",verbose = False)

    # Check if input file exists in the folder, create it if it doesn't
    if not os.path.exists(self.storage['path']['input']):
        with open('input.tglf', 'w') as f:
            f.write("")
   
    try:
        with open(self.storage['path']['input'], 'w') as f:
            for key, value in self.input.items():
                # Check if the value is a boolean
                if isinstance(value, bool):
                    # If it's a boolean, write True or False instead of 1 or 0
                    f.write(f"{key} = {str(value)}\n")
                else:
                    try:
                        # Try to convert the value to an integer
                        int_value = int(value)
                        # Check if the converted integer is equal to the original value
                        if int_value == value:
                            # If the conversion didn't lose precision, write the integer value
                            f.write(f"{key} = {int_value}\n")
                        else:
                            # Otherwise, write the original value
                            f.write(f"{key} = {value}\n")
                    except ValueError:
                        # If conversion to an integer fails, write the original value to the file
                        f.write(f"{key} = {value}\n")
    except FileNotFoundError as err:
        msg = f"Warning: file {err.filename} not found."
        self.msg_log('write_input', msg)

  def read(self,eigenfunction=True):    
    """ Load available TGLF input and output data
    Inputs
       eigenfunction  If True, re-run TGLF with USE_TRANSPORT_MODEL set to false
                      to obtain the file out.tglf.wavefunction and then read it
     """
    self.get_inputs()
    self.get_density_spectrum()
    self.get_eigenvalue()
    self.get_field_spectrum()
    self.get_gbflux()
    self.get_intensity_spectrum()
    self.get_ky_spectrum()
    self.get_nete_crossphase_spectrum()
    self.get_nsts_crossphase_spectrum()
    self.get_QL_flux_spectrum()
    self.get_QL_intensity_spectrum()
    self.get_sum_flux_spectrum()
    self.get_temperature_spectrum()
    self.get_version()
    self.get_width_spectrum()
    self.get_scalar_saturation_parameters()

    if eigenfunction:
      self.input['USE_TRANSPORT_MODEL'] = False
      self.write_input()
      self.run()
      self.get_wavefunction()




  def run(self):
    """Run TGLF simulation
    """
    # Navigate to the TGLF simulation directory
    bash = ['cd {}'.format(self.storage['flpth']),
            'tglf -e .']
    commands = '; '.join(bash)
    # Execute the commands in the shell
    execution = os.popen(commands)
    self.msg_log('run', execution.read(), verbose=False)









