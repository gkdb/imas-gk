"""
IN CONSTRUCTION: doesn't work currently -> need to be melted with the TGLFimas.py file. Are added:
- the imaspy ids in output with the fonctions either tglf2ids_gyrokinetic_local for gyrokinetic_local IDS structure or tglf2ids_imaspy for the imaspy IDS format
- filters on low values for the eigenfunctions (cf the two fonctions cited above)
- NEED TO RE-IMPLEMANT THE ALPHA GEOMETRY
"""
"""
This file contains functions to convert data between IMAS 'gyrokinetics' IDS
and TGLF inputs and outputs.

Functions:
    
- ids2tglf(gkids, RMAJ_LOC=None, verbose=False): 
    Convert an IMAS 'gyrokinetics' IDS to a TGLF input.
    Inputs:
        gkids: Instance of the idspy class.
        RMAJ_LOC: (Optional) Value for the TGLF's normalized major radius.
                  Can be specified if needed for a particular value.
                  Defaults to 1 since quantities are normalized.
        verbose: (Optional) Whether to print warnings. Defaults to False.
    Outputs:
        tglfrun: Instance of the TGLFrun class gathering all the TGLF inputs generated from the IDS.
                 Inputs are stored in tglfrun.input

- tglf2ids(tglfrun, provider=''): 
    Convert TGLF inputs and outputs to an IMAS 'gyrokinetics' IDS created with idspy.
    Inputs:
        tglfrun: Instance of the TGLFrun class containing TGLF input and output data.
        provider: (Optional) Provider information. Defaults to an empty string.
    Outputs:
        gkids: Instance of the GKIDS class containing the generated 'gyrokinetics' IDS.
    
Note: This file requires the idspy library for IMAS data manipulation.
"""

from scipy import integrate

import sys
import os
import numpy as np
from scipy.integrate import cumtrapz,trapz
import scipy.constants as codata
from scipy.interpolate import interp1d

import TGLFrun as tglfrun
import idspy_toolkit as idspy
from idspy_dictionaries import ids_gyrokinetics_local

# Import the file FS_param.py from the repository IMAS-GK 
# that can be found at  https://gitlab.com/gkdb/imas-gk/-/blob/main/gkids/tools/FS_param.py
if not '/home/anass/codes/imas-gk/gkids/tools' in sys.path :
    sys.path.append(os.path.relpath("imas-gk/gkids/tools"))
import FS_param

# imaspy add
try:
  import imaspy
  imaspy_loaded=False
except:
  print("IMPOSSIBLE TO LOAD IMASPY")
  imaspy_loaded=False



def ids2tglf(gkids,RMAJ_LOC = None,verbose=False):
  """ Convert an IMAS 'gyrokinetics' IDS to a TGLF input
  Inputs
    gkids       Instance of the idspy GKIDS class containing the reference 'gyrokinetics' IDS
  Outputs
    tglfrun     Instance of the TGLFrun class gathering all the TGLF inputs generated from the IDS
  """
  # Fix a value for RMAJ_LOC, it is mandatory because this value will be 
  # used to make links between TGLF and IMAS normalisations
  if not RMAJ_LOC:
    RMAJ_LOC = 1.0
    if gkids.code.name == 'TGLF':
      if len(gkids.non_linear.code.parameters) > 0:
        RMAJ_LOC = gkids.non_linear.code.parameters['RMAJ_LOC']
  
  # Check if the ids can be simulated properly by TGLF
  if gkids.model.include_centrifugal_effects == 1:
    if verbose:
      print("Warning: Centrifugal effects can't be simulated in TGLF\n\
            ids.model.include_centrifugal_effects should be 0")
  if (gkids.model.collisions_pitch_only == 0
      or gkids.model.collisions_momentum_conservation == 1
      or gkids.model.collisions_energy_conservation == 1
      or gkids.model.collisions_finite_larmor_radius == 1):
    if verbose:
      print("Warning: ids.model parameters can't be simulated with TGLF\n"
                "In TGLF, the values of:\n"
                "collisions_momentum_conservation\n"
                "collisions_energy_conservation\n"
                "collisions_finite_larmor_radius\n"
                "Are false, and 'collisions_pitch_only' is true")


  # Value of dr/dx used in tglf
  DRMINDX_LOC = 1.0
  # Obtain the Miller parametrisation coefficients from the ids input values
  miller = mxh_to_miller_idspy(gkids)
  # compute TGLF_ref/IMAS_ref ratio of reference quantities for TGLf to IMAS conversion
  # reference charge ratio
  q_tglf = 1.6021746e-19      # at: July 29, 2022 See: https://gafusion.github.io/doc/tglf.html
  q_imas = codata.physical_constants['elementary charge'][0] #(IMAS follows CODATA from NIST)
  q_rat = q_tglf / q_imas
  # reference mass ratio
  m_tglf = 3.345e-27          # at: Last July 29, 2022 See: https://gafusion.github.io/doc/tglf.html
  m_imas = codata.physical_constants['deuteron mass'][0] #(IMAS follows CODATA from NIST)
  m_rat = m_tglf / m_imas
  # reference temperature ratio
  T_rat = 1
  # reference length ratio
  L_rat = 1 / RMAJ_LOC
  # reference density ratio
  n_rat = 1
  # reference thermal velocity ratio
  v_thrat = (1 / np.sqrt(2)) * (np.sqrt(T_rat / m_rat))
  # reference magnetic field ratio
  B_rat = get_Brat_ids_tglf(miller, gkids.flux_surface.q)
  # reference Larmor radius ratio
  rho_rat = (m_rat * v_thrat) / (q_rat * B_rat)
  # Calculation of gthth
  metric = get_metric_idspy(gkids, np.linspace(0, 2 * np.pi, 501))
  Gq = metric['Gq'][0]
  gradr = metric['grad_r'][0]

  # Fill input dict
  inputs = {
      'GEOMETRY_FLAG': 1,
      'WRITE_WAVEFUNCTION_FLAG': 1,
      'RMIN_LOC': miller['r'] / L_rat,
      'RMAJ_LOC': miller['Rmil'] / L_rat,
      'ZMAJ_LOC': miller['Zmil'] / L_rat,
      'Q_LOC': abs(gkids.flux_surface.q),
      'Q_PRIME_LOC': gkids.flux_surface.magnetic_shear_r_minor
      * (gkids.flux_surface.q / (miller['r'] / L_rat)) ** 2,
      'P_PRIME_LOC': gkids.flux_surface.pressure_gradient_norm
      * (abs(gkids.flux_surface.q))
      / ((miller['r'] / L_rat) * (-8 * np.pi) * (B_rat ** 2 / L_rat)),
      'DRMINDX_LOC': DRMINDX_LOC,
      'DRMAJDX_LOC': miller['dRmildr'] * DRMINDX_LOC,
      'DZMAJDX_LOC': miller['dZmildr'] * DRMINDX_LOC,
      'KAPPA_LOC': miller['k'],
      'S_KAPPA_LOC': miller['sk'],
      'DELTA_LOC': miller['d'],
      'S_DELTA_LOC': miller['sd'],
      'ZETA_LOC': miller['z'],
      'S_ZETA_LOC': miller['sz'],
      'SIGN_IT': gkids.flux_surface.ip_sign,
      'SIGN_BT': gkids.flux_surface.b_field_tor_sign,
      'USE_BPER': bool(gkids.model.include_a_field_parallel),
      'USE_BPAR': False, # NOT in use in TGLF, The BPAR flutter is not well resolved by TGLF 
      'USE_MHD_RULE': True,  # (enforced in JETTO v060619)
      'BETAE': gkids.species_all.beta_reference * 1.5 * n_rat * T_rat / (B_rat ** 2),
      'NKY': 19,
      'NMODES': 2, 
      'SAT_RULE': 2,
      'NS': len(gkids.species),
      'UNITS': 'CGYRO',
      'NBASIS_MIN': 2,
      'NBASIS_MAX': 6,
      'VEXB_SHEAR': gkids.species_all.shearing_rate_norm *\
          (L_rat/(v_thrat*B_rat))*gkids.flux_surface.ip_sign,
      'DEBYE': (1/rho_rat)*np.sqrt(T_rat/(n_rat*q_rat**2))*gkids.species_all.debye_length_norm}
      
      
  # Calculation of KY
  # Note that if ky > 10 KYGRID_MODEL is set to either 1 or 4
  ky_max = 0.
  if len(gkids.non_linear.binormal_wavevector_norm) > 0 :
    for i in range(len(gkids.non_linear.binormal_wavevector_norm)):
      if gkids.non_linear.binormal_wavevector_norm[i] > ky_max:
        ky_max = gkids.non_linear.binormal_wavevector_norm[i]
  elif len(gkids.linear.wavevector) > 0 :
    for i in range(len(gkids.linear.wavevector)):
      if gkids.linear.wavevector[i].binormal_wavevector_norm > ky_max:
        ky_max = gkids.linear.wavevector[i].binormal_wavevector_norm
  else:
    ky_max = 20
  inputs['KY'] = round(ky_max * rho_rat / abs(Gq),4)
  if ky_max > 20 :
    if inputs['SAT_RULE'] == 3:
      inputs['KYGRID_MODEL'] = 4
    else:
      inputs['KYGRID_MODEL'] = 1
  else:
      inputs['KY'] = round(ky_max * rho_rat / abs(Gq),4)
      inputs['KYGRID_MODEL'] = 0
  # Checks if the ids was generated from TGLF and if so, takes the used parameters
  if gkids.code.name == 'TGLF':
    if len(gkids.non_linear.code.parameters) > 0:
      inputs['KY'] = gkids.non_linear.code.parameters['KY']
      inputs['NKY'] = int(gkids.non_linear.code.parameters['NKY'])
      inputs['KX0_LOC'] = gkids.non_linear.code.parameters['KX0_LOC']
      inputs['KYGRID_MODEL'] = gkids.non_linear.code.parameters['KYGRID_MODEL']
      
    
    
  # Find electron/ion collision frequency
  density_max = 0
  for i in range(len(gkids.species)):
    charge = gkids.species[i].charge_norm
    density = gkids.species[i].density_norm
    if charge < 0:
      index_electron = i
    if (density > density_max) and (charge > 0):
      density_max = density
  # Computation of ZEFF and XNUE
  zeff = 0
  xnue = 0
  for i in range(len(gkids.species)):
    if gkids.species[i].charge_norm > 0:
      if i != index_electron:
        zeff = zeff + gkids.species[i].density_norm * gkids.species[i].charge_norm ** 2
        xnue = xnue + gkids.collisions.collisionality_norm[index_electron][i]
  inputs['ZEFF'] = zeff
  inputs['XNUE'] = (1 / zeff) * (L_rat / v_thrat) * xnue
  # Electron species should be first
  list_species_index = np.insert(np.delete(np.arange(len(gkids.species)), index_electron), 0, index_electron)
  # Compute the species inputs
  for i in range(len(gkids.species)):
      charge_name = 'ZS_' + str(i + 1)
      charge = gkids.species[list_species_index[i]].charge_norm / q_rat
      inputs[charge_name] = np.round(charge)

      mass_name = 'MASS_' + str(i + 1)
      mass = gkids.species[list_species_index[i]].mass_norm / m_rat
      inputs[mass_name] = mass

      density_name = 'AS_' + str(i + 1)
      density = gkids.species[list_species_index[i]].density_norm / n_rat
      inputs[density_name] = density

      rln_name = 'RLNS_' + str(i + 1)
      rln = gkids.species[list_species_index[i]].density_log_gradient_norm * L_rat
      inputs[rln_name] = rln

      temperature_name = 'TAUS_' + str(i + 1)
      temperature = gkids.species[list_species_index[i]].temperature_norm / T_rat
      inputs[temperature_name] = temperature

      rlt_name = 'RLTS_' + str(i + 1)
      rlt = gkids.species[list_species_index[i]].temperature_log_gradient_norm * L_rat
      inputs[rlt_name] = rlt

      Vpar_name = 'VPAR_' + str(i + 1)
      Vpar = gkids.flux_surface.ip_sign * gkids.species_all.velocity_tor_norm * L_rat / v_thrat
      inputs[Vpar_name] = Vpar

      VparS_name = 'VPAR_SHEAR_' + str(i + 1)
      VparS = gkids.flux_surface.ip_sign * gkids.species[list_species_index[i]].velocity_tor_gradient_norm *\
          (L_rat) / v_thrat
      inputs[VparS_name] = VparS


  tglf = tglfrun.TGLFrun()
  tglf.input = inputs
  
  return tglf
  
  





# def tglf2ids(tglfrun,provider=''):
#   """ Convert GKW inputs and outputs to an IMAS 'gyrokinetics' IDS created with idspy
#     Inputs
#       tglfrun    Instance of the TGLFrun class containing TGLF input and output data
#     Outputs
#       gkids      Instance of the GKIDS class containing the generated 'gyrokinetics' IDS
  
#     Warning and error logs are stored in gkwrun.log
#   """
#   # initialise an empty GK IDS structure
#   gkids=ids_gyrokinetics_local.GyrokineticsLocal()
#   idspy.fill_default_values_ids(gkids)
  
#   ## ids_properties
#   gkids.ids_properties.provider = provider
#   gkids.ids_properties.creation_date = tglfrun.version['date']
#   gkids.ids_properties.homogeneous_time = 2
  
#   ## code 
#   gkids.code.name="TGLF"
#   gkids.code.commit = tglfrun.version['commit']
#   gkids.code.repository="https://gafusion.github.io/doc/tglf"
  
#   ## model
#   gkids.model.adiabatic_electrons=int(tglfrun.input_out['ADIABATIC_ELEC'])
#   gkids.model.include_a_field_parallel=int(tglfrun.input_out['USE_BPER'])
#   gkids.model.include_b_field_parallel=int(tglfrun.input_out['USE_BPAR'])
#   gkids.model.include_coriolis_drift=int(False)
#   gkids.model.include_centrifugal_effects=int(False)
#   gkids.model.collisions_pitch_only = int(True) 
#   gkids.model.collisions_momentum_conservation = int(False)
#   gkids.model.collisions_energy_conservation = int(False)
#   gkids.model.collisions_finite_larmor_radius = int(False)
#   gkids.model.include_full_curvature_drift = int(not tglfrun.input_out['USE_MHD_RULE'])
  
  
#   # Obtain the Miller eXtended Harmonics (MXH) parametrisation coefficients from the TGLF input values
#   mhx = miller_to_mxh(tglfrun)    
#   # compute TGLF_ref/IMAS_ref ratio of reference quantities for TGLf to IMAS conversion
#   # reference charge ratio
#   q_rat = 1
#   # reference mass ratio
#   m_rat = 1
#   # reference temperature ratio
#   T_rat = 1
#   # reference length ratio
#   L_rat =  1/tglfrun.input_out['RMAJ_LOC'] 
#   # reference magnetic field ratio
#   B_rat = get_B_rat(tglfrun)
#   # reference density ratio
#   n_rat = 1
#   # reference thermal velocity ratio
#   v_thrat = (1/np.sqrt(2))*(np.sqrt(T_rat/m_rat))
#   # reference Larmor radius ratio
#   rho_rat = (m_rat*v_thrat)/(q_rat*B_rat)
  
#   # compute theta_imas=f(theta_tglf)
#   th_tglf = np.array(tglfrun.wavefunction['theta'])
#   nb_pol_trurns = (max(th_tglf)-min(th_tglf))/(2*np.pi)
#   R_dum = tglfrun.input_out['RMAJ_LOC'] + tglfrun.input_out['RMIN_LOC']* \
#       np.cos(th_tglf + np.arcsin(tglfrun.input_out['DELTA_LOC'])*np.sin(th_tglf))
#   Z_dum = tglfrun.input_out['KAPPA_LOC']*tglfrun.input_out['RMIN_LOC']* \
#       np.sin(th_tglf + tglfrun.input_out['ZETA_LOC']*np.sin(2*th_tglf))
#   th = np.arctan2((Z_dum),R_dum-tglfrun.input_out['RMAJ_LOC'])
#   dum = np.cumsum(np.concatenate(([False],abs(np.diff(th))>np.pi)))
#   dum = dum - ((nb_pol_trurns-1)/2. )
#   th_imas_of_tglf = th + 2*np.pi*dum
#   th_sorted = th_imas_of_tglf
  
#   ## flux_surface
#   gkids.flux_surface.ip_sign = tglfrun.input_out['SIGN_IT']
#   gkids.flux_surface.b_field_tor_sign = tglfrun.input_out['SIGN_BT']
#   gkids.flux_surface.r_minor_norm = tglfrun.input_out['RMIN_LOC'] * L_rat
#   gkids.flux_surface.q = tglfrun.input_out['SIGN_BT']*\
#       tglfrun.input_out['SIGN_IT']*tglfrun.input_out['Q_LOC']
#   gkids.flux_surface.magnetic_shear_r_minor = (tglfrun.input_out['RMIN_LOC']/\
#       abs(tglfrun.input_out['Q_LOC']))**2 * tglfrun.input_out['Q_PRIME_LOC']
#   gkids.flux_surface.pressure_gradient_norm = (-8*np.pi)*(B_rat**2/L_rat)*\
#       (tglfrun.input_out['RMIN_LOC']/(tglfrun.input_out['Q_LOC'])) *\
#       tglfrun.input_out['P_PRIME_LOC']
#   gkids.flux_surface.dgeometric_axis_r_dr_minor = mhx['dR0dr']
#   gkids.flux_surface.dgeometric_axis_z_dr_minor = mhx['dZ0dr']
#   gkids.flux_surface.elongation = mhx['k']
#   gkids.flux_surface.delongation_dr_minor_norm = mhx['dkdr']
#   gkids.flux_surface.shape_coefficients_c = mhx['c']
#   gkids.flux_surface.dc_dr_minor_norm = mhx['dcdr']
#   gkids.flux_surface.shape_coefficients_s = mhx['s']
#   gkids.flux_surface.ds_dr_minor_norm = mhx['dsdr']
  
#   ## species
#   number_of_species = int(tglfrun.input_out['NS'])
#   for i in range(number_of_species):
#     dum=ids_gyrokinetics_local.Species()
#     dum.charge_norm = tglfrun.input_out['ZS_'+str(i+1)]*q_rat
#     dum.mass_norm = tglfrun.input_out['MASS_'+str(i+1)]*m_rat
#     dum.density_norm = tglfrun.input_out['AS_'+str(i+1)]*n_rat
#     dum.density_log_gradient_norm = tglfrun.input_out['RLNS_'+str(i+1)]/L_rat
#     dum.temperature_norm = tglfrun.input_out['TAUS_'+str(i+1)]*T_rat
#     dum.temperature_log_gradient_norm = tglfrun.input_out['RLTS_'+str(i+1)]/L_rat
#     dum.velocity_tor_gradient_norm = tglfrun.input_out['VPAR_SHEAR_'+str(i+1)]/(L_rat)*v_thrat*tglfrun.input_out['SIGN_IT']
#     gkids.species.append(dum)
  
#   ## species_all
#   gkids.species_all.angle_pol = []
#   gkids.species_all.debye_length_norm = tglfrun.input_out['DEBYE']*rho_rat*np.sqrt((n_rat*q_rat**2)/T_rat)
#   gkids.species_all.beta_reference = tglfrun.input_out['BETAE']*(B_rat**2)/(1.5*n_rat*T_rat) 
#   gkids.species_all.velocity_tor_norm = tglfrun.input_out['SIGN_IT']*tglfrun.input_out['VPAR_2']*(v_thrat/L_rat)
#   gkids.species_all.shearing_rate_norm = tglfrun.input_out['SIGN_IT']*tglfrun.input_out['VEXB_SHEAR']*(v_thrat/L_rat)*B_rat
  
#   ## collisions
#   # Find electron/main ion index
#   density_max = 0.
#   for i in range(number_of_species):
#     charge = gkids.species[i].charge_norm
#     density = gkids.species[i].density_norm
#     if charge < 0.:
#         index_electron = i
#     if (density > density_max) and (charge > 0.):
#         density_max = density
#         index_main_ion = i
#   gkids.collisions.collisionality_norm = np.full((number_of_species,number_of_species),0.0)
#   gkids.collisions.collisionality_norm[index_electron][index_main_ion] = tglfrun.input_out['XNUE']*(v_thrat/L_rat)
  
  
#   ## linear
#   gkids.linear=ids_gyrokinetics_local.GyrokineticsLinear()
  
#   ## wavevector
#   number_of_ky = int(len(tglfrun.ky))
#   number_of_modes = int(tglfrun.input_out['NMODES'])

#   metric = get_metric_idspy(gkids,np.linspace(0,2*np.pi,501))
#   Gq = metric['Gq'][0]
#   gradr = metric['grad_r'][0]
#   for iw in range(number_of_ky):
#     dumW=ids_gyrokinetics_local.Wavevector()
#     dumW.binormal_wavevector_norm = tglfrun.ky[iw]*abs(Gq)*(1/rho_rat)
#     dumW.radial_wavevector_norm = tglfrun.input_out['KX0_LOC']*tglfrun.ky[iw]*(1/rho_rat)*abs(gradr)
    
#     ## eigenmodes
#     for im in range(number_of_modes):
#       dum = ids_gyrokinetics_local.Eigenmode()
#       dum.poloidal_turns = int(nb_pol_trurns)
#       dum.angle_pol = th_sorted
#       dum.time_norm = []
#       dum.initial_value_run = int(False)
      
#       dum.growth_rate_norm = tglfrun.eigenvalue['gamma'][im][iw]*(v_thrat/L_rat)
#       dum.frequency_norm = -1*tglfrun.eigenvalue['freq'][im][iw]*(v_thrat/L_rat)
#       dum.growth_rate_tolerance = 0
      
#       ## code (filled only for the last wavevector)
#       if iw == (number_of_ky-1):
#         dum.code = ids_gyrokinetics_local.CodePartialConstant()
#         dum.code.parameters = tglfrun.input_out
#         dum.code.output_flag = 0
      
#       ## fields (filled only for the last wavevector and modes with positive growth rate)
#       if (iw == (number_of_ky-1) and im < len(tglfrun.wavefunction['phi'])):
        
#         fields={'name': ('phi','Bper','Bpar'),
#                 'is_field': (True,tglfrun.input_out['USE_BPER'],tglfrun.input_out['USE_BPAR']),
#                 'norm_fac': ((T_rat*rho_rat )/(q_rat*L_rat), (B_rat*rho_rat**2 )/(L_rat),(B_rat*rho_rat )/(L_rat))
#                 }
        
#         imas_eigenfunctions = {}
#         imas_eigenfunctions['phi'], imas_eigenfunctions['Bper'], imas_eigenfunctions['Bpar'] = np.zeros((3,len(dum.angle_pol)))
        
#         for ii, fields_ in enumerate(fields['name']):
#           if fields['is_field'][ii] :
#               imas_eigenfunctions[fields_] = np.array(tglfrun.wavefunction[fields_][im]) * fields['norm_fac'][ii]
        
#         epsilon = 1e-100  # Threshold

#         phi_cleaned = np.where(np.abs(imas_eigenfunctions['phi']) > epsilon, imas_eigenfunctions['phi'], 0)
#         Bper_cleaned = np.where(np.abs(imas_eigenfunctions['Bper']) > epsilon, imas_eigenfunctions['Bper'], 0)
#         Bpar_cleaned = np.where(np.abs(imas_eigenfunctions['Bpar']) > epsilon, imas_eigenfunctions['Bpar'], 0)

#         # Calcul de l'amplitude si la condition est remplie
#         if np.sqrt(integrate.simps(np.abs(phi_cleaned)**2, dum.angle_pol)) != 0:
#             amp_rat = np.sqrt(2 * np.pi) / np.sqrt(
#                 integrate.simps(np.abs(phi_cleaned)**2 
#                                 + np.abs(Bper_cleaned)**2 
#                                 + np.abs(Bpar_cleaned)**2, dum.angle_pol)
#             )
#         else:
#             amp_rat = 0

#         # rotate and normalise eigenfunctions
#         # rotate_factor=exp(i*alpha) in IMAS documentation
#         th_fine = np.linspace(th_sorted[0],th_sorted[-1],len(th_sorted)*10)
#         f_re=interp1d(th_sorted,np.real(phi_cleaned),kind='cubic')
#         f_im=interp1d(th_sorted,np.imag(phi_cleaned),kind='cubic')
#         Imax=np.argmax(np.abs(f_re(th_fine) + 1j*f_im(th_fine)))
#         rotate_factor = f_re(th_fine)[Imax] + 1j*f_im(th_fine)[Imax]
#         if rotate_factor != 0:
#             rotate_factor = abs(rotate_factor)/rotate_factor
#         else:
#             rotate_factor = 0
        
#         dum.fields=ids_gyrokinetics_local.EigenmodeFields()
        
#         if fields['is_field'][0]:
#           dum.fields.phi_potential_perturbed_norm = [phi_cleaned*rotate_factor*amp_rat]
#           dum.fields.phi_potential_perturbed_weight = [np.sqrt(np.trapz(np.abs(dum.fields.phi_potential_perturbed_norm[0])**2,dum.angle_pol,axis=0)/(2*np.pi))]
#           dum.fields.phi_potential_perturbed_parity = [(np.abs(np.trapz(dum.fields.phi_potential_perturbed_norm[0],dum.angle_pol,axis=0))
#                                                / np.trapz(np.abs(dum.fields.phi_potential_perturbed_norm[0]),dum.angle_pol,axis=0))]
#         if fields['is_field'][1]:
#           dum.fields.a_field_parallel_perturbed_norm = [-1*Bper_cleaned*rotate_factor*amp_rat]
#           dum.fields.a_field_parallel_perturbed_weight = [np.sqrt(np.trapz(np.abs(dum.fields.a_field_parallel_perturbed_norm[0])**2,dum.angle_pol,axis=0)/(2*np.pi))]
#           dum.fields.a_field_parallel_perturbed_parity = [(np.abs(np.trapz(dum.fields.a_field_parallel_perturbed_norm[0],dum.angle_pol,axis=0))
#                                                / np.trapz(np.abs(dum.fields.a_field_parallel_perturbed_norm[0]),dum.angle_pol,axis=0))]
#         if fields['is_field'][2]:
#           dum.fields.b_field_parallel_perturbed_norm = [-1*Bpar_cleaned*rotate_factor*amp_rat]
#           dum.fields.b_field_parallel_perturbed_weight = [np.sqrt(np.trapz(np.abs(dum.fields.b_field_parallel_perturbed_norm[0])**2,dum.angle_pol,axis=0)/(2*np.pi))]
#           dum.fields.b_field_parallel_perturbed_parity = [(np.abs(np.trapz(dum.fields.b_field_parallel_perturbed_norm[0],dum.angle_pol,axis=0))
#                                                / np.trapz(np.abs(dum.fields.b_field_parallel_perturbed_norm[0]),dum.angle_pol,axis=0))]
        
        
#       ## linear_weights (filled only for the last wavevector)
#         dum.linear_weights_rotating_frame=ids_gyrokinetics_local.Fluxes()
        
#         particle_flux_factor =  2.*((n_rat*v_thrat*rho_rat**2)/(L_rat**2)) * amp_rat**2
#         energy_flux_factor = 2.*((n_rat*v_thrat*T_rat*rho_rat**2)/(L_rat**2)) * amp_rat**2
#         momentum_flux_factor = 2.*((m_rat*n_rat*v_thrat**2*rho_rat**2)/(L_rat**2)) * amp_rat**2
        
#         dum.linear_weights_rotating_frame.particles_phi_potential = []
#         dum.linear_weights_rotating_frame.particles_a_field_parallel = []
#         dum.linear_weights_rotating_frame.particles_b_field_parallel = []
        
#         dum.linear_weights_rotating_frame.energy_phi_potential = []
#         dum.linear_weights_rotating_frame.energy_a_field_parallel = []
#         dum.linear_weights_rotating_frame.energy_b_field_parallel = []
        
#         dum.linear_weights_rotating_frame.momentum_tor_parallel_phi_potential = []
#         dum.linear_weights_rotating_frame.momentum_tor_parallel_a_field_parallel = []
#         dum.linear_weights_rotating_frame.momentum_tor_parallel_b_field_parallel = []
        
#         dum.linear_weights_rotating_frame.momentum_tor_perpendicular_phi_potential = []
#         dum.linear_weights_rotating_frame.momentum_tor_perpendicular_a_field_parallel = []
#         dum.linear_weights_rotating_frame.momentum_tor_perpendicular_b_field_parallel = []
        
#         for isp in range(number_of_species):
#           if tglfrun.input_out['AS_'+str(isp+1)] != 0 :
#             nsp = 1 / tglfrun.input_out['AS_'+str(isp+1)]
#           else:
#             nsp = 1
          
#           dum.linear_weights_rotating_frame.particles_phi_potential.append(
#               particle_flux_factor * nsp * tglfrun.QL_flux_spectrum['Gamma'][isp][im]['phi'][iw])
#           dum.linear_weights_rotating_frame.energy_phi_potential.append(
#               energy_flux_factor * nsp * tglfrun.QL_flux_spectrum['Q'][isp][im]['phi'][iw])
#           dum.linear_weights_rotating_frame.momentum_tor_parallel_phi_potential.append(
#               momentum_flux_factor * nsp * tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['phi'][iw])         
#           dum.linear_weights_rotating_frame.momentum_tor_perpendicular_phi_potential.append(
#               momentum_flux_factor * nsp *(
#                   tglfrun.QL_flux_spectrum['Pi_tor'][isp][im]['phi'][iw] - tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['phi'][iw]))
          
#           if tglfrun.input_out['USE_BPER'] :
#             dum.linear_weights_rotating_frame.particles_a_field_parallel.append(
#                 particle_flux_factor * nsp * tglfrun.QL_flux_spectrum['Gamma'][isp][im]['Bper'][iw])                 
#             dum.linear_weights_rotating_frame.energy_a_field_parallel.append(
#                 energy_flux_factor * nsp * tglfrun.QL_flux_spectrum['Q'][isp][im]['Bper'][iw])          
#             dum.linear_weights_rotating_frame.momentum_tor_parallel_a_field_parallel.append(
#                 momentum_flux_factor * nsp * tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['Bper'][iw])         
#             dum.linear_weights_rotating_frame.momentum_tor_perpendicular_a_field_parallel.append(
#                 momentum_flux_factor * nsp *(
#                     tglfrun.QL_flux_spectrum['Pi_tor'][isp][im]['Bper'][iw] - tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['Bper'][iw]))

#           if tglfrun.input_out['USE_BPAR'] :
#             dum.linear_weights_rotating_frame.particles_b_field_parallel.append(
#                 particle_flux_factor * nsp * tglfrun.QL_flux_spectrum['Gamma'][isp][im]['Bpar'][iw])          
#             dum.linear_weights_rotating_frame.energy_b_field_parallel.append(
#                 energy_flux_factor * nsp * tglfrun.QL_flux_spectrum['Q'][isp][im]['Bpar'][iw])
#             dum.linear_weights_rotating_frame.momentum_tor_parallel_b_field_parallel.append(
#                 momentum_flux_factor * nsp * tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['Bpar'][iw])         
#             dum.linear_weights_rotating_frame.momentum_tor_perpendicular_b_field_parallel.append(
#                 momentum_flux_factor * nsp *(
#                     tglfrun.QL_flux_spectrum['Pi_tor'][isp][im]['Bpar'][iw] - tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['Bpar'][iw]))
        

#       dumW.eigenmode.append(dum)
#     gkids.linear.wavevector.append(dumW)
    
#   ## non_linear
#   gkids.non_linear = ids_gyrokinetics_local.GyrokineticsNonLinear()
#   gkids.non_linear.quasi_linear = int(True)
#   gkids.non_linear.time_interval_norm = []
#   gkids.non_linear.time_norm = []
#   gkids.non_linear.angle_pol = th_sorted
#   gkids.non_linear.binormal_wavevector_norm = list(np.array(tglfrun.ky)*abs(Gq)*(1/rho_rat)) 
#   gkids.non_linear.radial_wavevector_norm = [0]
#   # code
#   gkids.non_linear.code=ids_gyrokinetics_local.CodePartialConstant()
#   gkids.non_linear.code.parameters = tglfrun.input_out
#   gkids.non_linear.code.output_flag = 0
#   # Fields intensity spectra
#   gkids.non_linear.fields_intensity_1d = ids_gyrokinetics_local.GyrokineticsFieldsNl1D()
#   gkids.non_linear.fields_intensity_1d.phi_potential_perturbed_norm = np.array(tglfrun.field_spectrum['phi'][0])*(T_rat*rho_rat/(q_rat*L_rat))** 2
#   gkids.non_linear.fields_intensity_1d.a_field_parallel_perturbed_norm = np.array(tglfrun.field_spectrum['Bper'][0])*(T_rat*rho_rat/(q_rat*L_rat))** 2
#   gkids.non_linear.fields_intensity_1d.b_field_parallel_perturbed_norm = np.array(tglfrun.field_spectrum['Bpar'][0])*(T_rat*rho_rat/(q_rat*L_rat))** 2

#   # Fluxes
  
  
  
#   gkids.non_linear.fluxes_1d_rotating_frame = ids_gyrokinetics_local.FluxesNl1D()
    
#   gkids.non_linear.fluxes_1d_rotating_frame.particles_phi_potential = []
#   gkids.non_linear.fluxes_1d_rotating_frame.particles_a_field_parallel = []
#   gkids.non_linear.fluxes_1d_rotating_frame.particles_b_field_parallel = []
  
#   gkids.non_linear.fluxes_1d_rotating_frame.energy_phi_potential = []
#   gkids.non_linear.fluxes_1d_rotating_frame.energy_a_field_parallel = []
#   gkids.non_linear.fluxes_1d_rotating_frame.energy_b_field_parallel = []
  
#   gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_phi_potential = []
#   gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_a_field_parallel = []
#   gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_b_field_parallel = []
  
#   gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_phi_potential = []
#   gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_a_field_parallel = []
#   gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_b_field_parallel = []
  
#   for isp in range(number_of_species):
#       if tglfrun.input_out['AS_'+str(isp+1)] != 0 :
#         nsp = 1 / tglfrun.input_out['AS_'+str(isp+1)]
#       else:
#         nsp = 1
#       particle_flux_factor =  (n_rat*nsp*v_thrat*rho_rat**2)/(L_rat**2)
#       energy_flux_factor = (n_rat*nsp*v_thrat*T_rat*rho_rat**2)/(L_rat**2)
#       momentum_flux_factor = (m_rat*n_rat*nsp*v_thrat**2*rho_rat**2)/(L_rat**2)

#       gkids.non_linear.fluxes_1d_rotating_frame.particles_phi_potential.append(
#           particle_flux_factor * sum(tglfrun.sum_flux_spectrum['particle_flux'][isp]['phi']))
#       gkids.non_linear.fluxes_1d_rotating_frame.particles_a_field_parallel.append(
#           particle_flux_factor * sum(tglfrun.sum_flux_spectrum['particle_flux'][isp]['Bper']))
#       gkids.non_linear.fluxes_1d_rotating_frame.particles_b_field_parallel.append(
#           particle_flux_factor * sum(tglfrun.sum_flux_spectrum['particle_flux'][isp]['Bpar']))
      
#       gkids.non_linear.fluxes_1d_rotating_frame.energy_phi_potential.append(
#           energy_flux_factor * sum(tglfrun.sum_flux_spectrum['energy_flux'][isp]['phi']))
#       gkids.non_linear.fluxes_1d_rotating_frame.energy_a_field_parallel.append(
#           energy_flux_factor * sum(tglfrun.sum_flux_spectrum['energy_flux'][isp]['Bper']))
#       gkids.non_linear.fluxes_1d_rotating_frame.energy_b_field_parallel.append(
#           energy_flux_factor * sum(tglfrun.sum_flux_spectrum['energy_flux'][isp]['Bpar']))
            
#       gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_phi_potential.append(
#           momentum_flux_factor * sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['phi']))
#       gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_a_field_parallel.append(
#           momentum_flux_factor * sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bper']))
#       gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_b_field_parallel.append(
#           momentum_flux_factor * sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bpar']))
      
#       gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_phi_potential.append(
#           momentum_flux_factor *(sum(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['phi']) - sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['phi']) ))
#       gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_a_field_parallel.append(
#           momentum_flux_factor *(sum(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['Bper']) - sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bper']) ))
#       gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_b_field_parallel.append(
#           momentum_flux_factor *(sum(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['Bpar']) - sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bpar']) ))
      
  
#   gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame = ids_gyrokinetics_local.FluxesNl2DSumKx()
  
#   gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_phi_potential = []
#   gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_a_field_parallel = []
#   gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_b_field_parallel = []
  
#   gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_phi_potential = []
#   gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_a_field_parallel = []
#   gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_b_field_parallel = []
  
#   gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_phi_potential = []
#   gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_a_field_parallel = []
#   gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_b_field_parallel = []
  
#   gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_phi_potential = []
#   gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_a_field_parallel = []
#   gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_b_field_parallel = []
      
#   for isp in range(number_of_species):
#       if tglfrun.input_out['AS_'+str(isp+1)] != 0 :
#         nsp = 1 / tglfrun.input_out['AS_'+str(isp+1)]
#       else:
#         nsp = 1
#       particle_flux_factor =  (n_rat*nsp*v_thrat*rho_rat**2)/(L_rat**2)
#       energy_flux_factor = (n_rat*nsp*v_thrat*T_rat*rho_rat**2)/(L_rat**2)
#       momentum_flux_factor = (m_rat*n_rat*nsp*v_thrat**2*rho_rat**2)/(L_rat**2)
      
      
#       gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_phi_potential.append(list(
#           particle_flux_factor * np.array(tglfrun.sum_flux_spectrum['particle_flux'][isp]['phi'])))
#       gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_phi_potential.append(list(
#           energy_flux_factor * np.array(tglfrun.sum_flux_spectrum['energy_flux'][isp]['phi'])))
#       gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_phi_potential.append(list(
#           momentum_flux_factor * np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['phi'])))
#       gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_phi_potential.append(list(
#           momentum_flux_factor *(np.array(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['phi']) - np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['phi']) )))
      
#       if tglfrun.input_out['USE_BPER'] :
#           gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_a_field_parallel.append(list(
#               particle_flux_factor * np.array(tglfrun.sum_flux_spectrum['particle_flux'][isp]['Bper'])))
#           gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_a_field_parallel.append(list(
#               energy_flux_factor * np.array(tglfrun.sum_flux_spectrum['energy_flux'][isp]['Bper'])))
#           gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_a_field_parallel.append(list(
#               momentum_flux_factor * np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bper'])))
#           gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_a_field_parallel.append(list(
#               momentum_flux_factor *(np.array(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['Bper']) - np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bper']) )))
#       if tglfrun.input_out['USE_BPAR'] :
#           gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_b_field_parallel.append(list(
#               particle_flux_factor * np.array(tglfrun.sum_flux_spectrum['particle_flux'][isp]['Bpar'])))
#           gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_b_field_parallel.append(list(
#               energy_flux_factor * np.array(tglfrun.sum_flux_spectrum['energy_flux'][isp]['Bpar'])))
#           gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_b_field_parallel.append(list(
#               momentum_flux_factor * np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bpar'])))
#           gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_b_field_parallel.append(list(
#               momentum_flux_factor *(np.array(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['Bpar']) - np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bpar']) )))
      

#   return gkids

def tglf2ids(tglfrun, provider=''):
  if imaspy_loaded:
    return tglf2ids_imaspy(tglfrun,provider=provider)
  else:
    return tglf2ids_gyrokinetic_local(tglfrun,provider=provider)    

def tglf2ids_imaspy(tglfrun,provider=''):
  """ Convert GKW inputs and outputs to an IMAS 'gyrokinetics' IDS created with idspy
    Inputs
      tglfrun    Instance of the TGLFrun class containing TGLF input and output data
    Outputs
      gkids      Instance of the GKIDS class containing the generated 'gyrokinetics' IDS
  
    Warning and error logs are stored in gkwrun.log
  """
  # initialise an empty GK IDS structure
  ids_factory=imaspy.IDSFactory("3.41.0")
  gkids=ids_factory.gyrokinetics_local()
  
  ## ids_properties
  gkids.ids_properties.provider = provider
  gkids.ids_properties.creation_date = tglfrun.version['date']
  gkids.ids_properties.homogeneous_time = 2
  
  ## code 
  gkids.code.name="TGLF"
  gkids.code.commit = tglfrun.version['commit']
  gkids.code.repository="https://gafusion.github.io/doc/tglf"
  
  ## model
  gkids.model.adiabatic_electrons=int(tglfrun.input_out['ADIABATIC_ELEC'])
  gkids.model.include_a_field_parallel=int(tglfrun.input_out['USE_BPER'])
  gkids.model.include_b_field_parallel=int(tglfrun.input_out['USE_BPAR'])
  gkids.model.include_coriolis_drift=int(False)
  gkids.model.include_centrifugal_effects=int(False)
  gkids.model.collisions_pitch_only = int(True) 
  gkids.model.collisions_momentum_conservation = int(False)
  gkids.model.collisions_energy_conservation = int(False)
  gkids.model.collisions_finite_larmor_radius = int(False)
  gkids.model.include_full_curvature_drift = int(not tglfrun.input_out['USE_MHD_RULE'])
  
  
  # Obtain the Miller eXtended Harmonics (MXH) parametrisation coefficients from the TGLF input values
  mhx = miller_to_mxh(tglfrun)    
  # compute TGLF_ref/IMAS_ref ratio of reference quantities for TGLf to IMAS conversion
  # reference charge ratio
  q_rat = 1
  # reference mass ratio
  m_rat = 1
  # reference temperature ratio
  T_rat = 1
  # reference length ratio
  L_rat =  1/tglfrun.input_out['RMAJ_LOC'] 
  # reference magnetic field ratio
  B_rat = get_B_rat(tglfrun)
  # reference density ratio
  n_rat = 1
  # reference thermal velocity ratio
  v_thrat = (1/np.sqrt(2))*(np.sqrt(T_rat/m_rat))
  # reference Larmor radius ratio
  rho_rat = (m_rat*v_thrat)/(q_rat*B_rat)
  
  # compute theta_imas=f(theta_tglf)
  th_tglf = np.array(tglfrun.wavefunction['theta'])
  nb_pol_trurns = (max(th_tglf)-min(th_tglf))/(2*np.pi)
  R_dum = tglfrun.input_out['RMAJ_LOC'] + tglfrun.input_out['RMIN_LOC']* \
      np.cos(th_tglf + np.arcsin(tglfrun.input_out['DELTA_LOC'])*np.sin(th_tglf))
  Z_dum = tglfrun.input_out['KAPPA_LOC']*tglfrun.input_out['RMIN_LOC']* \
      np.sin(th_tglf + tglfrun.input_out['ZETA_LOC']*np.sin(2*th_tglf))
  th = np.arctan2((Z_dum),R_dum-tglfrun.input_out['RMAJ_LOC'])
  dum = np.cumsum(np.concatenate(([False],abs(np.diff(th))>np.pi)))
  dum = dum - ((nb_pol_trurns-1)/2. )
  th_imas_of_tglf = th + 2*np.pi*dum
  th_sorted = th_imas_of_tglf
  
  ## flux_surface
  gkids.flux_surface.ip_sign = tglfrun.input_out['SIGN_IT']
  gkids.flux_surface.b_field_tor_sign = tglfrun.input_out['SIGN_BT']
  gkids.flux_surface.r_minor_norm = tglfrun.input_out['RMIN_LOC'] * L_rat
  gkids.flux_surface.q = tglfrun.input_out['SIGN_BT']*\
      tglfrun.input_out['SIGN_IT']*tglfrun.input_out['Q_LOC']
  gkids.flux_surface.magnetic_shear_r_minor = (tglfrun.input_out['RMIN_LOC']/\
      abs(tglfrun.input_out['Q_LOC']))**2 * tglfrun.input_out['Q_PRIME_LOC']
  gkids.flux_surface.pressure_gradient_norm = (-8*np.pi)*(B_rat**2/L_rat)*\
      (tglfrun.input_out['RMIN_LOC']/(tglfrun.input_out['Q_LOC'])) *\
      tglfrun.input_out['P_PRIME_LOC']
  gkids.flux_surface.dgeometric_axis_r_dr_minor = mhx['dR0dr']
  gkids.flux_surface.dgeometric_axis_z_dr_minor = mhx['dZ0dr']
  gkids.flux_surface.elongation = mhx['k']
  gkids.flux_surface.delongation_dr_minor_norm = mhx['dkdr']
  gkids.flux_surface.shape_coefficients_c = mhx['c']
  gkids.flux_surface.dc_dr_minor_norm = mhx['dcdr']
  gkids.flux_surface.shape_coefficients_s = mhx['s']
  gkids.flux_surface.ds_dr_minor_norm = mhx['dsdr']
  
  ## species
  number_of_species = int(tglfrun.input_out['NS'])
  for i in range(number_of_species):
    dum=ids_gyrokinetics_local.Species()
    dum.charge_norm = tglfrun.input_out['ZS_'+str(i+1)]*q_rat
    dum.mass_norm = tglfrun.input_out['MASS_'+str(i+1)]*m_rat
    dum.density_norm = tglfrun.input_out['AS_'+str(i+1)]*n_rat
    dum.density_log_gradient_norm = tglfrun.input_out['RLNS_'+str(i+1)]/L_rat
    dum.temperature_norm = tglfrun.input_out['TAUS_'+str(i+1)]*T_rat
    dum.temperature_log_gradient_norm = tglfrun.input_out['RLTS_'+str(i+1)]/L_rat
    dum.velocity_tor_gradient_norm = tglfrun.input_out['VPAR_SHEAR_'+str(i+1)]/(L_rat)*v_thrat*tglfrun.input_out['SIGN_IT']
    gkids.species.append(dum)
  
  ## species_all
  gkids.species_all.angle_pol = []
  gkids.species_all.debye_length_norm = tglfrun.input_out['DEBYE']*rho_rat*np.sqrt((n_rat*q_rat**2)/T_rat)
  gkids.species_all.beta_reference = tglfrun.input_out['BETAE']*(B_rat**2)/(1.5*n_rat*T_rat) 
  gkids.species_all.velocity_tor_norm = tglfrun.input_out['SIGN_IT']*tglfrun.input_out['VPAR_2']*(v_thrat/L_rat)
  gkids.species_all.shearing_rate_norm = tglfrun.input_out['SIGN_IT']*tglfrun.input_out['VEXB_SHEAR']*(v_thrat/L_rat)*B_rat
  
  ## collisions
  # Find electron/main ion index
  density_max = 0.
  for i in range(number_of_species):
    charge = gkids.species[i].charge_norm
    density = gkids.species[i].density_norm
    if charge < 0.:
        index_electron = i
    if (density > density_max) and (charge > 0.):
        density_max = density
        index_main_ion = i
  gkids.collisions.collisionality_norm = np.full((number_of_species,number_of_species),0.0)
  gkids.collisions.collisionality_norm[index_electron][index_main_ion] = tglfrun.input_out['XNUE']*(v_thrat/L_rat)
  
  
  ## linear
  # gkids.linear.resize(1)
  
  ## wavevector
  number_of_ky = int(len(tglfrun.ky))
  number_of_modes = int(tglfrun.input_out['NMODES'])

  metric = get_metric_idspy(gkids,np.linspace(0,2*np.pi,501))
  Gq = metric['Gq'][0]
  gradr = metric['grad_r'][0]

  gkids.linear.wavevector.resize(number_of_ky)
  for iw in range(number_of_ky):
    gkids.linear.wavevector[iw].binormal_wavevector_norm = tglfrun.ky[iw]*abs(Gq)*(1/rho_rat)
    gkids.linear.wavevector[iw].radial_wavevector_norm = tglfrun.input_out['KX0_LOC']*tglfrun.ky[iw]*(1/rho_rat)*abs(gradr)
    
    ## eigenmodes
    gkids.linear.wavevector[iw].eigenmode.resize(number_of_modes)
    for im in range(number_of_modes):
      gkids.linear.wavevector[iw].eigenmode[im].poloidal_turns = int(nb_pol_trurns)
      gkids.linear.wavevector[iw].eigenmode[im].angle_pol = th_sorted
      gkids.linear.wavevector[iw].eigenmode[im].time_norm = np.array([])
      gkids.linear.wavevector[iw].eigenmode[im].initial_value_run = int(False)
      
      gkids.linear.wavevector[iw].eigenmode[im].growth_rate_norm = tglfrun.eigenvalue['gamma'][im][iw]*(v_thrat/L_rat)
      gkids.linear.wavevector[iw].eigenmode[im].frequency_norm = -1*tglfrun.eigenvalue['freq'][im][iw]*(v_thrat/L_rat)
      gkids.linear.wavevector[iw].eigenmode[im].growth_rate_tolerance = 0
      
      ## code (filled only for the last wavevector)
      if iw == (number_of_ky-1):
        # gkids.linear.wavevector[iw].eigenmode[im].code.resize(1)
        gkids.linear.wavevector[iw].eigenmode[im].code.parameters = tglfrun.input_out
        gkids.linear.wavevector[iw].eigenmode[im].code.output_flag = 0
      
      ## fields (filled only for the last wavevector and modes with positive growth rate)
      if (iw == (number_of_ky-1) and im < len(tglfrun.wavefunction['phi'])):
        
        fields={'name': ('phi','Bper','Bpar'),
                'is_field': (True,tglfrun.input_out['USE_BPER'],tglfrun.input_out['USE_BPAR']),
                'norm_fac': ((T_rat*rho_rat )/(q_rat*L_rat), (B_rat*rho_rat**2 )/(L_rat),(B_rat*rho_rat )/(L_rat))
                }
        
        imas_eigenfunctions = {}
        imas_eigenfunctions['phi'], imas_eigenfunctions['Bper'], imas_eigenfunctions['Bpar'] = np.zeros((3,len(gkids.linear.wavevector[iw].eigenmode[im].angle_pol)))
        
        for ii, fields_ in enumerate(fields['name']):
          if fields['is_field'][ii] :
              imas_eigenfunctions[fields_] = np.array(tglfrun.wavefunction[fields_][im]) * fields['norm_fac'][ii]
        
        epsilon = 1e-100  # Threshold

        phi_cleaned = np.where(np.abs(imas_eigenfunctions['phi']) > epsilon, imas_eigenfunctions['phi'], 0)
        Bper_cleaned = np.where(np.abs(imas_eigenfunctions['Bper']) > epsilon, imas_eigenfunctions['Bper'], 0)
        Bpar_cleaned = np.where(np.abs(imas_eigenfunctions['Bpar']) > epsilon, imas_eigenfunctions['Bpar'], 0)

        # Calcul de l'amplitude si la condition est remplie
        if np.sqrt(integrate.simps(np.abs(phi_cleaned)**2, gkids.linear.wavevector[iw].eigenmode[im].angle_pol)) != 0:
            amp_rat = np.sqrt(2 * np.pi) / np.sqrt(
                integrate.simps(np.abs(phi_cleaned)**2 
                                + np.abs(Bper_cleaned)**2 
                                + np.abs(Bpar_cleaned)**2, gkids.linear.wavevector[iw].eigenmode[im].angle_pol)
            )
        else:
            amp_rat = 0

        # rotate and normalise eigenfunctions
        # rotate_factor=exp(i*alpha) in IMAS documentation
        th_fine = np.linspace(th_sorted[0],th_sorted[-1],len(th_sorted)*10)
        f_re=interp1d(th_sorted,np.real(phi_cleaned),kind='cubic')
        f_im=interp1d(th_sorted,np.imag(phi_cleaned),kind='cubic')
        Imax=np.argmax(np.abs(f_re(th_fine) + 1j*f_im(th_fine)))
        rotate_factor = f_re(th_fine)[Imax] + 1j*f_im(th_fine)[Imax]
        if rotate_factor != 0:
            rotate_factor = abs(rotate_factor)/rotate_factor
        else:
            rotate_factor = 0
        
        # gkids.linear.wavevector[iw].eigenmode[im].fields=ids_gyrokinetics_local.EigenmodeFields()
        ''' A TERMINER : GERER LES TYPAGES
        if fields['is_field'][0]:
          gkids.linear.wavevector[iw].eigenmode[im].fields.phi_potential_perturbed_norm = phi_cleaned*rotate_factor*amp_rat
          # gkids.linear.wavevector[iw].eigenmode[im].fields.phi_potential_perturbed_weight = [np.sqrt(np.trapz(np.abs(gkids.linear.wavevector[iw].eigenmode[im].fields.phi_potential_perturbed_norm[0])**2,gkids.linear.wavevector[iw].eigenmode[im].angle_pol,axis=0)/(2*np.pi))]
          # gkids.linear.wavevector[iw].eigenmode[im].fields.phi_potential_perturbed_parity = [(np.abs(np.trapz(gkids.linear.wavevector[iw].eigenmode[im].fields.phi_potential_perturbed_norm[0],gkids.linear.wavevector[iw].eigenmode[im].angle_pol,axis=0))
          #                                      / np.trapz(np.abs(gkids.linear.wavevector[iw].eigenmode[im].fields.phi_potential_perturbed_norm[0]),gkids.linear.wavevector[iw].eigenmode[im].angle_pol,axis=0))]
        if fields['is_field'][1]:
          gkids.linear.wavevector[iw].eigenmode[im].fields.a_field_parallel_perturbed_norm = [-1*Bper_cleaned*rotate_factor*amp_rat]
          # gkids.linear.wavevector[iw].eigenmode[im].fields.a_field_parallel_perturbed_weight = [np.sqrt(np.trapz(np.abs(gkids.linear.wavevector[iw].eigenmode[im].fields.a_field_parallel_perturbed_norm[0])**2,gkids.linear.wavevector[iw].eigenmode[im].angle_pol,axis=0)/(2*np.pi))]
          # gkids.linear.wavevector[iw].eigenmode[im].fields.a_field_parallel_perturbed_parity = [(np.abs(np.trapz(gkids.linear.wavevector[iw].eigenmode[im].fields.a_field_parallel_perturbed_norm[0],gkids.linear.wavevector[iw].eigenmode[im].angle_pol,axis=0))
          #                                      / np.trapz(np.abs(gkids.linear.wavevector[iw].eigenmode[im].fields.a_field_parallel_perturbed_norm[0]),gkids.linear.wavevector[iw].eigenmode[im].angle_pol,axis=0))]
        if fields['is_field'][2]:
          gkids.linear.wavevector[iw].eigenmode[im].fields.b_field_parallel_perturbed_norm = [-1*Bpar_cleaned*rotate_factor*amp_rat]
          # gkids.linear.wavevector[iw].eigenmode[im].fields.b_field_parallel_perturbed_weight = [np.sqrt(np.trapz(np.abs(gkids.linear.wavevector[iw].eigenmode[im].fields.b_field_parallel_perturbed_norm[0])**2,gkids.linear.wavevector[iw].eigenmode[im].angle_pol,axis=0)/(2*np.pi))]
          # gkids.linear.wavevector[iw].eigenmode[im].fields.b_field_parallel_perturbed_parity = [(np.abs(np.trapz(gkids.linear.wavevector[iw].eigenmode[im].fields.b_field_parallel_perturbed_norm[0],gkids.linear.wavevector[iw].eigenmode[im].angle_pol,axis=0))
          #                                      / np.trapz(np.abs(gkids.linear.wavevector[iw].eigenmode[im].fields.b_field_parallel_perturbed_norm[0]),gkids.linear.wavevector[iw].eigenmode[im].angle_pol,axis=0))]
        
        
      ## linear_weights (filled only for the last wavevector)
        
        particle_flux_factor =  2.*((n_rat*v_thrat*rho_rat**2)/(L_rat**2)) * amp_rat**2
        energy_flux_factor = 2.*((n_rat*v_thrat*T_rat*rho_rat**2)/(L_rat**2)) * amp_rat**2
        momentum_flux_factor = 2.*((m_rat*n_rat*v_thrat**2*rho_rat**2)/(L_rat**2)) * amp_rat**2
        
        gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.particles_phi_potential = []
        gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.particles_a_field_parallel = []
        gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.particles_b_field_parallel = []
        
        gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.energy_phi_potential = []
        gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.energy_a_field_parallel = []
        gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.energy_b_field_parallel = []
        
        gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_parallel_phi_potential = []
        gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_parallel_a_field_parallel = []
        gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_parallel_b_field_parallel = []
        
        gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_perpendicular_phi_potential = []
        gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_perpendicular_a_field_parallel = []
        gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_perpendicular_b_field_parallel = []
        
        for isp in range(number_of_species):
          if tglfrun.input_out['AS_'+str(isp+1)] != 0 :
            nsp = 1 / tglfrun.input_out['AS_'+str(isp+1)]
          else:
            nsp = 1

          gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.particles_phi_potential = np.append(
              gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.particles_phi_potential,
              particle_flux_factor * nsp * tglfrun.QL_flux_spectrum['Gamma'][isp][im]['phi'][iw])

          gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.energy_phi_potential = np.append(
              gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.energy_phi_potential,
              energy_flux_factor * nsp * tglfrun.QL_flux_spectrum['Q'][isp][im]['phi'][iw])

          gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_parallel_phi_potential = np.append(
              gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_parallel_phi_potential,
              momentum_flux_factor * nsp * tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['phi'][iw])

          gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_perpendicular_phi_potential = np.append(
              gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_perpendicular_phi_potential,
              momentum_flux_factor * nsp * (
                  tglfrun.QL_flux_spectrum['Pi_tor'][isp][im]['phi'][iw] - tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['phi'][iw]))

          
          if tglfrun.input_out['USE_BPER'] :
            gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.particles_a_field_parallel = np.append(
                gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.particles_a_field_parallel,
                particle_flux_factor * nsp * tglfrun.QL_flux_spectrum['Gamma'][isp][im]['Bper'][iw])

            gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.energy_a_field_parallel = np.append(
                gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.energy_a_field_parallel,
                energy_flux_factor * nsp * tglfrun.QL_flux_spectrum['Q'][isp][im]['Bper'][iw])

            gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_parallel_a_field_parallel = np.append(
                gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_parallel_a_field_parallel,
                momentum_flux_factor * nsp * tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['Bper'][iw])

            gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_perpendicular_a_field_parallel = np.append(
                gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_perpendicular_a_field_parallel,
                momentum_flux_factor * nsp * (
                    tglfrun.QL_flux_spectrum['Pi_tor'][isp][im]['Bper'][iw] - tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['Bper'][iw]))

          if tglfrun.input_out['USE_BPAR'] :
            gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.particles_b_field_parallel = np.append(
                gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.particles_b_field_parallel,
                particle_flux_factor * nsp * tglfrun.QL_flux_spectrum['Gamma'][isp][im]['Bpar'][iw])

            gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.energy_b_field_parallel = np.append(
                gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.energy_b_field_parallel,
                energy_flux_factor * nsp * tglfrun.QL_flux_spectrum['Q'][isp][im]['Bpar'][iw])

            gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_parallel_b_field_parallel = np.append(
                gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_parallel_b_field_parallel,
                momentum_flux_factor * nsp * tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['Bpar'][iw])

            gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_perpendicular_b_field_parallel = np.append(
                gkids.linear.wavevector[iw].eigenmode[im].linear_weights_rotating_frame.momentum_tor_perpendicular_b_field_parallel,
                momentum_flux_factor * nsp * (
                    tglfrun.QL_flux_spectrum['Pi_tor'][isp][im]['Bpar'][iw] - tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['Bpar'][iw]))
        

    
  ## non_linear
  # gkids.non_linear = ids_gyrokinetics_local.GyrokineticsNonLinear()
  gkids.non_linear.quasi_linear = int(True)
  gkids.non_linear.time_interval_norm = []
  gkids.non_linear.time_norm = []
  gkids.non_linear.angle_pol = th_sorted
  gkids.non_linear.binormal_wavevector_norm = list(np.array(tglfrun.ky)*abs(Gq)*(1/rho_rat)) 
  gkids.non_linear.radial_wavevector_norm = [0]
  # code
  # gkids.non_linear.code=ids_gyrokinetics_local.CodePartialConstant()
  gkids.non_linear.code.parameters = tglfrun.input_out
  gkids.non_linear.code.output_flag = 0
  # Fields intensity spectra
  # gkids.non_linear.fields_intensity_1d = ids_gyrokinetics_local.GyrokineticsFieldsNl1D()
  gkids.non_linear.fields_intensity_1d.phi_potential_perturbed_norm = np.array(tglfrun.field_spectrum['phi'][0])*(T_rat*rho_rat/(q_rat*L_rat))** 2
  gkids.non_linear.fields_intensity_1d.a_field_parallel_perturbed_norm = np.array(tglfrun.field_spectrum['Bper'][0])*(T_rat*rho_rat/(q_rat*L_rat))** 2
  gkids.non_linear.fields_intensity_1d.b_field_parallel_perturbed_norm = np.array(tglfrun.field_spectrum['Bpar'][0])*(T_rat*rho_rat/(q_rat*L_rat))** 2

  # Fluxes
  
  
  
  # gkids.non_linear.fluxes_1d_rotating_frame = ids_gyrokinetics_local.FluxesNl1D()
    
  gkids.non_linear.fluxes_1d_rotating_frame.particles_phi_potential = []
  gkids.non_linear.fluxes_1d_rotating_frame.particles_a_field_parallel = []
  gkids.non_linear.fluxes_1d_rotating_frame.particles_b_field_parallel = []
  
  gkids.non_linear.fluxes_1d_rotating_frame.energy_phi_potential = []
  gkids.non_linear.fluxes_1d_rotating_frame.energy_a_field_parallel = []
  gkids.non_linear.fluxes_1d_rotating_frame.energy_b_field_parallel = []
  
  gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_phi_potential = []
  gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_a_field_parallel = []
  gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_b_field_parallel = []
  
  gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_phi_potential = []
  gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_a_field_parallel = []
  gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_b_field_parallel = []
  
  for isp in range(number_of_species):
      if tglfrun.input_out['AS_'+str(isp+1)] != 0 :
        nsp = 1 / tglfrun.input_out['AS_'+str(isp+1)]
      else:
        nsp = 1
      particle_flux_factor =  (n_rat*nsp*v_thrat*rho_rat**2)/(L_rat**2)
      energy_flux_factor = (n_rat*nsp*v_thrat*T_rat*rho_rat**2)/(L_rat**2)
      momentum_flux_factor = (m_rat*n_rat*nsp*v_thrat**2*rho_rat**2)/(L_rat**2)

      gkids.non_linear.fluxes_1d_rotating_frame.particles_phi_potential = np.append(
          gkids.non_linear.fluxes_1d_rotating_frame.particles_phi_potential,
          particle_flux_factor * sum(tglfrun.sum_flux_spectrum['particle_flux'][isp]['phi']))

      gkids.non_linear.fluxes_1d_rotating_frame.particles_a_field_parallel = np.append(
          gkids.non_linear.fluxes_1d_rotating_frame.particles_a_field_parallel,
          particle_flux_factor * sum(tglfrun.sum_flux_spectrum['particle_flux'][isp]['Bper']))

      gkids.non_linear.fluxes_1d_rotating_frame.particles_b_field_parallel = np.append(
          gkids.non_linear.fluxes_1d_rotating_frame.particles_b_field_parallel,
          particle_flux_factor * sum(tglfrun.sum_flux_spectrum['particle_flux'][isp]['Bpar']))

      gkids.non_linear.fluxes_1d_rotating_frame.energy_phi_potential = np.append(
          gkids.non_linear.fluxes_1d_rotating_frame.energy_phi_potential,
          energy_flux_factor * sum(tglfrun.sum_flux_spectrum['energy_flux'][isp]['phi']))

      gkids.non_linear.fluxes_1d_rotating_frame.energy_a_field_parallel = np.append(
          gkids.non_linear.fluxes_1d_rotating_frame.energy_a_field_parallel,
          energy_flux_factor * sum(tglfrun.sum_flux_spectrum['energy_flux'][isp]['Bper']))

      gkids.non_linear.fluxes_1d_rotating_frame.energy_b_field_parallel = np.append(
          gkids.non_linear.fluxes_1d_rotating_frame.energy_b_field_parallel,
          energy_flux_factor * sum(tglfrun.sum_flux_spectrum['energy_flux'][isp]['Bpar']))

      gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_phi_potential = np.append(
          gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_phi_potential,
          momentum_flux_factor * sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['phi']))

      gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_a_field_parallel = np.append(
          gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_a_field_parallel,
          momentum_flux_factor * sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bper']))

      gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_b_field_parallel = np.append(
          gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_b_field_parallel,
          momentum_flux_factor * sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bpar']))

      gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_phi_potential = np.append(
          gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_phi_potential,
          momentum_flux_factor * (sum(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['phi']) - sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['phi'])))

      gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_a_field_parallel = np.append(
          gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_a_field_parallel,
          momentum_flux_factor * (sum(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['Bper']) - sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bper'])))

      gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_b_field_parallel = np.append(
          gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_b_field_parallel,
          momentum_flux_factor * (sum(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['Bpar']) - sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bpar'])))
      
  
  # gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame = ids_gyrokinetics_local.FluxesNl2DSumKx()
  
  non_linear_fluxes2d_rotating_frame_particles_phi_potential = []
  non_linear_fluxes2d_rotating_frame_particles_a_field_parallel = []
  non_linear_fluxes2d_rotating_frame_particles_b_field_parallel = []
  
  non_linear_fluxes2d_rotating_frame_energy_phi_potential = []
  non_linear_fluxes2d_rotating_frame_energy_a_field_parallel = []
  non_linear_fluxes2d_rotating_frame_energy_b_field_parallel = []
  
  non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_phi_potential = []
  non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_a_field_parallel = []
  non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_b_field_parallel = []
  
  non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_phi_potential = []
  non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_a_field_parallel = []
  non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_b_field_parallel = []
      
  for isp in range(number_of_species):
      if tglfrun.input_out['AS_'+str(isp+1)] != 0 :
        nsp = 1 / tglfrun.input_out['AS_'+str(isp+1)]
      else:
        nsp = 1
      particle_flux_factor =  (n_rat*nsp*v_thrat*rho_rat**2)/(L_rat**2)
      energy_flux_factor = (n_rat*nsp*v_thrat*T_rat*rho_rat**2)/(L_rat**2)
      momentum_flux_factor = (m_rat*n_rat*nsp*v_thrat**2*rho_rat**2)/(L_rat**2)
      
      
      non_linear_fluxes2d_rotating_frame_particles_phi_potential.append(list(
          particle_flux_factor * np.array(tglfrun.sum_flux_spectrum['particle_flux'][isp]['phi'])))
      non_linear_fluxes2d_rotating_frame_energy_phi_potential.append(list(
          energy_flux_factor * np.array(tglfrun.sum_flux_spectrum['energy_flux'][isp]['phi'])))
      non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_phi_potential.append(list(
          momentum_flux_factor * np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['phi'])))
      non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_phi_potential.append(list(
          momentum_flux_factor *(np.array(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['phi']) - np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['phi']) )))
      
      if tglfrun.input_out['USE_BPER'] :
          non_linear_fluxes2d_rotating_frame_particles_a_field_parallel.append(list(
              particle_flux_factor * np.array(tglfrun.sum_flux_spectrum['particle_flux'][isp]['Bper'])))
          non_linear_fluxes2d_rotating_frame_energy_a_field_parallel.append(list(
              energy_flux_factor * np.array(tglfrun.sum_flux_spectrum['energy_flux'][isp]['Bper'])))
          non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_a_field_parallel.append(list(
              momentum_flux_factor * np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bper'])))
          non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_a_field_parallel.append(list(
              momentum_flux_factor *(np.array(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['Bper']) - np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bper']) )))
      if tglfrun.input_out['USE_BPAR'] :
          non_linear_fluxes2d_rotating_frame_particles_b_field_parallel.append(list(
              particle_flux_factor * np.array(tglfrun.sum_flux_spectrum['particle_flux'][isp]['Bpar'])))
          non_linear_fluxes2d_rotating_frame_energy_b_field_parallel.append(list(
              energy_flux_factor * np.array(tglfrun.sum_flux_spectrum['energy_flux'][isp]['Bpar'])))
          non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_b_field_parallel.append(list(
              momentum_flux_factor * np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bpar'])))
          non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_b_field_parallel.append(list(
              momentum_flux_factor *(np.array(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['Bpar']) - np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bpar']) )))
      
  non_linear_fluxes2d_rotating_frame_particles_phi_potential = np.array(non_linear_fluxes2d_rotating_frame_particles_phi_potential)
  non_linear_fluxes2d_rotating_frame_particles_a_field_parallel = np.array(non_linear_fluxes2d_rotating_frame_particles_a_field_parallel)
  non_linear_fluxes2d_rotating_frame_particles_b_field_parallel = np.array(non_linear_fluxes2d_rotating_frame_particles_b_field_parallel)

  non_linear_fluxes2d_rotating_frame_energy_phi_potential = np.array(non_linear_fluxes2d_rotating_frame_energy_phi_potential)
  non_linear_fluxes2d_rotating_frame_energy_a_field_parallel = np.array(non_linear_fluxes2d_rotating_frame_energy_a_field_parallel)
  non_linear_fluxes2d_rotating_frame_energy_b_field_parallel = np.array(non_linear_fluxes2d_rotating_frame_energy_b_field_parallel)

  non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_phi_potential = np.array(non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_phi_potential)
  non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_a_field_parallel = np.array(non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_a_field_parallel)
  non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_b_field_parallel = np.array(non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_b_field_parallel)

  non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_phi_potential = np.array(non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_phi_potential)
  non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_a_field_parallel = np.array(non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_a_field_parallel)
  non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_b_field_parallel = np.array(non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_b_field_parallel)

  for isp in range(number_of_species):
      gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_phi_potential=non_linear_fluxes2d_rotating_frame_particles_phi_potential
      gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_phi_potential=non_linear_fluxes2d_rotating_frame_energy_phi_potential
      gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_phi_potential=non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_phi_potential
      gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_phi_potential=non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_phi_potential
      
      if tglfrun.input_out['USE_BPER'] :
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_a_field_parallel=non_linear_fluxes2d_rotating_frame_particles_a_field_parallel
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_a_field_parallel=non_linear_fluxes2d_rotating_frame_energy_a_field_parallel
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_a_field_parallel=non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_a_field_parallel
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_a_field_parallel=non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_a_field_parallel
      if tglfrun.input_out['USE_BPAR'] :
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_b_field_parallel=non_linear_fluxes2d_rotating_frame_particles_b_field_parallel
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_b_field_parallel=non_linear_fluxes2d_rotating_frame_energy_b_field_parallel
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_b_field_parallel=non_linear_fluxes2d_rotating_frame_momentum_tor_parallel_b_field_parallel
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_b_field_parallel=non_linear_fluxes2d_rotating_frame_momentum_tor_perpendicular_b_field_parallel

  '''
        

  return gkids
     
   

def tglf2ids_gyrokinetic_local(tglfrun,provider=''):
  """ Convert GKW inputs and outputs to an IMAS 'gyrokinetics' IDS created with idspy
    Inputs
      tglfrun    Instance of the TGLFrun class containing TGLF input and output data
    Outputs
      gkids      Instance of the GKIDS class containing the generated 'gyrokinetics' IDS
  
    Warning and error logs are stored in gkwrun.log
  """
  # initialise an empty GK IDS structure
  gkids=ids_gyrokinetics_local.GyrokineticsLocal()
  idspy.fill_default_values_ids(gkids)
  
  ## ids_properties
  gkids.ids_properties.provider = provider
  gkids.ids_properties.creation_date = tglfrun.version['date']
  gkids.ids_properties.homogeneous_time = 2
  
  ## code 
  gkids.code.name="TGLF"
  gkids.code.commit = tglfrun.version['commit']
  gkids.code.repository="https://gafusion.github.io/doc/tglf"
  
  ## model
  gkids.model.adiabatic_electrons=int(tglfrun.input_out['ADIABATIC_ELEC'])
  gkids.model.include_a_field_parallel=int(tglfrun.input_out['USE_BPER'])
  gkids.model.include_b_field_parallel=int(tglfrun.input_out['USE_BPAR'])
  gkids.model.include_coriolis_drift=int(False)
  gkids.model.include_centrifugal_effects=int(False)
  gkids.model.collisions_pitch_only = int(True) 
  gkids.model.collisions_momentum_conservation = int(False)
  gkids.model.collisions_energy_conservation = int(False)
  gkids.model.collisions_finite_larmor_radius = int(False)
  gkids.model.include_full_curvature_drift = int(not tglfrun.input_out['USE_MHD_RULE'])
  
  
  # Obtain the Miller eXtended Harmonics (MXH) parametrisation coefficients from the TGLF input values
  mhx = miller_to_mxh(tglfrun)    
  # compute TGLF_ref/IMAS_ref ratio of reference quantities for TGLf to IMAS conversion
  # reference charge ratio
  q_rat = 1
  # reference mass ratio
  m_rat = 1
  # reference temperature ratio
  T_rat = 1
  # reference length ratio
  L_rat =  1/tglfrun.input_out['RMAJ_LOC'] 
  # reference magnetic field ratio
  B_rat = get_B_rat(tglfrun)
  # reference density ratio
  n_rat = 1
  # reference thermal velocity ratio
  v_thrat = (1/np.sqrt(2))*(np.sqrt(T_rat/m_rat))
  # reference Larmor radius ratio
  rho_rat = (m_rat*v_thrat)/(q_rat*B_rat)
  
  # compute theta_imas=f(theta_tglf)
  th_tglf = np.array(tglfrun.wavefunction['theta'])
  nb_pol_trurns = (max(th_tglf)-min(th_tglf))/(2*np.pi)
  R_dum = tglfrun.input_out['RMAJ_LOC'] + tglfrun.input_out['RMIN_LOC']* \
      np.cos(th_tglf + np.arcsin(tglfrun.input_out['DELTA_LOC'])*np.sin(th_tglf))
  Z_dum = tglfrun.input_out['KAPPA_LOC']*tglfrun.input_out['RMIN_LOC']* \
      np.sin(th_tglf + tglfrun.input_out['ZETA_LOC']*np.sin(2*th_tglf))
  th = np.arctan2((Z_dum),R_dum-tglfrun.input_out['RMAJ_LOC'])
  dum = np.cumsum(np.concatenate(([False],abs(np.diff(th))>np.pi)))
  dum = dum - ((nb_pol_trurns-1)/2. )
  th_imas_of_tglf = th + 2*np.pi*dum
  th_sorted = th_imas_of_tglf
  
  ## flux_surface
  gkids.flux_surface.ip_sign = tglfrun.input_out['SIGN_IT']
  gkids.flux_surface.b_field_tor_sign = tglfrun.input_out['SIGN_BT']
  gkids.flux_surface.r_minor_norm = tglfrun.input_out['RMIN_LOC'] * L_rat
  gkids.flux_surface.q = tglfrun.input_out['SIGN_BT']*\
      tglfrun.input_out['SIGN_IT']*tglfrun.input_out['Q_LOC']
  gkids.flux_surface.magnetic_shear_r_minor = (tglfrun.input_out['RMIN_LOC']/\
      abs(tglfrun.input_out['Q_LOC']))**2 * tglfrun.input_out['Q_PRIME_LOC']
  gkids.flux_surface.pressure_gradient_norm = (-8*np.pi)*(B_rat**2/L_rat)*\
      (tglfrun.input_out['RMIN_LOC']/(tglfrun.input_out['Q_LOC'])) *\
      tglfrun.input_out['P_PRIME_LOC']
  gkids.flux_surface.dgeometric_axis_r_dr_minor = mhx['dR0dr']
  gkids.flux_surface.dgeometric_axis_z_dr_minor = mhx['dZ0dr']
  gkids.flux_surface.elongation = mhx['k']
  gkids.flux_surface.delongation_dr_minor_norm = mhx['dkdr']
  gkids.flux_surface.shape_coefficients_c = mhx['c']
  gkids.flux_surface.dc_dr_minor_norm = mhx['dcdr']
  gkids.flux_surface.shape_coefficients_s = mhx['s']
  gkids.flux_surface.ds_dr_minor_norm = mhx['dsdr']
  
  ## species
  number_of_species = int(tglfrun.input_out['NS'])
  for i in range(number_of_species):
    dum=ids_gyrokinetics_local.Species()
    dum.charge_norm = tglfrun.input_out['ZS_'+str(i+1)]*q_rat
    dum.mass_norm = tglfrun.input_out['MASS_'+str(i+1)]*m_rat
    dum.density_norm = tglfrun.input_out['AS_'+str(i+1)]*n_rat
    dum.density_log_gradient_norm = tglfrun.input_out['RLNS_'+str(i+1)]/L_rat
    dum.temperature_norm = tglfrun.input_out['TAUS_'+str(i+1)]*T_rat
    dum.temperature_log_gradient_norm = tglfrun.input_out['RLTS_'+str(i+1)]/L_rat
    dum.velocity_tor_gradient_norm = tglfrun.input_out['VPAR_SHEAR_'+str(i+1)]/(L_rat)*v_thrat*tglfrun.input_out['SIGN_IT']
    gkids.species.append(dum)
  
  ## species_all
  gkids.species_all.angle_pol = []
  gkids.species_all.debye_length_norm = tglfrun.input_out['DEBYE']*rho_rat*np.sqrt((n_rat*q_rat**2)/T_rat)
  gkids.species_all.beta_reference = tglfrun.input_out['BETAE']*(B_rat**2)/(1.5*n_rat*T_rat) 
  gkids.species_all.velocity_tor_norm = tglfrun.input_out['SIGN_IT']*tglfrun.input_out['VPAR_2']*(v_thrat/L_rat)
  gkids.species_all.shearing_rate_norm = tglfrun.input_out['SIGN_IT']*tglfrun.input_out['VEXB_SHEAR']*(v_thrat/L_rat)*B_rat
  
  ## collisions
  # Find electron/main ion index
  density_max = 0.
  for i in range(number_of_species):
    charge = gkids.species[i].charge_norm
    density = gkids.species[i].density_norm
    if charge < 0.:
        index_electron = i
    if (density > density_max) and (charge > 0.):
        density_max = density
        index_main_ion = i
  gkids.collisions.collisionality_norm = np.full((number_of_species,number_of_species),0.0)
  gkids.collisions.collisionality_norm[index_electron][index_main_ion] = tglfrun.input_out['XNUE']*(v_thrat/L_rat)
  
  
  ## linear
  gkids.linear=ids_gyrokinetics_local.GyrokineticsLinear()
  
  ## wavevector
  number_of_ky = int(len(tglfrun.ky))
  number_of_modes = int(tglfrun.input_out['NMODES'])

  metric = get_metric_idspy(gkids,np.linspace(0,2*np.pi,501))
  Gq = metric['Gq'][0]
  gradr = metric['grad_r'][0]
  for iw in range(number_of_ky):
    dumW=ids_gyrokinetics_local.Wavevector()
    dumW.binormal_wavevector_norm = tglfrun.ky[iw]*abs(Gq)*(1/rho_rat)
    dumW.radial_wavevector_norm = tglfrun.input_out['KX0_LOC']*tglfrun.ky[iw]*(1/rho_rat)*abs(gradr)
    
    ## eigenmodes
    for im in range(number_of_modes):
      dum = ids_gyrokinetics_local.Eigenmode()
      dum.poloidal_turns = int(nb_pol_trurns)
      dum.angle_pol = th_sorted
      dum.time_norm = []
      dum.initial_value_run = int(False)
      
      dum.growth_rate_norm = tglfrun.eigenvalue['gamma'][im][iw]*(v_thrat/L_rat)
      dum.frequency_norm = -1*tglfrun.eigenvalue['freq'][im][iw]*(v_thrat/L_rat)
      dum.growth_rate_tolerance = 0
      
      ## code (filled only for the last wavevector)
      if iw == (number_of_ky-1):
        dum.code = ids_gyrokinetics_local.CodePartialConstant()
        dum.code.parameters = tglfrun.input_out
        dum.code.output_flag = 0
      
      ## fields (filled only for the last wavevector and modes with positive growth rate)
      if (iw == (number_of_ky-1) and im < len(tglfrun.wavefunction['phi'])):
        
        fields={'name': ('phi','Bper','Bpar'),
                'is_field': (True,tglfrun.input_out['USE_BPER'],tglfrun.input_out['USE_BPAR']),
                'norm_fac': ((T_rat*rho_rat )/(q_rat*L_rat), (B_rat*rho_rat**2 )/(L_rat),(B_rat*rho_rat )/(L_rat))
                }
        
        imas_eigenfunctions = {}
        imas_eigenfunctions['phi'], imas_eigenfunctions['Bper'], imas_eigenfunctions['Bpar'] = np.zeros((3,len(dum.angle_pol)))
        
        for ii, fields_ in enumerate(fields['name']):
          if fields['is_field'][ii] :
              imas_eigenfunctions[fields_] = np.array(tglfrun.wavefunction[fields_][im]) * fields['norm_fac'][ii]
        
        epsilon = 1e-100  # Threshold

        phi_cleaned = np.where(np.abs(imas_eigenfunctions['phi']) > epsilon, imas_eigenfunctions['phi'], 0)
        Bper_cleaned = np.where(np.abs(imas_eigenfunctions['Bper']) > epsilon, imas_eigenfunctions['Bper'], 0)
        Bpar_cleaned = np.where(np.abs(imas_eigenfunctions['Bpar']) > epsilon, imas_eigenfunctions['Bpar'], 0)

        # Calcul de l'amplitude si la condition est remplie
        if np.sqrt(integrate.simps(np.abs(phi_cleaned)**2, dum.angle_pol)) != 0:
            amp_rat = np.sqrt(2 * np.pi) / np.sqrt(
                integrate.simps(np.abs(phi_cleaned)**2 
                                + np.abs(Bper_cleaned)**2 
                                + np.abs(Bpar_cleaned)**2, dum.angle_pol)
            )
        else:
            amp_rat = 0

        # rotate and normalise eigenfunctions
        # rotate_factor=exp(i*alpha) in IMAS documentation
        th_fine = np.linspace(th_sorted[0],th_sorted[-1],len(th_sorted)*10)
        f_re=interp1d(th_sorted,np.real(phi_cleaned),kind='cubic')
        f_im=interp1d(th_sorted,np.imag(phi_cleaned),kind='cubic')
        Imax=np.argmax(np.abs(f_re(th_fine) + 1j*f_im(th_fine)))
        rotate_factor = f_re(th_fine)[Imax] + 1j*f_im(th_fine)[Imax]
        if rotate_factor != 0:
            rotate_factor = abs(rotate_factor)/rotate_factor
        else:
            rotate_factor = 0
        
        dum.fields=ids_gyrokinetics_local.EigenmodeFields()
        
        if fields['is_field'][0]:
          dum.fields.phi_potential_perturbed_norm = [phi_cleaned*rotate_factor*amp_rat]
          dum.fields.phi_potential_perturbed_weight = [np.sqrt(np.trapz(np.abs(dum.fields.phi_potential_perturbed_norm[0])**2,dum.angle_pol,axis=0)/(2*np.pi))]
          dum.fields.phi_potential_perturbed_parity = [(np.abs(np.trapz(dum.fields.phi_potential_perturbed_norm[0],dum.angle_pol,axis=0))
                                               / np.trapz(np.abs(dum.fields.phi_potential_perturbed_norm[0]),dum.angle_pol,axis=0))]
        if fields['is_field'][1]:
          dum.fields.a_field_parallel_perturbed_norm = [-1*Bper_cleaned*rotate_factor*amp_rat]
          dum.fields.a_field_parallel_perturbed_weight = [np.sqrt(np.trapz(np.abs(dum.fields.a_field_parallel_perturbed_norm[0])**2,dum.angle_pol,axis=0)/(2*np.pi))]
          # dum.fields.a_field_parallel_perturbed_parity = [(np.abs(np.trapz(dum.fields.a_field_parallel_perturbed_norm[0],dum.angle_pol,axis=0))
          #                                      / np.trapz(np.abs(dum.fields.a_field_parallel_perturbed_norm[0]),dum.angle_pol,axis=0))]
        if fields['is_field'][2]:
          dum.fields.b_field_parallel_perturbed_norm = [-1*Bpar_cleaned*rotate_factor*amp_rat]
          dum.fields.b_field_parallel_perturbed_weight = [np.sqrt(np.trapz(np.abs(dum.fields.b_field_parallel_perturbed_norm[0])**2,dum.angle_pol,axis=0)/(2*np.pi))]
          dum.fields.b_field_parallel_perturbed_parity = [(np.abs(np.trapz(dum.fields.b_field_parallel_perturbed_norm[0],dum.angle_pol,axis=0))
                                               / np.trapz(np.abs(dum.fields.b_field_parallel_perturbed_norm[0]),dum.angle_pol,axis=0))]
        
        
      ## linear_weights (filled only for the last wavevector)
        dum.linear_weights_rotating_frame=ids_gyrokinetics_local.Fluxes()
        
        particle_flux_factor =  2.*((n_rat*v_thrat*rho_rat**2)/(L_rat**2)) * amp_rat**2
        energy_flux_factor = 2.*((n_rat*v_thrat*T_rat*rho_rat**2)/(L_rat**2)) * amp_rat**2
        momentum_flux_factor = 2.*((m_rat*n_rat*v_thrat**2*rho_rat**2)/(L_rat**2)) * amp_rat**2
        
        dum.linear_weights_rotating_frame.particles_phi_potential = []
        dum.linear_weights_rotating_frame.particles_a_field_parallel = []
        dum.linear_weights_rotating_frame.particles_b_field_parallel = []
        
        dum.linear_weights_rotating_frame.energy_phi_potential = []
        dum.linear_weights_rotating_frame.energy_a_field_parallel = []
        dum.linear_weights_rotating_frame.energy_b_field_parallel = []
        
        dum.linear_weights_rotating_frame.momentum_tor_parallel_phi_potential = []
        dum.linear_weights_rotating_frame.momentum_tor_parallel_a_field_parallel = []
        dum.linear_weights_rotating_frame.momentum_tor_parallel_b_field_parallel = []
        
        dum.linear_weights_rotating_frame.momentum_tor_perpendicular_phi_potential = []
        dum.linear_weights_rotating_frame.momentum_tor_perpendicular_a_field_parallel = []
        dum.linear_weights_rotating_frame.momentum_tor_perpendicular_b_field_parallel = []
        
        for isp in range(number_of_species):
          if tglfrun.input_out['AS_'+str(isp+1)] != 0 :
            nsp = 1 / tglfrun.input_out['AS_'+str(isp+1)]
          else:
            nsp = 1
          
          dum.linear_weights_rotating_frame.particles_phi_potential.append(
              particle_flux_factor * nsp * tglfrun.QL_flux_spectrum['Gamma'][isp][im]['phi'][iw])
          dum.linear_weights_rotating_frame.energy_phi_potential.append(
              energy_flux_factor * nsp * tglfrun.QL_flux_spectrum['Q'][isp][im]['phi'][iw])
          dum.linear_weights_rotating_frame.momentum_tor_parallel_phi_potential.append(
              momentum_flux_factor * nsp * tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['phi'][iw])         
          dum.linear_weights_rotating_frame.momentum_tor_perpendicular_phi_potential.append(
              momentum_flux_factor * nsp *(
                  tglfrun.QL_flux_spectrum['Pi_tor'][isp][im]['phi'][iw] - tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['phi'][iw]))
          
          if tglfrun.input_out['USE_BPER'] :
            dum.linear_weights_rotating_frame.particles_a_field_parallel.append(
                particle_flux_factor * nsp * tglfrun.QL_flux_spectrum['Gamma'][isp][im]['Bper'][iw])                 
            dum.linear_weights_rotating_frame.energy_a_field_parallel.append(
                energy_flux_factor * nsp * tglfrun.QL_flux_spectrum['Q'][isp][im]['Bper'][iw])          
            # dum.linear_weights_rotating_frame.momentum_tor_parallel_a_field_parallel.append(
            #     momentum_flux_factor * nsp * tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['Bper'][iw])  
            dum.linear_weights_rotating_frame.momentum_tor_parallel_a_field_parallel = 0. # FOR OPEN IDS       
            dum.linear_weights_rotating_frame.momentum_tor_perpendicular_a_field_parallel.append(
                momentum_flux_factor * nsp *(
                    tglfrun.QL_flux_spectrum['Pi_tor'][isp][im]['Bper'][iw] - tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['Bper'][iw]))

          if tglfrun.input_out['USE_BPAR'] :
            dum.linear_weights_rotating_frame.particles_b_field_parallel.append(
                particle_flux_factor * nsp * tglfrun.QL_flux_spectrum['Gamma'][isp][im]['Bpar'][iw])          
            dum.linear_weights_rotating_frame.energy_b_field_parallel.append(
                energy_flux_factor * nsp * tglfrun.QL_flux_spectrum['Q'][isp][im]['Bpar'][iw])
            dum.linear_weights_rotating_frame.momentum_tor_parallel_b_field_parallel.append(
                momentum_flux_factor * nsp * tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['Bpar'][iw])         
            dum.linear_weights_rotating_frame.momentum_tor_perpendicular_b_field_parallel.append(
                momentum_flux_factor * nsp *(
                    tglfrun.QL_flux_spectrum['Pi_tor'][isp][im]['Bpar'][iw] - tglfrun.QL_flux_spectrum['Pi_par'][isp][im]['Bpar'][iw]))
        

      dumW.eigenmode.append(dum)
    gkids.linear.wavevector.append(dumW)
    
  ## non_linear
  gkids.non_linear = ids_gyrokinetics_local.GyrokineticsNonLinear()
  gkids.non_linear.quasi_linear = int(True)
  gkids.non_linear.time_interval_norm = []
  gkids.non_linear.time_norm = []
  gkids.non_linear.angle_pol = th_sorted
  gkids.non_linear.binormal_wavevector_norm = list(np.array(tglfrun.ky)*abs(Gq)*(1/rho_rat)) 
  gkids.non_linear.radial_wavevector_norm = [0]
  # code
  gkids.non_linear.code=ids_gyrokinetics_local.CodePartialConstant()
  gkids.non_linear.code.parameters = tglfrun.input_out
  gkids.non_linear.code.output_flag = 0
  # Fields intensity spectra
  gkids.non_linear.fields_intensity_1d = ids_gyrokinetics_local.GyrokineticsFieldsNl1D()
  gkids.non_linear.fields_intensity_1d.phi_potential_perturbed_norm = np.array(tglfrun.field_spectrum['phi'][0])*(T_rat*rho_rat/(q_rat*L_rat))** 2
  gkids.non_linear.fields_intensity_1d.a_field_parallel_perturbed_norm = np.array(tglfrun.field_spectrum['Bper'][0])*(T_rat*rho_rat/(q_rat*L_rat))** 2
  gkids.non_linear.fields_intensity_1d.b_field_parallel_perturbed_norm = np.array(tglfrun.field_spectrum['Bpar'][0])*(T_rat*rho_rat/(q_rat*L_rat))** 2

  # Fluxes
  
  
  
  gkids.non_linear.fluxes_1d_rotating_frame = ids_gyrokinetics_local.FluxesNl1D()
    
  gkids.non_linear.fluxes_1d_rotating_frame.particles_phi_potential = []
  gkids.non_linear.fluxes_1d_rotating_frame.particles_a_field_parallel = []
  gkids.non_linear.fluxes_1d_rotating_frame.particles_b_field_parallel = []
  
  gkids.non_linear.fluxes_1d_rotating_frame.energy_phi_potential = []
  gkids.non_linear.fluxes_1d_rotating_frame.energy_a_field_parallel = []
  gkids.non_linear.fluxes_1d_rotating_frame.energy_b_field_parallel = []
  
  gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_phi_potential = []
  gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_a_field_parallel = []
  gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_b_field_parallel = []
  
  gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_phi_potential = []
  gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_a_field_parallel = []
  gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_b_field_parallel = []
  
  for isp in range(number_of_species):
      if tglfrun.input_out['AS_'+str(isp+1)] != 0 :
        nsp = 1 / tglfrun.input_out['AS_'+str(isp+1)]
      else:
        nsp = 1
      particle_flux_factor =  (n_rat*nsp*v_thrat*rho_rat**2)/(L_rat**2)
      energy_flux_factor = (n_rat*nsp*v_thrat*T_rat*rho_rat**2)/(L_rat**2)
      momentum_flux_factor = (m_rat*n_rat*nsp*v_thrat**2*rho_rat**2)/(L_rat**2)

      gkids.non_linear.fluxes_1d_rotating_frame.particles_phi_potential.append(
          particle_flux_factor * sum(tglfrun.sum_flux_spectrum['particle_flux'][isp]['phi']))
      gkids.non_linear.fluxes_1d_rotating_frame.particles_a_field_parallel.append(
          particle_flux_factor * sum(tglfrun.sum_flux_spectrum['particle_flux'][isp]['Bper']))
      gkids.non_linear.fluxes_1d_rotating_frame.particles_b_field_parallel.append(
          particle_flux_factor * sum(tglfrun.sum_flux_spectrum['particle_flux'][isp]['Bpar']))
      
      gkids.non_linear.fluxes_1d_rotating_frame.energy_phi_potential.append(
          energy_flux_factor * sum(tglfrun.sum_flux_spectrum['energy_flux'][isp]['phi']))
      gkids.non_linear.fluxes_1d_rotating_frame.energy_a_field_parallel.append(
          energy_flux_factor * sum(tglfrun.sum_flux_spectrum['energy_flux'][isp]['Bper']))
      gkids.non_linear.fluxes_1d_rotating_frame.energy_b_field_parallel.append(
          energy_flux_factor * sum(tglfrun.sum_flux_spectrum['energy_flux'][isp]['Bpar']))
            
      gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_phi_potential.append(
          momentum_flux_factor * sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['phi']))
      gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_a_field_parallel.append(
          momentum_flux_factor * sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bper']))
      gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_parallel_b_field_parallel.append(
          momentum_flux_factor * sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bpar']))
      
      gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_phi_potential.append(
          momentum_flux_factor *(sum(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['phi']) - sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['phi']) ))
      gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_a_field_parallel.append(
          momentum_flux_factor *(sum(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['Bper']) - sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bper']) ))
      gkids.non_linear.fluxes_1d_rotating_frame.momentum_tor_perpendicular_b_field_parallel.append(
          momentum_flux_factor *(sum(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['Bpar']) - sum(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bpar']) ))
      
  
  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame = ids_gyrokinetics_local.FluxesNl2DSumKx()
  
  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_phi_potential = []
  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_a_field_parallel = []
  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_b_field_parallel = []
  
  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_phi_potential = []
  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_a_field_parallel = []
  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_b_field_parallel = []
  
  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_phi_potential = []
  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_a_field_parallel = []
  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_b_field_parallel = []
  
  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_phi_potential = []
  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_a_field_parallel = []
  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_b_field_parallel = []
      
  for isp in range(number_of_species):
      if tglfrun.input_out['AS_'+str(isp+1)] != 0 :
        nsp = 1 / tglfrun.input_out['AS_'+str(isp+1)]
      else:
        nsp = 1
      particle_flux_factor =  (n_rat*nsp*v_thrat*rho_rat**2)/(L_rat**2)
      energy_flux_factor = (n_rat*nsp*v_thrat*T_rat*rho_rat**2)/(L_rat**2)
      momentum_flux_factor = (m_rat*n_rat*nsp*v_thrat**2*rho_rat**2)/(L_rat**2)
      
      
      gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_phi_potential.append(list(
          particle_flux_factor * np.array(tglfrun.sum_flux_spectrum['particle_flux'][isp]['phi'])))
      gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_phi_potential.append(list(
          energy_flux_factor * np.array(tglfrun.sum_flux_spectrum['energy_flux'][isp]['phi'])))
      gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_phi_potential.append(list(
          momentum_flux_factor * np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['phi'])))
      gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_phi_potential.append(list(
          momentum_flux_factor *(np.array(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['phi']) - np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['phi']) )))
      
      if tglfrun.input_out['USE_BPER'] :
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_a_field_parallel.append(list(
              particle_flux_factor * np.array(tglfrun.sum_flux_spectrum['particle_flux'][isp]['Bper'])))
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_a_field_parallel.append(list(
              energy_flux_factor * np.array(tglfrun.sum_flux_spectrum['energy_flux'][isp]['Bper'])))
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_a_field_parallel.append(list(
              momentum_flux_factor * np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bper'])))
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_a_field_parallel.append(list(
              momentum_flux_factor *(np.array(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['Bper']) - np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bper']) )))
      if tglfrun.input_out['USE_BPAR'] :
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.particles_b_field_parallel.append(list(
              particle_flux_factor * np.array(tglfrun.sum_flux_spectrum['particle_flux'][isp]['Bpar'])))
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.energy_b_field_parallel.append(list(
              energy_flux_factor * np.array(tglfrun.sum_flux_spectrum['energy_flux'][isp]['Bpar'])))
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_parallel_b_field_parallel.append(list(
              momentum_flux_factor * np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bpar'])))
          gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame.momentum_tor_perpendicular_b_field_parallel.append(list(
              momentum_flux_factor *(np.array(tglfrun.sum_flux_spectrum['toroidal_stress'][isp]['Bpar']) - np.array(tglfrun.sum_flux_spectrum['parallel_stress'][isp]['Bpar']) )))
      

  return gkids
     
   

def miller_to_mxh(tglfrun):
    '''
    Return a dict with Miller eXtended Harmonics (MXH) parametrisation parameters
    From the TGLF Miller input parameters 
    '''
    #Get the values of r, R and Z
    r, R, Z = FS_param.miller2rz(tglfrun.input_out['RMIN_LOC'],tglfrun.input_out['RMAJ_LOC'],
                        tglfrun.input_out['ZMAJ_LOC'],tglfrun.input_out['KAPPA_LOC'],
                        tglfrun.input_out['DELTA_LOC'],tglfrun.input_out['ZETA_LOC'],
                        tglfrun.input_out['DRMAJDX_LOC']/tglfrun.input_out['DRMINDX_LOC'],
                        tglfrun.input_out['DZMAJDX_LOC']/tglfrun.input_out['DRMINDX_LOC'],
                        tglfrun.input_out['S_KAPPA_LOC'],tglfrun.input_out['S_DELTA_LOC'],
                        tglfrun.input_out['S_ZETA_LOC'],code='tglf',dr_frac=0.01,Nth=500)       
    #Get the mxh coeffs parameters       
    r_out,R0,dR0dr,Z0,dZ0dr,k,dkdr,c,dcdr,s,dsdr,R_out,Z_out,err_out = FS_param.rz2mxh(R,Z,'imas',
                                                                              tglfrun.input_out['RMIN_LOC'],
                                                                              Nsh=10,doplots=False)

    #Check if there is an issue with the function
    if ((abs(r_out-tglfrun.input_out['RMIN_LOC']) > 1e-5)
        or (abs(R0-tglfrun.input_out['RMAJ_LOC']) > 1e-5)
        or (abs(Z0-tglfrun.input_out['ZMAJ_LOC']) > 1e-5)
        or (abs(dR0dr-tglfrun.input_out['DRMAJDX_LOC']) > 1e-5)):
        print('There is an issue with _miller_to_mxh(), different values are obtained for r0, R0 and Z0 from the input')
        print('Obtained values: r0 = ',r_out,', R0 = ',R0,', Z0 = ',Z0,', dR0dr = ',dR0dr)

    return { 'r':r_out, 'R0':R0  ,  'dR0dr':dR0dr  ,  'Z0':Z0  ,
            'dZ0dr':dZ0dr  ,  'k':k  ,  'dkdr':dkdr  ,  'c':c  ,
            'dcdr':dcdr  ,  's':s  ,  'dsdr':dsdr  ,  'R_out':R_out ,
            'Z_out':Z_out }

def get_B_rat(tglfrun):
    '''
    Return B_rat, the ratio between the reference magnetic field of TGLF and IMAS
    '''
    # Nuber of points in the theta grid
    Nth=289
    # Flux surface geometry
    r0 = tglfrun.input_out['RMIN_LOC'] 
    R0 = tglfrun.input_out['RMAJ_LOC'] 
    #Z0 = tglfrun.input_out['ZMAJ_LOC']
    k = tglfrun.input_out['KAPPA_LOC']
    d = tglfrun.input_out['DELTA_LOC']
    z = tglfrun.input_out['ZETA_LOC']
    dRmildr = tglfrun.input_out['DRMAJDX_LOC']
    dZmildr = tglfrun.input_out['DZMAJDX_LOC']
    drmildr = tglfrun.input_out['DRMINDX_LOC']
    sk = tglfrun.input_out['S_KAPPA_LOC']
    sd = tglfrun.input_out['S_DELTA_LOC']/np.sqrt(1.0-d**2)
    sz = tglfrun.input_out['S_ZETA_LOC']
    # define the fine theta grid on which the integrals are performed
    th=np.linspace(0,2*np.pi,Nth)
    
    arg_r = th + np.arcsin(d)*np.sin(th)
    darg_r = 1.0 + np.arcsin(d)*np.cos(th)
    arg_z = th + z*np.sin(2.0*th)
    darg_z = 1.0 + z*2.0*np.cos(2.0*th)
    
    R = R0 + r0*np.cos(arg_r)
    #Z = Z0 + r0*k*np.sin(arg_z)
    
    dRdr = dRmildr + drmildr*np.cos(arg_r) - np.sin(arg_r)*sd*np.sin(th)
    dZdr = dZmildr + k*np.sin(arg_z)*(drmildr + sk) + k*np.cos(arg_z)*sz*np.sin(2.0*th)
    dRdth = -1*r0*np.sin(arg_r)*darg_r
    dZdth = k*r0*np.cos(arg_z)*darg_z
    r = 0.5*(max(R)-min(R))
    
    J_r = -R*(dRdr*dZdth - dRdth*dZdr)
    dldth = np.sqrt((dRdth)**2+(dZdth)**2)
    grad_r = R/J_r*dldth

    B_rat = R0/(2*np.pi*r)*np.trapz(dldth/(R*abs(grad_r)),th)

    return B_rat



def mxh_to_miller_idspy(ids):
    # Get the values of r, R and Z (R0 is supposed to be equal to 1 and Z0 to 0, IMAS normalisations)
    r, R, Z =  FS_param.mxh2rz(ids.flux_surface.r_minor_norm, 1, 0,
                     ids.flux_surface.elongation,
                     ids.flux_surface.shape_coefficients_c,
                     ids.flux_surface.shape_coefficients_s,
                     ids.flux_surface.dgeometric_axis_r_dr_minor,
                     ids.flux_surface.dgeometric_axis_z_dr_minor,
                     ids.flux_surface.delongation_dr_minor_norm,
                     ids.flux_surface.dc_dr_minor_norm,
                     ids.flux_surface.ds_dr_minor_norm,
                     code='imas', dr_frac=0.01, Nth=500)
    # Get the miller coeffs parameters
    k, d, z, sk, sd, sz, Rmil, Zmil, r, dRmildr, dZmildr =  FS_param.rz2miller(R, Z, code='tglf', doplots=False)    
    return {'k': k[1],
        'd': d[1],
        'z': z[1],
        'sk': sk[1],
        'sd': sd[1],
        'sz': sz[1],
        'Rmil': Rmil[1],
        'Zmil': Zmil[1],
        'r': r[1],
        'dRmildr': dRmildr[1],
        'dZmildr': dZmildr[1]}


def get_Brat_ids_tglf(miller,q):
    Nth=500
    k,d,z,sk,sd,sz,R0,Z0,r0,dRmildr,dZmildr = miller.values()
    drmildr = 1
    sd = sd/np.sqrt(1.0-d**2)
    
    # define the fine theta grid on which the integrals are performed
    th=np.linspace(0,2*np.pi,Nth)
    
    arg_r = th + np.arcsin(d)*np.sin(th)
    darg_r = 1.0 + np.arcsin(d)*np.cos(th)
    arg_z = th + z*np.sin(2.0*th)
    darg_z = 1.0 + z*2.0*np.cos(2.0*th)
    
    R = R0 + r0*np.cos(arg_r)
    
    dRdr = dRmildr + drmildr*np.cos(arg_r) - np.sin(arg_r)*sd*np.sin(th)
    dZdr = dZmildr + k*np.sin(arg_z)*(drmildr + sk) + k*np.cos(arg_z)*sz*np.sin(2.0*th)
    dRdth = -1*r0*np.sin(arg_r)*darg_r
    dZdth = k*r0*np.cos(arg_z)*darg_z
    r = 0.5*(max(R)-min(R))

    J_r = -R*(dRdr*dZdth - dRdth*dZdr)
    dldth = np.sqrt((dRdth)**2+(dZdth)**2)
    grad_r = R/J_r*dldth
    
    B_rat = R0/(2*np.pi*r)*np.trapz(dldth/(R*abs(grad_r)),th)

    return B_rat

def get_metric_idspy(ids, th_in, Nth=501):
    """ Compute the magnetic equilibrium and associated metric for a GKDB point
    
    Inputs:
        ids           dict with the content of a GKDB point, as obtained from loading a GKDB JSON file
        th_in         numpy array with the poloidal angles at which the metric terms will be computed 
                      (a finer theta grid is used internally)
                      If empty, uses the internal finer theta grids
        Nth           number of points for the fine theta grid used internally for integral computations (default 501)
                      
    Outputs: Magnetic equilibrium quantities, all normalized following the GKDB conventions
        th_out        poloidal angles at which the metric terms were computed
        amin          flux surface minor radius
        R             flux surface horizontal coordinates
        Z             flux surface vertical coordinates
        J_r           (r,theta,phi) Jacobian J_r = 1 / (grad_r x grad_theta . grad_phi)
        dVdr          radial derivative of the plasma volume
        grad_r        |grad_r|
        dpsidr        radial derivative of the poloidal magnetic flux (positive if s_j = 1)
        bt            toroidal magnetic field (normalized to Bref, positive if s_b = 1)
        bp            poloidal magnetic field (normalized to Bref, positive if s_j = 1)
        grr           radial-radial metric tensor to compute kperp from kr* and kth*
        grth          radial-binormal metric tensor to compute kperp from kr* and kth*
        gthth         radial-binormal metric tensor to compute kperp from kr* and kth*
    
    """
    
    # Convert th_out into an ndarray if needed
    if isinstance(th_in, np.ndarray):
        th_out = th_in.copy()
    else:
        th_out = np.asarray(th_in)
    
    # Retrieve the relevant information from the ids dict
    r = np.asarray(ids.flux_surface.r_minor_norm)
    k = np.asarray(ids.flux_surface.elongation)
    dkdr = np.asarray(ids.flux_surface.delongation_dr_minor_norm)
    dR0dr = np.asarray(ids.flux_surface.dgeometric_axis_r_dr_minor)
    dZ0dr = np.asarray(ids.flux_surface.dgeometric_axis_z_dr_minor)
    c = np.asarray(ids.flux_surface.shape_coefficients_c)
    s = np.asarray(ids.flux_surface.shape_coefficients_s)
    dcdr = np.asarray(ids.flux_surface.dc_dr_minor_norm)
    dsdr = np.asarray(ids.flux_surface.ds_dr_minor_norm)
    sb = np.asarray(ids.flux_surface.b_field_tor_sign)
    sj = np.asarray(ids.flux_surface.ip_sign)
    q = np.asarray(ids.flux_surface.q)
    shat = np.asarray(ids.flux_surface.magnetic_shear_r_minor)
    pprime = np.asarray(ids.flux_surface.pressure_gradient_norm)
    
    # define the fine theta grid on which the integrals are performed
    th=np.linspace(0,2*np.pi,Nth)

    # R, Z and their radial/poloidal derivatives
    Nsh=np.reshape(np.arange(len(c)),(1,-1))
    TH=np.reshape(th,(-1,1))
    THr = th + np.sum(np.reshape(c,(1,-1))*np.cos(Nsh*TH) + np.reshape(s,(1,-1))*np.sin(Nsh*TH),axis=1)
    dTHrdr = np.sum(np.reshape(dcdr,(1,-1))*np.cos(Nsh*TH) + np.reshape(dsdr,(1,-1))*np.sin(Nsh*TH),axis=1)
    dTHrdth = 1 + np.sum(Nsh*(np.reshape(s,(1,-1))*np.cos(Nsh*TH) - np.reshape(c,(1,-1))*np.sin(Nsh*TH)),axis=1)
    d2THrdth2 = -1*np.sum((Nsh**2)*(np.reshape(c,(1,-1))*np.cos(Nsh*TH) + np.reshape(s,(1,-1))*np.sin(Nsh*TH)),axis=1)
    R0 = 1.
    Z0 = 0.
    R = R0 + r*np.cos(THr)
    Z = Z0 + k*r*np.sin(th)
    dRdr = dR0dr + np.cos(THr) - r*np.sin(THr)*dTHrdr
    dZdr = dZ0dr + k*np.sin(th) + dkdr*r*np.sin(th)
    dRdth = -1.*r*np.sin(THr)*dTHrdth
    dZdth = k*r*np.cos(th)
    d2Rdth2 = -1*r*(np.cos(THr)*(dTHrdth**2) + np.sin(THr)*d2THrdth2)
    d2Zdth2 = -1*k*r*np.sin(th)
    
    
    # Jacobian, dldth and grad_r
    J_r = -R*(dRdr*dZdth - dRdth*dZdr) 
    dldth = np.sqrt((dRdth)**2+(dZdth)**2)
    grad_r = R/J_r*dldth

    # dpsidr and dVdr
    dpsidr=sj/abs(q)*trapz(J_r/R,th)
    dVdr=2*np.pi*trapz(J_r,th)

    # bt and bp
    bt=sb/R
    bp=grad_r/(2*np.pi*R)*dpsidr

    # intermediate quantities for the calculation of kperp
    cos_u=-dZdth/dldth
    r_c=-dldth**3/(dRdth*d2Zdth2-dZdth*d2Rdth2)
    E1or=2*cumtrapz(J_r/R**2*bt/bp*(1/r_c-1/R*cos_u),th,initial=0)
    E2=cumtrapz(J_r/R**2*(bp**2+bt**2)/bp**2,th,initial=0)
    bE3=0.5*cumtrapz(dldth/R*bt/bp**3*pprime,th,initial=0)
    fstar=(2*np.pi*q*shat/r-E1or[-1]+bE3[-1])/E2[-1]

    # interpolate on the output grid if needed
    if not th_out.any():
      th_out=th
    else:
      th_out_2pi = th_out % (2*np.pi)
      n_2pi = th_out // (2*np.pi)
      # deal first with the 2pi periodic quantities
      f=interp1d(th,R)
      R=f(th_out_2pi)
      f=interp1d(th,Z)
      Z=f(th_out_2pi)
      f=interp1d(th,J_r)
      J_r=f(th_out_2pi)
      f=interp1d(th,grad_r)
      grad_r=f(th_out_2pi)
      f=interp1d(th,bt)
      bt=f(th_out_2pi)
      f=interp1d(th,bp)
      bp=f(th_out_2pi)
      # then deal with the cumulative integrals 
      f=interp1d(th,E1or)
      E1or=f(th_out_2pi)+f(2*np.pi)*n_2pi
      f=interp1d(th,E2)
      E2=f(th_out_2pi)+f(2*np.pi)*n_2pi
      f=interp1d(th,bE3)
      bE3=f(th_out_2pi)+f(2*np.pi)*n_2pi

    # finally compute Gq, Theta and the metric tensors for the calculation of kperp
    Gq=r*np.sqrt(bp**2+bt**2)/(q*R*bp)
    Theta=R*bp*grad_r*(E1or+fstar*E2-bE3)/np.sqrt(bp**2+bt**2)
    grr=(grad_r/grad_r[0])**2
    grth=grad_r/grad_r[0]*Gq*Theta
    gthth=Gq**2*(1+Theta**2)

    metric = {'th_out':th_out,
              'R':R,
              'Z':Z,
              'J_r':J_r,
              'dVdr':dVdr,
              'grad_r':grad_r,
              'dpsidr':dpsidr,
              'bt':bt,
              'bp':bp,
              'Gq':Gq,
              'grr':grr,
              'grth':grth,
              'gthth':gthth}

    return metric
  

