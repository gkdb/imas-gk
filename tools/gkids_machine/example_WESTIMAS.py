import numpy as np
# gkids script
from IMAS2GKIDS import IMASgkids
# WEST libraries, works only on WEST servers
import pywed as pw
import imas_west as iw
import imas
from imas import imasdef

shot =  57617
user_time = 8 
user_radius = 0.5
run     = 0
occ     = 0
user    = 'imas_simulation'#'imas_public'
machine = 'west'

# core_profiles ids
core_profiles = iw.get(shot, 'core_profiles', run, occ, user, 'west_simu_preparation')

# equilibrium ids
idd = imas.DBEntry(imasdef.HDF5_BACKEND, machine, shot, run, user)
idd.open()
equilibrium = idd.get('equilibrium', occurrence=0)

# t_ignitron 
t_igni_found = False
n_conn_try = 0
N_CONN_MAX = 50
while not(t_igni_found) and  n_conn_try < N_CONN_MAX:
    try:
        t_ignitron = pw.tsbase(shot, 'RIGNITRON')[0][0][0]
    except:
        n_conn_try += 1  
    else:
        t_igni_found = True
if n_conn_try == N_CONN_MAX:
    raise(BufferError('tsbase doesnt work, retry...'))

# gk ids construction
gk_object = IMASgkids(user_time, user_radius, t_ignitron, core_profiles, equilibrium)

IDS_GK = gk_object.IDS_GK