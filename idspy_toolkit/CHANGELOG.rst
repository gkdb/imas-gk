Changelog
============

[ Planned Changes ]:

* deprecation warning linked to numpy 2.0
* update of hdf5 requirements based on numpy major release 2.0

[ Release history ]:

v 0.1.0
    * initial release
        - generate python class
        - version as comment

v 0.1.1
    * remove not used info from ids_properties through flag : ``--simplified-ids``
    * choice between cluster or single file package through flag : ``--single-package``

v 0.2.1
 * remove members linked to access layer
 * use numpy ndarray type for arrays
 * removed --simplified-ids command line option
 * [BUG] class associated with DD entries : signal_int/float/cplx_1d/2d/3d
 * [FIXED] attributes of ids_properties not fully removed

v 0.3.0
 * removed future deprecation warning associated with typing.List/Tuple/Dict
 * [ADDED] functions to read/write IDS to hdf5
 * [ADDED] function to list all members of an IDS
 * [FIXED] attributes types for at list the eigenmode ids
 * update to data dictionary @dd6854b4d07

v 0.3.1

  * [FIXED] bug for HDF5 IO `#6 <https://gitlab.com/gkdb/imas-gk/-/issues/6>`_
  * [FIXED] bug concerning initialization of ndarray while reading HDF5 files

v 0.4.0
 * directory structure for python packaging
 * [ADDED] tox for automatic testing
 * increase of code coverage

v 0.4.1

 * [FIXED] bug with numpy.array having ndim=0
 * better error message when trying to reach the root of an IDS using a path as string
 * list_ids_members returns only all the ids members and not anymore ids root path also
 * [ADDED] possibility to overwrite HDF5 file written

v 0.4.2

 * compatibility issue solved for some test with python 3.11
 * increased code coverage
 * handling numpy array default values
 * rewrite of tox.ini file
 * value of type np.array(number) are converted as number. This has to be considered as example when used in parallel with xarray. Example :
    test = np.array(42) -> test = int(test)

v 0.4.3

 * [ADDED] option to overwrite HDF5 file
 * [ADDED] implementation of IMAS default values
 * remove python3.8 from testenv in tox

v 0.5.0
 * h5py updated to 3.10 due to a bug with scalar dataset interpreted as numpy array
 * [CHANGED] dict are automatically stored as xml string and not json strings anymore
 * [CHANGED] automatic conversion of dict to xml while writing to hdf5 and option to convert xml string to dict while reading
 * [FIXED] bug for HDF5 IO `#9 <https://gitlab.com/gkdb/imas-gk/-/issues/9>`_
 * [FIXED] bug for HDF5 IO `#10 <https://gitlab.com/gkdb/imas-gk/-/issues/10>`_
 * better handling of 0D numpy array
 * better handling/type checking of datatype associated to IDS dictionaries while reading it from hdf5 file
 * [CHANGED] no need anymore tu use `fill_default_values_ids` before `hdf5_to_ids`
 * updated tox.ini and requirements.txt

v 0.6.0
 * [CHANGED] update minimal idspy_dictionaries version to *34000.2.0*
 * [FIXED] due to nested class data members solved with new ids definitions
 * [ADDED] version info and ids version info in hdf5 file (need ids_dictionaries version >= )

v 0.6.1

 * [FIXED] bug `#12 <gitlab.com/gkdb/imas-gk/-/issues/12>`_

v 0.6.2

 * [FIXED] bug `#13 <gitlab.com/gkdb/imas-gk/-/issues/13>`_
 * [FIXED] bug `#14 <gitlab.com/gkdb/imas-gk/-/issues/14>`_

v 0.6.3

 * [FIXED] bug `#15 <gitlab.com/gkdb/imas-gk/-/issues/15>`_

v 0.6.4

 * [FIXED] bug `#16 <gitlab.com/gkdb/imas-gk/-/issues/16>`_

v 0.6.5

 * [FIXED] bug `#11 <gitlab.com/gkdb/imas-gk/-/issues/11>`_

v 0.7.0
 * **min. required version for the gkdb client**
 * [ADDED] IDS Error specific exceptions
 * [CHANGED] metadata concerning idspy_toolkit version
 * [ADDED] version checking for hdf5 generated files
 * [ADDED] function to check if an IDS member contains an IMAS default value (*is_default_imas_value*)
 * [CHANGED] gzip compression and chunks by default when creating hdf5 file
 * Correction of future deprecation warning linked to numpy in conversion routines
 * [CHANGED] BuiltIn Exception starts to be replaced by dedicated ones, work in progress

v 0.7.1
 * [FIXED] handling of missing field in a dataclass
 * [FIXED] Bug where nested StructArray of IDS where created as list and not StructArray
 * [FIXED] bug `#17 <gitlab.com/gkdb/imas-gk/-/issues/17>`_
 * [ADDED] For IDS members, type checking when using the .append list function

v 0.8.0
 * **expected to be the last version before the switch to the final IDS format**
 * [UPDATED] readme file for the ids to hdf5 conversion
 * [ADDED] max_array_dim, max_array_size, max_array_elements parameters for the ids to hdf5 conversion
 * [UPDATED] code coverage
 * [UPDATED] accessor.py to handle correctly the case where the source and destination ids are not the same type
 * [UPDATED] Field type detection

v 0.8.1
 * [FIXED] creating a new ids with fill_default_values_ids and fill_list=True was causing an error
 * [FIXED] version identification if the git branch is a commit and not a tag