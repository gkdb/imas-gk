from dataclasses import dataclass, field, fields
from typing import Optional
import copy
from numpy import array, ndarray
from typing import Generator


def default_field(obj):
    return field(default_factory=lambda: copy.deepcopy(obj))


@dataclass#(slots=True)
class BaseClassUT:

    @classmethod
    def _get_root_members(cls)->tuple:
        return tuple([x.name for x in fields(BaseClassUT)])

    def get_members_name(self) -> Generator[str, None, None]:
        """
            get a tuple of current IDS members
        """
        return (x.name for x in fields(self) if x.name not in BaseClassUT._get_root_members())



class ClassOldStyle(BaseClassUT):
    time: str = "12345"


@dataclass#(slots=True)
class ClassEmpty(BaseClassUT):
    time: Optional[str] = None


@dataclass#(slots=True)
class Class1(BaseClassUT):
    time: str = "1.234"


@dataclass#(slots=True)
class Class3(BaseClassUT):
    time: float = 1.234
    space: int = 567
    name: str = "azerty"


@dataclass#(slots=True)
class ClassList(BaseClassUT):
    time: list[str] = field(default_factory=lambda: ["1.234", "5.678910", "abcdefghijk"])


@dataclass#(slots=True)
class ClassList2(BaseClassUT):
    time: list[str] = field(default_factory=lambda: [1.234, 5.678])


@dataclass#(slots=True)
class ClassListMix(BaseClassUT):
    time: list[str] = field(default_factory=lambda: [1.234, 5.678])
    space: int = 567
    name: str = "azerty"


@dataclass#(slots=True)
class ClassListNightmare(BaseClassUT):
    time: list[str] = field(default_factory=lambda: ["1.234", "5.678"])
    array_value: ndarray = field(default_factory=lambda: array([[1, 2, 4], [1, 2, 3.5]]))
    list_array: list[ndarray] = field(default_factory=lambda: [array([1, 2, 4]), array([1, 2, 3.5]), ])
    list_list: list[list[float]] = field(default_factory=lambda: [[1, 2], [3, 4]])
    list_list_list: list[list[list[float]]] = field(
        default_factory=lambda: [[[1, 2], [3, 4]], [[1, 2], [3, 4]], [[1, 2], [3, 4]], ])
    array_array: ndarray = field(default_factory=lambda: array([array([1, 2, 3]), array([1, 2, 3.5]), ]))


@dataclass#(slots=True)
class ClassListMixDict(BaseClassUT):
    time: list[str] = default_field(["1.234", "5.678"])
    space: int = 567
    name: str = "azerty"
    jsonarg: dict = field(default_factory=lambda: {"a": 1, "b": 2.3, "c": "azertyuiop"})


@dataclass#(slots=True)
class ClassListMixDict2(BaseClassUT):
    time: list[str] = field(default_factory=lambda: ["1.234", "5.678"])
    space: int = 567
    name: str = "azerty"
    jsonarg: list[dict] = default_field([{"a": 1, "b": 2.3, "c": "azertyuiop"},
                                         {"a": 2, "b": 3.4, "c1": "azertyuiope"}])


@dataclass#(slots=True)
class ClassListMixDictIn(BaseClassUT):
    time1: list[str] = field(default_factory=lambda: (1.2345, 5.6789))
    space1: int = 5678
    name1: str = "azERty"
    jsonarg1: list[dict] = field(default_factory=lambda: [{"a": 11, "b": 22.33, "c": "azertyuiopQS"},
                                                          {"a": 22, "b": 33.44, "c1": "azertyuiopeQSD"}])


@dataclass#(slots=True, )
class ClassListNested(BaseClassUT):
    time: list[str] = field(default_factory=lambda: [1.234, 5.678])
    space: int = 567
    name: str = "azerty"
    jsonarg: list[dict] = field(default_factory=lambda: [{"a": 1, "b": 2.3, "c": "azertyuiop"},
                                                         {"a": 2, "b": 3.4, "c1": "azertyuiope"}])
    nestedclass: ClassListMixDictIn = field(default_factory=ClassListMixDictIn)


@dataclass#(slots=True)
class ClassListNestedList(BaseClassUT):
    time: list[str] = default_field([1.234, 5.678])
    space: int = 567
    name: str = "azerty"
    jsonarg: list[dict] = field(default_factory=lambda: [{"a": 1, "b": 2.3, "c": "azertyuiop"},
                                                         {"a": 2, "b": 3.4, "c1": "azertyuiope"}])
    nestedclass: list[ClassListMixDictIn] = field(
        default_factory=lambda: [ClassListMixDictIn(space1=8888, name1="IMAS"),
                                 ClassListMixDictIn(space1=9999, name1="IDS")]
    )


@dataclass
class subsubclass(BaseClassUT):
    member_subsubclass_aa: str = field(default=""
                                       )
    member_subsubclass_bb: int = field(default=999999999
                                       )
    member_subsubclass_cc: float = field(default=None
                                         )


@dataclass
class subclass(BaseClassUT):
    member_subclass: Optional[subsubclass] = field(
        default=None
    )


@dataclass
class BaseClass(BaseClassUT):
    list_member: list[subclass] = field(
        default_factory=list,
    )


@dataclass
class ArrayClass(BaseClassUT):
    val_array_0d: float = field(default_factory=lambda: array(42.42))  # array of shape 0
    val_0d: float = 999.999
    val_array_1d: ndarray = field(default_factory=lambda: array([]))
    val_array_2d: ndarray = field(default_factory=lambda: array([[],[]]))
    val_array_3d: ndarray = field(default_factory=lambda: array([[],[],[]]))
