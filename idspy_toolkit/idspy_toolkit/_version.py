__version_info__ = ("0", "8", "0")
__version__ = ".".join(__version_info__)


def get_version():
    return __version__
